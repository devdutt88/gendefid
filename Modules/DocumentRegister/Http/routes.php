<?php

Route::group(['middleware' => 'web', 'prefix' => 'documentregister', 'namespace' => 'Modules\DocumentRegister\Http\Controllers'], function()
{
    Route::get('/', 'DocumentRegisterController@index');
});
