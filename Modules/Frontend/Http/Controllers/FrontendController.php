<?php

namespace Modules\Frontend\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Input;
use mjanssen\BreadcrumbsBundle\Breadcrumbs;
use App\User;
use App\Models\superadmin\UserDetails;
use Kodeine\Acl\Models\Eloquent\Permission;
use Kodeine\Acl\Models\Eloquent\Role;
use Auth;
use Helpers as Helper;
use Crypt;
use File;
use Image;
use Hash;

class FrontendController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {                
        return view('frontend::dashboard.index');
    }

    public function dashboard()
    {   
        $data['title'] = "Dashboard";
        return view('frontend::dashboard')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('frontend::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('frontend::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('frontend::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }

    /**
     * Set the specified Company.
     * @return Response
     */
    public function setCompany()
    {                
        $company_id = (Input::get('company_id'));
        \Session::set('company', $company_id);
        Helper::getCompanyDetails($company_id);                
    }

    /**
     * Set the specified Company.
     * @return Response
     */
    public function removeCompany(Request $request)
    {                
        $request->session()->forget('company');        
        return redirect('/');
        // \Session::set('company', $company_id);
        // Helper::getCompanyDetails($company_id);                
    }

    /**
     * Display login user profile
     * @return Response
     */

    public function profile(){                
        Breadcrumbs::addBreadcrumb('profile',  url('profile'));
        $page_data['breadcrumbs'] = Breadcrumbs::generate();
        $data['scripts'] = array('angularjs/angular.min','angularjs/angular-messages.min','angularjs/angular-flash.min','angularjs/controllers/frontend/profilecontroller','fileinput_bootstrap-fileinput','profile');
        $data['css'] = array("bootstrap-fileinput","profile","tasks","frontend_custom");
        return view('frontend::user.profile',compact('data'))->with($page_data);
    }

    public function getProfileData(){
        $user_det = User::where('id','=',Auth::user()->id)->first();
        $udetail = $user_det->UserDetails;                        
        $role = Helper::findRoleName($user_det->role_id);
        $user_det->role_id = $role[0]->name;
        $data['id'] = Crypt::encrypt($user_det->id);
        $data['name'] = $user_det->name;
        $data['full_name'] = $user_det->full_name;
        $data['email'] = $user_det->email;
        $data['phone'] = $user_det->phone;
        $data['role'] = $user_det->role_id;
        $data['logo_image'] = !empty($udetail[0])?$udetail[0]->logo_image:'';
        echo json_encode($data);        
        die;
    }

    public function user_check_emailexist($id,$email){        
        $id = Crypt::decrypt($id);
        if (isset($username) && !empty($username)){
            if (!empty($id)){
                $is_current_name = User::where('name',"=",$username)->where('is_deleted',"=",0)->where('id',"=",$id)->get();   
                if (isset($is_current_name[0]) && !empty($is_current_name[0]) && $is_current_name[0]->name == $username)
                {
                    echo "true";
                    die;
                }
            }            
            $is_username = User::where('name',"=",$username)->where('is_deleted',"=",0)->get();            
            if (isset($is_username[0]) && !empty($is_username[0])){
                echo 'false';
            }else{
                echo 'true';
            }
        }
        if (isset($email) && !empty($email)){
            if (!empty($id)){
                $is_current_email = User::where('email',"=",$email)->where('is_deleted',"=",0)->where('id',"=",$id)->get();   
                if (isset($is_current_email[0]) && !empty($is_current_email[0]) && $is_current_email[0]->email == $email)
                {
                    echo "false";
                    die;
                }
            }
            $is_email = User::where('email',"=",$email)->where('is_deleted',"=",0)->get();
            if (isset($is_email[0]) && !empty($is_email[0])){
                echo 'true';
            }else{
                echo 'false';
            }
        }
        die;
    }

    public function saveProfile(){
        $sys_token = \Session::token();         
        $profile_form = Input::get('profile');        
        $input = Input::all();            
        $token = Input::get('_token');               
        if ($token == $sys_token) {              
            switch ($profile_form) {
                case 'profile':
                    $id = Crypt::decrypt($input['id']);
                    $user = User::firstOrNew(array('id' => $id));
                    $user->name = strtolower($input['name']);                
                    $user->full_name = $input['full_name'];
                    $user->email = $input['email'];
                    $user->phone = $input['phone'];            
                    $success = $user->save();
                    if ($success) {
                        $success = \Session::flash('flash_alert_notice', 'Profile details updated successfully!');
                        return $success;
                    } else {
                        return "there was some server error";
                    }
                    break;
                case 'image':
                    if(empty(Input::file())){
                        $success = \Session::flash('flash_alert_notice_logo', 'Please select file!');
                        return $success;
                        die;
                    }                    
                    $user_id = Crypt::decrypt($input['id']);
                    $file = Input::file('user_image');                     
                    $user = User::select('name')->where('id' ,'=' ,$user_id)->first();
                    // Make directory if  not exist
                    $temp_path = public_path('uploads/user/temp/');
                    if (!File::exists($temp_path)) {
                        File::makeDirectory($temp_path, 0777, true, true);
                    }
                    $path_200 = public_path('uploads/user/thumbnail_200x200/');
                    if (!File::exists($path_200)) {
                        File::makeDirectory($path_200, 0777, true, true);
                    }
                    $path_29 = public_path('uploads/user/thumbnail_29x29/');
                    if (!File::exists($path_29)) {
                        File::makeDirectory($path_29, 0777, true, true);
                    }                
                    if ($file) {
                        $image_name = $file->getClientOriginalName();
                        $img_path = public_path('uploads/user/');
                        $extension = $file->getClientOriginalExtension(); // getting image extension
                        $fileName = $user_id . '_' .$user->name.".". $extension;
                        $uploaded_at = $file->move($temp_path, $fileName);
                        // Main image thumbnail
                        $img = Image::make($temp_path.$fileName)->resize(500,400);
                        if (file_exists($img_path.$fileName)) {
                                unlink($img_path.$fileName);
                        }                
                        $save_thumb = $img->save($img_path.$fileName);
                        // 200x200 image thumbnail
                        $img_thumb1 = Image::make($temp_path.$fileName)->resize(200,200);
                        if (file_exists($img_path. "thumbnail_200x200/" .$fileName)) {
                                unlink($img_path. "thumbnail_200x200/" .$fileName);
                        }                
                        $save_thumb = $img_thumb1->save($img_path. "thumbnail_200x200/" .$fileName);
                        // 29x29 image thumbnail
                        $img_thumb2 = Image::make($temp_path.$fileName)->resize(29,29);
                        if (file_exists($img_path. "thumbnail_29x29/" .$fileName)) {
                                unlink($img_path. "thumbnail_29x29/" .$fileName);
                            }
                        $save_thumb = $img_thumb2->save($img_path . "thumbnail_29x29/" .$fileName);            
                        // Remove temp folder image 
                        if (file_exists($temp_path.$fileName)) {
                            unlink($temp_path.$fileName);
                        }
                        
                    } else {
                        $fileName = '';
                    }
                    // update user_details table data
                    $user_details = UserDetails::firstOrNew(array('user_id' => $user_id));
                    $user_details->user_id = $user_id;
                    $user_details->logo_image = $fileName;
                    $success = $user_details->save();
                    if ($success) {
                        Helper::getLoginUserDetails(); 
                        //$success = \Session::flash('flash_alert_notice_logo', 'Logo has been updated successfully !');
                        return "1";
                    } else {
                        return "2";
                    }
                    break;
                default:
                    # code...
                    break;
            }            
        }else{
            return "there was some server error";
        }
    }

    public function check_user_current_password($pwd){        
        $current_password = Auth::guard()->user()->password;
        if (Hash::check($pwd, $current_password)){
            echo "true";
        }else{
            echo "false";
        }
        exit;
    }

    public function changeUserPassword(){
        $sys_token = \Session::token();
        $token = Input::get('_token'); 
        if ($token != $sys_token) { 
            return false;
        } 
        $user = Auth::guard()->user();//Auth::user();
        $user->password = Hash::make(Input::get('password'));
        $user->plain_password = Crypt::encrypt(Input::get('password'));
        $user->save();
        return \Session::flash('flash_alert_notice', 'Your new password has been updated!');
        exit;
    }

}