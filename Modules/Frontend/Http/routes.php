<?php

Route::group(['middleware' => ['web','auth','company'], 'prefix' => '', 'namespace' => 'Modules\Frontend\Http\Controllers'], function()
{
    Route::get('/', 'FrontendController@dashboard');
    Route::get('/dashboard', 'FrontendController@dashboard');
    Route::post('setCompany', 'FrontendController@setCompany');
    Route::get('/index', 'FrontendController@index');
    Route::get('/profile', 'FrontendController@profile');
    Route::get('/getProfileData', 'FrontendController@getProfileData');
    Route::get('/user_check_emailexist/{id}/{email}', 'FrontendController@user_check_emailexist');
    Route::post('saveProfile', 'FrontendController@saveProfile');
    Route::get('userCurrentPwd/{pwd}', 'FrontendController@check_user_current_password');
    Route::post('changeUserPassword', 'FrontendController@changeUserPassword');
    Route::get('remove-company', 'FrontendController@removeCompany');
    
    //User
    Route::resource('user', 'UserController');
    Route::get('usr_list', 'UserController@getData');
    Route::post('save_user', 'UserController@store');
    Route::get('user_types', 'UserController@getuserTypes');
    Route::get('delete-user/{id}', 'UserController@destroy');
    Route::get('check_isexist', 'UserController@check_isexist');
    Route::get('edit-user/{id}/{userform}', 'UserController@edit');
    Route::post('updateUser', 'UserController@updateUser');
    Route::get('user-details/{id}', 'UserController@show');
    Route::post('update_user', 'UserController@update');
    /*
    Route::get('user_emailid/{id}/{email}', 'UserController@check_email');
    Route::get('add-user', 'UserController@create');
    Route::get('getUserProjectData/{id}', 'UserController@getUserProjectData');
    Route::get('getUserCompanies/{id}', 'UserController@getUserCompanies');
    Route::get('user/getAllCompanies/{id}','UserController@getAllCompanies');
    Route::post('associateUserCompany','UserController@associateCompany');
    Route::post('unlinkUserCompany','UserController@unlinkCompany');
    // Route::post('changeUserPassword/{user_id}','UserController@changePassword');
    // Route::get('check_current_password/{id}/{pwd}', 'UserController@check_current_password');
    */
    
});
