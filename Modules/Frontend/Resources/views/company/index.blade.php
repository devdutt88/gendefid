<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Select Company</title>
		<script src="{{url('/')}}/js/jquery.min.js"></script>
		<script src="{{url('/')}}/sweetalert/sweetalert2.js" type="text/javascript"></script>
		<link href="{{url('/')}}/sweetalert/sweetalert2.min.css" rel="stylesheet" type="text/css">		
	</head>
	<div id="company" style="display: none;">{{$arr}}</div>
  <body>
  <form id="logout-form" action="{{url('/')}}/logout" method="POST" style="display: none;">
    <input type="hidden" name="_token" value="{{csrf_token()}}">
  </form>
  <input type="hidden" name="_token" value="{{csrf_token()}}">
  <script type="text/javascript">  
  baseUrl = '{{url('/')}}';
  var csrf_token = $('[name="_token"]').val();
	swal({
		title: 'Select Company',
		input: 'select',
		inputOptions: JSON.parse($('#company').text()),
		inputPlaceholder: 'Select company',
		showCancelButton: true ,
		allowOutsideClick: false,
		cancelButtonText:'Logout',
		
		inputValidator: function (value) {
			return new Promise(function (resolve, reject) {
				if(!value){
					location.reload();
				}
				var data1 = {'company_id':value,'_token':csrf_token};  
				$.ajax({
			        url: baseUrl+'/setCompany',
			        type: "post",
			        data: data1,
			        success: function(response1) {            		            
			            resolve();
			        }            
	        	});			
			})
		},
	}).then(function (result) {
		// swal({
		// 	type: 'success',
		// 	html: 'Company selected Successfully!',				
		// });
		location.reload();
	}, function (dismiss) {
		if (dismiss === 'cancel') {
			console.log("test function");
			$('#logout-form').submit();
		}
	});


    </script>
  </body>
</html>