@extends('frontend::layouts.master')

@section('content')
@php
$company_details = \Session::get('company_details');		
@endphp
<!-- BEGIN PAGE HEAD -->
<div class="page-head">
	<div class="container">
		<!-- BEGIN PAGE TITLE -->
		@include('frontend::layouts.company_setting')		
		<!-- END PAGE TITLE -->
	</div>
</div>
<!-- END PAGE HEAD -->
<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
	<div class="container">
		<!-- BEGIN PAGE BREADCRUMB -->
		<ul class="page-breadcrumb breadcrumb">
			<li>
				<a href="#">Home</a><i class="fa fa-circle"></i>
			</li>
			<li class="active">
				 Dashboard
			</li>
		</ul>
		<!-- END PAGE BREADCRUMB -->
		<!-- BEGIN PAGE CONTENT INNER -->
			DashBoard
		<!-- END PAGE CONTENT INNER -->
	</div>
</div>
<!-- END PAGE CONTENT -->

@stop