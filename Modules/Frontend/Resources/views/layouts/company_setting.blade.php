@php $company_count = \Session::get('company_count');
$company_details = \Session::get('company_details');		
@endphp
@if($company_count > 1)	
	<div class="page-title">
			<h1 class="" style="text-transform: capitalize;">{{$company_details['name']}} <small>{{$title}}</small></h1>
		</div>
	<div class="btn-group btn-theme-panel change_company pull-right" title="Change Company">
		<!-- <span class="caption bold font-green uppercase pull-left cst_label">{{$company_details['name']}}</span> -->
		<a href="{{url('/')}}/remove-company" class="btn dropdown-toggle" aria-expanded="false">
			<i class="icon-settings"></i>
		</a>
	</div>
@else
	<div class="page-title">
			<h1 class="" style="text-transform: capitalize;">{{$company_details['name']}} <small>statistics & reports</small></h1>
	</div>	
@endif

