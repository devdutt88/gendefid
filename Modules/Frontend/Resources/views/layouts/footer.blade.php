	</div>
	<!-- END PAGE CONTENT -->
</div>
<!-- END PAGE CONTAINER -->
<!-- BEGIN FOOTER -->
<div class="page-footer cst_bar">
	<div class="container text-right cst_label">
		 Copyright © 2017 All Rights Reserved - Wiseworking
	</div>
</div>
<div class="scroll-to-top">
	<i class="icon-arrow-up"></i>
</div>
<!-- END FOOTER -->
<!-- BEGIN JAVASCRIPTS (Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<![endif]-->
<script type="text/javascript" src="{{ elixir('js/frontend.js') }}"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script type="text/javascript">
var baseUrl='{{url('/')}}'
</script>
@if(!empty($data['scripts']))
	@foreach ($data['scripts'] as $js)
		<script src="{{url('/')}}/js/{{$js}}.js"></script>
	@endforeach
@endif
<script>
jQuery(document).ready(function() {    
  // Metronic.init(); // init metronic core componets
      Metronic.init(); // init metronic core components
Layout.init(); // init current layout
Demo.init(); // init demo features
//Todo.init(); // init todo page
$('[data-toggle="tooltip"]').tooltip(); 
});
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>