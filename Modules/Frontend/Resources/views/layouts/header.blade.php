<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8">
<title>{{ config('app.name', 'Laravel') }}</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport">
<meta content="" name="description">
<meta content="" name="author">
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css">

<link rel="stylesheet" href="{{ elixir('css/frontend.css') }}">
<link rel="stylesheet" href="{{ url('css/themeCss/') }}/{{\Session::get('company')}}_theme.css">
@if(!empty($data['css']))
	@foreach ($data['css'] as $css)
		<link rel="stylesheet" href="{{url('/')}}/css/{{$css}}.css">
	@endforeach
@endif
<!-- END THEME STYLES -->
<link rel="shortcut icon" href="{{url('/')}}/img/favicon.ico"/>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-footer-fixed page-header-fixed page-container-bg-solid">
<!-- BEGIN HEADER -->
<div class="page-header">
	<!-- BEGIN HEADER TOP -->
	<div class="page-header-top">
		<div class="container">
			<!-- BEGIN LOGO -->
			<div class="page-logo">
				<a href="{{url('/')}}">				
				@php 				
				$company_details = \Session::get('company_details');		
				$path_138 = public_path('uploads/company/thumbnail_138x30/').$company_details['logo'];			
				@endphp
				@if(!empty($company_details['logo']) && file_exists($path_138))
					<img src="{{url('/')}}/uploads/company/thumbnail_138x30/{{$company_details['logo']}}" alt="logo" class="logo-default"/>
				@else
					<img src="{{url('/')}}/assets/img/logo_middle.png" alt="logo" class="logo-default"/>
				@endif
				</a>
				<!-- <div class="menu-toggler sidebar-toggler">
				</div> -->
			</div>
			<!-- END LOGO -->
			<!-- BEGIN RESPONSIVE MENU TOGGLER -->
			<a href="javascript:;" class="menu-toggler"></a>
			<!-- END RESPONSIVE MENU TOGGLER -->
			<!-- BEGIN TOP NAVIGATION MENU -->
			<div class="top-menu">
				<ul class="nav navbar-nav pull-right">
					
					<li class="droddown dropdown-separator">
						<span class="separator"></span>
					</li>
					<!-- BEGIN INBOX DROPDOWN -->
					<!-- <li class="dropdown dropdown-extended dropdown-dark dropdown-inbox" id="header_inbox_bar">
						<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
						<span class="circle">3</span>
						<span class="corner"></span>
						</a>
						<ul class="dropdown-menu">
							<li class="external">
								<h3>You have <strong>7 New</strong> Messages</h3>
								<a href="javascript:;">view all</a>
							</li>
							<li>
								<ul class="dropdown-menu-list scroller" style="height: 275px;" data-handle-color="#637283">
									<li>
										<a href="inbox.html?a=view">
										<span class="photo">
										<img src="../../assets/admin/layout3/img/avatar2.jpg" class="img-circle" alt="">
										</span>
										<span class="subject">
										<span class="from">
										Lisa Wong </span>
										<span class="time">Just Now </span>
										</span>
										<span class="message">
										Vivamus sed auctor nibh congue nibh. auctor nibh auctor nibh... </span>
										</a>
									</li>
									<li>
										<a href="inbox.html?a=view">
										<span class="photo">
										<img src="../../assets/admin/layout3/img/avatar3.jpg" class="img-circle" alt="">
										</span>
										<span class="subject">
										<span class="from">
										Richard Doe </span>
										<span class="time">16 mins </span>
										</span>
										<span class="message">
										Vivamus sed congue nibh auctor nibh congue nibh. auctor nibh auctor nibh... </span>
										</a>
									</li>
									<li>
										<a href="inbox.html?a=view">
										<span class="photo">
										<img src="../../assets/admin/layout3/img/avatar1.jpg" class="img-circle" alt="">
										</span>
										<span class="subject">
										<span class="from">
										Bob Nilson </span>
										<span class="time">2 hrs </span>
										</span>
										<span class="message">
										Vivamus sed nibh auctor nibh congue nibh. auctor nibh auctor nibh... </span>
										</a>
									</li>
									<li>
										<a href="inbox.html?a=view">
										<span class="photo">
										<img src="../../assets/admin/layout3/img/avatar2.jpg" class="img-circle" alt="">
										</span>
										<span class="subject">
										<span class="from">
										Lisa Wong </span>
										<span class="time">40 mins </span>
										</span>
										<span class="message">
										Vivamus sed auctor 40% nibh congue nibh... </span>
										</a>
									</li>
									<li>
										<a href="inbox.html?a=view">
										<span class="photo">
										<img src="../../assets/admin/layout3/img/avatar3.jpg" class="img-circle" alt="">
										</span>
										<span class="subject">
										<span class="from">
										Richard Doe </span>
										<span class="time">46 mins </span>
										</span>
										<span class="message">
										Vivamus sed congue nibh auctor nibh congue nibh. auctor nibh auctor nibh... </span>
										</a>
									</li>
								</ul>
							</li>
						</ul>
					</li> -->
					<!-- END INBOX DROPDOWN -->
					<!-- BEGIN USER LOGIN DROPDOWN -->
					@php $admin = \Session::get('adminData');
					$path_200 = public_path('uploads/user/thumbnail_200x200/').$admin['logo_image'];					 				
					@endphp

					<li class="dropdown dropdown-user dropdown-dark">
						<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
						@if(!empty($admin['logo_image']) && file_exists($path_200))
							<img id="logo_image_profile"  src="{{url('/')}}/uploads/user/thumbnail_200x200/{{$admin['logo_image']}}" class="img-responsive img-circle" alt="{{$admin['fullname']}}" />
						@else
							<img id="logo_image_profile"  src="{{url('/')}}/uploads/user_icon.jpg" alt="user image" class="img-responsive img-circle" alt="{{$admin['fullname']}}"/>
						@endif						
						<span class="username username-hide-mobile">
							@if(!empty($admin['fullname']))
								{{$admin['fullname']}}
							@else
								Username
							@endif
						</span>
						</a>
						<ul class="dropdown-menu dropdown-menu-default">
							<li>
								<a href="{{ url('/profile') }}">
								<i class="icon-user"></i> My Profile </a>
							</li>
							<li>
								<a href="{{ url('/logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
								<i class="icon-key"></i> Log Out </a>
								<form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
	                                {{ csrf_field() }}
	                            </form>
							</li>
						</ul>
					</li>
					<!-- END USER LOGIN DROPDOWN -->
				</ul>
			</div>
			<!-- END TOP NAVIGATION MENU -->
		</div>
	</div>
	<!-- END HEADER TOP -->
	<!-- BEGIN HEADER MENU -->
	<div class="page-header-menu cst_bar">
		<div class="container">
			<!-- BEGIN HEADER SEARCH BOX -->
			<!-- <form class="search-form" action="extra_search.html" method="GET">
				<div class="input-group">
					<input type="text" class="form-control" placeholder="Search" name="query">
					<span class="input-group-btn">
					<a href="javascript:;" class="btn submit"><i class="icon-magnifier"></i></a>
					</span>
				</div>
			</form> -->
			
			
			<!-- END HEADER SEARCH BOX -->
			<!-- BEGIN MEGA MENU -->
			<!-- DOC: Apply "hor-menu-light" class after the "hor-menu" class below to have a horizontal menu with white background -->
			<!-- DOC: Remove data-hover="dropdown" and data-close-others="true" attributes below to disable the dropdown opening on mouse hover -->
			<div class="hor-menu ">
				<ul class="nav navbar-nav">
					<li class="{{ (Request::segment(1) == '' ) ? 'active' : '' }}" >
						<a class="cst_label" href="{{url('/')}}">Dashboard</a>
					</li>		
					<li class="{{ (Request::segment(1) == 'project' ) ? 'active' : '' }}">
						<a class="cst_label" href="{{url('/project')}}">Projects</a>
					</li>					
			        <li class="{{ (Request::segment(1) == 'user' || Request::segment(1) == 'user-details') ? 'active' : '' }}">
						<a class="cst_label" href="{{url('/user')}}">Users</a>
					</li>
				</ul>
			</div>
			<!-- END MEGA MENU -->
		</div>
	</div>
	<!-- END HEADER MENU -->
</div>
<!-- END HEADER -->
<!-- BEGIN PAGE CONTAINER -->
<div class="page-container">
