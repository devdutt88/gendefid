@extends('superadmin::layouts.master')

@section('content')
<!-- BEGIN CONTAINER -->
	
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE HEADER -->
			@include('superadmin::partials.breadcrumb')
			<!-- END PAGE HEADER -->
			
			<!-- BEGIN CONTENT -->
			<div class="row" id="editUserModel">
				<div class="col-md-12">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-equalizer font-blue-hoki"></i>
								<span class="caption-subject font-blue-hoki bold uppercase">Edit User Details</span>
								<span class="caption-helper"></span>
							</div>

							<div class="tools"></div>
						</div>
						
						<div class="portlet-body form">
							<!-- BEGIN FORM-->
							<form action="{{ url('superadmin/update_user') }}" id="add_user_form" class="horizontal-form" method="post" enctype="multipart/form-data">
								{{ csrf_field() }}
								<input type="hidden" id="user_id" name="user_id" value="@if(isset($user->id)){{$user->id}}@endif" />

								@include('superadmin::user.form')
							</form>
							<!-- END FORM-->
						</div>
					</div>
				</div>
			</div>
			<!-- END CONTENT -->
			<div class="clearfix">
			</div>
		</div>
	</div>

<!-- END CONTAINER -->
@stop
