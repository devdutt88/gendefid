@extends('frontend::layouts.master')

@section('content')
<!-- END HEADER -->
<!-- BEGIN PAGE CONTAINER -->
<div class="page-container" ng-app="profileModule"  ng-controller="profileController">
	<!-- BEGIN PAGE HEAD -->
	
	<!-- END PAGE HEAD -->
	<!-- BEGIN PAGE CONTENT -->
	<div class="page-content">
		<div class="container">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
							<h4 class="modal-title">Modal title</h4>
						</div>
						<div class="modal-body">
							 Widget settings form goes here
						</div>
						<div class="modal-footer">
							<button type="button" class="btn blue">Save changes</button>
							<button type="button" class="btn default" data-dismiss="modal">Close</button>
						</div>
					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			</div>			
			<!-- /.modal -->
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE BREADCRUMB -->
			@include('project::partials.breadcrumb')
			<!-- END PAGE BREADCRUMB -->
			<!-- BEGIN PAGE CONTENT INNER -->
			<div class="row margin-top-10">
				<div class="col-md-12">
					<!-- BEGIN PROFILE SIDEBAR -->
					<div class="profile-sidebar" style="width: 250px;">
						<!-- PORTLET MAIN -->
						<div class="portlet light profile-sidebar-portlet">
							<!-- SIDEBAR USERPIC -->
							<div class="profile-userpic">											
								<img id="user_logo" ng-model="profile.image"  ng-src="@{{user_img}}" alt="user image" class="img-responsive" alt=""/>
							</div>
							<!-- END SIDEBAR USERPIC -->
							<!-- SIDEBAR USER TITLE -->
							<div class="profile-usertitle">
								<div class="profile-usertitle-name">
								 	@{{profile.full_name}}
								</div>
								<div class="profile-usertitle-job">
									@{{profile.role}}
								</div>
							</div>
							<!-- END SIDEBAR USER TITLE -->
							<!-- SIDEBAR BUTTONS -->
							<!-- <div class="profile-userbuttons">
								<button type="button" class="btn btn-circle green-haze btn-sm">Follow</button>
								<button type="button" class="btn btn-circle btn-danger btn-sm">Message</button>
							</div> -->
							<!-- END SIDEBAR BUTTONS -->
							<!-- SIDEBAR MENU -->
							<div class="profile-usermenu">
								<ul class="nav">
									<!-- <li class="active">
										<a href="extra_profile.html">
										<i class="icon-home"></i>
										Overview </a>
									</li> -->
									<li class="active">
										<a href="javascript:;">
										<i class="icon-settings"></i>
										Account Settings </a>
									</li>
									<!-- <li>
										<a href="page_todo.html" target="_blank">
										<i class="icon-check"></i>
										Tasks </a>
									</li>
									<li>
										<a href="extra_profile_help.html">
										<i class="icon-info"></i>
										Help </a>
									</li> -->
								</ul>
							</div>
							<!-- END MENU -->
						</div>
						<!-- END PORTLET MAIN -->						
					</div>
					<!-- END BEGIN PROFILE SIDEBAR -->
					<!-- BEGIN PROFILE CONTENT -->
					<div class="profile-content">
						<div class="row">
							<div class="col-md-12">
								<div class="portlet light">
									<div class="portlet-title tabbable-line">
										<div class="caption caption-md">
											<i class="icon-globe theme-font hide"></i>
											<span class="caption-subject font-blue-madison bold uppercase">Profile Account</span>
										</div>
										<ul class="nav nav-tabs">
											<li class="active">
												<a href="#tab_1_1" data-toggle="tab" aria-expanded="true">Personal Info</a>
											</li>
											<li class="">
												<a href="#tab_1_2" data-toggle="tab" aria-expanded="false">Change Avatar</a>
											</li>
											<li class="">
												<a href="#tab_1_3" data-toggle="tab" aria-expanded="false">Change Password</a>
											</li>
											<!-- <li class="">
												<a href="#tab_1_4" data-toggle="tab" aria-expanded="false">Privacy Settings</a>
											</li> -->
										</ul>
									</div>
									<div class="portlet-body">
										<div class="tab-content">
											<!-- PERSONAL INFO TAB -->
											<div class="tab-pane active" id="tab_1_1">
												<flash-message show-close="false" on-dismiss="myCallback(flash)"></flash-message>
												<form role="form" action="#" id="profileInfo" name="profileInfo">
													<input type="hidden" name="user-id" ng-model="profile.id">
													<div class="form-group">
														<label class="control-label">Name <span class="red">*</span></label>
														<input placeholder="Name" name="name" class="form-control" ng-model="profile.full_name" type="text" ng-required="true">
													</div>														
													<div class="form-group">
														<label class="control-label">Username <span class="red">*</span></label>
														<input placeholder="Username" readonly="readonly" name="username" ng-model="profile.name" class="form-control" type="text" ng-required="true">
													</div>	
													<div class="form-group">
														<label class="control-label">Email <span class="red">*</span></label>														
														<input type="email" id="email" name="email" class="form-control" placeholder="Email" ng-model="profile.email" email-exists ng-model-options="{ updateOn: 'blur' }" ng-required="true"/>
													    <p ng-messages="profileInfo.email.$error"  ng-if="profileInfo.email.$dirty" role="alert">
														    <span class="error" ng-message="required">Email address is required.</span>
														    <span class="error" ng-message="email">This field must be a valid email address.</span>
														    <span class="error" ng-message="emailExists">Email address already exist.</span>
													  	</p>



													</div>										
													<div class="form-group">
														<label class="control-label">Contact Number <span class="red">*</span></label>
														<input placeholder="9876543210" class="form-control" name="phone" ng-model="profile.phone" type="text" ng-required="true">
													</div>													
													<div class="margiv-top-10">														
														<button type="button" class="btn green-haze" ng-disabled="profileInfo.name.$dirty && profileInfo.name.$invalid ||  
															profileInfo.email.$dirty && profileInfo.email.$invalid ||  
															profileInfo.phone.$dirty && profileInfo.phone.$invalid" id="company_form" ng-click="save('profile')">Save Changes</button>

														<a href="javascript:;" class="btn default">
														Cancel </a>
													</div>
												</form>
											</div>
											<!-- END PERSONAL INFO TAB -->
											<!-- CHANGE AVATAR TAB -->
											<div class="tab-pane" id="tab_1_2">												
												<flash-message show-close="false" on-dismiss="myCallback(flash)"></flash-message>
													<form role="form" action="#" id="profileInfoAvtar" name="profileInfoAvtar">
													<div class="form-group">
														<div class="fileinput fileinput-new" data-provides="fileinput">
															<div class="fileinput-new thumbnail" style="max-width: 200px; max-height: 150px; line-height: 10px;">																
																<img src="@{{user_logo}}" alt="">
															</div>
															<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;">
															</div>
															<div class="margin-top-10">
																<span class="btn default btn-file">
																<span class="fileinput-new">
																Select image </span>
																<span class="fileinput-exists">
																Change </span>
																<input name="user_image" id="user_image" type="file">
																</span>
																<a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput">
																Remove </a>
															</div>
														</div>														
													</div>
													<div class="margin-top-10">														
														<button type="button" class="btn green-haze" ng-disabled="profileInfoAvtar.name.$dirty && profileInfoAvtar.name.$invalid" id="" ng-click="save('image')">Submit</button>
														<a href="javascript:;" class="btn default">
														Cancel </a>
													</div>
												</form>
											</div>
											<!-- END CHANGE AVATAR TAB -->
											<!-- CHANGE PASSWORD TAB -->
											<div class="tab-pane" id="tab_1_3">
												<flash-message show-close="false" on-dismiss="myCallback(flash)"></flash-message>
												<form action="#" name="changePasswordFrm" id="changePasswordFrm">													
													<div class="form-group" ng-class="{ 'has-error' : changePasswordFrm.current_psw.$dirty && changePasswordFrm.current_psw.$invalid }">
														<label class="control-label">Current Password</label>
														<input type="password" class="form-control" name="current_psw" id="current_psw" ng-model="adminPassword.current_psw" placeholder="Current Password" password-exists ng-model-options="{ updateOn: 'blur' }" required />
														<!-- <span class="error" ng-show="changePasswordFrm.$error.passwordExists">Invalid password please try again</span> -->
														<div class="help-block" ng-messages="changePasswordFrm.current_psw.$error" ng-if="changePasswordFrm.current_psw.$dirty">
															<p class="error" ng-message="required">This field is required</p>
															<p class="error" ng-message="passwordExists">Invalid password please try again</p>
														</div>
													</div>


													<div class="form-group" ng-class="{ 'has-error' : changePasswordFrm.password.$dirty && changePasswordFrm.password.$invalid }">
												        	<label class="control-label">New Password</label>
													        	<input type="password" class="form-control" id="password" name="password" placeholder="password" required ng-model="adminPassword.password" password-verify="@{{adminPassword.confirm_password}}" ng-pattern="/^(?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!%*#?&])[A-Za-z\d$@$!%*#?&]{8,}$/">
																<div class="help-block" ng-messages="changePasswordFrm.password.$error" ng-if="changePasswordFrm.password.$dirty">
																	<p class="error" ng-message="required">This field is required</p>
																	<p class="error" ng-message="minlength">This field is too short</p>
																	<p class="error" ng-message="maxlength">This field is too long</p>
																	<p ng-message="pattern">Password should be atleast 8 characters long and should contain one number,one character and one special character
														          	</p>
																	<!-- <p class="error" ng-message="passwordVerify">No match!</p> -->
																</div>
												      	</div>
												      	<div class="form-group" ng-class="{ 'has-error' : changePasswordFrm.confirm_password.$dirty && changePasswordFrm.confirm_password.$invalid }">
												        	<label class="control-label">Confirm New Password</label>
												        	
													        	<input class="form-control" id="confirm_password" ng-model="adminPassword.confirm_password" name="confirm_password" type="password" placeholder="confirm password" required password-verify="@{{adminPassword.password}}">
													        	<div class="help-block" ng-messages="changePasswordFrm.confirm_password.$error" ng-if="changePasswordFrm.confirm_password.$dirty">
														          <p class="error" ng-message="required">This field is required</p>
														          <p class="error" ng-message="minlength">This field is too short</p>
														          <p class="error" ng-message="maxlength">This field is too long</p>
														          <p class="error" ng-message="required">This field is required</p>
														          <p class="error" ng-message="passwordVerify">password did not match!</p>
														        </div>													        
												      	</div>
													<div class="margin-top-10">
														<button type="button" href="javascript:;" ng-disabled="changePasswordFrm.$invalid" class="btn green-haze" ng-click="save('chngpwd')">
														Change Password </button>
														<a href="javascript:;" class="btn default">
														Cancel </a>
													</div>
												</form>
											</div>
											<!-- END CHANGE PASSWORD TAB -->											
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- END PROFILE CONTENT -->
				</div>
			</div>
			<!-- END PAGE CONTENT INNER -->
		</div>
	</div>
	<!-- END PAGE CONTENT -->
</div>
<!-- END PAGE CONTAINER -->
<!-- BEGIN PRE-FOOTER -->
<!-- END PRE-FOOTER -->
@stop