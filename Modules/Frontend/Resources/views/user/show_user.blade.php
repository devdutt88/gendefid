@extends('frontend::layouts.master')

@section('content')
<!-- BEGIN CONTAINER -->
<link href="{{url('/')}}/assets/css/tasks.css" rel="stylesheet" type="text/css"/>
<link href="{{url('/')}}/assets/css/profile.css" rel="stylesheet" type="text/css"/>
<div class="page-head">
		<div class="container">
			<!-- BEGIN PAGE TITLE -->
			@include('frontend::layouts.company_setting')		
			<!-- END PAGE TITLE -->
		</div>
	</div>
<div class="page-content" ng-app="userModule">
	<!-- BEGIN PAGE HEAD -->	
	<div class="container" ng-controller="showuserController">
		<!-- BEGIN PAGE HEADER-->
		@include('superadmin::partials.breadcrumb')
		<!-- {!! $breadcrumbs !!} -->
		<!-- END PAGE HEADER-->
		
		<!-- BEGIN ROW 1 CONTENT-->
		<div class="row">
			<!-- BEGIN PROFILE SIDEBAR -->
			<!-- END BEGIN PROFILE SIDEBAR -->
			<!-- BEGIN PROFILE CONTENT -->
			<div class="profile-content col-md-6">
				<div class="row">				
					<div class="col-md-12">
					<!-- Alert Section -->
						@if(Session::has('flash_profile_alert_notice'))
					        <div class="alert alert-success alert-dismissable cst-alert cst-normal-alert">
					          	<!-- <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button> -->
					          	<i class="icon fa fa-check"></i>  
					           	{{ Session::get('flash_profile_alert_notice') }} 
					        </div>
					   	@endif
					<!-- End Alert Section -->
						<div class="portlet light bordered min-height-450">
							<div class="portlet-title">
								<!-- <div class="caption">
									<i class="icon-globe theme-font hide"></i>
									<span class="caption-subject font-blue-madison bold uppercase">User Account</span>
								</div> -->
								<div class="actions">
									<a href="" class="btn btn-circle btn-default btn-sm cst_btn" ng-click="showModelFrm('{{Crypt::encrypt($user->id)}}', 'profile')">
									<i class="fa fa-pencil"></i> Edit </a>
								</div>
								<ul class="nav nav-tabs pull-left">
									<li class="bold">
										<a class="">Personal Info</a>
									</li>
								</ul>
							</div>
							<div class="portlet-body">
								<div class="tab-content">
									<!-- PERSONAL INFO TAB -->
									<div class="">
										<div class="row">
											<div class="text-center col-sm-3">
												<div class="form-group">
													<!-- SIDEBAR USERPIC -->
													@php
														$path =public_path('uploads/user/thumbnail_200x200/');
													@endphp
													@if(!empty($udetail[0]) && !empty($udetail[0]->logo_image) && file_exists($path.$udetail[0]->logo_image))
														<img id="logo_image"  src="{{url('/')}}/uploads/user/thumbnail_200x200/{{$udetail[0]->logo_image}}"  class="img-responsive frontend_user_profile" alt="your image" />
													@else
														<img id="logo_image"  src="{{url('/')}}/uploads/user_icon.jpg" alt="user image"  class="img-responsive" />
													@endif
													<!-- END SIDEBAR USERPIC -->
												</div>	
												<div class="profile-stat">
													<div class="profile-usertitle-name">
														 {{$user->full_name}}
													</div>
												</div>
											</div>
											<div class="col-sm-9">
												<div class="form-group">
													
													<div class="row">
														<label class="control-label col-xs-5"><strong>Name :</strong></label>
														<div class="col-xs-7">
																 {{$user->full_name}}
														</div>
													</div>
													<div class="row">
														<label class="control-label col-xs-5"><strong>User Type :</strong></label>
														<div class="col-xs-7">
																{{((isset($roles[$user->role_id])) ? $roles[$user->role_id] : 'NA')}}
														</div>
													</div>
													<div class="row">
														<label class="control-label col-xs-5"><strong>Username :</strong></label>
														<div class="col-xs-7">
																 {{$user->name}}
														</div>
													</div>
													<div class="row">
														<label class="control-label col-xs-5"><strong>Password :</strong></label>
														<div class="col-xs-7">
															XXXXXXXXX
															<!-- @if(!empty($user->plain_password))
															 {{Crypt::decrypt($user->plain_password)}}
															@endif -->
														</div>
													</div>
													<div class="row">
														<label class="control-label col-xs-5"><strong>Email :</strong></label>
														<div class="col-xs-7">
																 {{$user->email}}
														</div>
													</div>
													<div class="row">
														<label class="control-label col-xs-5"><strong>Contact :</strong></label>
														<div class="col-xs-7">
																{{$user->phone}}
														</div>
													</div>
												</div>
											</div>
											<!--/span-->
										</div>
									</div>
									<!-- END PERSONAL INFO TAB -->
									<!-- Profile Dialog -->
									@include('frontend::user.forms.profile')
									<!-- USER ADDRESS TAB -->
									
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		

			<div class="profile-content col-md-6">
				<div class="row">				
					<div class="col-sm-12">
					<!-- Alert Section -->
						@if(Session::has('flash_address_alert_notice'))
					        <div class="alert alert-success alert-dismissable cst-alert cst-normal-alert">
					          	<!-- <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button> -->
					          	<i class="icon fa fa-check"></i>  
					           	{{ Session::get('flash_address_alert_notice') }} 
					        </div>
					   	@endif
					<!-- End Alert Section -->
						<div class="portlet light bordered min-height-450">
							<div class="portlet-title">
								<!-- <div class="caption">
									<i class="icon-globe theme-font hide"></i>
									<span class="caption-subject font-blue-madison bold uppercase">User Account</span>
								</div> -->
								<div class="actions">
									<a href="" class="btn btn-circle btn-default btn-sm cst_btn" ng-click="showModelFrm('{{Crypt::encrypt($user->id)}}', 'address')">
									<i class="fa fa-pencil"></i> Edit </a>
								</div>
								<ul class="nav nav-tabs pull-left">
									<li class="bold">
										<a class="">Addresses</a>
									</li>
								</ul>
							</div>
							<div class="portlet-body">
								<div class="tab-content">
									<!-- USER ADDRESS TAB -->
									<div class="">
										<div class="row">
											<div class="col-sm-12">
												<div class="form-group">
													<h5 class="caption-subject font-blue-steel bold uppercase">Business Details</h5>
													<div class="row">
														<label class="control-label col-xs-5"><strong> Address :</strong></label>
														<div class="col-xs-7">
																 @if(!empty($user_details)){{$user_details->busi_address1}}@endif
																 @if(!empty($user_details)){{$user_details->busi_address2}}@endif
														</div>
													</div>
													<div class="row">
														<label class="control-label col-xs-5"><strong> Postcode :</strong></label>
														<div class="col-xs-7">
																@if(!empty($user_details)){{$user_details->busi_post}}@endif
														</div>
													</div>
													<div class="row">
														<label class="control-label col-xs-5"><strong> Suburb(City) :</strong></label>
														<div class="col-xs-7">
																 @if(!empty($user_details)){{$user_details->busi_suburb}}@endif
														</div>
													</div>
													<div class="row">
														<label class="control-label col-xs-5"><strong> State :</strong></label>
														<div class="col-xs-7">
																@if(!empty($user_details)){{$user_details->busi_state}}@endif
														</div>
													</div>
													<div class="row">
														<label class="control-label col-xs-5"><strong> Country :</strong></label>
														<div class="col-xs-7">
																@if(!empty($user_details)){{$user_details->busi_country}}@endif
														</div>
													</div>
													<h5 class="caption-subject font-blue-steel bold uppercase">Billing Details</h5>
													<div class="row">
														<label class="control-label col-xs-5"><strong> Address :</strong></label>
														<div class="col-xs-7">
																 @if(!empty($user_details)){{$user_details->bill_address1}}@endif
																 @if(!empty($user_details)){{$user_details->bill_address2}}@endif
														</div>
													</div>
													<div class="row">
														<label class="control-label col-xs-5"><strong> Postcode :</strong></label>
														<div class="col-xs-7">
																@if(!empty($user_details)){{$user_details->bill_post}}@endif
														</div>
													</div>
													<div class="row">
														<label class="control-label col-xs-5"><strong> Suburb(City) :</strong></label>
														<div class="col-xs-7">
																 @if(!empty($user_details)){{$user_details->bill_suburb}}@endif
														</div>
													</div>
													<div class="row">
														<label class="control-label col-xs-5"><strong> State :</strong></label>
														<div class="col-xs-7">
																@if(!empty($user_details)){{$user_details->bill_state}}@endif
														</div>
													</div>
													<div class="row">
														<label class="control-label col-xs-5"><strong> Country :</strong></label>
														<div class="col-xs-7">
																@if(!empty($user_details)){{$user_details->bill_country}}@endif
														</div>
													</div>
												</div>
											</div>
											<!--/span-->
										</div>
									</div>
									<!-- END PERSONAL INFO TAB -->
									<!-- Profile Dialog -->
									@include('frontend::user.forms.address')
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- END ROW CONTENT-->
	</div>
</div>
<!-- END CONTAINER -->
@stop