@extends('superadmin::layouts.master')

@section('content')
<!-- BEGIN CONTAINER -->

	<div class="page-content-wrapper">
		<div class="page-content">
			
			@include('superadmin::partials.breadcrumb')
			
			<!-- BEGIN CONTENT -->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN EXAMPLE TABLE PORTLET-->
					<div class="portlet box grey-cascade">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-globe"></i>User List
							</div>
							<div class="tools"></div>
						</div>
						<div class="portlet-body">
							@if(Session::has('flash_alert_notice'))
		                        <div class="alert alert-success alert-dismissable">
		                          	<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
		                          	<i class="icon fa fa-check"></i>  
		                           	{{ Session::get('flash_alert_notice') }} 
		                        </div>
	                       	@endif
							
							<div class="table-toolbar">
								<div class="row">
									
									<div class="col-md-12 text-right">
										<div class="btn-group">
											<a class="btn cst-yellow" id="" href="{{url('/')}}/superadmin/add-user"> <i class="fa fa-plus"></i> Add New </a>
										</div>
									</div>
								</div>
							</div>
							
							<table class="table table-striped table-bordered table-hover" id="sample_1">
								<thead>
									<tr>
										<th> Username </th>
										<th> Full Name </th>
										<th> Email </th>
										<th> Role </th>
										<th> Action </th>
									</tr>
								</thead>														

							</table>
						</div>
					</div>
					<!-- END EXAMPLE TABLE PORTLET-->
				</div>
			</div>
			<!-- END CONTENT -->
			<div class="clearfix">
			</div>

		</div>
	</div>
	
<!-- END CONTAINER -->
@stop