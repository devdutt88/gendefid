@extends('frontend::layouts.master')

@section('content')
<!-- BEGIN CONTAINER -->

<!-- BEGIN PAGE HEAD -->
<div class="page-head">
	<div class="container">
		<!-- BEGIN PAGE TITLE -->
		@include('frontend::layouts.company_setting')		
		<!-- END PAGE TITLE -->
	</div>
</div>	

<div class="page-content" ng-app="userModule">
	<div class="container" ng-controller="userListController">
		
		@include('superadmin::partials.breadcrumb')
		@include('frontend::user.forms.form')
		<!-- BEGIN CONTENT -->
		<div class="row">
			<div class="col-md-12">
				<!-- BEGIN EXAMPLE TABLE PORTLET-->
				<div class="todo-content">
					<div class="portlet light">
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-microphone font-dark hide"></i>	
								<span class="caption-subject bold font-dark uppercase"> User List </span>
							</div>
							<div class="actions">
								<button class="btn btn-circle btn-sm btn-default cst_btn" id=""  ng-click="showModelFrm('add', 0)"> <i class="fa fa-plus"></i> Add New User</button>
							</div>	
						</div>	 
						<div class="portlet-body">
							@if(Session::has('flash_alert_notice'))
		                        <div class="alert alert-success alert-dismissable cst-alert cst-list-alert">
		                          	<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
		                          	<i class="icon fa fa-check"></i>  
		                           	{{ Session::get('flash_alert_notice') }} 
		                        </div>
	                       	@endif
							
							<!-- <div class="table-toolbar">
								<div class="row">
									
									<div class="col-md-12 text-right">
										<div class="btn-group">
											<a class="btn cst-yellow" id="" href="{{url('/')}}/superadmin/add-user"> <i class="fa fa-plus"></i> Add New </a>
										</div>
									</div>
								</div>
							</div> -->

							<div class="row">
								<div class="col-sm-12">
									
							
									<table class="table table-striped table-bordered table-hover" id="userList">
										<thead>
											<tr>
												<th></th>
												<th> Username </th>
												<th> Full Name </th>
												<th> Email </th>
												<th> Role </th>
												<th width="110"> Created </th>
												<th width="120"> Action </th>
											</tr>
										</thead>														

									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- END EXAMPLE TABLE PORTLET-->
			</div>
		</div>
		<!-- END CONTENT -->
		<div class="clearfix">
		</div>

	</div>
</div>
	
<!-- END CONTAINER -->
@stop