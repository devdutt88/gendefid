<?php

namespace Modules\Project\Http\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use Yajra\Datatables\Facades\Datatables as Datatables;
use mjanssen\BreadcrumbsBundle\Breadcrumbs;
use App\Models\superadmin\Project;
use Illuminate\Support\Facades\Input;
use App\Models\projectConfiguration\Location;
use App\Models\project\ProjectDefectList;
use Crypt;

class GeneralDefectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        Breadcrumbs::addBreadcrumb('Project',  url('/project'));
        Breadcrumbs::addBreadcrumb('Project General Defect',  '');
        $page_data = array('breadcrumbs' => Breadcrumbs::generate());
        $title = "General defect list";
        $pid = Crypt::decrypt($id);
        $data['css'] = array("dataTables.bootstrap","frontend_custom",'tasks','multiple-select.min');        
        $page_data['project'] = Project::select(['id','name'])->where('id',$pid)->first();
        $data['scripts'] = array('angularjs/angular.min','angularjs/angular-messages.min','angularjs/dataTable.min','angularjs/angular-flash.min','angularjs/controllers/frontend/generalDefectcontroller','multiple-select.min',"jquery.dataTables.min","dataTables.bootstrap");
        return view('project::generalDefect.general_defect_list',compact('data','id','title'))->with($page_data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {        
        $input = Input::except('_token');
        $data = array();
        $data['description'] = $input['description'];
        $data['locations'] = json_encode($input['location']);
        $data['project_id'] = Crypt::decrypt($input['project_id']);
        ProjectDefectList::insert($data);
        $success = \Session::flash('flash_alert_notice', 'General defect created successfully !');
        return $success;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $id = Crypt::decrypt($id);
        $general_defect = ProjectDefectList::where('id',$id)->get();        
        $data = array();
        $i = 0;
        foreach ($general_defect as $key => $value) {
            $data[$i]['description'] = $value->description;
            $data[$i]['locations'] = json_decode($value->locations);
        }
        return $data;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = Input::except('_token');
        $id = Crypt::decrypt($id);
        $project = ProjectDefectList::find($id);
        $project->description = $input['description'];
        $project->locations = json_encode($input['location']);
        $project->project_id = Crypt::decrypt($input['project_id']);
        $project->save();        
        $success = \Session::flash('flash_alert_notice', 'General defect list updated successfully !');
        return $success;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $id = Crypt::decrypt($id);
        $delete = ProjectDefectList::destroy($id);
        $success = \Session::flash('flash_alert_notice', 'General defect list deleted successfully!');
        return $success;
    }

    /**
     *  get location search query data.
     */
    public function locationTags($pid){
        $pid = Crypt::decrypt($pid);
        $results = Location::select('id','label')->where('project_id',$pid)->get();
        $tags = array();
        $i = 0;
        foreach ($results as $key => $value) {
            $tags[$i]['id'] = $value->id;
            $tags[$i]['name'] = $value->label;
            $i++;
        }                
        return $tags;
    }

    /**
     * Get General defect list list for specified project.
     *
     */
    public function getData(){
        $pid = Input::get('project_id');
        $pid = Crypt::decrypt($pid);
        $defect_list = ProjectDefectList::select(['id', 'description', 'locations'])->where('project_id','=',$pid)->get();
        $def_list = Datatables::of($defect_list)->addColumn('location', function ($loc) {
            $locations = json_decode($loc->locations);
            $data = array();
            foreach ($locations as $key => $loc) {
                $data[] = $loc->name;
            }
            $data = implode(", ", $data);
            return $data;
        })->addColumn('action', function ($dif_lst) {
                $id = Crypt::encrypt($dif_lst->id);
                return '<a id="editGeneralDefect" class="btn btn-warning btn-xs edit-general-defect" gd-id="'.$id.'" title="Edit"><i class="fa fa-pencil-square-o"></i></a>&nbsp;<a  id="deleteGdefect"  href="#" title="Delete" class="btn btn-danger btn-xs delete-general-defect"  deleteGdefectId="'.$id.'"><i class="fa fa-trash-o"></i></a>';
            })->make(true);
        return $def_list;
        exit;  
    }
}
