<?php

namespace Modules\Project\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Models\superadmin\Project;
use mjanssen\BreadcrumbsBundle\Breadcrumbs;
use Illuminate\Support\Facades\Input;
use File;
use Image;
use Crypt;

class ProjectConfigController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {        
        Breadcrumbs::addBreadcrumb('Project',  url('project'));
        Breadcrumbs::addBreadcrumb('Project Details',  '');
        $data['breadcrumbs'] = Breadcrumbs::generate();
        $prjId = Crypt::decrypt($id);
        $project = Project::where('id','=',$prjId)->where('is_deleted','=',0)->first();      
        $data['project'] = $project;
        $data['scripts'] = array("angularjs/angular.min",'angularjs/angular-messages.min','angularjs/angular-flash.min',"angularjs/controllers/frontend/projectConfigcontroller");
        //$data['scripts'] = array('angularjs/angular.min','angularjs/angular-messages.min','angularjs/angular-flash.min','angularjs/controllers/projectcontroller');        
        $data['css'] = array("frontend_custom");
        $title = "Project Configuration";
        return view('project::project.prj_detail',compact('id','data','title'))->with($data);        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        $condition = Input::get('projectform');
        switch ($condition) {
            case 'logo':
                $name = Input::get('name');
                $id = Input::get('id');
                $id = Crypt::decrypt($id);
                $file = Input::file('logo'); 
                
                // Make directory if  not exist
                $temp_path = public_path('uploads/project/temp/');
                if (!File::exists($temp_path)) {
                    File::makeDirectory($temp_path, 0777, true, true);
                }
                $path_200 = public_path('uploads/project/thumbnail_200x200/');
                if (!File::exists($path_200)) {
                    File::makeDirectory($path_200, 0777, true, true);
                }
                $path_29 = public_path('uploads/project/thumbnail_29x29/');
                if (!File::exists($path_29)) {
                    File::makeDirectory($path_29, 0777, true, true);
                }                
                if ($file) {
                    $image_name = $file->getClientOriginalName();
                    $img_path = public_path('uploads/project/');
                    $extension = $file->getClientOriginalExtension(); // getting image extension
                    $fileName = $id . '_' .$name.".". $extension;
                    $uploaded_at = $file->move($temp_path, $fileName);
                    // Main image thumbnail
                    $img = Image::make($temp_path.$fileName)->resize(200,200);
                    if (file_exists($img_path.$fileName)) {
                            unlink($img_path.$fileName);
                    }                
                    $save_thumb = $img->save($img_path.$fileName);
                    // 200x200 image thumbnail
                    $img_thumb1 = Image::make($temp_path.$fileName)->resize(200,200);
                    if (file_exists($img_path. "thumbnail_200x200/" .$fileName)) {
                            unlink($img_path. "thumbnail_200x200/" .$fileName);
                    }                
                    $save_thumb = $img_thumb1->save($img_path. "thumbnail_200x200/" .$fileName);
                    // 29x29 image thumbnail
                    $img_thumb2 = Image::make($temp_path.$fileName)->resize(29,29);
                    if (file_exists($img_path. "thumbnail_29x29/" .$fileName)) {
                            unlink($img_path. "thumbnail_29x29/" .$fileName);
                        }
                    $save_thumb = $img_thumb2->save($img_path . "thumbnail_29x29/" .$fileName);            
                    // Remove temp folder image 
                    if (file_exists($temp_path.$fileName)) {
                        unlink($temp_path.$fileName);
                    }
                    
                } else {
                    $fileName = '';
                }
                // update project table data
                $project = Project::firstOrNew(array('id' => $id));
                $project->id = $id;
                $project->logo = $fileName;
                $success = $project->save();
                if ($success) {
                    $success = \Session::flash('flash_alert_notice', 'Logo has been updated successfully !');
                    return $success;
                } else {
                    return "there was some server error";
                }
                break;
            
            default:
                # code...
                break;
        }
        die;        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
