<?php

namespace Modules\Project\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\Models\superadmin\Project;
use App\Models\superadmin\Company;
use App\Models\superadmin\CompanyProjectMapping;
use App\Models\superadmin\ProjectUserRole;
use Kodeine\Acl\Models\Eloquent\Permission;
use Kodeine\Acl\Models\Eloquent\Role;
use App\User;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Redirect;
use Modules\Project\Http\Requests\StoreProjectRequest;
use Yajra\Datatables\Facades\Datatables as Datatables;
use mjanssen\BreadcrumbsBundle\Breadcrumbs;
use View;
use URL;
use Auth;
use Crypt;
use PermissionHelpers as AccessRight;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $data['scripts'] = array("angularjs/angular.min",'angularjs/angular-messages.min','angularjs/angular-flash.min',"jquery.dataTables.min","angularjs/angular-datatables.min","dataTables.bootstrap","angularjs/controllers/frontend/projectcontroller");
        $data['css'] = array("dataTables.bootstrap","frontend_custom");        
        $data['title'] = "Project List";
        Breadcrumbs::addBreadcrumb('Project List',  url('project'));
        $page_data['breadcrumbs'] = Breadcrumbs::generate();        
        return view('project::project.list_project',compact('data'))->with($page_data)->with($data);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {        
        $cmp_id = \Session::get('company');        
        $page_data['cur_project'] = '';
        $page_data['proj_comps'] = array();
        Breadcrumbs::addBreadcrumb('Project List',  url('project'));
        Breadcrumbs::addBreadcrumb('Add Project', '');
        $page_data['breadcrumbs'] = Breadcrumbs::generate();
        $data['css'] = array('frontend_custom');        
        $data['scripts'] = array("angularjs/angular.min",'angularjs/angular-messages.min','angularjs/angular-flash.min',"angularjs/angular-datatables.min","jquery.dataTables.min","angularjs/controllers/frontend/projectcontroller");
        $data['label'] = 'addProject';
        $data['id'] = 0;
        $title = "Add Project";
        return view('project::project.create',compact('data','title'))->with($page_data);
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(StoreProjectRequest $request)
    {                
        //store in project        
        $project = new Project;
        $project->name = Input::get('projname');
        $project->code = str_slug(Input::get('projname'));
        $project->company_id = \Session::get('company');
        $project->type = Input::get('projecttype');
        $project->description = Input::get('description'); 
        $project->address1 = Input::get('address1'); 
        $project->address2 = Input::get('address2');
        //$project->geo_code = Input::get('geo_code'); 
        $project->suburb = Input::get('suburb'); 
        $project->state = Input::get('state'); 
        $project->country = Input::get('country');
        $project->postal_code = Input::get('postal_code');
        $project->created_by = Auth::user()->id; 
        $project->modified_by = Auth::user()->id; 
        $project->created_at = date('Y-m-d h:i:s'); 
        $insert_project = $project->save();
        
        if (!empty($project->id)) {            
            $cmp_id = \Session::get('company');
            //company project mapping
            $company = array('project_id' => $project->id, 'company_id'=> $cmp_id);
            CompanyProjectMapping::insert($company);
            //project user role
            $projectUserRole = new ProjectUserRole;
            $projectUserRole->project_id = $project->id;
            $projectUserRole->user_id = Auth::user()->id;
            $projectUserRole->role_id = Auth::user()->role_id;
            $projectUserRole->created_by = Auth::user()->id; 
            $projectUserRole->created_at = date('Y-m-d h:i:s'); 
            $projectUserRole->modified_by = Auth::user()->id;
            $projectUserRole->modified_at = date('Y-m-d h:i:s'); 
            $projectUserRole->resource_type = 'Webserver';             
            $success = $projectUserRole->save();
           
        } 
        if ($success) {
            $success = \Session::flash('flash_alert_notice', 'Project created successfully !');
            return $success;
        } else {
            return "there was some server error";
        }
        die;
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('project::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        #$id = Crypt::decrypt($id);
        $cmp_id = \Session::get('company');        
        $page_data['cur_project'] = '';
        $page_data['proj_comps'] = array();
        Breadcrumbs::addBreadcrumb('Project List',  url('project'));
        Breadcrumbs::addBreadcrumb('Edit Project', '');
        $page_data['breadcrumbs'] = Breadcrumbs::generate();
        $data['css'] = array('frontend_custom');        
        $data['scripts'] = array("angularjs/angular.min",'angularjs/angular-messages.min','angularjs/angular-flash.min',"angularjs/angular-datatables.min","jquery.dataTables.min","angularjs/controllers/frontend/projectcontroller");
        $data['label'] = 'editProject';
        $data['id'] = $id;
        $title = "Edit Project";
        return view('project::project.create',compact('data','title'))->with($page_data);
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(StoreProjectRequest $request)
    {
        $id = Input::get('id');
        $project_id = Crypt::decrypt($id);
        $project = Project::firstOrNew(array('id' => $project_id));
        $project->name = Input::get('projname');
        $project->code = str_slug(Input::get('projname'));
        $project->type = Input::get('projecttype'); 
        $project->description = Input::get('description'); 
        $project->address1 = Input::get('address1');
        $project->address2 = Input::get('address2');
        $project->suburb = Input::get('suburb');
        $project->state = Input::get('state');
        $project->postal_code = Input::get('postal_code');
        $project->country = Input::get('country');
        $project->geo_code = "";
        $success = $project->save();
        if ($success) {
            $success = \Session::flash('flash_alert_notice', 'Project updated successfully !');
            return $success;
        } else {
            return "there was some server error";
        }
        die;
    }
    
    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function projectData($id)
    {
       $id = ($id != '0') ? Crypt::decrypt($id) : 0;
        $project = Project::where('id','=',$id)->where('is_deleted','=',0)->get();
        $data = array();        
        foreach ($project as $key => $value) {
            $data['projname'] = $value->name;
            $data['projcode'] = $value->code;
            $data['projecttype'] = $value->type;
            $data['description'] = $value->description;
            $data['address1'] = $value->address1;
            $data['address2'] = $value->address2;
            $data['suburb'] = $value->suburb;
            $data['postal_code'] = $value->postal_code;
            $data['state'] = $value->state;
            $data['country'] = $value->country;
            $data['geo_code'] = $value->geo_code;
        }
        return $data;
        die;
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {        
        $id = Input::get('id');
        $id = Crypt::decrypt($id);
        Project::destroy($id);

        // $project = ProjectUserRole::where('project_id','=',$id)->where('user_id','=',Auth::user()->id)->where('role_id','=',Auth::user()->role_id)->first();
        // if(!empty($project)) {   
        //     $project->is_deleted = 1;
        //     $project->save();
        // }

        // $cmp_id = \Session::get('company');
        // $project = CompanyProjectMapping::where('project_id','=',$id)->where('company_id','=',$cmp_id)->first();
        // $project->is_deleted = 1;
        // $project->save();


        $success = \Session::flash('flash_alert_notice', 'Project deleted successfully !');
        return $success;
    }

    /**
     * get Project list the specified resource from storage.
     * @return Response
     */
    public function projectList()
    {   
        $cmp_id = \Session::get('company');
        // $project_user = ProjectUserRole::where('is_deleted','=',0)->where('user_id','=',Auth::user()->id)->get();       
        $data = array();
        $authers = array(Auth::user()->id,0);
        $projects = Project::where('company_id','=',$cmp_id)->whereIn('created_by', $authers)->get();
        $i = 0; 
        //print_r($projects);                
        foreach ($projects as $key => $value) {                         
            $data[$i]['name'] = $value->name;
            $data[$i]['type'] = $value->type;
            if($value->created_by == 0){
                $data[$i]['created_by'] = 'Superadmin';
            }else{
                $data[$i]['created_by'] = \Helpers::getColumnValById('name',$value->created_by,new User)->name;
            }
            $cdate   = strtotime($value->created_at);
            $created_at = date('Y-m-d', $cdate);
            $mdate   = strtotime($value->modified_at);
            $modified_at = date('Y-m-d', $mdate);
            $data[$i]['created_at'] = $created_at;
            $data[$i]['modified_at'] = $modified_at;
            $data[$i]['id'] = Crypt::encrypt($value->id);
            $i++;            
        } 
        return json_encode($data);
        die;
    }
    
    /**
    * Company issue to list show and return in  jsone
    */
    public function companyIssueToList(){
        
    }

    /**
     * get Project exist the specified resource from storage.
     * @return Response
     */
    public function projectIsexist($id){
        $name = Input::get('name');        
        if (!empty($name)){       
            $cmp_id = \Session::get('company');     
            $id = ($id != '0') ? Crypt::decrypt($id) : 0;            
            $jq = Input::get('jq');
            $is_name = ($id != '0') ? Project::where('name',"=",$name)->where('is_deleted',"=",0)->where('id',"!=",$id)->get() : Project::where('name',"=",$name)->where('is_deleted',"=",0)->get();                          
            $i = 0;
            if (isset($is_name) && !empty($is_name)){
                foreach ($is_name as $key => $value) {                    
                    if (isset($value->name) && !empty($value->name)){
                        if(!empty($value->projectCompany[0])){
                            if($value->projectCompany[0]->company_id == $cmp_id){
                                $i = 1;
                            }
                        }                        
                    }
                }
            }

            if($i == 1){
                echo 'true';
            }else{
                echo 'false';
            }            
        }
        exit;
    }

    public function test(){
        //echo "Test";
        $user_id = Auth::user()->id;
        $project_id = "eyJpdiI6IjJ5b0FKZmZxcDFWdk1MN2M1SWp5TWc9PSIsInZhbHVlIjoiZmN1eVZFMTNSYzdXRUEwY1BjZzQ1dz09IiwibWFjIjoiNDBiNzg2NTcyMzU5NGI3ZjhmZTZlZDg3NjkyMjc5YzhlZGQ1MjlkYTg2MGM3MTY0YjAwY2IyYjk2NzQ0NzBkZiJ9";
        $access = AccessRight::hasProjectRole($user_id,$project_id);
        if($access) {
            echo AccessRight::getProjectPermission($user_id,$project_id);
        }        
        return view('project::test');
    }
}
