<?php

namespace Modules\Project\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Models\superadmin\Project;
use App\Models\projectConfiguration\Location;
use mjanssen\BreadcrumbsBundle\Breadcrumbs;
use Illuminate\Support\Facades\Input;
use Modules\Project\Http\Requests\StoreLocationCsv;
use App\Models\superadmin\CompanyProjectMapping;
use App\Models\superadmin\Company;
use Illuminate\Support\MessageBag;
use Goodby\CSV\Import\Standard\Lexer;
use Goodby\CSV\Import\Standard\Interpreter;
use Goodby\CSV\Import\Standard\LexerConfig;
use Goodby\CSV\Export\Standard\Exporter;
use Goodby\CSV\Export\Standard\ExporterConfig;
use Goodby\CSV\Export\Standard\Collection\CallbackCollection;
use Excel;
use File;
use Image;
use Crypt;

class ProjectLocationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        //
        $prjId = $id;  
        $id = Crypt::decrypt($id);    
        $data['scripts'] = array('bootstrap-fileinput','jquery.contextMenu','jquery.ui.position','jquery-treeview','tree','angularjs/controllers/frontend/projectLocationcontroller','frontend/location');
        $data['css'] = array('jquery.contextMenu','jquery.treeview','bootstrap-fileinput','frontend_custom');
        $project = Project::select('id','name')->where('is_deleted','=',0)->where('id', $id)->first();                               
        $loc_data = array();
        if (!empty($id)) {
            $Categorys = Location::where('is_deleted','=',0)->where('project_id','=',$id)->where('parent_id','=',0)->get();
            if (!empty($Categorys)) {             
                $tree='<ul id="browser" class="filetree"><li class="location tree-view"></li>';
                foreach ($Categorys as $Category) {                   
                    $tree .='<li name="'.$Category->label.'" project_id='.$prjId.'  id='.$Category->id.' class="location tree-view closed"><a class="tree-name">'.$Category->label.'</a>';
                    if (count($Category->childs)) {
                    $tree .=$this->childView($Category,$prjId);
                    } else {
                        $tree .="</li>";
                    }
                }
                $tree .='<ul>';
            }               
        }
        $data['cmp_id'] = \Session::get('company');;
        Breadcrumbs::addBreadcrumb('Project',  url('project'));
        Breadcrumbs::addBreadcrumb('Project Location',  '');
        $data['breadcrumbs'] = Breadcrumbs::generate();
        $id = $prjId;
        $title = "Project Location";
        return view('project::location.location',compact('data','tree','cmp_name','cmp_id','id','title'))->with($data)->with('project',$project)->with('location',$tree);
        // $data['scripts'] = array("angularjs/angular.min",'angularjs/angular-messages.min','angularjs/angular-flash.min',"angularjs/controllers/frontend/projectConfigcontroller");
        // return view('project::location.location',compact('id'))->with($data);
    }

    /**
     * Fetch Child location of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function childView($Category,$prjId){
        $html ='<ul>';
        foreach ($Category->childs as $arr) {
            if (count($arr->childs)) {
                $html .='<li  name="'.$arr->label.'"  project_id='.$prjId.' id='.$arr->id.' class="location tree-view closed"><a class="tree-name">'.$arr->label.'</a>';                  
                $html.= $this->childView($arr,$prjId);
            } else {
                $html .='<li name="'.$arr->label.'"  project_id='.$prjId.'  id='.$arr->id.' class="location tree-view"><a class="tree-name">'.$arr->label.'</a>';                                 
                $html .="</li>";
            }                                   
        }
        $html .="</ul>";
        return $html;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //        
        $input = Input::except('_token');                
        $pid = Crypt::decrypt(Input::get('project_id')); 
        $project_id = Input::get('project_id');
        $cmp_id = \Session::get('company');              
        if ($input['case'] == "parent") {
            $insertInLocation = array(
                "label"  =>  $input['level1'],
                "company_id"  =>  $cmp_id,
                "project_id" =>  $pid,  
                "parent_id" => 0,
                "order_id" => 0,
                "is_deleted" => 0,
                "created_at" => date('Y-m-d h:i:s'),
                "created_by" => 0,
                "modified_by" => 0,
                "resource_type" => 'Webserver',            

            );        
            $save = Location::create($insertInLocation);
            if ($save) {       
                $id = $save->id;        
                $html = '<li name="'.$input['level1'].'" project_id="'.$project_id.'"  id="'.$id.'" class="location tree-view closed"><a class="tree-name">'.$input['level1'].'</a></li>';
                $msg = "Location saved successfully";
                $arr = array('html'=>$html,'msg'=>$msg);
                return json_encode($arr);
            } else {
                $html = "";
                $msg = "Location save failed";
                $arr = array('html'=>$html,'msg'=>$msg);
                return json_encode($arr);
            }
        } else if($input['case'] == "child") {            
            $insertInLocation = array(
                "label"  =>  $input['level1'],
                "company_id"  =>  $cmp_id,
                "project_id" =>  $pid,  
                "parent_id" => $input['parent_id'],
                "order_id" => 0,
                "is_deleted" => 0,
                "created_at" => date('Y-m-d h:i:s'),
                "created_by" => 0,
                "modified_by" => 0,
                "resource_type" => 'Webserver',            
            );        
            $save = Location::create($insertInLocation);
            if ($save) {
                $id = $save->id;        
                $html = '<li name="'.$input['level1'].'" id="'.$id.'" project_id='.$project_id.' class="location tree-view closed"><a class="tree-name">'.$input['level1'].'</a></li>';
                $msg = "Location saved successfully";
                $arr = array('html'=>$html,'msg'=>$msg);
                return json_encode($arr);                
            } else {
                $html = "";
                $msg = "Location save failed";
                $arr = array('html'=>$html,'msg'=>$msg);
                return json_encode($arr);
            }
        }
    }

    /**
     * Export location csv from database resource.
     *
     * @param  \App\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function exportCsv(StoreLocationCsv $request,Location $location,$pid)
    {            
        $pid = Crypt::decrypt($pid);        
        $cmp_id = \Session::get('company');
        $location = Location::where('is_deleted','=',0)->where('project_id','=',$pid)->where('company_id','=',$cmp_id)->get(); 
        $project = Project::where('is_deleted','=',0)->where('id','=',$pid)->first();
        $pname = $project->name;        
        if (!empty($location)) {
            $loc_data = array();
            $parent_data = array();
            $i = 0;
            $j = 0;
            foreach ($location as $key => $loc) {
                $loc_data[$loc->id]['id'] = $loc->id;
                $loc_data[$loc->id]['name'] = $loc->label;
                $loc_data[$loc->id]['parent_id'] = $loc->parent_id;                
                $i++;
            }   
        }
        $location_arr = array();
        $count = 0;
        $data =array();
        foreach ($loc_data as $key => $location) {
           $tree = $this->buildParentPath($loc_data,$location['id']);
           $total = count($tree);
           if ($count < $total) {
                $count = $total;
           }
           $data[] = $tree;           
        }
        $header = array();
        for ($i=0;$i<$count;$i++) {
            if($i == 0) {
                $header[0][$i] = "Location";
            } else {
                $header[0][$i] = "Sublocation ".$i;
            }
        }
        $data = array_merge($header, $data);
        $type = 'csv';
        return Excel::create($pname.'_location', function($excel) use ($data) {
            $excel->sheet('Location', function($sheet) use ($data) {
                $sheet->fromArray($data, null, 'A1', false, false);
            });
        })->download($type);
    }

    public function buildParentPath(array $elements, $id) {
        $children = array();
        $result = array();
        $path = array();
        foreach ($elements as $key => $element) {
            if ($element['id'] == $id) {   
                $result[] = $element['name'];        
                $result = array_merge($this->buildParentPath($elements, $element['parent_id']), $result);                
            }

        }        
        return $result;
    }

    /**
     * Import location from csv and save resource in storage.
     *
     * @param  \App\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function importCsv(StoreLocationCsv $request,Location $location)
    {                
        $pid = Crypt::decrypt(Input::get('project_id'));
        $cmp_id = \Session::get('company');
        $file = Input::file('locationcsv');
        $file_name = $file->getClientOriginalName();       
        $extension = $file->getClientOriginalExtension(); // getting image extension
        $temp_path = public_path('uploads/');        
        $uploaded_at = $file->move($temp_path, $file_name);
        $err = '';        
        if (in_array($extension, array('csv'))) {
            $location = array();
            $config = new LexerConfig();
            $config->setDelimiter("\t");
            $lexer = new Lexer($config);
            $interpreter = new Interpreter();
            $interpreter->addObserver(function(array $row) use (&$location) {
                $location[] = $row[0];
            });
            $lexer->parse($temp_path."/".$file_name, $interpreter);            
            $i = 0;
            $data = array();
            $header = array();
            foreach ($location as $key => $loc_arr1) {
                if($i!=0){
                    $data[] = explode(",", $loc_arr1);
                } else {
                    $header[] = explode(",", $loc_arr1);
                }
                $i++;
            }                        
            if (!empty($header[0][0] == "Location")) {
                foreach ($data as $key => $loc_arr2) {                  
                    foreach ($loc_arr2 as $key => $value) {                                        
                        if ($key == 0) {
                            $loc_parent = Location::firstOrNew(array('label' => $value,'parent_id'=>0,'is_deleted'=>0));
                            $loc_parent->project_id = $pid;
                            $loc_parent->parent_id = 0;
                            $loc_parent->company_id = $cmp_id;
                            $loc_parent->label = $value;
                            $loc_parent->order_id = 0;
                            $loc_parent->created_at = date('Y-m-d h:i:s');
                            $loc_parent->created_by = 0;
                            $loc_parent->modified_by = 0;
                            $loc_parent->resource_type = "Webserver";
                            $success = $loc_parent->save();
                            $parent_id = $loc_parent->id;                        
                        } else {
                            $loc_child = Location::firstOrNew(array('label' => $value,'parent_id'=>$parent_id,'is_deleted'=>0));
                            $loc_child->project_id = $pid;
                            $loc_child->parent_id = $parent_id;
                            $loc_child->company_id = $cmp_id;
                            $loc_child->label = $value;
                            $loc_child->order_id = 0;
                            $loc_child->created_at = date('Y-m-d h:i:s');
                            $loc_child->created_by = 0;
                            $loc_child->modified_by = 0;
                            $loc_child->resource_type = "Webserver";
                            $success = $loc_child->save();
                            $parent_id = $loc_child->id;
                        }
                    }
                }
                if (file_exists($temp_path."/".$file_name)) {
                    unlink($temp_path."/".$file_name);
                }
                return redirect('project-location/'.Input::get('project_id'))->with('flash_alert_notice_location', 'Location created successfully!');
            } else {
                if (file_exists($temp_path."/".$file_name)) {
                    unlink($temp_path."/".$file_name);
                }
                $err = "Its not a right csv file for Location.";
                $errors = new MessageBag(['error' => [$err]]);
                return Redirect::back()->withErrors($errors);    
            }            
        } else {
            $err = "Csv File is required.";
            $errors = new MessageBag(['error' => [$err]]);
            return Redirect::back()->withErrors($errors);
        }       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Location $location)
    {
        //
        $input = Input::except('_token'); 
        $location = $location->where('is_deleted','=',0)->where('id','=',$input['location_id'])->where('company_id','=',$input['cmp_id'])->where('project_id','=',Crypt::decrypt($input['project_id']))->first();
        $location->label = $input['location_name'];
        $save = $location->save();
        if($save){
            $msg = "Location Name Update    ";
            $arr = array('msg'=>$msg);
            return json_encode($arr); 
            exit;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,$pid)
    {
        //
        $location = Location::find($id);
        $location->delete();
        return redirect('project-location/'.$pid)->with('flash_alert_delete_notice', 'Location deleted successfully!');
    }

    /**
    * Download Project Location sample csv file
    */
    public function downloadFile()
    {         
        $myFile = public_path("uploads/csv/Project_location_template_export.csv");
        $headers = ['Content-Type: application/csv'];
        //$newName = 'projectLocation-sample-csv-file-'.time().'.csv';
        return response()->download($myFile, "Project_location_template_export.csv", $headers);      
    }
}
