<?php

namespace Modules\Project\Http\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\User;
use App\Models\superadmin\UserDetails;
use Kodeine\Acl\Models\Eloquent\Role;
use App\Models\superadmin\ProjectUserRole;
use App\Models\superadmin\CompanyUserMapping;
use App\Models\superadmin\Project;
use Helpers as Helper;
use Validator;
use Auth;
use Paginate;
use Grids;
use HTML;
use Form;
use View;
use URL;
use DB;
use Crypt;
use Yajra\Datatables\Facades\Datatables as Datatables;
use mjanssen\BreadcrumbsBundle\Breadcrumbs;

class associateUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        Breadcrumbs::addBreadcrumb('Project',  url('/project'));
        Breadcrumbs::addBreadcrumb('Project Associate user',  '');
        $page_data = array('breadcrumbs' => Breadcrumbs::generate());
        $pid = Crypt::decrypt($id);
        $page_data['project'] = Project::select('id','name')->where('is_deleted','=',0)->where('id', $pid)->first();
        $data['css'] = array("dataTables.bootstrap","frontend_custom",'tasks');
        
        $data['scripts'] = array('angularjs/angular.min','angularjs/angular-messages.min','angularjs/dataTable.min','angularjs/angular-flash.min','customSelect','angularjs/controllers/frontend/projectassociateusercontroller',"jquery.dataTables.min","dataTables.bootstrap");
        $title = "Project Associate User";
        return view('project::associateUser.user_list',compact('data','id','title'))->with($page_data);
    }

    /**
    * Get user List from resource
    * @return \Illuminate\Http\Response
    */
    public function userList($pid){
        $project_id = Crypt::decrypt($pid);
        $results = User::where('is_deleted','=',0)->where('is_deleted','=',0)->get();        
        $data = array();        
        $i = 0;        
        $cmp_id = \Session::get('company');
        foreach ($results as $key => $value) {            
            $arr = array();
            if(!empty($value->CompanyUserMapping)) {
                foreach($value->CompanyUserMapping as $val){ // one user can associate with multplae comapny filter
                    $arr[] = $val->company_id;
                }                
                if(!in_array($cmp_id, $arr)) {
                    continue;
                }
            }
            if(!empty($value->userProjectRole[0])){ // is user already exist in project user role table with current project id
                $upr = $value->userProjectRole->where('project_id',$project_id)->first();                  
                if(!empty($upr) && $upr->user_id){
                    continue;   
                }                
            }                          

            $data[$i]['id'] = $value->id;
            $data[$i]['name'] = $value->name;
            $data[$i]['phone'] = $value->phone;            
            $temp_path = public_path('uploads/user/thumbnail_29x29/');
            $path = '';
            $logo_image_src = '';                                
            if(!empty($value->UserDetails) && !empty($value->UserDetails[0])) {                    
                $path = $temp_path.$value->UserDetails[0]->logo_image;
                if(file_exists($path) && $value->UserDetails[0]->logo_image) {
                    $logo_image_src = url("/").'/uploads/user/thumbnail_29x29/'.$value->UserDetails[0]->logo_image;
                }else{
                    $logo_image_src = url("/").'/uploads/user_avatar.png';
                }
            }else{
                $logo_image_src = url("/").'/uploads/user_avatar.png';
            }
            $data[$i]['picture'] = $logo_image_src;
            $data[$i]['listType'] = "User";
            $i++;
        }
        return $data;
    }

    /**
    * Get role List from resource
    * @return \Illuminate\Http\Response
    */
    public function roleList(){
        $roles = Helper::getProjectUserRoles();
        $data = array();
        $i = 0;
        foreach ($roles as $key => $val) {
            $data[$i]['id'] = $val['id'];
            $data[$i]['name'] = $val['name'];
            $data[$i]['listType'] = "Role";            
            $i++;
        }
        return json_encode($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id = Input::get('user');
        $role_id = Input::get('role');
        $project_id = Crypt::decrypt(Input::get('project_id'));
        $cmp_id = \Session::get('company');
        $data['project_id'] = $project_id;
        $data['user_id'] = $id;
        $data['role_id'] = $role_id;
        $data['is_deleted'] = 0;
        $data['created_by'] = Auth::user()->id;
        $data['created_at']  = date('Y-m-d h:i:s');
        $data['modified_by']  = Auth::user()->id;
        $data['resource_type']  = "Webserver";
        $success = ProjectUserRole::insert($data);
        if ($success) {
            $success = \Session::flash('flash_alert_notice', 'Associated User Assigned successfully !');
            return $success;
        } else {
            return false;
        }
    }

    /**
     * Fetch the list from the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function assocUserList(){        
        $project_id = Crypt::decrypt(Input::get('project_id'));        
        $user = User::select("users.id","users.name","users.full_name","roles.name as role_name","project_user_role.id as project_user_role_id","project_user_role.modified_by as modified_by","project_user_role.modified_at as modified_at")
            ->join("project_user_role","project_user_role.user_id","=","users.id")
            ->join("roles","roles.id","=","project_user_role.role_id")
            ->where('project_id',$project_id)
            ->get();        
            //die;
            $associateUser = Datatables::of($user)->addColumn('modified_by', function ($user) {
                    $data = Helper::getColumnValById(array('full_name','email'),$user->modified_by,new User);
                   return $data->full_name;
                })->addColumn('action', function ($user) {
                    $id = Crypt::encrypt($user->project_user_role_id);
                    return '<a id="editPAURto" class="btn btn-warning btn-xs edit-ist" paur-id="'.$id.'" title="Edit"><i class="fa fa-pencil-square-o"></i></a>&nbsp;<a href="'.url('/').'/delete-associate-user-list/'.$id.'" title="Delete" class="btn btn-danger btn-xs" onclick="return confirm('."'Are you sure, you want to delete this User from This Project?'".');"><i class="fa fa-trash-o"></i></a>';
                })->make(true);
            return $associateUser;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {        
        $id = Crypt::decrypt($id);
        $pUrl = ProjectUserRole::find($id);
        $pUrl->role_id = Input::get('rolename');
        $success = $pUrl->save();
        if ($success) {
            $success = \Session::flash('flash_alert_notice', 'Associated User Role Updated successfully !');
            return $success;
        } else {
            return false;
        }
    }

    /**
     * Remove the specif ied resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user, $id)
    {
        $id = Crypt::decrypt($id);
        $pURole = ProjectUserRole::find($id);
        $project_id = $pURole->project_id;
        $pURole->delete();
        $project_id = Crypt::encrypt($project_id);
        return redirect('associate-user/'.$project_id)->with('flash_alert_notice', 'Associated User deleted successfully.');
    }
}
