<?php

namespace Modules\Project\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Input;
use App\Models\superadmin\Company;
use App\Models\superadmin\Project;
use App\Models\superadmin\QualityChecklist;
use App\Models\projectConfiguration\ProjectChecklist;
use App\Models\projectConfiguration\ProjectChecklistTasks;
use App\Models\projectConfiguration\Location;
use Helpers as Helper;
use Validator;
use Auth;
use Paginate;
use Grids;
use HTML;
use Form;
use View;
use URL;
use DB;
use Crypt;
use Yajra\Datatables\Facades\Datatables as Datatables;
use mjanssen\BreadcrumbsBundle\Breadcrumbs;

class projectChecklistController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($pid)
    {
        //
        $pid =  Crypt::decrypt($pid);
        $cmp_id = \Session::get('company');
        $page_title     = 'Project Checklist'; 
        $page_action    = 'Project Checklist Details';
        $viewPage       = 'project-checklist';
        $viewPage1       = '';

        Breadcrumbs::addBreadcrumb('Project',  url('/project'));
        Breadcrumbs::addBreadcrumb('Project Checklist',  '');
        $page_data = array('breadcrumbs' => Breadcrumbs::generate());
        $page_data['obj_companies'] = Company::select('id', 'name')->where('is_deleted','=', 0)->get();
        $page_data['companies'] =  Helper::getCompanyList();
        $page_data['checklist_comps'] = array();
        $page_data['cur_it'] = '';
        $page_data['company_id'] = $cmp_id;
        $page_data['project'] = Project::select('id','name')->where('is_deleted','=',0)->where('id', $pid)->get();
        $page_data['all_projects'] = Project::select('id','name')->where('is_deleted','=',0)->get();
        $id = Crypt::encrypt($pid);
        $data['css'] = array('dataTables.bootstrap','frontend_custom','tasks','ng-tags-input.min');
        $data['scripts'] = array('angularjs/angular.min','angularjs/controllers/frontend/projectchecklistcontroller','jquery.dataTables.min', 'angularjs/ng-tags-input.min',"dataTables.bootstrap");
        return view('project::checklist.checklist',compact('data','page_title','page_action','viewPage','viewPage1','id'))->with($page_data); 
    }

    /**
    * List view by Data table for Assciated project Details
    */
    public function getIssueToByCompany()
    {
        $cmp_id = \Session::get('company');
        $comp_projects = Helper::getCompanyProject($cmp_id);
        $project1 = Project::select([ 'id','name'])->where('is_deleted', '=', 0)->whereIn('id', $comp_projects)->get();
        $data = array();
        $i = 0;
        foreach ($project1 as $key => $value) {
            $data[$i]['id'] = Crypt::encrypt($value->id);
            $data[$i]['name'] = $value->name;
            $i++;
        }
        return json_encode($data);
        exit;
    }

    /**
     * Get Issueto list for specified project.
     *
     */
    public function getChecklistData(){
        $project_id = Input::get('project_id');
        $project_id = Crypt::decrypt($project_id);
        $company_id = \Session::get('company');
        $checklist1 = ProjectChecklist::select(['id', 'checklist_name', 'checklist_tags'])->where('is_deleted', '=', 0)->where('project_id','=', $project_id)->get(); 
        $checklist = Datatables::of($checklist1)->addColumn('checklist_tags', function ($qc) {
                        $tags=  json_decode($qc->checklist_tags);
                        #print_r($tags);
                        $data = array();
                        $tag = '';
                        if(!is_null($tags)){
                            foreach ($tags as $key => $value) {
                                $data[] = $value->text;
                            }
                            $tag = implode(" , ", $data);
                        }
                        return $tag;
        })->addColumn('action', function ($qc) {
                $id = Crypt::encrypt($qc->id);
                return '<a is-id="'.$id.'" class="btn btn-warning btn-xs edit-ist" id="editChecklist" title="Edit"><i class="fa fa-pencil-square-o"></i></a>&nbsp;<a href="/checklist-details/'.$id.'" class="btn btn-success btn-xs" title="View"><i class="fa fa-newspaper-o"></i></a>&nbsp;<a href="'.url('/').'/delete_checklist/'.$id.'" title="Delete" class="btn btn-danger btn-xs" onclick="return confirm('."'Are you sure, you want to delete this Checklist?'".');"><i class="fa fa-trash-o"></i></a>';
            })->make(true);
        return $checklist;
        exit;
    }

    /**
     * Add new Issueto or update specified Issueto details.
     *
     */
    public function updateChecklist()
    {
        $id = Input::get('id');
        $company_id = \Session::get('company');
        $project_id = Crypt::decrypt(Input::get('project_id'));
        if($id){
            $id = Crypt::decrypt($id);
        }
        $checklist = ProjectChecklist::firstOrNew(array('id' => $id));
        $checklist->project_id = $project_id;
        $checklist->checklist_name = Input::get('checklist_name');
        $checklist->checklist_tags = json_encode(Input::get('checklist_tags'));
        $checklist->created_at = date('Y-m-d h:i:s');
        $checklist->created_by = Auth::user()->id;
        $success = $checklist->save();
        if($success){
            $success_alert = \Session::flash('flash_alert_notice', 'Checklist added successfully !');
            return $success_alert;
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       
        $checklist_id = Crypt::decrypt($id);
        $checklist_details = ProjectChecklist::where('id','=',$checklist_id)->first();
        $page_data['checklist'] = $checklist_details;
        $pid = $id =Crypt::encrypt($checklist_details->project_id);
        Breadcrumbs::addBreadcrumb('Project',  url('/project'));
        Breadcrumbs::addBreadcrumb('Project Checklist',  url('/checklist/'.$pid));
        Breadcrumbs::addBreadcrumb('Project Checklist Task',  '');
        $page_data = array('breadcrumbs' => Breadcrumbs::generate());
        $data['css'] = array("datatables.bootstrap","frontend_custom",'tasks','ng-tags-input.min');
        $data['scripts'] = array('angularjs/angular.min','angularjs/angular-messages.min','angularjs/dataTable.min','angularjs/angular-flash.min','angularjs/ng-tags-input.min','angularjs/controllers/frontend/projectchecklisttaskcontroller',"jquery.dataTables.min");
        return view("project::checklist.show_checklist",compact('id','pid','data','checklist_id'))->with($page_data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $id = Crypt::decrypt($id);
        $checklist = ProjectChecklist::where('id','=',$id)->first();
        //return $project_checklist;
        //$id = Crypt::decrypt($enc_id);
        //$checklist = QualityChecklist::find($id);
        if(!empty($checklist)){
            $data = array();
            $data['id'] = $checklist->id;
            $data['checklist_name'] = $checklist->checklist_name;
            $data['checklist_tags'] = json_decode($checklist->checklist_tags);
            return json_encode($data);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editChecklistTask($id)
    {
        $id = Crypt::decrypt($id);
        $checklist_task = ProjectChecklistTasks::where('id','=',$id)->where('is_deleted','=',0)->first();
        //$data['issueto_id'] = Crypt::encrypt($issue_to_user->issueto_id);
        $data['id'] = Crypt::encrypt($checklist_task->id);
        $data['name'] = $checklist_task->name;
        $data['status'] = $checklist_task->status;
        $data['comment'] = $checklist_task->comment;
        $data['form_title'] = "Edit Checklist Task";
        return $data;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $pid)
    {
        //
        $sys_token = \Session::token();
        $token = Input::get('_token');
        if ($token != $sys_token) { 
            return false;
        } 
        $id = (Input::get('id')) ? Crypt::decrypt(Input::get('id')) : 0;
        $checklistTask = ProjectChecklistTasks::firstOrNew(array('id' => $id));
        if($id == 0) {
            $checklistTask->checklist_id = Crypt::decrypt(Input::get('checklist_id'));
            $checklistTask->created_at = date('Y-m-d h:i:s');
        }

        $checklistTask->name = Input::get('name');
        $checklistTask->status = Input::get('status');
        $checklistTask->comment = Input::get('comment');
        $checklistTask->modified_at = date('Y-m-d h:i:s');
        $checklistTask->modified_by = Auth::user()->id;
        $checklistTask->created_at = date('Y-m-d h:i:s');
        $checklistTask->created_by = Auth::user()->id;
        $success = $checklistTask->save();

        if($success){
            $success_alert = \Session::flash('flash_alert_notice', ($id!=0) ? 'Checklist Task updated successfully !' : 'Checklist Task created successfully !');
            return $success_alert;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $id = Crypt::decrypt($id);
        $project = ProjectChecklist::where('id','=',$id)->first();
        $project_id = $project->project_id;
        $project->is_deleted = 1;
        $project->save();
        $pid = Crypt::encrypt($project_id);
        return redirect('checklist/'.$pid)->with('flash_alert_notice', 'Checklist deleted successfully!');
    }

    public function destroyTask($id)
    {
        $id = Crypt::decrypt($id);
        $project = ProjectChecklistTasks::where('id','=',$id)->first();
        $checklist_id = $project->checklist_id;
        $project->is_deleted = 1;
        $project->save();
        $checklist_id = Crypt::encrypt($checklist_id);
        return redirect('checklist-details/'.$checklist_id)->with('flash_alert_notice', 'Checklist task deleted successfully!');
    }

    /**
     *  get location search query data.
     */
    public function locationTags(){
        $tag = Input::get('tag');
        $results = Location::select('label')->where('label','LIKE',$tag.'%')->get();
        $tags = array();
        foreach ($results as $key => $value) {
            $tags[]['text'] = $value->label;
        }
        //print_r($tags); die;
        return $tags;
    }

    /**
     * Get Issueto list for specified project.
     *
     */
    public function getChecklistTaskData($checklistId){
        $checklistId = Crypt::decrypt($checklistId);
        $checklist1 = ProjectChecklistTasks::select(['id', 'name', 'status', 'comment'])->where('is_deleted', '=', 0)->where('checklist_id','=',$checklistId)->where('created_by','=', Auth::user()->id)->get();
        $checklist = Datatables::of($checklist1)->addColumn('action', function ($ist) {
                $id = Crypt::encrypt($ist->id);
                return '<a id="editChecklist" class="btn btn-warning btn-xs edit-ist" Is-id="'.$id.'" title="Edit"><i class="fa fa-pencil-square-o"></i></a>&nbsp;<a href="'.url('/').'/delete_checklist_task/'.$id.'" title="Delete" class="btn btn-danger btn-xs" onclick="return confirm('."'Are you sure, you want to delete this Task?'".');"><i class="fa fa-trash-o"></i></a>';
            })->make(true);
        return $checklist;
        exit;
    }
}
