<?php

namespace Modules\Project\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Input;
use App\Models\superadmin\Company;
use App\Models\superadmin\Project;
use App\Models\superadmin\Issueto;
use App\Models\projectConfiguration\ProjectIssueto;
use App\Models\projectConfiguration\ProjectIssuetoUsers;
use App\Models\projectConfiguration\Location;
use Helpers as Helper;
use Validator;
use Auth;
use Paginate;
use Grids;
use HTML;
use Form;
use View;
use URL;
use DB;
use Crypt;
use Yajra\Datatables\Facades\Datatables as Datatables;
use mjanssen\BreadcrumbsBundle\Breadcrumbs;

class projectIssuetoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($pid)
    {
        //
        $pid =  Crypt::decrypt($pid);
        $cmp_id = \Session::get('company');
        $page_title     = 'Project Issuedto'; 
        $page_action    = 'Project Issuedto Details';
        $viewPage       = 'project-issueto';
        $viewPage1       = '';

        Breadcrumbs::addBreadcrumb('Project',  url('/project'));
        Breadcrumbs::addBreadcrumb('Project Issuedto',  '');
        $page_data = array('breadcrumbs' => Breadcrumbs::generate());
        $page_data['obj_companies'] = Company::select('id', 'name')->where('is_deleted','=', 0)->get();
        $page_data['companies'] =  Helper::getCompanyList();
        $page_data['issueto_comps'] = array();
        $page_data['cur_it'] = '';
        $page_data['company_id'] = $cmp_id;
        $page_data['project'] = Project::select('id','name')->where('is_deleted','=',0)->where('id', $pid)->first();
        $page_data['all_projects'] = Project::select('id','name')->where('is_deleted','=',0)->get();
        $page_data['option_activity'] = array(
                        "Brickwork"=>"Brickwork",
                        "Carpentry"=>"Carpentry",
                        "Cleaning"=>"Cleaning",
                        "Concreting"=>"Concreting",
                        "Electrical"=>"Electrical",
                        "Fire services"=>"Fire services",
                        "Joinery"=>"Joinery",
                        "Plasterboard"=>"Plasterboard",
                        "Painting"=>"Painting",
                        "Plumbing"=>"Plumbing",
                        "Metalwork"=>"Metalwork",
                        "Structural steel"=>"structural steel",
                        "Waterproofing"=>"Waterproofing",
                        "Other"=>"Other",
                    );
        $id = Crypt::encrypt($pid);
        // echo "<pre>";
        // print_r($page_data['project']);die;
        $title = "Project Issuedto";
        $data['css'] = array("dataTables.bootstrap","frontend_custom",'tasks');
        $data['scripts'] = array('angularjs/angular.min','angularjs/controllers/frontend/projectissuetocontroller','angularjs/angular-messages.min',"jquery.dataTables.min","dataTables.bootstrap");        
        return view('project::issueto.issueto_list',compact('data','page_title','page_action','viewPage','viewPage1','id','title'))->with($page_data); 
    }

    /**
    * List view by Data table for Assciated project Details
    */
    public function getIssueToByCompany()
    {
        $cmp_id = \Session::get('company');
        $comp_projects = Helper::getCompanyProject($cmp_id);        
        $project1 = Project::select([ 'id','name'])->where('is_deleted', '=', 0)->whereIn('id', $comp_projects)->get();
        $data = array();
        $i = 0;
        foreach ($project1 as $key => $value) {
            $data[$i]['id'] = Crypt::encrypt($value->id);
            $data[$i]['name'] = $value->name;
            $i++;
        }
        return json_encode($data);
        exit;        
    }

    /**
     * Get Issueto list for specified project.
     *
     */
    public function getIssuetoData(){        
        $project_id = Input::get('project_id');
        $project_id = Crypt::decrypt($project_id);
        $company_id = \Session::get('company');        
        $issueto1 = ProjectIssueto::select(['id', 'name','auther' ,'company_id', 'email', 'phone', 'activity'])->where('is_deleted', '=', 0)->where('project_id','=', $project_id)->where('company_id','=', $company_id)->get(); 
        $issueto = Datatables::of($issueto1)->addColumn('action', function ($ist) {
                $id = Crypt::encrypt($ist->id);
                return '<a id="editIssuto" class="btn btn-warning btn-xs edit-ist" Is-id="'.$id.'" title="Edit"><i class="fa fa-pencil-square-o"></i></a>&nbsp;<a href="/issueto-details/'.$id.'" class="btn btn-success btn-xs" title="View"><i class="fa fa-newspaper-o"></i></a>&nbsp;<a href="'.url('/').'/delete_issueto/'.$id.'" title="Delete" class="btn btn-danger btn-xs" onclick="return confirm('."'Are you sure, you want to delete this Issueto?'".');"><i class="fa fa-trash-o"></i></a>';
            })->make(true);
        return $issueto;
        exit;        
    }

    /**
     * Add new Issueto or update specified Issueto details.
     *
     */
    public function updateIssueto()
    {           
        $id = Input::get('id');        
        $company_id = \Session::get('company');
        $project_id = Crypt::decrypt(Input::get('project_id'));        
        if($id){
            $id = Crypt::decrypt($id);
        }        
        $issueto = ProjectIssueto::firstOrNew(array('id' => $id));
        $issueto->project_id = $project_id;
        $issueto->name = Input::get('name');
        $issueto->company_id = $company_id;
        $issueto->company_issueto_id = !empty($issueto->company_issueto_id)?$issueto->company_issueto_id:0;
        $issueto->email = Input::get('email'); 
        $issueto->phone = Input::get('phone');
        $issueto->activity = Input::get('activity'); 
        $issueto->auther = Input::get('auther'); 
        $issueto->description = Input::get('description'); 
        $issueto->created_at = date('Y-m-d h:i:s');
        $issueto->created_by = Auth::user()->id;
        $success = $issueto->save();
        if($success){
            $success_alert = \Session::flash('flash_alert_notice', 'Issueto added successfully !');
            return $success_alert;
        }
    }

    /**
    * get All issueto list from company
    * return in json
    */
    public function getAllIssuetoCompanies($pid){
        $issue_to_list = issueTo::where('is_deleted','=',0)->get();
        $data = array();
        $i = 0;                
        $pid = Crypt::decrypt($pid);
        foreach ($issue_to_list as $key => $value) {
            $data[$i]['name'] = $value['name'];
            $data[$i]['id'] = Crypt::encrypt($value['id']);
            $issue_to_exist_arr = array();
            $j = 0;
            $issue_to = $value->issueToCompany;
            foreach ($value->issueToCompany as $key => $value) {
                $issue_to_exist_arr[$j] = $value->project_id;
                $j++;
            }
            if(in_array($pid , $issue_to_exist_arr)) {
                $key = array_search($pid , $issue_to_exist_arr);
            }
            if(!empty($issue_to[$key]) && $issue_to[$key]->company_issueto_id != 0 && in_array($pid , $issue_to_exist_arr) && $issue_to[$key]->company_id == \Session::get('company') && $issue_to[$key]->is_deleted == 0) {
                $data[$i]['status'] = "linked";
            }else {
                $data[$i]['status'] = "unlinked";
            }
            $i++;
        }        
        return json_encode($data);
    }

    public function saveIssueToFromCompany(){
        
        $cmp_issue_id_arr = Input::get('id');
        foreach ($cmp_issue_id_arr as $key => $cmp_issue_id) {
            $cmp_issue_id = Crypt::decrypt($cmp_issue_id);            
            $project_id = Crypt::decrypt(Input::get('project_id'));            
            $issue_to = issueTo::where('is_deleted','=',0)->where('id','=',$cmp_issue_id)->first();
            $data = array();
            $issue_to = issueTo::where('is_deleted','=',0)->where('id','=',$cmp_issue_id)->first();
            $data = array();
            $data['project_id'] = $project_id;
            $data['company_id'] = \Session::get('company');
            $data['company_issueto_id'] = $issue_to->id;
            $data['name'] = $issue_to->name;
            $data['activity'] = $issue_to->activity;
            $data['email'] = $issue_to->email;
            $data['phone'] = $issue_to->phone;
            $data['auther'] = $issue_to->auther;
            $data['description'] = $issue_to->description;
            $data['created_at'] = date('Y-m-d h:i:s');
            $data['created_by'] = Auth::user()->id;        
            $is_issue_to_exist = ProjectIssueto::firstOrNew(array('company_issueto_id' => $issue_to->id,'project_id'=>$project_id,'company_id'=>\Session::get('company')));            
            if(!empty($is_issue_to_exist->id)){
                $is_issue_to_exist->is_deleted = 0;
                $save = $is_issue_to_exist->save($data);
            }else{
                $save = ProjectIssueto::insert($data);
            }
        }                
        if($save){
            $success_alert = \Session::flash('flash_alert_notice', 'Issueto added successfully!');
            return $success_alert;
        }
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {       
        $issue_to_id = Crypt::decrypt($id);
        $issueto_details = ProjectIssueto::where('id','=',$issue_to_id)->first();        
        $pid = $issueto_details->project_id;        
        $pid = $id =Crypt::encrypt($issueto_details->project_id);
        Breadcrumbs::addBreadcrumb('Project',  url('/project'));
        Breadcrumbs::addBreadcrumb('Project Issuedto',  url('/issueto/'.$pid));
        Breadcrumbs::addBreadcrumb('Project IssuedTo User',  '');
        $page_data = array('breadcrumbs' => Breadcrumbs::generate());
        $page_data['option_activity'] = array(
            "Brickwork"=>"Brickwork",
            "Carpentry"=>"Carpentry",
            "Cleaning"=>"Cleaning",
            "Concreting"=>"Concreting",
            "Electrical"=>"Electrical",
            "Fire services"=>"Fire services",
            "Joinery"=>"Joinery",
            "Plasterboard"=>"Plasterboard",
            "Painting"=>"Painting",
            "Plumbing"=>"Plumbing",
            "Metalwork"=>"Metalwork",
            "Structural steel"=>"structural steel",
            "Waterproofing"=>"Waterproofing",
            "Other"=>"Other",
        );
        $page_data['project'] = Project::select('id','name')->where('is_deleted','=',0)->where('id', $issueto_details->project_id)->first();          
        $page_data['issueto'] = $issueto_details;
        $data['css'] = array("dataTables.bootstrap","frontend_custom",'tasks','ng-tags-input.min');        
        $data['scripts'] = array('angularjs/angular.min','angularjs/angular-messages.min','angularjs/dataTable.min','angularjs/angular-flash.min','angularjs/ng-tags-input.min','angularjs/controllers/frontend/projectissuetousercontroller',"jquery.dataTables.min","dataTables.bootstrap");
        $title = "Project Issuedto User";
        return view("project::issueto.show_issueto",compact('id','pid','data','issue_to_id','title'))->with($page_data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $id = Crypt::decrypt($id);
        $project_issue_to = ProjectIssueto::where('id','=',$id)->first();
        return $project_issue_to;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editIssuetoUser($id)
    {
        $id = Crypt::decrypt($id);
        $issue_to_user = ProjectIssuetoUsers::where('id','=',$id)->where('is_deleted','=',0)->first();        
        //$data['issueto_id'] = Crypt::encrypt($issue_to_user->issueto_id);
        $data['id'] = Crypt::encrypt($issue_to_user->id);
        $data['name'] = $issue_to_user->name;
        $data['email'] = $issue_to_user->email;
        $data['phone'] = $issue_to_user->phone;
        $data['trade'] = $issue_to_user->trade;
        $data['tag'] = json_decode($issue_to_user->tag);
        $data['form_title'] = "Edit IssuedTo User";
        return $data;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $pid)
    {
        //
        $sys_token = \Session::token();
        $token = Input::get('_token');
        if ($token != $sys_token) { 
            return false;
        }         
        $id = (Input::get('id')) ? Crypt::decrypt(Input::get('id')) : 0;
        $issuetoUser = ProjectIssuetoUsers::firstOrNew(array('id' => $id));
        if($id == 0) {
            $issuetoUser->issueto_id = Crypt::decrypt(Input::get('issueto_id'));
            $issuetoUser->created_at = date('Y-m-d h:i:s');
        }

        $issuetoUser->name = Input::get('name');
        $issuetoUser->email = Input::get('email');
        $issuetoUser->phone = Input::get('phone');
        $issuetoUser->tag = json_encode(Input::get('tag'));
        $issuetoUser->trade = Input::get('trade');        
        $issuetoUser->created_by = Auth::user()->id;
        $issuetoUser->modified_by = Auth::user()->id;        
        $success = $issuetoUser->save();

        if($success){
            $success_alert = \Session::flash('flash_alert_notice', ($id!=0) ? 'Issuedto User updated successfully !' : 'Issuedto User created successfully !');
            return $success_alert;
        }        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $id = Crypt::decrypt($id);        
        $project = ProjectIssueto::where('id','=',$id)->first();
        $project_id = $project->project_id;
        ProjectIssueto::destroy($id);
                
        $pid = Crypt::encrypt($project_id);
        return redirect('issueto/'.$pid)->with('flash_alert_notice', 'issueTo deleted successfully!');        
    }

    /**
     *  get location search query data.
     */
    public function locationTags(){
        $tag = Input::get('tag');
        $results = Location::select('label')->where('label','LIKE',$tag.'%')->get();
        $tags = array();
        foreach ($results as $key => $value) {
            $tags[]['text'] = $value->label;
        }
        //print_r($tags); die;
        return $tags;
    }

    /**
     * Get Issueto list for specified project.
     *
     */
    public function getIssuetoUserData($issueToId){        
        $issueToId = Crypt::decrypt($issueToId);
        $issueto1 = ProjectIssuetoUsers::select(['id', 'name', 'email', 'phone', 'tag'])->where('is_deleted', '=', 0)->where('issueto_id','=',$issueToId)->where('created_by','=', Auth::user()->id)->get();
        $issueto = Datatables::of($issueto1)->addColumn('tag', function ($itu) {
                        $tags=  json_decode($itu->tag);
                        #print_r($tags);
                        $data = array();
                        $tag = '';
                        if(!empty($tags)){
                            foreach ($tags as $key => $value) {
                                $data[] = $value->text;
                            }
                            $tag = implode(" , ", $data);
                            return $tag;
                        }
                        return "";
                        
        })->addColumn('action', function ($ist) {
                $id = Crypt::encrypt($ist->id);
                return '<a id="editIssuto" class="btn btn-warning btn-xs edit-ist" Is-id="'.$id.'" title="Edit"><i class="fa fa-pencil-square-o"></i></a>&nbsp;<a href="'.url('/').'/delete_issueto/'.$id.'" title="Delete" class="btn btn-danger btn-xs" onclick="return confirm('."'Are you sure, you want to delete this Issueto?'".');"><i class="fa fa-trash-o"></i></a>';
            })->make(true);
        return $issueto;
        exit;        
    }

    /**
    * Project issueto email exist,
    * accept two parameter
    * return boolean
    */

    public function issueto_email_check_isexist($id){        
        $email = Input::get('email');
        $pid = Crypt::decrypt(Input::get('pid'));
        if (!empty($email)){
            $is_email = ProjectIssueto::where('email',"=",$email)->where('project_id',"=",$pid)->where('id',"!=",$id)->get();
            if (isset($is_email[0]) && !empty($is_email[0])){
                echo 'true';
            }else{
                echo 'false';
            }
        }
    }

    /**
    * Project issueto User email exist,
    * accept two parameter
    * return boolean
    */

    public function issueto_user_email_check_isexist($id){                
        $email = Input::get('email');
        $issued_id = Crypt::decrypt(Input::get('issuedto_id'));        
        $id = $id?Crypt::decrypt($id):0;        
        if (!empty($email)){
            $is_email = ProjectIssuetoUsers::where('email',"=",$email)->where('issueto_id',"=",$issued_id)->where('id',"!=",$id)->get();
            if (isset($is_email[0]) && !empty($is_email[0])){
                echo 'true';
            }else{
                echo 'false';
            }
        }
    }
}
