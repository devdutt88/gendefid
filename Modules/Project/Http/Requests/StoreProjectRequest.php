<?php

namespace Modules\Project\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreProjectRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ( $this->method() ) {
                case 'GET':
                case 'DELETE': {
                        return [ ];
                    }
                case 'POST': {
                        return [                               
                            'projname'=> "required|min:3",                            
                            'projecttype'=> "required",
                            'address1'=> "required",
                            'suburb'=> "required",                            
                            'postal_code'=> "required",
                            'state' => "required",
                            'country' => "required",
                        ];
                    }
                case 'PUT':
                case 'PATCH': 
                default:break;
            }
    }
}
