<?php

Route::group(['middleware' => ['web','auth','company'], 'prefix' => '', 'namespace' => 'Modules\Project\Http\Controllers'], function()
{
    //Project
    Route::get('project', 'ProjectController@index');
    Route::get('project_list', 'ProjectController@projectList');
    Route::get('project/add/', 'ProjectController@create');
    Route::get('project/edit/{id}', 'ProjectController@edit');
    Route::post('save-project', 'ProjectController@store');
    Route::post('update-project', 'ProjectController@update');
    Route::get('form-data/{id}', 'ProjectController@projectData'); 
    Route::post('project_isexist/{id}','ProjectController@projectIsexist'); 
    Route::post('delete-project','ProjectController@destroy'); 
    //Project config
    Route::get('project/config/{id}', 'ProjectConfigController@index');
    Route::post('update-logo', 'ProjectConfigController@update');    
         
    //Project Location
    Route::get('project/location/{id}', 'ProjectLocationController@index');
    Route::get('location-sample-csv', 'ProjectLocationController@downloadFile');    
    Route::post('save-location', 'ProjectLocationController@store');
    Route::get('export-csv/{id}', 'ProjectLocationController@exportCsv');
    Route::post('import-csv', 'ProjectLocationController@importCsv');
    Route::post('update-location', 'ProjectLocationController@update');
    Route::get('delete-location/{id}/{pid}', 'ProjectLocationController@destroy');

    // Project issue to
    Route::get('project/issueto/{id}', 'projectIssuetoController@index'); 
    Route::get('getAllIssuetoCompanies/{pid}', 'projectIssuetoController@getAllIssuetoCompanies');    
    Route::post('saveIssueToFromCompany', 'projectIssuetoController@saveIssueToFromCompany');            
    Route::get('issueto_list', 'projectIssuetoController@getIssuetoData');
    Route::post('save_issueto', 'projectIssuetoController@updateIssueto');    
    Route::get('issuetobycompany','projectIssuetoController@getIssueToByCompany');
    Route::get('delete_issueto/{id}', 'projectIssuetoController@destroy');
    Route::get('edit-issueto/{id}', 'projectIssuetoController@edit');
    Route::get('issueto-details/{id}', 'projectIssuetoController@show');
    Route::get('locationTags', 'projectIssuetoController@locationTags');
    Route::post('save_issueto_user/{pid}', 'projectIssuetoController@update');
    Route::get('issueTo-userList/{isId}', 'projectIssuetoController@getIssuetoUserData');
    Route::get('edit-issuetoUser/{id}', 'projectIssuetoController@editIssuetoUser');  
    Route::post('issueto-email-exist/{id}', 'projectIssuetoController@issueto_email_check_isexist');
    Route::post('issueto-user-email-check/{id}', 'projectIssuetoController@issueto_user_email_check_isexist');
    
    //associate User
    Route::get('project/associate-users/{id}', 'associateUserController@index');
    Route::get('user-list/{id}', 'associateUserController@userList');
    Route::get('role-list', 'associateUserController@roleList');
    Route::post('save-associate', 'associateUserController@store');
    Route::get('associate-user-list', 'associateUserController@assocUserList');
    Route::get('delete-associate-user-list/{id}', 'associateUserController@destroy');
    Route::post('update-user-role/{id}', 'associateUserController@update');

    // Project Checklist
    Route::get('project/checklist/{id}', 'projectChecklistController@index'); 
    Route::post('save_checklist', 'projectChecklistController@updateChecklist');    
    Route::get('checklist', 'projectChecklistController@getChecklistData');
    Route::get('edit-checklist/{id}', 'projectChecklistController@edit');
    Route::get('checklist-details/{id}', 'projectChecklistController@show');
    Route::post('save_checklist_task/{pid}', 'projectChecklistController@update');
    Route::get('checklist-taskList/{isId}', 'projectChecklistController@getChecklistTaskData');
    Route::get('edit-checklistTask/{id}', 'projectChecklistController@editChecklistTask'); 
    Route::get('delete_checklist/{id}', 'projectChecklistController@destroy');
    Route::get('delete_checklist_task/{id}', 'projectChecklistController@destroyTask');
    Route::get('qc-locations','projectChecklistController@locationTags');

    // General Defect Controller
    Route::get('project/general-defect/{pid}','GeneralDefectController@index');
    Route::get('locationTagsval/{pid}', 'GeneralDefectController@locationTags');
    Route::post('save-general-defect', 'GeneralDefectController@store');
    Route::get('general_defect_list','GeneralDefectController@getData');
    Route::get('edit_general_defect/{id}','GeneralDefectController@edit');
    Route::post('update-general-defect/{id}', 'GeneralDefectController@update');
    Route::get('delete-general-defect/{id}','GeneralDefectController@destroy');

    Route::get("test","ProjectController@test");

});
