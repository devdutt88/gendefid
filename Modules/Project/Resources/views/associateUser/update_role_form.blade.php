<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
<div class="modal fade bd-example-modal-sm align-center" id="project_role" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-sm" style="top:25%">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Update Role</h4>
			</div>
			<form id="roleForm" name="roleForm" class="horizontal-form" ng-submit="saveIssueto(label,id)" nonvalidate>
			<div class="modal-body">				
					<select name="rolename" class="form-control" id="rolename" ng-model="role.rolename" ng-required="true">
					 	<option value="">Please select Role</option>
				      	<option ng-repeat="option in allRoles" value="@{{option.id}}">@{{option.name}}</option>
				    </select>					    
				    <span class="error" ng-show="roleForm.rolename.$invalid && roleForm.rolename.$touched">Role is required</span>			    
			</div>
			<div class="modal-footer">				
				<button type="button" class="btn default" data-dismiss="modal">Close</button>
				<button type="button" ng-click="changeRole(uid)"  ng-disabled="roleForm.$invalid" class="btn blue">Save</button>
				<span ng-show="showLoader">
                	<img src="{{url('/')}}/assets/img/input-spinner.gif">
                </span>
			</div>
			</form>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->