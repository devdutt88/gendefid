@extends('frontend::layouts.master')

@section('content')
<!-- <link href="{{url('/')}}/assets/css/tasks.css" rel="stylesheet" type="text/css"/> -->
<div class="page-container" ng-app="projectAssociateUserModule" ng-controller="projectAssociateUsercontroller" ng-cloak="" class="autocompletedemoInsideDialog" >
<input type="hidden" name="project_id" ng-model="project_id" ng-init="project_id='{{$id}}'"/>
	<!-- BEGIN PAGE HEAD -->	
	<div class="page-head">
		<div class="container">
			<!-- BEGIN PAGE TITLE -->
			@include('frontend::layouts.company_setting')		
			<!-- END PAGE TITLE -->
		</div>
	</div>		
	<!-- END PAGE HEAD -->
	<!-- BEGIN PAGE CONTENT -->
	<div class="page-content">
		<div class="container">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- /.modal -->
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE BREADCRUMB -->
			@include('project::partials.breadcrumb')
			<!-- END PAGE BREADCRUMB -->
			<!-- BEGIN PAGE CONTENT INNER -->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN TODO SIDEBAR -->					
					@include('project::layouts.project_sidebar')
					<!-- END TODO SIDEBAR -->
					<!-- BEGIN TODO CONTENT -->
					<div class="todo-content">
					<!-- Alert Section -->
					@if(Session::has('flash_alert_notice'))
				        <div class="alert alert-success alert-dismissable cst-alert cst-normal-alert">
				          	<!-- <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button> -->
				          	<i class="icon fa fa-check"></i>  
				           	{{ Session::get('flash_alert_notice') }} 
				        </div>
				   	@endif
				<!-- End Alert Section -->
						<div class="portlet light">
						<!-- issue to list -->
						<div ng-init="companyIssueToList()">
						<div id="wait" class="loader" ng-show="loader"><img src='{{url("/")}}/img/demo_wait.gif' width="64" height="64" /><br>Loading..</div>
							<div class="portlet">
			                    <div class="portlet-title">
			                        <div class="caption">
			                            <i class="icon-microphone font-dark hide"></i>
			                            <span class="caption-subject bold associate_user uppercase"> Assign User to Project </span>
			                        </div>							                       									 
			                    </div>
			                    <flash-message duration="2000" show-close="true" on-dismiss="myCallback(flash)"></flash-message>
								<div class="portlet-body" >
					                <div class="task-content">
										<div layout="row">
											<div class="col-md-5">												
												
												 <div custom-select="t as t.name for t in people | filter: { name: $searchTerm }" ng-model="person">
									                <div class="pull-left" style="width: 40px">
									                    <img ng-src="@{{ t.picture }}" style="width: 30px" />
									                </div>
									                <div class="pull-left">
									                    <strong>@{{ t.name }}</strong><br />
									                    <span>@{{ t.phone }}</span>
									                </div>
									                
									            </div>

											</div>
											<div class="col-md-5" id="role" style="display: none;">
												<div custom-select="s as s.name for s in role | filter: { name: $searchTerm }" ng-model="roles">									                
									                <div class="pull-left">
									                    <strong>@{{ s.name }}</strong>									                    
									                </div>									                
									            </div>
											</div>
											<div class="col-md-2" id="submit" style="display: none;">
												<button class="btn btn-primary" ng-disabled="isDisable" ng-click="saveAssociateUser('companyAssociate')">Submit</button>
											</div>

										</div>										
										<div class="clearfix"></div>
					                </div>
					            </div>
							</div>
						</div>
						<!-- issue to list -->
							<!-- PROJECT HEAD -->
							<!-- <div class="portlet-title">
								<div class="caption pull-right">
		                            <i class="icon-microphone font-dark hide"></i>		                            
		                            <button class="btn btn-circle btn-sm green" id="" ng-click="showIssueto('add')"> <i class="fa fa-plus"></i> Add New </button>
		                        </div>							
							</div> -->
							<!-- end PROJECT HEAD -->
													
								<!-- start form -->
								@include('project::associateUser.update_role_form')			
								<div class="portlet-body">
									<div class="row">
										<div class="col-md-12">
											<div class="table-responsive">
												<table class="table table-striped table-bordered table-hover" id="sample_1">
													<thead>
														<tr>
															<th> Login Name </th>
															<th> Fullname </th>
															<th> Role </th>
															<th> Modified By </th>
															<th> Modified Date </th>														
															<th> Action </th>
														</tr>
													</thead>
												</table>
											</div>
										</div>										
									</div>
								</div>
								<!-- end form -->
							
						</div>
					</div>

					
					<!-- END TODO CONTENT -->
				</div>
			</div>
			<!-- END PAGE CONTENT INNER -->
		</div>
	</div>
	<!-- END PAGE CONTENT -->
</div>

@stop