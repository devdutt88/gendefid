@extends('frontend::layouts.master')

@section('content')

<!-- <link href="{{url('/')}}/assets/css/tasks.css" rel="stylesheet" type="text/css"/> -->
<div class="page-container" ng-app="projectChecklistModule" ng-controller="projectChecklistController">
<input type="hidden" name="project_id" ng-model="project_id" ng-init="project_id='{{$id}}'"/>

	<!-- BEGIN PAGE HEAD -->
	<div class="page-head">
		<div class="container">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<h1>Project Configuration</h1>
			</div>
			<!-- END PAGE TITLE -->
			<!-- BEGIN PAGE TOOLBAR -->
			
			<!-- END PAGE TOOLBAR -->
		</div>
	</div>
	<!-- END PAGE HEAD -->
	<!-- BEGIN PAGE CONTENT -->
	<div class="page-content">
		<div class="container">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
							<h4 class="modal-title">Modal title</h4>
						</div>
						<div class="modal-body">
							 Widget settings form goes here
						</div>
						<div class="modal-footer">
							<button type="button" class="btn blue">Save changes</button>
							<button type="button" class="btn default" data-dismiss="modal">Close</button>
						</div>
					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			</div>
			<!-- /.modal -->
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE BREADCRUMB -->
			@include('project::partials.breadcrumb')
			<!-- END PAGE BREADCRUMB -->
			<!-- BEGIN PAGE CONTENT INNER -->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN TODO SIDEBAR -->
					@include('project::layouts.project_sidebar')
					<!-- END TODO SIDEBAR -->
					<!-- BEGIN TODO CONTENT -->
					<div class="todo-content">
					<!-- Alert Section -->
					@if(Session::has('flash_alert_notice'))
				        <div class="alert alert-success alert-dismissable cst-alert cst-normal-alert">
				          	<!-- <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button> -->
				          	<i class="icon fa fa-check"></i>  
				           	{{ Session::get('flash_alert_notice') }} 
				        </div>
				   	@endif
				<!-- End Alert Section -->
						<div class="portlet light">
						<!-- issue to list -->

						<!-- issue to list -->
							<!-- PROJECT HEAD -->
							<div class="portlet-title">
								<div class="caption pull-right">
		                            <i class="icon-microphone font-dark hide"></i>	
		                            <!-- <button class="btn btn-circle btn-sm blue" id="" ng-click="showChecklist('cmpChk')"> <i class="fa fa-plus"></i> Inherit Quality Checklist </button> -->
		                            <button class="btn btn-circle btn-sm green" id="" ng-click="showChecklist('add')"> <i class="fa fa-plus"></i> Add New </button>
		                        </div>
							</div>
							<!-- end PROJECT HEAD -->
							<div class="portlet-body">
								<!-- start form -->
								<input type="hidden" name="checklist_id" id="checklist_id" ng-model="checklist_id" value="">
								<div class="portlet-body">
									<div class="row">
										<div class="col-md-12">											
											@include('project::checklist.checklist_form')
											<?php /* @include('project::issueto.issuetoCompany') */ ?>
											<table class="table table-striped table-bordered table-hover" id="sample_1">
												<thead>
													<tr>
														<th> Name </th>
														<th> Tags </th>
														<th> Action </th>
													</tr>
												</thead>
											</table>
										</div>
									</div>
								</div>
								<!-- end form -->
							</div>
						</div>
					</div>

					<!-- END TODO CONTENT -->
				</div>
			</div>
			<!-- END PAGE CONTENT INNER -->
		</div>
	</div>
	<!-- END PAGE CONTENT -->
</div>
@stop