<div class="modal fade" id="checklistModal" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog modal-lg">
	    <div class="modal-content">
			<!-- BEGIN FORM-->
		 	<form id="checklistInfo" name="checklistInfo" class="horizontal-form" ng-submit="saveChecklist(label,id)" nonvalidate>
				<div class="modal-header">
					{{ Form::button("×", $attributes = array("class"=>"close","ng-click"=>"reset(checklistInfo)", "data-dismiss" => "modal", "aria-hidden"=>"true")) }}
					<h4 class="modal-title">@{{checklist.form_title}}</h4>
				</div>
				<div class="modal-body">
					{{ csrf_field() }}
					{{ Form::hidden("checklist_id", "checklist.id", $attributes = array("id" => "checklist_id","ng-model"=>"checklist.id", "ng-init" => "checklist.id=checklist.id")) }}
					<div class="form-body">
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label class="control-label">Name <span class="red">*</span></label>
									{{ Form::text("checklist_name", "", $attributes = array("class"=>"form-control", "id"=>"checklist_name","ng-model"=>"checklist.checklist_name", "ng-required" => "true")) }}
									<span class="error" ng-show="checklistInfo.checklist_name.$invalid && checklistInfo.checklist_name.$touched">Checklist name is required</span>
								</div>
							</div>
						</div>
						<!--/row-->
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label class="control-label">Tags</label>
									 <?php //$query=""; ?>
									 <tags-input ng-model="checklist.checklist_tags">
								        <auto-complete source="loadTags($query)"></auto-complete>
								    </tags-input>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">

					{{ Form::button("Cancel", $attributes = array("class"=>"btn btn-default","ng-click"=>"reset(checklistInfo)", "data-dismiss"=>"modal")) }}
					<button type="submit" class="btn btn-primary" id="details_form" ng-disabled="checklistInfo.$invalid || isDisabled">Save</button>
					<span ng-show="showLoader">
	                	<img src="{{url('/')}}/assets/img/input-spinner.gif">
	                </span>
				</div>
			</form>
			<!-- END FORM-->
		</div>
	</div>
</div>
