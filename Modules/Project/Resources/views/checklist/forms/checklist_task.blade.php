<div class="modal fade" id="checklistTaskModal" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog">
	    <div class="modal-content">
			<!-- BEGIN FORM-->
		 	<form id="checklistTaskFrm" name="checklistTaskFrm" class="horizontal-form" method="post" ng-submit="saveChecklistTask('{{Crypt::encrypt($checklist_id)}}')" nonvalidate>
				<div class="modal-header">
					{{ Form::button("×", $attributes = array("class"=>"close","ng-click"=>"reset(checklistTaskFrm)", "data-dismiss" => "modal", "aria-hidden"=>"true")) }}
					<h4 class="modal-title">@{{checklistTask.form_title}}</h4>
				</div>
				<div class="modal-body">
					{{ csrf_field() }}
					<input type="hidden" name="checklist_id" ng-model="checklist_id" ng-init="checklist_id='{{Crypt::encrypt($checklist_id)}}'"/>
					{{ Form::hidden("checklist_task_id", "checklistTask.id", $attributes = array("id" => "checklist_task_id","ng-model"=>"checklistTask.id", "ng-init" => "checklistTask.id=checklistTask.id")) }}
					<div class="form-body">
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Name <span class="red">*</span></label>
									{{ Form::text("name", "", $attributes = array("class"=>"form-control", "id"=>"name","ng-model"=>"checklistTask.name", "ng-required" => "true")) }}
									<span class="error" ng-show="checklistTaskFrm.name.$invalid && checklistTaskFrm.name.$touched">Name is required</span>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Status <span class="red"></span></label>
									{{ Form::text("status", "", $attributes = array("class"=>"form-control", "id"=>"status","ng-model"=>"checklistTask.status")) }}
								</div>
							</div>
						</div>
						<!--/row-->
						<div class="row">
							<div class="form-group col-md-12">
								<label class="control-label">Comments</label>
								{{ Form::textarea('comment', '', $attributes = array('class'=>'form-control', 'id' => 'comment', 'rows' => '3', 'ng-model'=>'checklistTask.comment')) }} 	
							</div>
						</div>
						<!--/row-->
					</div>
				</div>
				<div class="modal-footer">
					{{ Form::button("Cancel", $attributes = array("class"=>"btn btn-default","ng-click"=>"reset(checklistTaskFrm)", "data-dismiss" => "modal")) }}
					<button type="submit" class="btn btn-primary" id="details_form" ng-disabled="checklistTaskFrm.$invalid || isDisabled">Save</button>
					<span ng-show="showLoader">
	                	<img src="{{url('/')}}/assets/img/input-spinner.gif">
	                </span>
				</div>
			</form>
			<!-- END FORM-->
		</div>
	</div>
</div>
