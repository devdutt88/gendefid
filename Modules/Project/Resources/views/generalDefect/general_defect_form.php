<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
<div class="modal fade bd-example-modal-sm align-center" id="generalDefectModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">	
	<div class="modal-dialog">
		<div id="wait" class="loader" ng-show="loader"><img src='/img/demo_wait.gif' width="64" height="64" /><br>Loading..</div>
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">{{formname}} General Defect</h4>
			</div>
			<div class="modal-body">
				<div class="">
				<form name="multipleSelectForm" novalidate>
					<div class="row">
						<div class="col-md-12">			
							<div class="form-body">
								<div class="row">
									<div class="col-md-12">
										<label>Description</label>
										<textarea class="form-control" name="description" ng-model="defect.description" rows="3" cols="7" required></textarea>
										<span class="error-msg" ng-show="multipleSelectForm.description.$invalid && multipleSelectForm.description.$touched">Description is required</span>
									</div>
								</div>
								<br>
								<div class="row">
									<div class="col-md-12">
										<div class="form-group">
										    <div ng-class="{'has-error' : multipleSelectForm.multipleSelect.$invalid && multipleSelectForm.multipleSelect.$dirty, 'has-success' : !multipleSelectForm.multipleSelect.$invalid && multipleSelectForm.multipleSelect.$dirty}">
										        <label>Location</label>
										        <multiple-autocomplete ng-model="defect.selectedList" name="multipleSelect" required="true" object-property="name" suggestions-arr="optionsList">
										        </multiple-autocomplete>
										        
										       <span class="error-msg" ng-show="multipleSelectForm.multipleSelect.$invalid && multipleSelectForm.multipleSelect.$touched">Please select something from Locations</span>
										    </div>
										    <br/>
										</div>
									</div>
								</div>
								<!--/row-->
							</div>							
						</div>
					</div>
					</form>
				</div>
			</div>
			<input type="hidden" name="genral_defect_id">
			<div class="modal-footer">
				<button type="button" data-dismiss="modal" ng-click="cancel('multipleSelectForm')" class="btn default">Close</button>
				<button type="button" class="btn green" ng-disabled="multipleSelectForm.$invalid || isDisable" ng-click="save(label)">Submit</button>
			</div>
		</div>
	</div>
</div>
<!-- /.modal -->
<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->