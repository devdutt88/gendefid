<div class="modal fade" id="issuetoUserModal" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog">
	    <div class="modal-content">
			<!-- BEGIN FORM-->
			<div id="wait" class="loader" ng-show="loader"><img src='{{url("/")}}/img/demo_wait.gif' width="64" height="64" /><br>Loading..</div>
		 	<form id="issuetoUserFrm" name="issuetoUserFrm" class="horizontal-form" method="post" ng-submit="saveIssuetoUser('{{Crypt::encrypt($issue_to_id)}}')" nonvalidate>
				<div class="modal-header">
					{{ Form::button("×", $attributes = array("class"=>"close","ng-click"=>"reset(issuetoUserFrm)", "data-dismiss" => "modal", "aria-hidden"=>"true")) }}
					<h4 class="modal-title">@{{issuetoUser.form_title}}</h4>
				</div>
				<div class="modal-body">
					{{ csrf_field() }}										
					<input type="hidden" name="issueto_id" ng-model="issueto_id" ng-init="issueto_id='{{Crypt::encrypt($issue_to_id)}}'"/>
					{{ Form::hidden("issueto_user_id", "issuetoUser.id", $attributes = array("id" => "issueto_user_id","ng-model"=>"issuetoUser.id", "ng-init" => "issuetoUser.id=issuetoUser.id")) }}
					<div class="form-body">
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Name <span class="red">*</span></label>
									{{ Form::text("name", "", $attributes = array("class"=>"form-control", "id"=>"name","ng-model"=>"issuetoUser.name", "ng-required" => "true")) }}
									<span class="error" ng-show="issuetoUserFrm.name.$invalid && issuetoUserFrm.name.$touched">Name is required</span>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Email Address <span class="red">*</span></label>
									<input type="email" id="email" name="email" class="form-control" placeholder="" ng-model="issuetoUser.email" issueto-user-email-exists ng-model-options="{ updateOn: 'blur' }" ng-required="true"/>
								    <div ng-messages="issuetoUserFrm.email.$error" ng-if="issuetoUserFrm.email.$dirty" role="alert">
									    <span class="error" ng-message="required">Please enter a value for this field.</span>
									    <span class="error" ng-message="email">This field must be a valid email address.</span>
									    <span class="error" ng-message="issuetoUserEmailExists">Email address already exist for this Issuedto</span>
								  	</div>
								  	<span ng-show="showLoader">
					                	<img src="{{url('/')}}/assets/img/input-spinner.gif">
					                </span>
								</div>
							</div>
						</div>
						<!--/row-->
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Contact Number <span class="red">*</span></label>
									{{ Form::number("phone", "", $attributes = array("class"=>"form-control", "id"=>"phone", "ng-model"=>"issuetoUser.phone", "ng-minlength"=>"8", "ng-maxlength"=>"15", "ng-required"=>"true")) }}
									<div ng-show="issuetoUserFrm.phone.$invalid && issuetoUserFrm.phone.$touched">
										<span class="error" ng-show="issuetoUserFrm.phone.$error.required">Contact number is required.</span>
										<span class="error" ng-show="issuetoUserFrm.phone.$error.number">Please enter only numeric value.</span>
									    <span class="error" ng-show="issuetoUserFrm.phone.$error.minlength || issuetoUserFrm.phone.$error.maxlength">
									    Contact number must be in b/w 8 to 15.</span>
								    </div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Trade</label>
									{{ Form::text("trade", "", $attributes = array("class"=>"form-control", "id"=>"trade","ng-model"=>"issuetoUser.trade")) }}
								</div>								
							</div>
						</div>
						<!--/row-->
						<div class="row">
							<div class="form-group col-md-12">
								<label class="control-label">Tags</label>
								 <tags-input ng-model="issuetoUser.tag">
							        <auto-complete source="loadTags($query)"></auto-complete>
							    </tags-input>
							</div>
						</div>
						<!--/row-->
					</div>
				</div>
				<div class="modal-footer">
					{{ Form::button("Cancel", $attributes = array("class"=>"btn btn-default","ng-click"=>"reset(issuetoUserFrm)", "data-dismiss" => "modal")) }}
					<button type="submit" class="btn btn-primary" id="details_form" ng-disabled="issuetoUserFrm.$invalid">Save</button>
				</div>
			</form>
			<!-- END FORM-->
		</div>
	</div>
</div>
