<div class="modal fade" id="issuetoCmpModal" data-backdrop="static" data-keyboard="false">

	<div class="modal-dialog">
	    <div class="modal-content">
			<!-- BEGIN FORM-->
		 	
				<div class="modal-header">
					<span class="caption-subject bold font-green uppercase success">IssuedTo Companies List<span ng-show="showLoader"> {{ Form::button("×", $attributes = array("class"=>"close","ng-click"=>"reset(issuetoCmpModal)", "data-dismiss" => "modal", "aria-hidden"=>"true")) }}					
				</div>
			<div class="modal-body">						
				<div id="wait" class="loader" ng-show="loader"><img src='{{url("/")}}/img/demo_wait.gif' width="64" height="64" /><br>Loading..</div>

					<div class="form-body"  ng-init="companyIssueToList()">
					
						<div class="portlet light bordered tasks-widget">
                    <div class="portlet-title caption">
						 <div class="inputs pull-left">
		                    <div class="portlet-input input-inline input-small">
		                        <div class="input-icon right">                            
		                            <i class="icon-magnifier"></i>                            
		                            <input type="text" id="query" class="form-control input-circle" ng-model="query" onfocus="pxtrack.emit('counter', '1')" placeholder="search..."/>
		                        </div>
		                    </div>
		                </div>
	                		<label class="pull-right check_all">
								Select All 
								<input type="checkbox"  ng-model="selectAll"  ng-checked="checkVal" ng-click="checkAll()" >
							</label>
                    </div>
                    <flash-message duration="2000" show-close="true" on-dismiss="myCallback(flash)"></flash-message>
					<div class="portlet-body" >
		                <div class="">
							<div class="scroller portlet-height">
								<!-- START TASK LIST -->  

                        		<!-- <div class="task-title">
									<span class="task-title-sp list_status issueto_comp_name"></span>
									<span class="pull-right check_all">Select All 
									<input type="checkbox"  ng-model="selectAll"  ng-checked="checkVal" ng-click="checkAll()" ></span>
								</div> -->
		                        
		                        <ul class="task-list">
		                        	
		                            <li ng-repeat="list in plists | filter:query | orderBy: orderList" ng-class="getClass(list.status)">
		                            	<div class="task-checkbox">
											<!-- <input type="checkbox" name="status" class="liChild" ng-model="companymap.status" ng-click="associateCompany(list.id)" ng-if="list.status == 'linked'"> -->
										</div>
										<div class="task-title">
											<span class="task-title-sp list_status issueto_comp_name"> @{{list.name}}</span>
											<span class="label label-lg task-right pull-right list_status" ng-if="list.status == 'linked'"><i class="fa fa-check font-green"></i></span>
											<span class="label label-lg task-right pull-right list_status" ng-if="list.status == 'unlinked'"><input type="checkbox" value="@{{list.id}}" ng-click="addCheckedvalue(list.id,$event)" class="list_status_checkbox" ng-model="list.select"></span>
										</div>
		                            </li>
		                        </ul>                        
		                    </div>

		                </div>
		            </div>
				</div>	
			</div>
		</div>			

		<div class="modal-footer">
			<button class="btn btn-default" ng-click="reset(issuetoCmpModal)" data-dismiss="modal" type="button">Cancel</button>
			<button type="submit" class="btn btn-primary" id="details_form" ng-disabled="isDisabled" disabled="disabled" ng-click="addIssueto($event)">Save</button>
			<span ng-show="showLoader" class="ng-hide">
	        	<img src="http://www.gendefid.com/assets/img/input-spinner.gif">
	        </span>
		</div>

			<!-- END FORM-->
		</div>
	</div>
</div>
