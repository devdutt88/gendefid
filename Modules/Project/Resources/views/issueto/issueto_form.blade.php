<div class="modal fade" id="issuetoModal" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog modal-lg">
	    <div class="modal-content">
			<!-- BEGIN FORM-->
		 	<form id="issuetoInfo" name="issuetoInfo" class="horizontal-form" ng-submit="saveIssueto(label,id)" nonvalidate>
				<div class="modal-header">
					{{ Form::button("×", $attributes = array("class"=>"close","ng-click"=>"reset(issuetoInfo)", "data-dismiss" => "modal", "aria-hidden"=>"true")) }}
					<h4 class="modal-title">@{{form_title}}</h4>
				</div>
				<div class="modal-body">
				<div id="wait" class="loader" ng-show="loader"><img src='{{url("/")}}/img/demo_wait.gif' width="64" height="64" /><br>Loading..</div>
					{{ csrf_field() }}
					{{ Form::hidden("issueto_id", "issueto.id", $attributes = array("id" => "issueto_id","ng-model"=>"issueto.id", "ng-init" => "issueto.id=issueto.id")) }}
					<div class="form-body">
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Company Name <span class="red">*</span></label>
									{{ Form::text("name", "", $attributes = array("class"=>"form-control", "id"=>"name","ng-model"=>"issueto.name", "ng-required" => "true")) }}
									<span class="error" ng-show="issuetoInfo.name.$invalid && issuetoInfo.name.$touched">Company name is required</span>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Company Author <span class="red">*</span></label>
									{{ Form::text("auther", "", $attributes = array("class"=>"form-control", "id"=>"auther","ng-model"=>"issueto.auther", "ng-required" => "true")) }}
									<span class="error" ng-show="issuetoInfo.auther.$invalid && issuetoInfo.auther.$touched">Author name is required</span>
								</div>
							</div>
						</div>
						<!--/row-->
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Contact Number <span class="red">*</span></label>
									{{ Form::number("phone", "", $attributes = array("class"=>"form-control", "id"=>"phone", "ng-model"=>"issueto.phone", "ng-minlength"=>"8", "ng-maxlength"=>"15", "ng-required"=>"true")) }}
									<div ng-show="issuetoInfo.phone.$invalid && issuetoInfo.phone.$touched && issuetoInfo.phone.$dirty">
										<span class="error" ng-show="issuetoInfo.phone.$error.required">Contact number is required.</span>
										<span class="error" ng-show="issuetoInfo.phone.$error.number">Please enter only numeric value.</span>
									    <span class="error" ng-show="issuetoInfo.phone.$error.minlength || issuetoInfo.phone.$error.maxlength">number must be b/w 8 to 15 digits.</span>
								    </div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Email Address <span class="red">*</span></label>
									<?php /*{{ Form::email("email", "", $attributes = array("class"=>"form-control", "id"=>"email", "ng-model"=>"issueto.email", "ng-required"=>"true")) }}
									<div ng-show="issuetoInfo.email.$invalid && issuetoInfo.email.$touched">
								        <span class="error" ng-show="issuetoInfo.email.$error.required">Email is required.</span>
								        <span class="error" ng-show="issuetoInfo.email.$error.email">Invalid email address.</span>
							        </div> */ ?>
							        <input type="email" id="email" name="email" class="form-control" placeholder="" ng-model="issueto.email" issueto-email-exists ng-model-options="{ updateOn: 'blur' }" ng-required="true"/>
								    <div ng-messages="issuetoInfo.email.$error" ng-if="issuetoInfo.email.$dirty" role="alert">
									    <span class="error" ng-message="required">Please enter a value for this field.</span>
									    <span class="error" ng-message="email">This field must be a valid email address.</span>
									    <span class="error" ng-message="issuetoEmailExists">Email address already exist</span>
								  	</div>
								  	<span ng-show="showLoader">
					                	<img src="{{url('/')}}/assets/img/input-spinner.gif">
					                </span>
								</div>
							</div>
						</div>
						<!--/row-->
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Description</label>
									{{ Form::textarea('description', '', $attributes = array('class'=>'form-control', 'id' => 'description', 'rows' => '3', 'ng-model'=>'issueto.description')) }} 									
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Activity</label>
									{{ Form::select('activity', (object)$option_activity, null, ['class' => 'select2_category form-control','id'=>'activity', "ng-model"=>"issueto.activity"]) }}
								</div>
							</div>
						</div>
						<!--/row-->
					</div>
				</div>
				<div class="modal-footer">

					{{ Form::button("Cancel", $attributes = array("class"=>"btn btn-default","ng-click"=>"reset(issuetoInfo)", "data-dismiss"=>"modal")) }}
					<button type="submit" class="btn btn-primary" id="details_form" ng-disabled="issuetoInfo.$invalid || isDisable">Save</button>					
				</div>
			</form>
			<!-- END FORM-->
		</div>
	</div>
</div>
