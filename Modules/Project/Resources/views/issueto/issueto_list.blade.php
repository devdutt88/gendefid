@extends('frontend::layouts.master')

@section('content')

<!-- <link href="{{url('/')}}/assets/css/tasks.css" rel="stylesheet" type="text/css"/> -->
<div class="page-container" ng-app="projectIssuetoModule" ng-controller="projectIssuetoController">
<input type="hidden" name="project_id" ng-model="project_id" ng-init="project_id='{{$id}}'"/>

<!-- BEGIN PAGE HEAD -->
<div class="page-head">
	<div class="container">
		<!-- BEGIN PAGE TITLE -->
		@include('frontend::layouts.company_setting')		
		<!-- END PAGE TITLE -->
	</div>
</div>	
	<!-- BEGIN PAGE CONTENT -->
	<div class="page-content">
		<div class="container">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->			
			<!-- /.modal -->
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE BREADCRUMB -->
			@include('project::partials.breadcrumb')
			<!-- END PAGE BREADCRUMB -->
			<!-- BEGIN PAGE CONTENT INNER -->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN TODO SIDEBAR -->					
					@include('project::layouts.project_sidebar')
					<!-- END TODO SIDEBAR -->
					<!-- BEGIN TODO CONTENT -->
					<div class="todo-content">
					<!-- Alert Section -->
					@if(Session::has('flash_alert_notice'))
				        <div class="alert alert-success alert-dismissable cst-alert cst-normal-alert">
				          	<!-- <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button> -->
				          	<i class="icon fa fa-check"></i>  
				           	{{ Session::get('flash_alert_notice') }} 
				        </div>
				   	@endif
				<!-- End Alert Section -->
						<div class="portlet light">
						<!-- issue to list -->
						
						<!-- issue to list -->
							<!-- PROJECT HEAD -->
							<div class="portlet-title">
								<div class="caption">
									<i class="icon-microphone font-dark hide"></i>	
									<span class="caption-subject bold font-dark uppercase"> IssuedTo </span>
								</div>
								<div class="caption pull-right">
		                            <i class="icon-microphone font-dark hide"></i>	
		                            <!-- <span class="caption-subject bold font-dark uppercase"> Project Detail </span> -->
		                            <button class="btn btn-circle btn-sm blue" id="" ng-click="showIssueto('cmpIsto')"> <i class="fa fa-plus"></i> Inherit Issue to </button>
		                            <button class="btn btn-circle btn-sm green cst_btn" id="" ng-click="showIssueto('add')"> <i class="fa fa-plus"></i> Add New </button>
		                        </div>							
							</div>
							<!-- end PROJECT HEAD -->
							<div class="portlet-body">							
								<!-- start form -->
								<input type="hidden" name="issue_to_id" id="issue_to_id" ng-model="issue_to_id" value="">								
								<div class="portlet-body">
									<div class="row">
										<div class="col-md-12">											
											@include('project::issueto.issueto_form')
											@include('project::issueto.issuetoCompany')
											<table class="table table-striped table-bordered table-hover" id="sample_1">
												<thead>
													<tr>
														<th> Name </th>
														<th> Author </th>
														<th> Email </th>
														<th> Phone </th>
														<th> Activity </th>
														<th> Action </th>
													</tr>
												</thead>
											</table>
										</div>										
									</div>
								</div>
								<!-- end form -->
							</div>
						</div>
					</div>
				
					<!-- END TODO CONTENT -->
				</div>
			</div>
			<!-- END PAGE CONTENT INNER -->
		</div>
	</div>
	<!-- END PAGE CONTENT -->
</div>
@stop