<div class="todo-sidebar">
	<div class="portlet light">
		<div class="portlet-title">
			<div class="caption" data-toggle="collapse" data-target=".todo-project-list-content">
				<span class="caption-subject font-green-sharp bold uppercase">{{$project->name}} </span>
				<span class="caption-helper visible-sm-inline-block visible-xs-inline-block">click to view project list</span>
			</div>
			<div class="actions">
				<div class="btn-group">
					<!-- <a class="btn green-haze btn-circle btn-sm todo-projects-config" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
					<i class="icon-settings"></i> &nbsp; <i class="fa fa-angle-down"></i>
					</a> -->
					<!-- <ul class="dropdown-menu pull-right">
						<li>
							<a href="javascript:;">
							<i class="i"></i> New Project </a>
						</li>
						<li class="divider">
						</li>
						<li>
							<a href="javascript:;">
							Pending <span class="badge badge-danger">
							4 </span>
							</a>
						</li>
						<li>
							<a href="javascript:;">
							Completed <span class="badge badge-success">
							12 </span>
							</a>
						</li>
						<li>
							<a href="javascript:;">
							Overdue <span class="badge badge-warning">
							9 </span>
							</a>
						</li>
						<li class="divider">
						</li>
						<li>
							<a href="javascript:;">
							<i class="i"></i> Archived Projects </a>
						</li>
					</ul> -->
				</div>
			</div>
		</div>
		<div class="portlet-body todo-project-list-content">
			<div class="todo-project-list">
				<ul class="nav nav-pills nav-stacked">
				<li class="{{ (Request::segment(2) == 'config' ) ? 'active' : '' }}" >
						<a href="{{url('/')}}/project/config/{{$id}}"> Project Details </a>
					</li>
					<li class="{{ (Request::segment(2) == 'location' ) ? 'active' : '' }}" >
						<a href="{{url('/')}}/project/location/{{$id}}"> Project Location </a>
					</li>
					<li class="{{ (Request::segment(2) == 'issueto' ) ? 'active' : '' }}" >
						<a href="{{url('/')}}/project/issueto/{{$id}}"> Issuedto </a>
					</li>

					<li class="{{ (Request::segment(2) == 'associate-users' ) ? 'active' : '' }}" >
						<a href="{{url('/')}}/project/associate-users/{{$id}}"> Associate User </a>
					</li>
					<li class="{{ (Request::segment(2) == 'general-defect' ) ? 'active' : '' }}" >
						<a href="{{url('/')}}/project/general-defect/{{$id}}"> General Defect </a>
					</li>					
				</ul>
			</div>
		</div>
	</div>
</div>
