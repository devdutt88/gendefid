<div class="alert alert-success alert-dismissable" id="loc_msg2" style="display: none;">
    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
    <i class="icon fa fa-check"></i>      
</div>
@if(Session::has('flash_alert_notice'))
    <div class="alert alert-success alert-dismissable">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <i class="icon fa fa-check"></i>  
        {{ Session::get('flash_alert_notice') }} 
    </div>
@endif

<div class="row">
    <div class="col-lg-12 col-xs-12 col-sm-12">
        <div class="portlet light bordered neighbor_div">
        <!-- PROJECT HEAD -->
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-microphone font-dark hide"></i>  
                    <span class="caption-subject bold font-dark uppercase"> Project Location </span>
                </div>
            </div>
            <!-- end PROJECT HEAD -->
            <!-- <div class="portlet-title tabbable-line">
                <div class="caption col-md-7">
                    <i class="icon-screen-desktop font-green-sharp"></i>
                    <span class="caption-subject font-green-sharp bold uppercase">Company name</span>
                </div>
  
                 <div class="caption col-md-6 text-right">
                    <i class="icon-bubbles font-dark hide"></i>
                    <span class="caption-subject font-dark bold uppercase">Project : </span>
                </div>   -->                                     
            <!--</div> -->
            <div class="portlet-body">            
                <div class="tab-content">
                    <div class="tab-pane active" id="portlet_comments_1">
                        <!-- BEGIN: Comments -->
                        <div class="mt-comments">
                            <div class="mt-comment">
                                <!-- Tree -->
                                @if(Session::has('flash_alert_notice_location'))
                                    <div class="alert alert-success alert-dismissable">
                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                        <i class="icon fa fa-check"></i>  
                                        {{ Session::get('flash_alert_notice_location') }} 
                                    </div>
                                @endif
                                <form method="post" action="{{url('/')}}/import-csv" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <div class="row">
                                        <div class="col-md-6">
                                        <label class="control-label">Please select csv file</label>
                                            <div class="form-group">                                                           
                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                <div class="input-group input-large">
                                                    <div class="form-control uneditable-input span3" data-trigger="fileinput">
                                                        <i class="fa fa-file fileinput-exists"></i>&nbsp; <span class="fileinput-filename">
                                                        </span>
                                                    </div>
                                                    <span class="input-group-addon btn default btn-file">
                                                    <span class="fileinput-new">
                                                    Csv file </span>
                                                    <span class="fileinput-exists">
                                                    Change </span>
                                                    <input type="file" name="locationcsv" id="locationcsv">
                                                    </span>
                                                    <a href="javascript:;" id="remove" class="input-group-addon btn red fileinput-exists" data-dismiss="fileinput">
                                                    Remove </a>
                                                </div>
                                                @if ($errors->has('locationcsv'))
                                                    <span class=error>The type field is required</span>
                                                @elseif($errors->has('error'))
                                                    <span class=error>{{ $errors->first('error') }}</span>
                                                @endif
                                            </div>
                                                <input type="hidden" name="project_id" id="project_id1" value="{{$id}}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">                                            
                                                <button type="submit" class="btn green"><i class="icon-cloud-upload icons"> </i> Import CSV</button>
                                            </div>
                                        </div>
                                        <!-- <div class="col-md-4 pull-right">
                                            <div class="form-group">                                            
                                                 <a href="{{url('superadmin/projectConfiguration/location-sample-csv')}}" class="btn"><i class="icon-cloud-download icons"> </i>Download csv sample file</a>
                                            </div>
                                        </div> -->
                                    </div>  
                                </form>                                                      
                                <!-- End Tree -->                                
                            </div>           
                        </div>
                        <!-- END: Comments -->
                    </div>
                   
                </div>
                <!-- <div class="desc"> -->
                    <div class="form-group">                                            
                         <a href="{{url('location-sample-csv')}}" class="desc"><i class="icon-cloud-download icons"> </i> Download csv sample file</a>
                    </div>
                <!-- </div> -->
            </div>                
        </div>        
    
        <div class="portlet light bordered">  
            <div class="portlet-title tabbable-line">
                <div class="row">
                    <div class="caption col-md-6">
                        <div class="form-group">                                
                            <a  href="javascript:;" class="add_location btn btn-default cst_btn" pid="" pname="">Add Location</a>                            
                        </div>
                    </div>
                    <div class="col-md-6 text-right">
                        <a href="{{url('/')}}/export-csv/{{$id}}" class="btn  green"><i class="icon-cloud-download icons"></i> Export CSV</a>
                    </div>
                </div> 
            </div>

            <div class="portlet-body">
                <div class="tab-content">
                    <div class="tab-pane active" id="portlet_comments_1">                        
                        <!-- BEGIN: Comments -->
                        <div class="mt-comments">
                            <div class="mt-comment">
                                <!-- Tree -->
                                </div>                                   
                                <div class="parent_tree location_box">                            
                                    {!! $tree !!}
                                </div>                                
                                <!-- End Tree -->                                
                            </div>           
                        </div>
                        <!-- END: Comments -->
                    </div>
                   
                </div>
        </div>
    </div>        
</div>
@include('project::location.popup.add')
@include('project::location.popup.edit')