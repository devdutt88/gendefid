@extends('frontend::layouts.master')

@section('content')
<div class="page-container" ng-app="ProjectApp" ng-controller="projectController">
<!-- BEGIN PAGE HEAD -->
<div class="page-head">
	<div class="container">
		<!-- BEGIN PAGE TITLE -->
		@include('frontend::layouts.company_setting')		
		<!-- END PAGE TITLE -->
	</div>
</div>
	<!-- BEGIN PAGE CONTENT -->
	<!-- BEGIN PAGE CONTENT -->
	<div class="page-content">
		<div class="container">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->			
			<!-- /.modal -->
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE BREADCRUMB -->
			@include('project::partials.breadcrumb')
			<!-- END PAGE BREADCRUMB -->
			<!-- BEGIN PAGE CONTENT INNER -->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN TODO SIDEBAR -->					
					@include('project::layouts.project_sidebar')
					<!-- END TODO SIDEBAR -->
					<!-- BEGIN TODO CONTENT -->
					<div class="todo-content">
					<!-- Alert Section -->
					@if(Session::has('flash_alert_notice'))
				        <div class="alert alert-success alert-dismissable cst-alert cst-normal-alert">
				          	<!-- <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button> -->
				          	<i class="icon fa fa-check"></i>  
				           	{{ Session::get('flash_alert_notice') }} 
				        </div>
				   	@endif
				   	@if(Session::has('flash_alert_delete_notice'))
				        <div class="alert alert-success alert-dismissable cst-alert cst-normal-alert">
				          	<!-- <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button> -->
				          	<i class="icon fa fa-check"></i>
				           	{{ Session::get('flash_alert_delete_notice') }}
				        </div>
				   	@endif
				<!-- End Alert Section -->
						<!-- <div class="portlet light"> -->							
							<!-- <div class="portlet-body"> -->
								<!-- start form -->
								@include('project::location.form')
								<!-- end form -->
							<!-- </div> -->
						<!-- </div> -->
					</div>
					<!-- END TODO CONTENT -->
				</div>
			</div>
			<!-- END PAGE CONTENT INNER -->
		</div>
	</div>
	<!-- END PAGE CONTENT -->
</div>                        
@stop
<!-- END CONTAINER -->