<div class="modal fade" id="editModal" close="cancel()">
	<div class="modal-dialog locationModel">
	    <div class="modal-content">
			<!-- BEGIN FORM-->
		 	<form id="editLocation" name="sublocation" class="horizontal-form" novalidate="">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					<h4 class="modal-title">Edit Location</h4>					
				</div>
				<div class="modal-body">
					{{ csrf_field() }}					
					<div class="form-body">
							<!--/row for buisness-->
					<!-- <div class="row text-center hide alert-success" id="loc_msg2">
						
					</div>	 -->					
						<div class="row">
							<div class="col-md-3">
								<div class="form-group">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Location Name</label>
									<input type="text" id="location_name" required="required" name="location_name" class="form-control"  placeholder="">
									<input type="hidden" id="location_id" required="required" name="location_id" class="form-control">
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group">
								</div>
							</div>							
						</div>
					<!--/row-->
					</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal" ng-click="cancel()">Cancel</button>
					<button type="submit" class="btn blue"><i class="fa fa-check"></i> Save</button>
				</div>
			</div>
		</form>
	</div>
</div>
</div>


