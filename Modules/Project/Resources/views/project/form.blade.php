<!-- BEGIN FORM-->
<form id="projForm" name="projForm" class="horizontal-form" ng-submit="submitForm('{{$data['label']}}','{{$data['id']}}')" novalidate>	
	<div class="modal-body">
		{{ csrf_field() }}
		<input type="hidden" name="project_hid" ng-model="project_hid" value="{{$data['id']}}">
		<div class="form-body">
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">Project Name <span class="red">*</span></label>
					<!-- 	<input type="text" id="projname" name="projname" class="form-control" placeholder="" ng-minlength="3" ng-model="project.projname" ng-required="true"/> --> 

						<input type="text" id="projname" name="projname" class="form-control" placeholder="" ng-minlength="3" ng-model="project.projname" project-exists ng-model-options="{ updateOn: 'blur' }" ng-required="true"/>
						<span class="error" ng-show="projForm.projname.$error.minlength">Project name should be 3 characters!</span>
					    <div ng-messages="projForm.projname.$error"  ng-if="projForm.projname.$error"  role="alert">
						    <span class="error" ng-message="required" ng-if="projForm.projname.$touched">Please enter a value for this field.</span>						    
						    <span class="error" ng-message="projectExists">Project already exists</span>
					  	</div>
					</div>
				</div>
				<!--/span-->
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">Project Type <span class="red">*</span></label>
						<select name="projecttype" class="form-control" ng-model="project.projecttype" ng-required="true" />
							<option value="">Select any one</option>
							<option ng-repeat="x in types" value="@{{x}}">@{{x}}</option>
						</select>
						<span class="error" ng-show="projForm.projecttype.$invalid && projForm.projecttype.$touched">Project Type is required.</span>
					</div>
				</div> 
				<!-- <div class="col-md-6">
					<div class="form-group">
						<label class="control-label">Project Code <span class="red">*</span></label>
						<input type="text" id="projcode" name="projcode" class="form-control" placeholder="" ng-model="project.projcode" ng-required="true"/>
						<span class="error" ng-show="projForm.projcode.$invalid && projForm.projcode.$touched">Project code is required.</span>
					</div>
				</div> -->
				<!--/span-->
			</div><!-- End Row -->			
			<div class="row">			    
				<!--/span-->
				<div class="col-md-12">
					<div class="form-group">						
						<label class="control-label">Description </label>
						<textarea id="description" name="description" class="form-control" placeholder="" ng-model="project.description" , ng-trim='false' maxlength=120 rows="2"/></textarea>
						<span>@{{120 - project.description.length}} Character Left</span> 	
					</div>
				</div> 
				<!--/span-->						
				<!--/span-->
				
				<!--/span-->
			</div>
			<!--/row-->
			<hr>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label"><u><i><strong>Address Details:</strong></i></u></label>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">Address 1 <span class="red">*</span></label>
						<input type="text" id="address1" name="address1" class="form-control" placeholder="" ng-model="project.address1" ng-required="true"/>
						<span class="error" ng-show="projForm.address1.$invalid && projForm.address1.$touched">Address1 is required.</span>
					</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">Address 2</label>
						<input type="text" id="address2" name="address2" class="form-control" placeholder="" ng-model="project.address2" />						
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">Suburb <span class="red">*</span></label>
						<input type="text" id="suburb" name="suburb" class="form-control" placeholder="" ng-model="project.suburb" ng-required="true">
						<span class="error" ng-show="projForm.suburb.$invalid && projForm.suburb.$touched">Suburb is required.</span>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">Post Code <span class="red">*</span></label>
						<input type="text" id="postal_code" name="postal_code" class="form-control" placeholder="" ng-model="project.postal_code" ng-required="true">
						<span class="error" ng-show="projForm.postal_code.$invalid && projForm.postal_code.$touched">Post Code is required.</span>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">State <span class="red">*</span></label>						
						<select name="state" class="form-control" ng-model="project.state" ng-required="true" />
							<option value="">Select any one</option>
							<option ng-repeat="x in states" value="@{{x}}">@{{x}}</option>
						</select>
						<span class="error" ng-show="projForm.state.$invalid && projForm.state.$touched">State is required.</span>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">Country <span class="red">*</span></label>
						<input type="text" id="country" name="country" class="form-control" placeholder="" ng-model="project.country" ng-required="true" ng-readonly="true">
						<span class="error" ng-show="projForm.country.$invalid && projForm.country.$touched">Country is required.</span>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="modal-footer">
		<button type="button" class="btn btn-default" data-dismiss="modal" onclick="history.go(-1); return false;">Cancel</button>
		<button type="submit" class="btn btn-primary" id="profile_form" ng-disabled="projForm.$invalid  || isaddPermissionDisabled">@{{label_button}}</button>
		<span ng-show="showLoader"><img src="{{url('/')}}/assets/img/input-spinner.gif"></span>
	</div>
</form>
<!-- END FORM-->