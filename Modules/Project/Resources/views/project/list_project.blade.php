@extends('frontend::layouts.master')

@section('content')
<div class="page-container" ng-app="ProjectApp" ng-controller="projectController">
<!-- BEGIN PAGE HEAD -->
<div class="page-head">
	<div class="container">
		<!-- BEGIN PAGE TITLE -->
		@include('frontend::layouts.company_setting')		
		<!-- END PAGE TITLE -->
	</div>
</div>
	<!-- BEGIN PAGE CONTENT -->
	<div class="page-content">
		<div class="container">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
							<h4 class="modal-title">Modal title</h4>
						</div>
						<div class="modal-body">
							 Widget settings form goes here
						</div>
						<div class="modal-footer">
							<button type="button" class="btn blue">Save changes</button>
							<button type="button" class="btn default" data-dismiss="modal">Close</button>
						</div>
					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			</div>
			<!-- /.modal -->
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE BREADCRUMB -->
			@include('project::partials.breadcrumb')
			<!-- END PAGE BREADCRUMB -->
			<!-- BEGIN PAGE CONTENT INNER -->
			<div class="row">
				<div class="col-md-12">					
					<!-- BEGIN TODO CONTENT -->
					<div class="todo-content">
						<div class="portlet light">
							<!-- PROJECT HEAD -->
							<div class="portlet-title">
								<div class="caption">
									<i class="icon-microphone font-dark hide"></i>	
									<span class="caption-subject bold font-dark uppercase"> Project List </span>
								</div>
								<div class="actions">
									<i class="icon-bar-chart font-green-sharp hide"></i>
									<!-- <span class="caption-helper">Company:</span> &nbsp; <span class="caption-subject font-green-sharp bold uppercase">Tune Website</span> -->
									<a href="{{url('/')}}/project/add/" class="btn btn-circle green cst_btn_text cst_btn">
										<i class="fa fa-plus"></i>
										<span class="hidden-480">
										Add New </span>
									</a>
								</div>								
							</div>
							<!-- end PROJECT HEAD -->
							<div class="portlet-body">
								@if(Session::has('flash_alert_notice'))
			                        <div class="alert alert-success alert-dismissable cst-alert cst-list-alert">
			                          	<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
			                          	<i class="icon fa fa-check"></i>  
			                           	{{ Session::get('flash_alert_notice') }} 
			                        </div>
		                       	@endif
								<div class="row">
									<div class="col-md-12 col-sm-8">
										<!-- <div class="portlet-body flip-scroll">
										<table class="table table-bordered table-striped table-condensed flip-content" datatable="ng" dt-options="vm.dtOptions"> -->
										<div class="table-responsive">
										<table class="table table-striped table-bordered table-hover" datatable="ng" dt-options="vm.dtOptions">
										    <thead>
										      <tr><th>Name</th><th>Type</th><th>Author</th><th>Role</th><th>Created</th><th>Action</th></tr>
										    </thead>
										    <tbody>
												<tr ng-repeat="proj in projectList">
													<td>@{{proj.name}}</td>
													<td>@{{proj.type}}</td>
													<td>@{{proj.created_by}}</td>
													<td>@{{proj.created_at}}</td>
													<td>@{{proj.modified_at}}</td>
													<td width="100px">
													<div class="btn-group">
													    <a href="{{url('/')}}/project/edit/@{{proj.id}}" type="button" class="btn btn-warning btn-xs" ng-click="edit(proj.id);"><i class="fa fa-pencil-square-o"></i></a>  
													    <a href="javascript:;" type="button" class="btn btn-danger btn-xs" ng-click="delete(proj.id);"><i class="fa fa-trash-o"></i></a>
													    <a href="{{url('/')}}/project/config/@{{proj.id}}" type="button" class="btn btn-success btn-xs"><i class="fa fa-star-o"></i></a>													    
													</div>
												</td>
												</tr>
										    </tbody>
										  </table>
										  </div>
									</div>
								</div>								
								<!-- </div> -->
						<!-- </div> -->
					</div>
					<!-- END TODO CONTENT -->
				</div>
			</div>
			<!-- END PAGE CONTENT INNER -->
		</div>
	</div>
	<!-- END PAGE CONTENT -->
</div>                        
@stop