@extends('frontend::layouts.master')

@section('content')
<!-- <link href="{{url('/')}}/assets/css/tasks.css" rel="stylesheet" type="text/css"/> -->
<div class="page-container" ng-app="ProjectApp" ng-controller="projectController">
<!-- BEGIN PAGE HEAD -->
<div class="page-head">
	<div class="container">
		<!-- BEGIN PAGE TITLE -->
		@include('frontend::layouts.company_setting')
		
		<!-- END PAGE TITLE -->
	</div>
</div>
	<!-- BEGIN PAGE CONTENT -->
	<div class="page-content">
		<div class="container">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
							<h4 class="modal-title">Modal title</h4>
						</div>
						<div class="modal-body">
							 Widget settings form goes here
						</div>
						<div class="modal-footer">
							<button type="button" class="btn blue">Save changes</button>
							<button type="button" class="btn default" data-dismiss="modal">Close</button>
						</div>
					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			</div>
			<!-- /.modal -->
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE BREADCRUMB -->
			@include('project::partials.breadcrumb')
			<!-- END PAGE BREADCRUMB -->
			<!-- BEGIN PAGE CONTENT INNER -->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN TODO SIDEBAR -->					
					@include('project::layouts.project_sidebar')
					<!-- END TODO SIDEBAR -->
					<!-- BEGIN TODO CONTENT -->
					<div class="todo-content">
					<!-- Alert Section -->
					@if(Session::has('flash_alert_notice'))
				        <div class="alert alert-success alert-dismissable cst-alert cst-normal-alert">
				          	<!-- <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button> -->
				          	<i class="icon fa fa-check"></i>  
				           	{{ Session::get('flash_alert_notice') }} 
				        </div>
				   	@endif
				<!-- End Alert Section -->
						<div class="portlet light">
							<!-- PROJECT HEAD -->
							<div class="portlet-title">
								<div class="caption">
		                            <i class="icon-microphone font-dark hide"></i>
		                            <span class="caption-subject bold font-dark uppercase"> Project Detail </span>
		                            <!-- <span class="caption-helper">..{{$project->type}}</span> -->
		                        </div>							
							</div>
							<!-- end PROJECT HEAD -->
							<div class="portlet-body">
								<!-- start form -->
								<div class="portlet-body">
						<div class="row">
							<div class="col-md-12">
								<div class="col-sm-4 text-center cst-logo-section">
									<form id="projectLogoFrm" runat="server">
										<div class="form-group">
											@if(!empty($project->logo))
												<img id="logo_image"  width="200" height="200"  src="{{url('/')}}/uploads/project/{{$project->logo}}" alt="project image" />
											@else
												<img id="logo_image"  width="200" height="200"  src="{{url('/')}}/uploads/project_icon.png" alt="project image" />
											@endif
										</div>
										<div class="form-group">
											<span class="btn btn-circle cst-yellow fileinput-button cst_btn">
							                    @if(!empty($project->logo))
							                    	<span>Change Logo</span>
							                    @else
							                    	<span>Add Logo</span>
							                    @endif
												 	<input type='file' name="logo" id="projectImg"/>
											</span>
										</div>
										<div class="form-group">	
											<button type="submit" id="upload_btn" style="display:none;" class="btn btn-success btn-circle 	start" ng-click="update_details('{{$id}}','logo','{{$project->name}}')"><i class="fa fa-upload"></i> Start upload</button>
							                
							                <span ng-show="showLogoLoader">
							                	<img src="{{url('/')}}/assets/img/ajax-loader.gif">
							                </span>
										</div>	
									</form>
								</div>
								<div class="col-sm-8">
									<div class="row">
										<label class="form-control-static col-xs-4"><strong>Address :</strong></label>
										<div class="form-control-static col-xs-8">
												 {{$project->address1}}
												 {{$project->address2}}
										</div>
									</div>
									<div class="row">
										<label class="form-control-static col-xs-4"><strong>Suburb :</strong></label>
										<div class="form-control-static col-xs-8">
												 {{$project->suburb}}
										</div>
									</div>
									<div class="row">
										<label class="form-control-static col-xs-4"><strong>Postal Code :</strong></label>
										<div class="form-control-static col-xs-8">
												{{$project->postal_code}}
										</div>
									</div>
									<div class="row">
										<label class="form-control-static col-xs-4"><strong>State :</strong></label>
										<div class="form-control-static col-xs-8">
												{{$project->state}}
										</div>
									</div>
									<div class="row">
										<label class="form-control-static col-xs-4"><strong>Country :</strong></label>
										<div class="form-control-static col-xs-8">
												{{$project->country}}
										</div>
									</div>
									<!-- <div class="row">
										<label class="form-control-static col-xs-4"><strong>GPS Coordinates :</strong></label>
										<div class="form-control-static col-xs-8">
												 {{$project->geo_code}}
										</div>
									</div> -->
								</div>
							</div>
						</div>
						<hr>
						<div class="row">
								<!-- <div class="form-group">
									<label class="control-label col-md-5"><strong>Project Type :</strong></label>
									<div class="col-md-7">
										<p class="form-control-static">
											 {{$project->type}}
										</p>
									</div>
								</div> -->
							<label class="col-xs-12"><strong>Description</strong></label>
							<div class="col-xs-12">
									 {{$project->description}}
							</div> 
						</div>
					</div>
								<!-- end form -->
							</div>
						</div>
					</div>
					<!-- END TODO CONTENT -->
				</div>
			</div>
			<!-- END PAGE CONTENT INNER -->
		</div>
	</div>
	<!-- END PAGE CONTENT -->
</div>
@stop