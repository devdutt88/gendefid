<?php

namespace Modules\Projectconfiguration\Http\Controllers;

use App\Models\projectConfiguration\Location;
use App\Models\superadmin\Project;
use App\Models\superadmin\CompanyProjectMapping;
use App\Models\superadmin\Company;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Redirect;
use Modules\Projectconfiguration\Http\Requests\StoreLocationCsv;
use App\Http\Requests;
use mjanssen\BreadcrumbsBundle\Breadcrumbs;
use Illuminate\Support\Facades\Input;
use Helpers as Helper;
use Auth;
use Form;
use URL;
use Yajra\Datatables\Facades\Datatables as Datatables;
use Illuminate\Support\MessageBag;
use Goodby\CSV\Import\Standard\Lexer;
use Goodby\CSV\Import\Standard\Interpreter;
use Goodby\CSV\Import\Standard\LexerConfig;
use Goodby\CSV\Export\Standard\Exporter;
use Goodby\CSV\Export\Standard\ExporterConfig;
use Goodby\CSV\Export\Standard\Collection\CallbackCollection;
use Excel;
use Crypt;

class LocationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {                        
        $id = \Session::get('projectConfig');             
        $data['scripts'] = array('bootstrap-fileinput','jquery.contextMenu','jquery.ui.position','location','jquery-treeview','tree','angularjs/angular.min','angularjs/controllers/locationcontroller');
        $project = Project::select('id','name')->where('is_deleted','=',0)->where('id', $id)->get();                    
        $allProject = $this->getAllProjectData();        
        $loc_data = array();
        if (!empty($id)) {
            $Categorys = Location::where('is_deleted','=',0)->where('project_id','=',$id)->where('parent_id','=',0)->get();
            if (!empty($Categorys)) {             
                $tree='<ul id="browser" class="filetree"><li class="location tree-view"></li>';
                foreach ($Categorys as $Category) {                   
                    $tree .='<li name="'.$Category->label.'"  id='.$Category->id.' class="location tree-view closed"><a class="tree-name">'.$Category->label.'</a>';
                    if (count($Category->childs)) {
                    $tree .=$this->childView($Category);
                    } else {
                        $tree .="</li>";
                    }
                }
                $tree .='<ul>';
            }               
        }
        $cmp_id = \Session::get('cmp_id');
        $cmp_name = Company::select('id','name')->where('is_deleted','=',0)->where('id', $cmp_id)->get();
        Breadcrumbs::addBreadcrumb('Project Location',  url('superadmin/project/location'));        
        $page_data = array('breadcrumbs' => Breadcrumbs::generate());        
        return view('projectconfiguration::location.location',compact('data','tree','allProject','cmp_name','cmp_id','id'))->with($page_data)->with('project',$project)->with('location',$tree);
    }

    /**
     * Fetch Child location of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function childView($Category){
        $html ='<ul>';
        foreach ($Category->childs as $arr) {
            if (count($arr->childs)) {
                $html .='<li  name="'.$arr->label.'" id='.$arr->id.' class="location tree-view closed"><a class="tree-name">'.$arr->label.'</a>';                  
                $html.= $this->childView($arr);
            } else {
                $html .='<li name="'.$arr->label.'"  id='.$arr->id.' class="location tree-view"><a class="tree-name">'.$arr->label.'</a>';                                 
                $html .="</li>";
            }                                   
        }
        $html .="</ul>";
        return $html;
    }    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {                
        $input = Input::except('_token');                
        $pid = Input::get('project_id'); 
        $cmp_id = Input::get('company_id');              
        if ($input['case'] == "parent") {
            $insertInLocation = array(
                "label"  =>  $input['level1'],
                "company_id"  =>  $cmp_id,
                "project_id" =>  $pid,  
                "parent_id" => 0,
                "order_id" => 0,
                "is_deleted" => 0,
                "created_at" => date('Y-m-d h:i:s'),
                "created_by" => 0,
                "modified_by" => 0,
                "resource_type" => 'Webserver',            

            );        
            $save = Location::create($insertInLocation);
            if ($save) {       
                $id = $save->id;        
                $html = '<li name="'.$input['level1'].'" id="'.$id.'" class="location tree-view closed"><a class="tree-name">'.$input['level1'].'</a></li>';
                $msg = "Location saved successfully";
                $arr = array('html'=>$html,'msg'=>$msg);
                return json_encode($arr);
            } else {
                $html = "";
                $msg = "Location save failed";
                $arr = array('html'=>$html,'msg'=>$msg);
                return json_encode($arr);
            }
        } else if($input['case'] == "child") {            
            $insertInLocation = array(
                "label"  =>  $input['level1'],
                "company_id"  =>  $cmp_id,
                "project_id" =>  $pid,  
                "parent_id" => $input['parent_id'],
                "order_id" => 0,
                "is_deleted" => 0,
                "created_at" => date('Y-m-d h:i:s'),
                "created_by" => 0,
                "modified_by" => 0,
                "resource_type" => 'Webserver',            
            );        
            $save = Location::create($insertInLocation);
            if ($save) {
                $id = $save->id;        
                $html = '<li name="'.$input['level1'].'" id="'.$id.'" class="location tree-view closed"><a class="tree-name">'.$input['level1'].'</a></li>';
                $msg = "Location saved successfully";
                $arr = array('html'=>$html,'msg'=>$msg);
                return json_encode($arr);                
            } else {
                $html = "";
                $msg = "Location save failed";
                $arr = array('html'=>$html,'msg'=>$msg);
                return json_encode($arr);
            }
        }
    }

    /**
     * Import location from csv and save resource in storage.
     *
     * @param  \App\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function importCsv(StoreLocationCsv $request,Location $location)
    {        
        $pid = Input::get('project_id');
        $cmp_id = Input::get('cmp_id');
        $file = Input::file('locationcsv');
        $file_name = $file->getClientOriginalName();       
        $extension = $file->getClientOriginalExtension(); // getting image extension
        $temp_path = public_path('uploads/');        
        $uploaded_at = $file->move($temp_path, $file_name);
        $err = '';        
        if (in_array($extension, array('csv'))) {
            $location = array();
            $config = new LexerConfig();
            $config->setDelimiter("\t");
            $lexer = new Lexer($config);
            $interpreter = new Interpreter();
            $interpreter->addObserver(function(array $row) use (&$location) {
                $location[] = $row[0];
            });
            $lexer->parse($temp_path."/".$file_name, $interpreter);            
            $i = 0;
            $data = array();
            $header = array();
            foreach ($location as $key => $loc_arr1) {
                if($i!=0){
                    $data[] = explode(",", $loc_arr1);
                } else {
                    $header[] = explode(",", $loc_arr1);
                }
                $i++;
            }             
            
            if (!empty($header[0][0] == "Location")) {
                foreach ($data as $key => $loc_arr2) {                  
                    foreach ($loc_arr2 as $key => $value) {                                        
                        if ($key == 0) {
                            $loc_parent = Location::firstOrNew(array('label' => $value,'parent_id'=>0,'is_deleted'=>0));
                            $loc_parent->project_id = $pid;
                            $loc_parent->parent_id = 0;
                            $loc_parent->company_id = $cmp_id;
                            $loc_parent->label = $value;
                            $loc_parent->order_id = 0;
                            $loc_parent->created_at = date('Y-m-d h:i:s');
                            $loc_parent->created_by = 0;
                            $loc_parent->modified_by = 0;
                            $loc_parent->resource_type = "Webserver";
                            $success = $loc_parent->save();
                            $parent_id = $loc_parent->id;                        
                        } else {
                            $loc_child = Location::firstOrNew(array('label' => $value,'parent_id'=>$parent_id,'is_deleted'=>0));
                            $loc_child->project_id = $pid;
                            $loc_child->parent_id = $parent_id;
                            $loc_child->company_id = $cmp_id;
                            $loc_child->label = $value;
                            $loc_child->order_id = 0;
                            $loc_child->created_at = date('Y-m-d h:i:s');
                            $loc_child->created_by = 0;
                            $loc_child->modified_by = 0;
                            $loc_child->resource_type = "Webserver";
                            $success = $loc_child->save();
                            $parent_id = $loc_child->id;
                        }
                    }
                }
                if (file_exists($temp_path."/".$file_name)) {
                    unlink($temp_path."/".$file_name);
                }
                return redirect('superadmin/projectConfiguration/location')->with('flash_alert_notice_location', 'Location created successfully!');
            } else {
                if (file_exists($temp_path."/".$file_name)) {
                    unlink($temp_path."/".$file_name);
                }
                $err = "Its not a right csv file for Location.";
                $errors = new MessageBag(['error' => [$err]]);
                return Redirect::back()->withErrors($errors);    
            }            
        } else {
            $err = "Csv File is required.";
            $errors = new MessageBag(['error' => [$err]]);
            return Redirect::back()->withErrors($errors);
        }       
    }


    /**
     * Export location csv from database resource.
     *
     * @param  \App\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function exportCsv(StoreLocationCsv $request,Location $location,$pid,$cmp_id)
    {            
        $pid = Crypt::decrypt($pid);
        $cmp_id = Crypt::decrypt($cmp_id);
        $location = Location::where('is_deleted','=',0)->where('project_id','=',$pid)->where('company_id','=',$cmp_id)->get(); 
        $project = Project::where('is_deleted','=',0)->where('id','=',$pid)->first();
        $pname = $project->name;        
        if (!empty($location)) {
            $loc_data = array();
            $parent_data = array();
            $i = 0;
            $j = 0;
            foreach ($location as $key => $loc) {
                $loc_data[$loc->id]['id'] = $loc->id;
                $loc_data[$loc->id]['name'] = $loc->label;
                $loc_data[$loc->id]['parent_id'] = $loc->parent_id;                
                $i++;
            }   
        }
        $location_arr = array();
        $count = 0;
        $data =array();
        foreach ($loc_data as $key => $location) {
           $tree = $this->buildParentPath($loc_data,$location['id']);
           $total = count($tree);
           if ($count < $total) {
                $count = $total;
           }
           $data[] = $tree;           
        }
        $header = array();
        for ($i=0;$i<$count;$i++) {
            if($i == 0) {
                $header[0][$i] = "Location";
            } else {
                $header[0][$i] = "Sublocation ".$i;
            }
        }
        $data = array_merge($header, $data);
        $type = 'csv';
        return Excel::create($pname.'_location', function($excel) use ($data) {
            $excel->sheet('Location', function($sheet) use ($data) {
                $sheet->fromArray($data, null, 'A1', false, false);
            });
        })->download($type);
    }

    public function buildParentPath(array $elements, $id) {
        $children = array();
        $result = array();
        $path = array();
        foreach ($elements as $key => $element) {
            if ($element['id'] == $id) {   
                $result[] = $element['name'];        
                $result = array_merge($this->buildParentPath($elements, $element['parent_id']), $result);                
            }

        }        
        return $result;
    }



    public function buildTree(array $elements, $parentId = 0) {
    $branch = array();
    $result = array();
    foreach ($elements as $element) {
        if ($element['parent_id'] == $parentId) {            
            $result[] = $element['id'];
            $children = $this->buildTree($elements, $element['id']);
            if ($children) {
                foreach($children as $chRows) {
                        $result[] = $chRows;
                    }

                $element['child'] = $children;
                #print_r($element);die;
            }
            //$branch[] = $element;
            $branch[] = $element;
            //////$parent_name = '';
        }
    }

    return $branch;
}
    /**
     * Display the specified resource.
     *
     * @param  \App\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function show(Location $location)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function edit(Location $location)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Location $location)
    {
        $input = Input::except('_token'); 
        $location = $location->where('is_deleted','=',0)->where('id','=',$input['location_id'])->where('company_id','=',$input['cmp_id'])->where('project_id','=',$input['project_id'])->first();
        $location->label = $input['location_name'];
        $save = $location->save();
        if($save){
            $msg = "Location Name Update successfully";
            $arr = array('msg'=>$msg);
            return json_encode($arr); 
            exit;
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function destroy(Location $location,$id='')
    {
        $location = Location::find($id);
        $location->delete();
        return redirect('superadmin/projectConfiguration/location')->with('flash_alert_notice', 'Location deleted successfully!');
    }

    /**
    * List view by Data table for Assciated project Details
    */
    public function getAllProjectData()
    {                  
        $cmp_id = \Session::get('cmp_id');
        $comp_projects = Helper::getCompanyProject($cmp_id);        
        $project1 = Project::select([ 'id','name'])->where('is_deleted', '=', 0)->whereIn('id', $comp_projects)->get();
        $data = array();
        $i = 0;
        foreach ($project1 as $key => $value) {
            $data[$i]['id'] = Crypt::encrypt($value->id);
            $data[$i]['name'] = $value->name;
            $i++;
        }
        return json_encode($data);
        exit;        
    }

    /**
    * Download Project Location sample csv file
    */
    public function downloadFile()
    {         
        $myFile = public_path("uploads/csv/Project_location_template_export.csv");
        $headers = ['Content-Type: application/csv'];
        //$newName = 'projectLocation-sample-csv-file-'.time().'.csv';
        return response()->download($myFile, "Project_location_template_export.csv", $headers);      
    }
}
