<?php
use Illuminate\Support\Facades\Route;


Route::group(['middleware' => ['web','admin','ProjectConfig'], 'prefix' => 'superadmin/projectConfiguration', 'namespace' => 'Modules\Projectconfiguration\Http\Controllers'], function()
{       
    // Issueto Routes   
    Route::get('issueto', 'ProjectIssuetoController@index');
    Route::post('save_issueto', 'ProjectIssuetoController@updateIssueto');
    Route::get('issueto_list', 'ProjectIssuetoController@getIssuetoData');
    Route::get('delete_issueto/{issueto_id}', 'ProjectIssuetoController@destroyIssueto');
    Route::get('issueto_users_list/{issueto_id}', 'ProjectIssuetoController@getIssuetoUserData');
    Route::get('issueto-details/{issueto_id}', 'ProjectIssuetoController@show');    
    Route::post('issueto/edit-issueto-user/{user_id}', 'ProjectIssuetoController@editIssuetoUser');
    Route::post('save_issueto_user/{issueto_id}', 'ProjectIssuetoController@updateIssuetoUser');
    Route::get('delete_issueto_user/{issueto_user_id}', 'ProjectIssuetoController@destroyIssuetoUser');
    Route::get('getAllIssuetoProjectData', 'ProjectIssuetoController@getAllProjectData');

    // Location Routes
    Route::get('location', 'LocationController@index');
    Route::post('save-location', 'LocationController@store');
    Route::post('update-location', 'LocationController@update');
    Route::get('getAllProjectData', 'LocationController@getAllProjectData');
    Route::get('delete-location/{id}', 'LocationController@destroy');
    Route::post('import-csv', 'LocationController@importCsv');
    Route::get('export-csv/{pid}/{cmp_id}', 'LocationController@exportCsv');
    Route::get('location-sample-csv', 'LocationController@downloadFile');    
    Route::get('projectConfigId/{id}', function(){
    	
    });
});


