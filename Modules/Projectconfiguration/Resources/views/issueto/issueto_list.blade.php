@extends('superadmin::layouts.master')

@section('content')
<!-- BEGIN CONTAINER -->

<div class="page-content-wrapper" ng-app="projectIssuetoModule">
	<div class="page-content" ng-controller="projectIssuetoController">
		
		<div class="page-head">
			<div class="page-title uppercase">
				<h1>Project Configuration <small></small></h1>
			</div>
		</div>
		@include('superadmin::partials.breadcrumb')

		<link href="{{url('/')}}/css/jquery.treeview.css" rel="stylesheet" type="text/css"/>
		<div class="row">
		    <div class="col-lg-8 col-xs-12 col-sm-12">
		        <div class="portlet light bordered">
		            <div class="portlet-title tabbable-line">
		                <div class="caption col-md-5">
		                    <i class="icon-bubbles font-dark hide"></i>
		                    <span class="caption-subject font-dark bold uppercase">Issuedto List</span>
		                </div>
		  
		                <div class="caption col-md-7 text-right">
		                    <i class="icon-bubbles font-dark hide"></i>
		                    <span class="caption-subject font-dark bold uppercase">Project : {{$project[0]->name}}</span>
		                </div>                                       
		            </div>
		            <div class="portlet-body">
		            	@if(Session::has('flash_alert_notice'))
	                        <div class="alert alert-success alert-dismissable">
	                          	<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
	                          	<i class="icon fa fa-check"></i>  
	                           	{{ Session::get('flash_alert_notice') }} 
	                        </div>
                       	@endif
		                <!-- BEGIN CONTENT -->
						<div class="row">
							<div class="col-md-12">
								<!-- BEGIN EXAMPLE TABLE PORTLET-->
								@if(isset($_REQUEST['i']) && !empty($_REQUEST['i']) && empty($j))
			                        <div class="alert alert-success alert-dismissable">
			                          	<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
			                          	<i class="icon fa fa-check"></i>  
			                           		@php echo $_REQUEST['i']; @endphp;
			                        </div>
	                       		@endif
								<div class="" id="project_issueto_section">
									<div class="edit_btn">
										<div class="row">
											<div class="col-md-12 text-right">
												<div class="row">
													<div class="col-md-12">
														<a class="btn cst-yellow" id="" href="javascript:;" ng-click="showIssueto('add')"> <i class="fa fa-plus"></i> Add New </a>
													</div>

												</div>
											</div>
										</div>
									</div>

								</div>
								@include('projectconfiguration::issueto.issueto_form')
								<div class="row">
									<div class="col-md-12">
										<table class="table table-striped table-bordered table-hover" id="sample_1">
											<thead>
												<tr>
													<th> Name </th>
													<th> Company </th>
													<th> Email </th>
													<th> Phone </th>
													<th> Activity </th>
													<th> Action </th>
												</tr>
											</thead>
										</table>
									</div>
								</div>
							</div>
							
						</div>
					</div>
					<!-- END EXAMPLE TABLE PORTLET-->
				</div>
			</div>
			<!-- END CONTENT -->
			<div class="clearfix">
			</div>

		</div>
	</div>

	
<!-- END CONTAINER -->           																									
@stop