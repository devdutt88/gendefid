@extends('superadmin::layouts.master')

@section('content')
<!-- BEGIN CONTAINER -->
<link href="{{url('/')}}/assets/css/tasks.css" rel="stylesheet" type="text/css"/>
<div class="page-content-wrapper" ng-app="issuetoModule">
	<div class="page-content" ng-controller="issuetoController">
		<!-- {!! $breadcrumbs !!} -->
		@include('superadmin::partials.breadcrumb')
		<!-- BEGIN CONTENT -->
		<div class="row">
			<div class="col-lg-8">
				<div class="portlet light bordered box-min-height">
					<div class="portlet-title caption font-green-sharp">
                        <div class="caption">
                            <i class="icon-microphone font-dark hide"></i>
                            <span class="caption-subject bold theme-font-color uppercase"> {{$issueto->name}} : Users List </span>
                        </div>
                       	<div class="actions">
							<a href="" class="btn btn-circle btn-default btn-sm" ng-click="showIssueto('edit', {{$issueto_id}});">
							<i class="fa fa-pencil"></i> Edit </a>
							<!-- <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title="">
							</a> -->
						</div>
                    </div>
					<div class="portlet-body">
						@if(Session::has('flash_alert_notice'))
	                        <div class="alert alert-success alert-dismissable">
	                          	<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
	                          	<i class="icon fa fa-check"></i>  
	                           	{{ Session::get('flash_alert_notice') }} 
	                        </div>
                       	@endif
						@include('superadmin::issueto.issueto_form')
						
						<div class="row">
							<input type="hidden" name="issueto_id" id="issueto_id" value="{{$issueto_id}}" />
							<div class="row">
								<div class="col-md-12">
									<label class="control-label col-md-3"><strong>Description :</strong></label>
									<div class="col-md-9">
											{{$issueto->description}}
									</div>
								</div>
							</div>
						</div>
						<div class="edit_btn">								
							<div class="text-right">
								<div class="row">
									<div class="col-md-12">
										<!-- <a href="#"><button type="button" onClick="history.go(-1); return false;" class="btn default"><i class="fa fa-chevron-left "></i> Back</button></a> -->
										<a href="javascript:;" id="" ng-click="showIssuetoUser('add');" class="btn cst-yellow"><i class="fa fa-plus"></i> Add New User</a>
									</div>
								</div>
							</div>								
						</div>
						@include('superadmin::issueto.issueto_user_form') <!-- Edit Issueto Form -->
						<div class="form-horizontal">
							<div class="form-body">
								<h3 class="form-section">Issuedto Users</h3>
								<table class="table table-striped table-bordered table-hover" id="sample_2">
									<thead>
										<tr>
											<th> Name </th>
											<th> Email </th>
											<th> Phone </th>
											<th> Activity </th>
											<th> Action </th>
										</tr>
									</thead>
								</table>
							</div>
						</div>
					</div>
				</div>	
			</div>
			<div class="col-lg-4" ng-init="companyList({{$issueto_id}})">
				<div class="portlet light bordered tasks-widget">
                    <div class="portlet-title caption font-green-sharp">
                        <div class="caption">
                            <i class="icon-microphone font-dark hide"></i>
                            <span class="caption-subject bold font-dark uppercase"> Companies </span>
                        </div>
                       <!-- 	<div class="actions">
							<a href="" class="btn btn-circle btn-default btn-sm" ng-click="show_details({{$issueto_id}}, 'companies')">
							<i class="fa fa-pencil"></i> Edit </a>
						</div> -->
						 <div class="inputs">
		                    <div class="portlet-input input-inline input-small">
		                        <div class="input-icon right">                            
		                            <i class="icon-magnifier"></i>                            
		                            <input type="text" id="query" class="form-control input-circle" ng-model="query" onfocus="pxtrack.emit('counter', '1')" placeholder="search..."/>
		                        </div>
		                    </div>
		                </div>
                    </div>
                    <flash-message duration="2000" show-close="true" on-dismiss="myCallback(flash)"></flash-message>
					<div class="portlet-body" >
		                <div class="task-content">
							<div class="scroller portlet-height">
								<!-- START TASK LIST -->                       
		                        <ul class="task-list">
		                            <li class="" ng-repeat="list in plists | filter:query | orderBy: orderList">
		                            	<div class="task-checkbox">
											<!-- <input type="checkbox" name="status" class="liChild" ng-model="companymap.status" ng-click="associateCompany(list.id)" ng-if="list.status == 'linked'"> -->
										</div>
										<div class="task-title">
											<span class="task-title-sp"> @{{list.name}}</span>
											<span class="label label-lg label-danger task-right" ng-if="list.status == 'linked'" ng-click="unlinkCompany(list.companyId,list.issuetoId)">Remove</span>
											<span class="label label-lg label-success task-right" ng-if="list.status == 'unlinked'" ng-click="associateCompany(list.companyId,list.issuetoId)">Add</span>
										</div>
		                            </li>
		                        </ul>                        
		                    </div>                                           
		                </div>
		            </div>
				</div>
			</div>
			<div class="clearfix"></div>															
		</div>
		<!-- END CONTENT -->
	</div>
</div>
<!-- END CONTAINER -->
@stop