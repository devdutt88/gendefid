@extends('superadmin::layouts.master')
<!-- BEGIN CONTAINER -->	
@section('content')
                <!-- BEGIN CONTENT -->
                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content">
                        <!-- BEGIN PAGE HEADER-->
                        @include('superadmin::partials.breadcrumb')
                        <!-- END PAGE HEADER-->
                        <!-- BEGIN DASHBOARD STATS 1-->
                       
                        <div class="clearfix"></div>
                        <!-- END DASHBOARD STATS 1-->
                        @include('projectconfiguration::location.form')                        
                    </div>
                    <!-- END CONTENT BODY -->
                </div>
                <!-- END CONTENT -->                         
@stop
<!-- END CONTAINER -->