<div class="modal fade" id="addModal" close="cancel()">
	<div class="modal-dialog locationModel">
	    <div class="modal-content">
			<!-- BEGIN FORM-->
		 	<form id="location" name="location" class="horizontal-form" novalidate="">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					<h4 class="modal-title">Add Location</h4>					
				</div>
				<div class="modal-body">
					{{ csrf_field() }}					
					<div class="form-body">
							<!--/row for buisness-->
					<div class="row text-center hide alert-success" id="loc_msg">
						
					</div>
						<div class="row">
							<div class="col-md-3">
								<div class="form-group">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Project Name</label>
									<input type="text" readonly="" id="pname" required="required" name="pname" class="form-control" value="{{$project[0]->name}}" placeholder="">
									<input type="hidden" name="project_id" id="project_id" value="{{$id}}">
									<input type="hidden" name="cmp_id" id="cmp_id" value="{{$cmp_id}}">
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group">
								</div>
							</div>							
						</div>
						<div class="row">
							<div class="col-md-3">
								<div class="form-group">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Location Name</label>
									<input type="text" id="level1" name="level1" class="form-control" placeholder="">
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group">
								</div>
							</div>							
						</div>
					<!--/row-->
					</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal" ng-click="cancel()">Cancel</button>
					<button type="submit" class="btn blue"><i class="fa fa-check"></i> Save</button>
				</div>
			</div>
		</form>
	</div>
</div>
</div>


<!--child -->


<div class="modal fade" id="childModal" close="cancel()">
	<div class="modal-dialog locationModel">
	    <div class="modal-content">
			<!-- BEGIN FORM-->
		 	<form id="sublocation" name="sublocation" class="horizontal-form" novalidate="">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					<h4 class="modal-title">Add Location</h4>					
				</div>
				<div class="modal-body">
					{{ csrf_field() }}					
					<div class="form-body">
							<!--/row for buisness-->
					<div class="row text-center hide alert-success" id="loc_msg1">
						
					</div>
						<div class="row">
							<div class="col-md-3">
								<div class="form-group">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Location Name</label>
									<input type="text" readonly="" id="parent" required="required" name="parent" class="form-control" value="" placeholder="">
									<input type="hidden" readonly="" id="parent_id" required="required" name="parent_id" class="form-control" value="" placeholder="">
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group">
								</div>
							</div>							
						</div>
						<div class="row">
							<div class="col-md-3">
								<div class="form-group">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">SubLocation Name</label>
									<input type="text" id="sublocation_name" required="required" name="sublocation_name" class="form-control"  placeholder="">
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group">
								</div>
							</div>							
						</div>
					<!--/row-->
					</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal" ng-click="cancel()">Cancel</button>
					<button type="submit" class="btn blue"><i class="fa fa-check"></i> Save</button>
				</div>
			</div>
		</form>
	</div>
</div>
</div>


