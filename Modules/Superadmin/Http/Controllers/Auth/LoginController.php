<?php

namespace Modules\Superadmin\Http\Controllers;

use authenticate;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = 'superadmin/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {           
        $this->middleware('guest', ['except' => 'logout']);
    }

    public function username()
    {
        if(filter_var(Input::get('name'), FILTER_VALIDATE_EMAIL)) {            
            return 'email';
        }
        else {            
            return 'name';
        }  
    }

    protected function authenticated($request, $user)
    {                   
        #echo $user->role_id;die;  
        #print_r(Auth::User());die;
        if($user->role_id === 0) {
            return redirect()->intended('superadmin/');
        }

        return redirect()->intended('/');
    }

}
