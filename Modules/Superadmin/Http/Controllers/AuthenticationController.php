<?php

namespace Modules\Superadmin\Http\Controllers;

use Kodeine\Acl\Models\Eloquent\Permission;
use Kodeine\Acl\Models\Eloquent\Role;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Yajra\Datatables\Facades\Datatables as Datatables;
use mjanssen\BreadcrumbsBundle\Breadcrumbs;
use Illuminate\Support\Facades\Input;
use Auth;
use Crypt;
use Helpers as Helper;

class AuthenticationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {         
        return true;
    }

    public function roleList()
    {        
        Breadcrumbs::addBreadcrumb('roles',  url('superadmin/roles'));        
        $page_data = array('breadcrumbs' => Breadcrumbs::generate());
        $data['scripts'] = array('angularjs/angular.min',
                            'jquery.dataTables.min',
                            'angularjs/angular-datatables.min',
                            'angularjs/angular-messages.min',
                            'angularjs/angular-flash.min',
                            'angularjs/controllers/rolecontroller',
                            //'Treemodule/src/Controllers/TreeController',
                            'Treemodule/src/Services/TreeService',
                            //'Treemodule/src/Config/URLConfig',
                            'Treemodule/src/Directives/TreeDirectives',
                            'angularjs/angular-flash.min'
                        );
        return view('superadmin::role.role_list',compact('data'))->with($page_data);
    }

    public function permissionList()
    {        
        Breadcrumbs::addBreadcrumb('Permission List',  url('superadmin/permissions'));        
        $page_data = array('breadcrumbs' => Breadcrumbs::generate());        
        $permissions = Permission::select(['id','name','description'])->get();
        $data['permissions'] = $permissions;
        $data['scripts'] = array('angularjs/angular.min',
                            'angularjs/angular-messages.min',
                            'angularjs/angular-flash.min',
                            'angularjs/permissionModule'
                        );
        return view('superadmin::permissions.permission_list',compact('data'))->with($page_data);
    }

    /**
    * List view by Data table for Assciated project Details
    */
    public function getRoles()
    {         
        $role = Role::select(['id', 'name','description'])->get(); 
        $data = array();
        $i = 0;
        foreach ($role as $key => $value) {
            $data[$i]['id'] = Crypt::encrypt($value->id);
            $data[$i]['name'] = $value->name;
            $data[$i]['description'] = $value->description;
            $i++;
        }
        return json_encode($data);
        exit;        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {        
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeRole(Request $request)
    {             
        // echo "<pre>";   
        // print_r(Input::all());
        // die;
        $input = Input::all();  
        $sys_token = \Session::token();
        $token = Input::get('_token'); 
        if ($token != $sys_token) {
            return "there was some server error";
            die;
        }                
        //$type = (!empty($input["type"]) && $input["type"]==true)?1:0;
        $roleObj = new Role();
        $roleCreat = $roleObj->create([
            "name"  =>  $input["name"],
            "slug"  =>  str_slug($input["name"]),
            "description"  =>  $input["description"],
            "type"  =>  (!empty($input["type"]) && $input["type"]==true)?1:0,
            "created_at" => date('Y-m-d h:i:s')
        ]);
       
        $success = \Session::flash('flash_alert_notice', 'Role has been added successfully !');
        return $success;        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storePermission()
    {                        
        $sys_token = \Session::token();
        $token = Input::get('_token'); 
        if ($token != $sys_token) {
            return "there was some server error";
            exit;
        } 

        $permission = new Permission();
        $insertInPermission = $permission->create([ 
            'name'        => Input::get("name"),
            'slug'        => [          // pass an array of permissions.
                'create'     => true,
                'view'       => true,
                'update'     => true,
                'delete'     => true
            ],
            'description' => Input::get("description"),
            "created_at"=> date('Y-m-d h:i:s')
        ]);
        
        $success = \Session::flash('flash_alert_notice', 'Permission added successfully !');
        return $success; 
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function showRole(Role $role,$id)
    {        
        $id = Crypt::decrypt($id);
        
        Breadcrumbs::addBreadcrumb('Role',  url('superadmin/roles'));       
        Breadcrumbs::addBreadcrumb('View',  url('superadmin/roles'));        
        $page_data['breadcrumbs'] = Breadcrumbs::generate();
        $page_data['role'] = Role::where('id','=',$id)->first(); 
        $data['scripts'] = array('Treemodule/lib/angular.min',
                                'Treemodule/src/Controllers/TreeController',
                                'Treemodule/src/Services/TreeService',
                                'Treemodule/src/Config/URLConfig',
                                'Treemodule/src/Directives/TreeDirectives'
                        );
        return view('superadmin::role.show_role',compact('data','allparent','allchild'))->with($page_data);
    }

    /**
     * API REQUEST 
     * return permission of the role
     ***
     */
    public function getRolePermissions($roleid){
        $roleid = Crypt::decrypt($roleid);
        $roleObj = Role::where('id','=',$roleid)->first();
        $roleObj->getPermissions();
        $permIdArr = [];
        foreach ($roleObj['permissions'] as $key => $value) {
            $permIdArr[] = $value->id;
        }
        //echo "<pre>"; print_r($permIdArr); die;
        $authentication = Permission::get(); 
        $element = array(); 
        foreach ($authentication as $key => $parent) {
            //$element[$pkey]['id'] = $parent->id;
            if(in_array($parent->id, $permIdArr)){
                $element[$key]['name'] = $parent->name;
                $element[$key]['checked'] = true;
            }else{
                $element[$key]['name'] = $parent->name;
                $element[$key]['checked'] = false;
            }
            
            //$children = json_decode($parent->slug);
            // $children = $parent->slug;
            // $i = 0; 
            // foreach ($children as $ckey => $child) {
            //     $element[$key]['children'][$i]['name'] = $ckey;
            //     $element[$key]['children'][$i]['checked'] = $child;
            //     $i++;
            // }
        }
        
        //$permissionTree = Helper::buildTree($element);
        //$menuauthArr = Helper::super_unique($menuauthArr); 
        //echo "<pre>"; print_r($permIdArr); die;
        return json_encode($element);
        exit;
    }

    

    /**
     * Store a newly created permission for roles in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeRolePermission($roleid)
    {           
        $roleid = Crypt::decrypt($roleid);
        $tree = Input::get('tree');
        $roleObj = Role::where('id', '=', $roleid)->first(); // administrator
        //$roleObj->syncPermissions([1,2,3]);
        //print_r($tree);  die;
        foreach ($tree as $key => $value) {
            // assign by name
            if($value['checked'] == 'true'){
                $roleObj->assignPermission($value['name']); 
            }else{
                $roleObj->revokePermission($value['name']);
            }
        }
        exit;
    }

    /**
     * Get the specfic record according to give id in role
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Authentication  $authentication
     * @return \Illuminate\Http\Response in json
     */
    public function editRole($id)
    {
        $id = Crypt::decrypt($id);
        $role[0] = Role::find($id);
        $data = array();
        $i = 0;        
        foreach ($role as $key => $value) {
            $data['id'] = Crypt::encrypt($value->id);
            $data['name'] = $value->name;
            $data['description'] = $value->description;
            $i++;
        }
        return json_encode($data);        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function updateRole(Request $request, Role $role)
    {                        
        $sys_token = \Session::token();
        $token = Input::get('_token');
        if ($token != $sys_token) {
            return "there was some server error";
            die;
        }
        $input = Input::all();    
        $id =  Crypt::decrypt($input['id']);
        $roles = Role::firstOrNew(array("id"=>$id));
        $roles->name = $input['name'];
        $roles->slug = str_slug($input["name"]);
        $roles->description = $input['description'];
        $roles->type =  (!empty($input["type"]) && $input["type"]==true)?1:0;
        //update role //
        $roles->save();        
        $success = \Session::flash('flash_alert_notice', 'Role updated successfully !');
        return $success;        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function deleteRole(Role $role,$id)
    {
        $id = Crypt::decrypt($id);
        $delete = Role::destroy($id);
        return redirect('superadmin/roles')->with('flash_alert_notice', 'Role deleted successfully!');
    }


    public function editPermission($id)
    {        
        $id = Crypt::decrypt($id);
        $permission[0] = Permission::find($id);
        $data = array();
        foreach ($permission as $key => $value) {
            $data['id'] = Crypt::encrypt($value->id);
            $data['name'] = $value->name;
            $data['description'] = $value->description;
        }
        return json_encode($data);   
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Authentication  $authentication
     * @return \Illuminate\Http\Response
     */
    public function updatePermission()
    {           
        $sys_token = \Session::token();
        $token = Input::get('_token'); 
        if ($token != $sys_token) {
            return "there was some server error";
            die;
        }            
        $input = Input::all();    
        $id = Crypt::decrypt($input['id']);
        $permission = Permission::firstOrNew(array("id"=>$id));
        $permission->name = $input['name'];
        $permission->description = $input['description'];
        //update role //
        $permission->save();        
        $success = \Session::flash('flash_alert_notice', 'Roles updated successfully !');
        return $success; 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Authentication  $authentication
     * @return \Illuminate\Http\Response
     */
    public function destroyPermission($id)
    {
        $id = Crypt::decrypt($id);
        $delete = Permission::destroy($id);
        //$permission->delete();
        return redirect('superadmin/permissions')->with('flash_alert_notice', 'Permission deleted successfully!');
    }


    /**
    * Check the specified Role is exist
    * And return as true and false
    */
    public function checkRole($id,$cname)
    {      
        if (!empty($cname)){
            $id = $id?Crypt::decrypt($id):0;
            $is_current_name = Role::where('name',"=",$cname)->where('id',"!=",$id)->get();
            
            if (isset($is_current_name[0]) && !empty($is_current_name[0])){
                echo 'true';
            }else{
                echo 'false';
            }
        }              
    }

    /**
    * List view by Data table for Role Details
    */  
    public function getRoleData(){
        $role = Role::select(['id', 'name'])->get();                          
        $company = Datatables::of($role)->addColumn('action', function ($role) {
            return '<a href="show-role/'.$role->id.'" class="btn btn-warning btn-xs"><i class="fa fa-newspaper-o" title="View"></i></a>&nbsp;&nbsp;<a href="delete-role/'.$role->id.'" title="Delete" class="btn btn-danger btn-xs"  onclick="return confirm('."'Are you sure, you want to delete this user?'".');"><i class="glyphicon glyphicon-trash"></i></a>';
        })->make(true);
        return $company;die;           
    }

    /**
    * List view by Data table for Permission Details
    */
    public function getPermissionData(){
        $permission = Permission::select(['id', 'name','description','created_at'])->get();        
        $perm = Datatables::of($permission)->addColumn('created_at', function ($cmp) {
                    $date = $cmp->created_at;
                    $ts   = strtotime($date);
                    return date('Y-m-d', $ts);
                })->addColumn('action', function ($cmp) {
                    $id = Crypt::encrypt($cmp->id);
                return '<a id="editT" class="btn btn-warning btn-xs edit" perid="'.$id.'"><i class="fa fa-pencil-square-o" title="Edit"></i></a>&nbsp;&nbsp;<a href="delete-permission/'.$id.'" title="Delete" class="btn btn-danger btn-xs"  onclick="return confirm('."'Are you sure, you want to delete this user?'".');"><i class="glyphicon glyphicon-trash"></i></a>';
            })->make(true);
        return $perm;
        exit;        
    }

}
