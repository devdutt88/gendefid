<?php

namespace Modules\Superadmin\Http\Controllers;

use App\Models\superadmin\Issueto;
use App\Models\superadmin\IssuetoUsers;
use App\Models\superadmin\Company;
use App\Models\superadmin\CompanyIssuetoMapping;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Redirect;
use Modules\Superadmin\Http\Requests\StoreIssuetoPost;
use Modules\Superadmin\Http\Requests\StoreIssuetoUsersPost;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Helpers as Helper;
use Validator;
use Auth;
use Paginate;
use Grids;
use HTML;
use Form;
use View;
use URL;
use Lang;
use DB;
use Yajra\Datatables\Facades\Datatables as Datatables;
use mjanssen\BreadcrumbsBundle\Breadcrumbs;
use Crypt;


class IssuetoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        Breadcrumbs::addBreadcrumb('Issuedto List',  url('superadmin/issueto'));
        $page_data = array('breadcrumbs' => Breadcrumbs::generate());
        $page_data['obj_companies'] = Company::select('id', 'name')->where('is_deleted','=', 0)->get();
        $page_data['companies'] =  Helper::getCompanyList();
        $page_data['issueto_comps'] = array();
        $page_data['cur_it'] = '';
        $issueto = Issueto::where('is_deleted', '=', 0)->paginate(10);

        $page_data['option_activity'] = array(
            "Brickwork"=>"Brickwork",
            "Carpentry"=>"Carpentry",
            "Cleaning"=>"Cleaning",
            "Concreting"=>"Concreting",
            "Electrical"=>"Electrical",
            "Fire services"=>"Fire services",
            "Joinery"=>"Joinery",
            "Plasterboard"=>"Plasterboard",
            "Painting"=>"Painting",
            "Plumbing"=>"Plumbing",
            "Metalwork"=>"Metalwork",
            "Structural steel"=>"structural steel",
            "Waterproofing"=>"Waterproofing",
            "Other"=>"Other",
        );

        $data['scripts'] = array('angularjs/angular.min','angularjs/angular-messages.min','angularjs/angular-flash.min','angularjs/ng-tags-input.min','angularjs/controllers/issuetocontroller');
        return view('superadmin::issueto.issueto_list',compact('data','page_title','page_action','viewPage','viewPage1'))->with($page_data); 
    }

    /**
     * Add new Issueto or update specified Issueto details.
     *
     */

    public function saveIssueto(StoreIssuetoPost $request)
    {
        $sys_token = \Session::token();
        $token = Input::get('_token');
        if ($token != $sys_token) { 
            return false;
        }               
        $id = (Input::get('id')) ? Crypt::decrypt(Input::get('id')) : 0;
        $issueto = Issueto::firstOrNew(array('id' => $id));
        $issueto->name = Input::get('name');
        $issueto->auther = Input::get('auther');
        $issueto->phone = Input::get('phone');
        $issueto->email = Input::get('email');
        $issueto->description = Input::get('description');
        $issueto->activity = Input::get('activity'); 
        $issueto->created_by = 0;
        $issueto->created_at = date('Y-m-d h:i:s');
        $success = $issueto->save();
        if($success){
            $success_alert = \Session::flash('flash_alert_notice', ($id!=0) ? 'Issuedto updated successfully !' : 'Issuedto created successfully !');
            return $success_alert;
        }
    }

    public function editIssueto($id)
    {
        $id = Crypt::decrypt($id);
        $issueto = Issueto::find($id);
        if(!empty($issueto)){
            $data = array();
            $data['id'] = Crypt::encrypt($issueto->id);
            //$data['id'] = $issueto->id;
            $data['name'] = $issueto->name;
            $data['auther'] = $issueto->auther;
            $data['phone'] = $issueto->phone;
            $data['email'] = $issueto->email;
            $data['description'] = $issueto->description;
            $data['activity'] = $issueto->activity;
            return json_encode($data);
        }
    }

    public function editIssuetoUser($id)
    {
        $id = Crypt::decrypt($id);
        $issuetoUser = IssuetoUsers::find($id);
        if(!empty($issuetoUser)){
            $data = array();
            $data['id'] = Crypt::encrypt($issuetoUser->id);
            $data['name'] = $issuetoUser->name;
            $data['email'] = $issuetoUser->email;
            $data['phone'] = $issuetoUser->phone;
            $data['tag'] = json_decode($issuetoUser->tag);
            $data['trade'] = $issuetoUser->trade;
            $data['issueto_id'] = Crypt::encrypt($issuetoUser->issueto_id);
            return json_encode($data);
        }
    }

    public function saveIssuetoUser(StoreIssuetoUsersPost $request)
    {
        $sys_token = \Session::token();
        $token = Input::get('_token');
        if ($token != $sys_token) { 
            return false;
        }        
        $id = (Input::get('id')) ? Crypt::decrypt(Input::get('id')) : 0;
        $issuetoUser = IssuetoUsers::firstOrNew(array('id' => $id));
        $issuetoUser->issueto_id = Crypt::decrypt(Input::get('issueto_id'));
        $issuetoUser->name = Input::get('name');
        $issuetoUser->email = Input::get('email');
        $issuetoUser->phone = Input::get('phone');
        $issuetoUser->tag = json_encode(Input::get('tag'));
        $issuetoUser->trade = Input::get('trade');
        $issuetoUser->modified_by = 0;
        $success = $issuetoUser->save();
        
        if($success){
            $success_alert = \Session::flash('flash_alert_notice', ($id!=0) ? 'Issuedto User updated successfully !' : 'Issuedto User created successfully !');
            return $success_alert;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function show(Issueto $issueto,$id=0)
    {
        $id = Crypt::decrypt($id);
        Breadcrumbs::addBreadcrumb('Issuedto',  url('superadmin/issueto'));
        Breadcrumbs::addBreadcrumb('Issuedto Details', '');
        $page_data = array('breadcrumbs' => Breadcrumbs::generate());

        //Get specific user details according to id
        $data['scripts'] = array('issueto');
        $page_data['issueto_comps'] =  Helper::getCompanyIssuetoMapping($id);
        $page_data['obj_companies'] =  Company::select('id', 'name')->where('is_deleted', '=', 0)->orderBy('name', 'asc')->get();
        $page_data['companies'] =  Helper::getCompanyList();
        $page_data['issueto'] = $issueto->where('is_deleted','=',0)->find($id);
        $page_data['issueto_id'] = $id;
        
        
        //Get specific user details according to id
        $page_title     = 'Issueto'; 
        $page_action    = '';
        $viewPage       = 'issueto';
        $viewPage1      = '';

        if(!empty($page_data['issueto'])){
            //$page_data['issueto_users'] = IssuetoUsers::where('issueto_id', '=', $id)->first();
            $data['scripts'] = array('angularjs/angular.min','angularjs/angular-messages.min','angularjs/dataTable.min','angularjs/angular-flash.min','angularjs/ng-tags-input.min','angularjs/controllers/issuetocontroller');
            return view('superadmin::issueto.show_issueto',compact('data','page_title','page_action','viewPage','viewPage1'))->with($page_data);
           
        }else{
            return redirect('superadmin/issueto');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function edit(Issueto $isueto,$id=0)
    {
       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function update(StoreIssuetoPost $request)
    {
        $issueto_id = Input::get('issueto_id');
        $issueto = Issueto::firstOrNew(array('id' => $issueto_id));
        $issueto->name = Input::get('name');
        $issueto->description = Input::get('description');
        $success = $issueto->save();
        if ($success) {
            $ci_map = new CompanyIssuetoMapping;
            $obj_issueto_comps = $ci_map->select('company_id')->where('issueto_id', '=', $issueto_id)->where('is_deleted', '=', 0)->get();
            $issueto_comps = array();
            foreach($obj_issueto_comps as $comp) {
                $issueto_comps[] = $comp->company_id;
            }
            $obj_issueto_comps_del = $ci_map->select("company_id")->where('issueto_id', '=', $issueto_id)->where('is_deleted', '=', 1)->get();
            $issueto_comps_del = array();
            foreach($obj_issueto_comps_del as $comp) {
                $issueto_comps_del[] = $comp->company_id;
            }
            $comps = Input::get('company');
            $delete_issueto_comps = array_diff($issueto_comps,$comps);
            $to_del_comps = $ci_map->where('issueto_id', '=', $issueto_id)->whereIn('company_id', $delete_issueto_comps);
            if (!empty($to_del_comps)) {
                $to_del_comps->update(array('is_deleted'=>1));
            }
            $add_issueto_comps = array_diff($comps,$issueto_comps);
            foreach($add_issueto_comps as $comp) {
                if (in_array($comp, $issueto_comps_del)) {
                    
                    $to_add_comps = $ci_map->where('issueto_id', '=', $issueto_id)->where('company_id', '=', $comp);
                    if (!empty($to_add_comps)) {
                        $to_add_comps->update(array('is_deleted'=>0));
                    }
                } else {
                    $ci_map->issueto_id = $issueto_id;
                    $ci_map->company_id = $comp;
                    $ci_map->save();
                }
            }            
            return redirect("superadmin/issueto-details/$issueto_id")->with('flash_alert_notice', 'Issueto details updated successfully.');
        } else {
            return redirect('superadmin/issueto');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function destroyIssueto(Issueto $issueto,$id)
    {
        $id = Crypt::decrypt($id);
        if (!empty($id)) {
            $issueto = Issueto::where('id','=',$id)->first();
            $issueto->is_deleted = 1;
            $issueto-> save();
            
            $issuetoUsers = IssuetoUsers::where('issueto_id','=',$id);
            if (!empty($issuetoUsers)) {
                $issuetoUsers->update(array('is_deleted' => 1));
            }
        }
        return redirect('superadmin/issueto')->with('flash_alert_notice', 'Issuedto deleted successfully.');
    }

    public function destroyIssuetoUser(IssuetoUsers $issuetoUsers,$id)
    {
        if (!empty($id)) {
            $id = Crypt::decrypt($id);
            $cur_user = $issuetoUsers->where('id', $id);
            if(!empty($cur_user)){
                $cur_user->update(array('is_deleted' => 1));
            }
        }
        return redirect('superadmin/issueto')->with('flash_alert_notice', 'Issuedto user deleted successfully.');
    }

    public function checkIssuetoEmail($enc_id, $email){
        
        $id = ($enc_id != '0') ? Crypt::decrypt($enc_id) : 0;
        if (!empty($email)){
            //$is_email = Issueto::where('email',"=",$email)->where('is_deleted',"=",0);

            $is_email = ($id !=0) ? Issueto::where('email',"=",$email)->where('is_deleted',"=",0)->where('id',"!=",$id)->get() : Issueto::where('email',"=",$email)->where('is_deleted',"=",0)->get();
            
            if (isset($is_email[0]) && !empty($is_email[0])){
                echo 'true';
            }else{
                echo 'false';
            }
        }
        exit;
    }

    public function checkIssuetoUserEmail($enc_id, $issueto_id, $email){
        
        $id = ($enc_id != '0') ? Crypt::decrypt($enc_id) : 0;
        if (!empty($email )){
           // $is_email = IssuetoUsers::where('email',"=",$email)->where('is_deleted',"=",0)->where('id',"!=",$id)->where('issueto_id','=',$issueto_id)->get();
            $is_email = ($id != 0) ? IssuetoUsers::where('email',"=",$email)->where('is_deleted',"=",0)->where('id',"!=",$id)->where('issueto_id','=',$issueto_id)->get() : IssuetoUsers::where('email',"=",$email)->where('is_deleted',"=",0)->where('issueto_id','=',$issueto_id)->get();
            if (isset($is_email[0]) && !empty($is_email[0])){
                echo 'true';
            }else{
                echo 'false';
            }
        }
        exit;
    }

    /**
    * Check the specified issue to is exist
    * And return as true and false
    */
    public function issueto_check_isexist()
    {
        $name = Input::get('name');
        $id = Input::get('id');
        if (isset($name) && !empty($name)) {
            if (!empty($id)) {
                $is_current_name = Issueto::where('name',"=",$name)->where('is_deleted',"=",0)->where('id',"=",$id)->get();
                if(isset($is_current_name[0]) && !empty($is_current_name[0]) && $is_current_name[0]->name == $name)
                {
                    echo "true";
                    die;
                }
            }
            $is_name = Issueto::where('name',"=",$name)->where('is_deleted',"=",0)->get();
            if (isset($is_name[0]) && !empty($is_name[0])) {
                echo 'false';
            } else {
                echo 'true';
            }
        }
        die;
    }
    /**
    * Check the specified issue to user is exist
    * And return as true and false
    */
    public function issueto_user_check_isexist()
    {        
        $issueto_id = Input::get('issueto_id');
        $email = Input::get('email');
        $id = Input::get('id');
        if(isset($email) && !empty($email)){
            if(!empty($id)){
                $is_current_email = IssuetoUsers::where('email',"=",$email)->where('is_deleted',"=",0)->where('id',"=",$id)->where('issueto_id',"=",$issueto_id)->get();   
                if(isset($is_current_email[0]) && !empty($is_current_email[0]) && $is_current_email[0]->email == $email)
                {
                    echo "true";
                    die;
                }
            }
            $is_email = IssuetoUsers::where('email',"=",$email)->where('is_deleted',"=",0)->where('issueto_id',"=",$issueto_id)->get();
            if(isset($is_email[0]) && !empty($is_email[0])){
                echo 'false';
            }else{
                echo 'true';
            }
        }
        die;
    }

    /**
    * List view by Data table for issue to Details
    */
    public function getIssuetoData()
    {

        $issueto1 = Issueto::select(['id', 'name', 'auther', 'phone', 'email','activity','modified_at'])->where('is_deleted', '=', 0)->orderBy('id','DESC')->get();
        $issueto = Datatables::of($issueto1)->addColumn('modified_at', function ($cmp) {
                $date = $cmp->modified_at;
                $ts   = strtotime($date);
                return date('Y-m-d', $ts);
            })->addColumn('action', function ($it) {
            $id = Crypt::encrypt($it->id);
            return '<a id="editIssuto" class="btn btn-warning btn-xs edit-ist" Is-id="'.$id.'" title="Edit"><i class="fa fa-pencil-square-o"></i></a>&nbsp;&nbsp;
                <a href="issueto-details/'.$id.'" class="btn btn-success btn-xs" title="View"><i class="fa fa-newspaper-o"></i></a>&nbsp;&nbsp;
                <a href="'.url('/').'/superadmin/delete-issueto/'.$id.'" title="Delete" class="btn btn-danger btn-xs"  onclick="return confirm('."'Are you sure, you want to delete this Issueto?'".');"><i class="glyphicon glyphicon-trash"></i></a>';
            })->make(true);
        return $issueto;
        exit;
    }

    /**
    * Store a newly created isssue to user resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store_issueto_user(StoreIssuetoUsersPost $request)
    {
        $issueto_id = Input::get('issueto_id');
        $issuetoUser = new IssuetoUsers;
        $issuetoUser->name = Input::get('name');
        $issuetoUser->issueto_id = $issueto_id;
        $issuetoUser->activity = Input::get('activity');
        $issuetoUser->phone = Input::get('phone');
        $issuetoUser->email = Input::get('email');
        $insert_issueto_user = $issuetoUser->save();

        if(!empty($issuetoUser->id)){
            
            return redirect("superadmin/issueto-details/$issueto_id")->with('flash_alert_notice','Issueto user created succesfully.');
        }else{
            return redirect('superadmin/issueto');
        }
    }

    /**
    * Update isssue to user resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function update_issueto_user(StoreIssuetoUsersPost $request)
    {
        $issueto_id = Input::get('issueto_id');
        $issueto_user_id = Input::get('issueto_user_id');
        $issuetoUser = IssuetoUsers::firstOrNew(array('id' => $issueto_user_id));
        $issuetoUser->name = Input::get('name');
        $issuetoUser->issueto_id = $issueto_id;
        $issuetoUser->activity = Input::get('activity');
        $issuetoUser->phone = Input::get('phone');
        $issuetoUser->email = Input::get('email');
        $success = $issuetoUser->save();
        if ($success) {   
            return redirect("superadmin/issueto-details/$issueto_id")->with('flash_alert_notice','Issueto user created succesfully.');
        } else {
            return redirect('superadmin/issueto');
        }
    }

    /**
    * Remove isssue to user resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function destroy_issueto_user(IssuetoUsers $issuetoUsers, $id)
    {
       if (!empty($id)) {
            $id = Crypt::decrypt($id);
            $cur_user = $issuetoUsers->where('id', $id);
            if(!empty($cur_user)){
                $cur_user->update(array('is_deleted' => 1));
            }
        }
        return redirect('superadmin/issueto')->with('flash_alert_notice', 'Issueto user deleted successfully.');
    }

    /**
    * List view by Data table for issue to user data
    */
    public function getIssuetoUsersData($issueto_id)
    {
        $issuetoUsers1 = IssuetoUsers::select(['id', 'name', 'email', 'phone', 'issueto_id','tag'])->where('is_deleted', '=', 0)->where('issueto_id', '=', $issueto_id)->get();
        $issuetoUsers = Datatables::of($issuetoUsers1)->addColumn('tag', function ($itu) {
                        $tags=  json_decode($itu->tag);
                        #print_r($tags);
                        $data = array();
                        $tag = '';
                        if(!is_null($tags)){
                            foreach ($tags as $key => $value) {
                                $data[] = $value->text;
                            }
                            $tag = implode(" , ", $data);
                        }
                        return $tag;
        })->addColumn('action', function ($itu) {
                $id = Crypt::encrypt($itu->id);
                return '<a id="editT" class="btn btn-warning btn-xs edit_ius" IsUID="'.$id.'" Is-Id="'.$itu->issueto_id.'" title="Edit"><i class="fa fa-pencil-square-o"></i></a>&nbsp;&nbsp;<a href="'.url('/').'/superadmin/delete-issueto-user/'.$id.'" title="Delete" class="btn btn-danger btn-xs"  onclick="return confirm('."'Are you sure, you want to delete this Issueto?'".');"><i class="glyphicon glyphicon-trash"></i></a>';
            })->make(true);
        return $issuetoUsers;
        exit;
    }

    /**
    * Fetch isssue to user resource from storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function get_issueto_user_details()
    {
        $id = Input::get('id');
        if (!empty($id)) {
            $issuetoUsers = IssuetoUsers::select(['id', 'issueto_id', 'name', 'email', 'phone', 'activity'])->where('is_deleted', '=', 0)->where('id', '=', $id)->get();
        }
        echo $issuetoUsers;
        exit();
    }

    /**
    * List view by Data table for user Details
    */    
    public function getIssuetoCompanies($issuetoid)
    {
        $companies = Company::select('id','name')->where('is_deleted', '=', 0)->get();
        $issuetoCompniesMap = Helper::getCompanyIssuetoMapping($issuetoid);
        
        $data = array();
        foreach ($companies as $key => $value) {
            
            if(in_array($value->id, $issuetoCompniesMap)){
                $data[$key]['companyId'] = $value->id;
                $data[$key]['name'] = $value->name;
            }
        }
        //print_r($data);
        return json_encode($data);
        exit;
    }

    /**
     * Get associated Company list using DataTables.
     */
    public function getAllCompanies($issueto_id){
        $companies = Company::select('id','name')->where('is_deleted', '=', 0)->get();
        $issuetoCompniesMap = Helper::getCompanyIssuetoMapping($issueto_id);
        
        $data = array();
        foreach ($companies as $key => $value) {
            $data[$key]['companyId'] = $value->id;
            $data[$key]['issuetoId'] = $issueto_id;
            $data[$key]['name'] = $value->name;
            if(in_array($value->id, $issuetoCompniesMap))
                $data[$key]['status'] = "linked";
            else
                $data[$key]['status'] = "unlinked";
        }
        //print_r($data);
        return json_encode($data);
        exit;
    }

    /**
     *  associated Company to respective Project.
     */
    public function associateCompany(){
        //print_r(Input::all()); die;
        $companymap = new CompanyIssuetoMapping;
        $companymap->issueto_id = Input::get('issuetoId');
        $companymap->company_id = Input::get('companyId');
        $success = $companymap->save();
        exit;
    }

    /**
     *  associated Company to respective Project.
     */
    public function unlinkCompany(){
        //print_r(Input::all()); die;
        $issuetoId = Input::get('issuetoId');
        $companyId = Input::get('companyId');
        $companymapid = CompanyIssuetoMapping::where('issueto_id','=',$issuetoId)->where('company_id','=',$companyId)->delete();
        exit;
    }

    /**
     *  get location search query data.
     */
    public function locationTags(){
        $tag = Input::get('tag');
        $results = Location::select('label')->where('label','LIKE',$tag.'%')->get();
        $tags = array();
        foreach ($results as $key => $value) {
            $tags[]['text'] = $value->label;
        }
        //print_r($tags); die;
        return $tags;
    }
}