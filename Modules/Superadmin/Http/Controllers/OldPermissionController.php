<?php

namespace Modules\Superadmin\Http\Controllers;
use App\Models\superadmin\Authentication;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Yajra\Datatables\Facades\Datatables as Datatables;
use mjanssen\BreadcrumbsBundle\Breadcrumbs;
use Illuminate\Support\Facades\Input;
use Modules\Superadmin\Http\Requests\AuthenticationPost;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Response;
use App\Http\Requests;
use Helpers as Helper;
use Validator;
use Auth;
use Paginate;
use Grids;
use HTML;
use Form;
use View;
use URL;
use DB;
use Crypt;

class AuthenticationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {        
        Breadcrumbs::addBreadcrumb('Permission List',  url('superadmin/permission'));        
        $page_data = array('breadcrumbs' => Breadcrumbs::generate());        
        $module_name = Authentication::select(['id','name'])->where('is_deleted',"=",0)->where('module_id',"=",0)->get();
        $module=array(""=>"Please select permission module");
        foreach ($module_name as $key => $value) {
            $module[$value->id] = $value->name;
        }     
        $data['module_list'] = $module;
        $data['scripts'] = array('angularjs/angular.min','angularjs/angular-messages.min','angularjs/angular-flash.min','angularjs/ng-tags-input.min','angularjs/controllers/permissioncontroller','permission');
        return view('superadmin::permission.permission_list',compact('data'))->with($page_data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        Breadcrumbs::addBreadcrumb('Permission List',  url('superadmin/permission'));        
        Breadcrumbs::addBreadcrumb('Add Permission', '');
        $page_data = array('breadcrumbs' => Breadcrumbs::generate());
        $data['scripts'] = array('permission');        
        $module_name = Authentication::select(['id','name'])->where('is_deleted',"=",0)->where('module_id',"=",0)->get();
        $module=array(""=>"Please select permission module");
        foreach ($module_name as $key => $value) {
            $module[$value->id] = $value->name;
        }     
        $data['module_list'] = $module;        
        return view('superadmin::permission.add_permission',compact('data'))->with($page_data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getModule()
    {
        $module_name = Authentication::select(['id','name'])->where('is_deleted',"=",0)->where('module_id',"=",0)->get();
        $module=array();
        foreach ($module_name as $key => $value) {
            $module[$value->id] = $value->name;
        }             
        return $module;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AuthenticationPost $request)
    {                        
        $sys_token = \Session::token();
        $token = Input::get('_token'); 
        if ($token == $sys_token) {   
            $module_name = 0;        
            if (Input::get("type") != 'Menu') {
                $module_name = Input::get("module_id");
            }
            else{
                $module_name = 0;
            }
            $insertInPermission = array(
                "name"  =>  Input::get("name"),
                "type"  =>  Input::get("type"),
                "description" =>  !empty(Input::get("desc"))?Input::get("desc"):'',            
                "module_id"  =>  $module_name,
                "created_by"=> 0,
                "created_at"=> date('Y-m-d'),
                "modified_by"=> 0,          
            );                
            $save = Authentication::create($insertInPermission);
            $success = \Session::flash('flash_alert_notice', 'Permission added successfully !');
            return $success;
        } else {
            return "there was some server error";
        }        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Authentication  $authentication
     * @return \Illuminate\Http\Response
     */
    public function show(Authentication $authentication)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Authentication  $authentication
     * @return \Illuminate\Http\Response
     */
    public function edit(Authentication $authentication,$id=0)
    {        
        if(!empty($id))
        {
            $data['scripts'] = array('permission');
            $permission = $authentication->where('is_deleted','=',0)->find($id);                        
            Breadcrumbs::addBreadcrumb('Permission List', url('superadmin/permission'));
            Breadcrumbs::addBreadcrumb('Edit Permission', '');
            $page_data['breadcrumbs'] = Breadcrumbs::generate();

            $module_name = Authentication::select(['id','name'])->where('is_deleted',"=",0)->where('id',"!=",$id)->where('module_id',"=",0)->get();
            $module=array();
            $module[]="Select Module";
            foreach ($module_name as $key => $value) {
                $module[$value->id] = $value->name;
            }     
            $data['module_list']=$module;

            return view('superadmin::permission.edit_permission',compact('data'))->with('permission', $permission)->with($page_data);
        } else {
            return redirect('superadmin/permission');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Authentication  $authentication
     * @return \Illuminate\Http\Response
     */
    public function update(AuthenticationPost $request, Authentication $authentication)
    {           
        $sys_token = \Session::token();
        $token = Input::get('_token'); 
        if ($token == $sys_token) {
            $module_name = 0;
            if (Input::get("type") != 'Menu') {
                $module_name = Input::get("module_id");
            }            
            $id = Crypt::decrypt(Input::get('id'));
            $authentication = Authentication::firstOrNew(array('id' => $id));
            $authentication->name = Input::get('name');
            $authentication->type = Input::get('type');
            $authentication->description = Input::get('desc');
            $authentication->module_id = $module_name;
            // $authentication->created_by = Auth::user()->id;
            //$authentication->created_at = date('Y-m-d');
            $authentication->modified_by = 0;
            $authentication->modified_at = date('Y-m-d h:i:s');
            $authentication->save();        
            $success = \Session::flash('flash_alert_notice', 'Permission Updated successfully !');
            return $success;
        } else{
            return "there was some server error";
        }
    }

    /**
     * Get the specfic record according to give id in permission
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Authentication  $authentication
     * @return \Illuminate\Http\Response in json
     */
    public function editPermisson($id)
    {
        $id = Crypt::decrypt($id);
        $permission = Authentication::find($id);
        if(!empty($permission)){
            $data = array();
            $data['id'] = Crypt::encrypt($permission->id);
            $data['name'] = $permission->name;
            $data['type'] = $permission->type;
            $data['module_id'] = (String)$permission->module_id;
            $data['desc'] = $permission->description;
            return json_encode($data);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Authentication  $authentication
     * @return \Illuminate\Http\Response
     */
    public function destroy(Authentication $authentication,$id)
    {
        $id = Crypt::decrypt($id);
        $authentication = Authentication::where('id','=',$id)->first();
        $authentication->is_deleted = 1;
        $authentication->save();
        return redirect('superadmin/permission')->with('flash_alert_notice', 'Permission deleted successfully!');
    }

    /**
    * Check the specified permission name is exist
    * And return as true and false
    */
    public function check_isexist($id,$name)
    {                            
        if (isset($name) && !empty($name)) {
            if (!empty($id)) {
                $id = Crypt::decrypt($id);
                $is_current_name = Authentication::where('name',"=",$name)->where('is_deleted',"=",0)->where('id',"=",$id)->get();   

                if (isset($is_current_name[0]) && !empty($is_current_name[0]) && $is_current_name[0]->name == $name) {
                    echo "false";    
                    die;                
                }
            }            
            $is_name = Authentication::where('name',"=",$name)->where('is_deleted',"=",0)->get();                  
            if(isset($is_name[0]) && !empty($is_name[0])) {                    
                echo 'true';
            } else {
                echo 'false';
            }
        }
        die;
    }

    /**
    * List view by Data table for Permission Details
    */
    public function getData(){
        $perm1 = Authentication::select(['id', 'name', 'type', 'description', 'module_id', 'created_at'])->where('is_deleted', '=', 0)->get();        
        $perm = Datatables::of($perm1)->addColumn('module_name', function ($perm) {   
                    $module_name = Authentication::select(['name'])->where('id','=',$perm->module_id)->get();
                    if(isset($module_name[0]) && !empty($module_name[0])) {
                        return  $module_name[0]->name;
                    } else{
                        return "-";
                    }                    
                })->addColumn('created_at', function ($cmp) {
                    $date = $cmp->created_at;
                    $ts   = strtotime($date);
                    return date('Y-m-d', $ts);
                })->addColumn('action', function ($cmp) {
                    $id = Crypt::encrypt($cmp->id);
                return '<a id="editT" class="btn btn-warning btn-xs edit" perid="'.$id.'"><i class="fa fa-pencil-square-o" title="Edit"></i></a>&nbsp;&nbsp;<a href="delete-permission/'.$id.'" title="Delete" class="btn btn-danger btn-xs"  onclick="return confirm('."'Are you sure, you want to delete this user?'".');"><i class="glyphicon glyphicon-trash"></i></a>';
            })->make(true);
        return $perm;die;        
    }
}
