<?php

namespace Modules\Superadmin\Http\Controllers;

use App\Models\superadmin\Permissions;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Yajra\Datatables\Facades\Datatables as Datatables;
use mjanssen\BreadcrumbsBundle\Breadcrumbs;
use Illuminate\Support\Facades\Input;
use Modules\Superadmin\Http\Requests\AuthenticationPost;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Response;
use App\Http\Requests;
use Helpers as Helper;
use Validator;
use Auth;
use Paginate;
use Grids;
use HTML;
use Form;
use View;
use URL;
use DB;
use Crypt;

class PermissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {        
        Breadcrumbs::addBreadcrumb('Permission List',  url('superadmin/permissions'));        
        $page_data = array('breadcrumbs' => Breadcrumbs::generate());        
        $permissions = Permissions::select(['id','name','description'])->get();
        $data['permissions'] = $permissions;
        $data['scripts'] = array('angularjs/angular.min','angularjs/angular-messages.min','angularjs/angular-flash.min','angularjs/permissionModule');
        return view('superadmin::permissions.permission_list',compact('data'))->with($page_data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {                        
        $sys_token = \Session::token();
        $token = Input::get('_token'); 
        if ($token != $sys_token) {
            return "there was some server error";
            exit;
        } 

        $slugArr = array(          // pass an array of permissions.
            'create'     => true,
            'view'       => true,
            'update'     => true,
            'delete'     => true
        );
        $insertInPermission = array(
            "name" =>  Input::get("name"),
            "slug" =>  json_encode($slugArr),
            "description" =>  Input::get("description"),         
            "created_at"=> date('Y-m-d h:i:s'),
        );                
        $save = Permissions::create($insertInPermission);
        $success = \Session::flash('flash_alert_notice', 'Permission added successfully !');
        return $success; 
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Authentication  $authentication
     * @return \Illuminate\Http\Response
     */
    public function show(Permissions $permissions)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Authentication  $authentication
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {        
        $id = Crypt::decrypt($id);
        $permission[0] = Permissions::find($id);
        $data = array();
        foreach ($permission as $key => $value) {
            $data['id'] = Crypt::encrypt($value->id);
            $data['name'] = $value->name;
            $data['description'] = $value->description;
        }
        return json_encode($data);   
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Authentication  $authentication
     * @return \Illuminate\Http\Response
     */
    public function update()
    {           
        $sys_token = \Session::token();
        $token = Input::get('_token'); 
        if ($token != $sys_token) {
            return "there was some server error";
            die;
        }            
        $input = Input::all();    
        $id = Crypt::decrypt($input['id']);
        $permission = Permissions::firstOrNew(array("id"=>$id));
        $permission->name = $input['name'];
        $permission->description = $input['description'];
        //update role //
        $permission->save();        
        $success = \Session::flash('flash_alert_notice', 'Roles updated successfully !');
        return $success; 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Authentication  $authentication
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $id = Crypt::decrypt($id);
        $permission = Permissions::destroy($id);
        //$permission->delete();
        return redirect('superadmin/permissions')->with('flash_alert_notice', 'Permission deleted successfully!');
    }

    /**
    * Check the specified permission name is exist
    * And return as true and false
    */
    public function check_isexist($id,$name)
    {                            
        if (isset($name) && !empty($name)) {
            if (!empty($id)) {
                $id = Crypt::decrypt($id);
                $is_current_name = Authentication::where('name',"=",$name)->where('is_deleted',"=",0)->where('id',"=",$id)->get();   

                if (isset($is_current_name[0]) && !empty($is_current_name[0]) && $is_current_name[0]->name == $name) {
                    echo "false";    
                    die;                
                }
            }            
            $is_name = Authentication::where('name',"=",$name)->where('is_deleted',"=",0)->get();                  
            if(isset($is_name[0]) && !empty($is_name[0])) {                    
                echo 'true';
            } else {
                echo 'false';
            }
        }
        die;
    }

    /**
    * List view by Data table for Permission Details
    */
    public function getData(){
        $permission = Permissions::select(['id', 'name','description','created_at'])->get();        
        $perm = Datatables::of($permission)->addColumn('created_at', function ($cmp) {
                    $date = $cmp->created_at;
                    $ts   = strtotime($date);
                    return date('Y-m-d', $ts);
                })->addColumn('action', function ($cmp) {
                    $id = Crypt::encrypt($cmp->id);
                return '<a id="editT" class="btn btn-warning btn-xs edit" perid="'.$id.'"><i class="fa fa-pencil-square-o" title="Edit"></i></a>&nbsp;&nbsp;<a href="delete-permission/'.$id.'" title="Delete" class="btn btn-danger btn-xs"  onclick="return confirm('."'Are you sure, you want to delete this user?'".');"><i class="glyphicon glyphicon-trash"></i></a>';
            })->make(true);
        return $perm;
        exit;        
    }
}
