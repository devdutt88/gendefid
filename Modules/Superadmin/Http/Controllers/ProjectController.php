<?php

namespace Modules\Superadmin\Http\Controllers;

use App\User;
use App\Superadmin;
use App\Models\superadmin\Project;
use App\Models\superadmin\ProjectUserRole;
use App\Models\superadmin\CompanyProjectMapping;
use App\Models\superadmin\Role;
use App\Models\superadmin\Company;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Redirect;
use Modules\Superadmin\Http\Requests\StoreProjectPost;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Validator;
use Auth;
use Paginate;
use Grids;
use HTML;
use Form;
use View;
use URL; 
use Lang;
use DB;
use File;
use Image;
use Helpers as Helper;
use Yajra\Datatables\Facades\Datatables as Datatables;
use mjanssen\BreadcrumbsBundle\Breadcrumbs;
use Crypt;


class ProjectController extends Controller
{
    /**
     * Display a listing of the Projects.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {         
        $data['scripts'] = array('project');
        Breadcrumbs::addBreadcrumb('Project List',  url('superadmin/project'));
        $page_data = array('breadcrumbs' => Breadcrumbs::generate());
        return view('superadmin::project.project_list',compact('data'))->with($page_data);
    }
    
    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        $data['scripts'] = array('project');
        $page_data = array();
        $page_data['option_type'] = array(
            ""              =>  'Select Project Type',
            "Educational"   =>  'Educational',
            "Industrial"    =>  'Industrial',
            "Offices"       =>  'Offices',
            "Residential"   =>  'Residential',
            "Retail"        =>  'Retail',
            "Other"         =>  'Other'); 

        $page_data['option_state'] = array(
            ""      =>  'Select One',
            "NSW"   =>  'NSW',
            "QLD"   =>  'QLD',
            "SA"    =>  'SA',
            "VIC"   =>  'VIC',
            "WA"    =>  "WA");

        $page_data['option_country'] = array(
            ""          =>  'Select One',
            "Australia" =>  'Australia');
        $page_data['companies'] = Company::select('id', 'name')->where('is_deleted','=', 0)->get();
        $page_data['cur_project'] = '';
        $page_data['proj_comps'] = array();
        Breadcrumbs::addBreadcrumb('Project List',  url('superadmin/project'));
        Breadcrumbs::addBreadcrumb('Add Project', '');
        $page_data['breadcrumbs'] = Breadcrumbs::generate();
        return view('superadmin::project.add_project',compact('data'))->with($page_data);
    }

    /**
     * Save new Project and associate with all mapping tables.
     *
     */
    public function store(StoreProjectPost $request)
    {   
        $project = new Project;
        $project->name = Input::get('name');
        $project->code = str_slug(Input::get('name'));
        $project->company_id = Input::get('company');
        $project->type = Input::get('type');
        $project->description = Input::get('description'); 
        $project->address1 = Input::get('address1'); 
        $project->address2 = Input::get('address2');
        $project->geo_code = ""; 
        $project->suburb = Input::get('suburb'); 
        $project->state = Input::get('state'); 
        $project->country = Input::get('country');
        $project->postal_code = Input::get('postal_code');
        $project->created_by = 0; 
        $project->created_at = date('Y-m-d h:i:s'); 
        $insert_project = $project->save();
        
        if (!empty($project->id)) {           
            return redirect('superadmin/project')->with('flash_alert_notice', 'Project created succesfully.');        
        } else {
            return redirect('superadmin/project');
        }
    }

    /**
     * Display specified Project details.
     *
     */
    public function show(Project $project,$id)
    {
        $id = Crypt::decrypt($id);
        Breadcrumbs::addBreadcrumb('Project List',  url('superadmin/project'));
        Breadcrumbs::addBreadcrumb('Project Details', '');
        $page_data = array('breadcrumbs' => Breadcrumbs::generate());

        //Get specific user details according to id
        $page_data['project_companies'] =  Helper::getCompanyProjectMapping($id);
        $page_data['companies'] =  Helper::getCompanyList();
        $page_data['project'] = $project->where('is_deleted','=',0)->find($id);

        $comp_detail = CompanyProjectMapping::where('is_deleted','=',0);
        if (empty($comp_detail)) {
            return redirect(url('superadmin/project'));
        } else {
            $page_data['logo_image'] = 'default-profile.png';
            $page_data['option_type'] = array(
                "Educational"   =>  'Educational',
                "Industrial"    =>  'Industrial',
                "Offices"       =>  'Offices',
                "Residential"   =>  'Residential',
                "Retail"        =>  'Retail',
                "Other"         =>  'Other'); 

            $page_data['option_state'] = array(
                "NSW"   =>  'NSW',
                "QLD"   =>  'QLD',
                "SA"    =>  'SA',
                "VIC"   =>  'VIC',
                "WA"    =>  "WA");

            $page_data['option_country'] = array(
                "Australia" =>  'Australia');

            $page_data['companies'] = Company::select('id', 'name')->where('is_deleted', '=', 0)->orderBy('name', 'asc')->get();
            $proj_comps = array();
            foreach($comp_detail as $comp){
                $proj_comps[] = $comp->company_id;
            }
            $page_data['comp_detail'] = $comp_detail;
            $page_data['proj_comps'] = $proj_comps;
        }           
        if (!empty($page_data['project'])) {
            $data['scripts'] = array('angularjs/angular.min','angularjs/angular-messages.min','angularjs/angular-flash.min','angularjs/controllers/projectcontroller', 'project');
            return view('superadmin::project.show_project',compact('data'))->with($page_data);
        } else {
            return redirect('superadmin/project');
        }
    }

    /**
     * Edit specified Project details.
     *
     */
    public function edit($id,$projectform)
    {
        $id = Crypt::decrypt($id);
        if ($id == 0) {
            die("error message");
        }
        switch ($projectform) {

            case 'details':
                $project = Project::find($id);
                $projectdata = array();
                $projectdata['name'] = $project->name;
                $projectdata['code'] = $project->code;
                $projectdata['type'] = $project->type; 
                $projectdata['description'] = $project->description;
                $projectdata['address1'] = $project->address1;
                $projectdata['address2'] = $project->address2;
                $projectdata['suburb'] = $project->suburb; 
                $projectdata['state'] = $project->state; 
                $projectdata['postal_code'] = $project->postal_code; 
                $projectdata['country'] = $project->country;
                $projectdata['geo_code'] = $project->geo_code;
                return json_encode($projectdata);
            break;

            default:
                die("error message");
            break;
        }
        
    }

    /**
     * Update the specified Project details.
     *
     */
    public function update()
    {
        $project_id = Input::get('id');
        $project_id = Crypt::decrypt($project_id);
        $project_form = Input::get('projectform');
        switch ($project_form) {
            case 'details':
                $project = Project::firstOrNew(array('id' => $project_id));
                $project->name = Input::get('name');
                $project->code = str_slug(Input::get('name'));
                $project->type = Input::get('type'); 
                $project->description = Input::get('description'); 
                $project->address1 = Input::get('address1');
                $project->address2 = Input::get('address2');
                $project->suburb = Input::get('suburb');
                $project->state = Input::get('state');
                $project->postal_code = Input::get('postal_code');
                $project->country = Input::get('country');
                $project->geo_code = "";
                $success = $project->save();
                if ($success) {
                    $success = \Session::flash('flash_alert_notice', 'Project detials updated successfully !');
                    return $success;
                } else {
                    return "there was some server error";
                }
            break;

            case 'logo':
                $name = Input::get('name');
                $id = Input::get('id');
                $id = Crypt::decrypt($id);
                $file = Input::file('logo'); 
                
                // Make directory if  not exist
                $temp_path = public_path('uploads/project/temp/');
                if (!File::exists($temp_path)) {
                    File::makeDirectory($temp_path, 0777, true, true);
                }
                $path_200 = public_path('uploads/project/thumbnail_200x200/');
                if (!File::exists($path_200)) {
                    File::makeDirectory($path_200, 0777, true, true);
                }
                $path_29 = public_path('uploads/project/thumbnail_29x29/');
                if (!File::exists($path_29)) {
                    File::makeDirectory($path_29, 0777, true, true);
                }
                if ($file) {
                    $image_name = $file->getClientOriginalName();
                    $img_path = public_path('uploads/project/');
                    $extension = $file->getClientOriginalExtension(); // getting image extension
                    $fileName = $id . '_' .$name.".". $extension;
                    $uploaded_at = $file->move($temp_path, $fileName);
                    // Main image thumbnail
                    $img = Image::make($temp_path.$fileName)->resize(200,200);
                    if (file_exists($img_path.$fileName)) {
                            unlink($img_path.$fileName);
                    }
                    $save_thumb = $img->save($img_path.$fileName);
                    // 200x200 image thumbnail
                    $img_thumb1 = Image::make($temp_path.$fileName)->resize(200,200);
                    if (file_exists($img_path. "thumbnail_200x200/" .$fileName)) {
                            unlink($img_path. "thumbnail_200x200/" .$fileName);
                    }                
                    $save_thumb = $img_thumb1->save($img_path. "thumbnail_200x200/" .$fileName);
                    // 29x29 image thumbnail
                    $img_thumb2 = Image::make($temp_path.$fileName)->resize(29,29);
                    if (file_exists($img_path. "thumbnail_29x29/" .$fileName)) {
                            unlink($img_path. "thumbnail_29x29/" .$fileName);
                        }
                    $save_thumb = $img_thumb2->save($img_path . "thumbnail_29x29/" .$fileName);
                    // Remove temp folder image 
                    if (file_exists($temp_path.$fileName)) {
                        unlink($temp_path.$fileName);
                    }
                    
                } else {
                    $fileName = '';
                }
                // update project table data
                $project = Project::firstOrNew(array('id' => $id));
                $project->id = $id;
                $project->logo = $fileName;
                $success = $project->save();
                if ($success) {
                    $success = \Session::flash('flash_alert_notice', 'Logo has been updated successfully !');
                    return $success;
                } else {
                    return "there was some server error";
                }
            break;

            default:
                return redirect('superadmin/project')->with('flash_alert_notice', 'There was nothing to update.');
            break;
        }
       
    }

    public function projectIsexist($id){
        //$id = ($enc_id != '0') ? Crypt::decrypt($enc_id) : 0;
        $name = Input::get('name');
        if (!empty($name)){
            $jq = Input::get('jq');
            $is_name = ($id != '0') ? Project::where('name',"=",$name)->where('is_deleted',"=",0)->where('id',"!=",$id)->get() : Project::where('name',"=",$name)->where('is_deleted',"=",0)->get();
            
            if (isset($is_name[0]) && !empty($is_name[0])){
                echo $jq?'false':'true';
            }else{
                echo $jq?'true':'false';
            }
        }
        exit;
    }

    /**
     * Soft remove the specified Project and all its mapping.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(Project $project, $id)
    {   
        $id = Crypt::decrypt($id);
        $user = Project::where('id','=',$id)->first();
        $user->is_deleted = 1;
        $user->save();
        return redirect('superadmin/project')->with('flash_alert_notice', 'Project deleted successfully.');
    }
    
    /**
     * Get Project list using DataTables.
     *
     */
    public function getData(){
        $projectdata = Project::select(['id', 'name', 'company_id', 'type', 'created_by', 'created_at'])->get(); 
        $project = Datatables::of($projectdata)->addColumn('auther', function ($projectdata) {  
            if($projectdata->created_by == 0) 
            {
                $auther = Superadmin::select(['name'])->get();
            }else{
                $auther = User::select(['name'])->where('id','=',$projectdata->created_by)->get();
            }
            if(!empty($auther[0]) && !empty($auther[0]->name)){
                return $auther[0]->name;
            }
            else{
                return 'Superadmin';
            }
            })->addColumn('company', function ($projectdata) {  
                $company_name = Company::select('name')->where('id',$projectdata->company_id)->first();
                return $company_name?ucfirst($company_name->name):'-';
            })->addColumn('created_at', function ($cmp) {
                if(empty($cmp->created_at))
                {
                    return '';
                }else{
                    $date = $cmp->created_at;
                    $ts   = strtotime($date);
                    return date('Y-m-d', $ts);
                }
            })->addColumn('action', function ($prj) {
                $id = Crypt::encrypt($prj->id);
                return '<a href="project-details/'.$id.'" class="btn btn-warning btn-xs"><i class="fa fa-newspaper-o" title="View"></i></a>&nbsp;&nbsp;<a href="delete-project/'.$id.'" title="Delete" class="btn btn-danger btn-xs"  onclick="return confirm('."'Are you sure, you want to delete this Project?'".');"><i class="fa fa-trash-o"></i></a>';
            })->make(true);
            return $project;
        exit;        
    }

    /**
    * List view by Data table for user Details
    */    
    public function getProjectCompanies($projectid)
    {        
        $companies = Company::select('id','name')->where('is_deleted', '=', 0)->get();
        $projectCompniesMap = Helper::getCompanyProjectMapping($projectid);
        
        $data = array();
        foreach ($companies as $key => $value) {
            
            if(in_array($value->id, $projectCompniesMap)){
                $data[$key]['companyId'] = $value->id;
                $data[$key]['name'] = $value->name;
            }
        }
        //print_r($data);
        return json_encode($data);
        exit;
    }

    /**
     * Get associated Company list using DataTables.
     */
    public function getAllCompanies($project_id){
        $companies = Company::select('id','name')->where('is_deleted', '=', 0)->get();
        $projCompniesMap = Helper::getCompanyProjectMapping($project_id);
        
        $data = array();
        foreach ($companies as $key => $value) {
            $data[$key]['companyId'] = $value->id;
            $data[$key]['projectId'] = $project_id;
            $data[$key]['name'] = $value->name;
            if(in_array($value->id, $projCompniesMap))
                $data[$key]['status'] = "linked";
            else
                $data[$key]['status'] = "unlinked";
        }
        //print_r($data);
        return json_encode($data);
        exit;
    }

    /**
     *  associated Company to respective Project.
     */
    public function associateCompany(){
        //print_r(Input::all()); die;
        $companymap = new CompanyProjectMapping;
        $companymap->project_id = Input::get('projectId');
        $companymap->company_id = Input::get('companyId');
        $success = $companymap->save();
        exit;
    }

    /**
     *  associated Company to respective Project.
     */
    public function unlinkCompany(){
        //print_r(Input::all()); die;
        $projectId = Input::get('projectId');
        $companyId = Input::get('companyId');
        $companymapid = CompanyProjectMapping::where('project_id','=',$projectId)->where('company_id','=',$companyId)->delete();
        exit;
    }

    /**
    * Check the specified name and email is exist
    * And return as true and false
    */
    public function check_isexist(){
        $projName = Input::get('name');
        $id = Input::get('id');
        if (isset($projName) && !empty($projName)){
            if (!empty($id)){
                $is_current_name = Project::where('name',"=",$projName)->where('is_deleted',"=",0)->where('id',"=",$id)->get();
                if (isset($is_current_name[0]) && !empty($is_current_name[0]) && $is_current_name[0]->name == $projName)
                {
                    echo "true";
                    die;
                }
            }
            $is_projName = Project::where('name',"=",$projName)->where('is_deleted',"=",0)->get();
            if (isset($is_projName[0]) && !empty($is_projName[0])){
                echo 'false';
            }else{
                echo 'true';
            }
        }
        die;
    }

    public function generatePDF(){
        // ini_set('memory_limit', '-1');
        // $html = '<table>
        //   <tr>
        //     <th>Company</th>
        //     <th>Contact</th>
        //     <th>Country</th>
        //   </tr>';
        // for($i= 0; $i <= 100000; $i++){
            
        //     $html .= '<tr>
        //                 <td>Alfreds Futterkiste</td>
        //                 <td>Maria Anders</td>
        //                 <td>Germany</td>
        //               </tr>';
        // }
        // $html .= '</table>';

        // \PDF::loadHTML($html)->setPaper('a4')->setOrientation('landscape')->setOption('margin-bottom', 0)->save('table.pdf');
        echo "uncomment the code";
    }
}
