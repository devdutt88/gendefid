<?php

namespace Modules\Superadmin\Http\Controllers;

use App\Models\superadmin\QualityChecklist;
use App\Models\superadmin\QualityChecklistTask;
use App\Models\superadmin\Company;
use App\Models\superadmin\CompanyChecklistMapping;
use App\Models\projectConfiguration\Location;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Redirect;
use Modules\Superadmin\Http\Requests\StoreChecklistPost;
use Modules\Superadmin\Http\Requests\StoreChecklistTaskPost;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Helpers as Helper;
use Validator;
use Paginate;
use Grids;
use Crypt;
use HTML;
use Form;
use View;
use Auth;
use URL;
use DB;
use Yajra\Datatables\Facades\Datatables as Datatables;
use mjanssen\BreadcrumbsBundle\Breadcrumbs;

class QualityChecklistController extends Controller
{
    /**
     * Display a listing of quality checklist.
     *
     */
    public function index()
    {
        Breadcrumbs::addBreadcrumb('Quality Checklist',  url('superadmin/quality-checklist'));
        $page_data = array('breadcrumbs' => Breadcrumbs::generate());
        $data['scripts'] = array('quality_checklist');
        $page_data['obj_companies'] = Company::select('id', 'name')->where('is_deleted','=', 0)->get();
        $page_data['companies'] =  Helper::getCompanyList();
        $page_data['checklist_comps'] = array();
        $page_data['cur_checklist'] = '';
        $checklist = QualityChecklist::where('is_deleted', '=', 0)->paginate(10);
        $data['scripts'] = array('angularjs/angular.min','angularjs/angular-flash.min','angularjs/controllers/qualitychecklistcontroller','angularjs/ng-tags-input.min','quality_checklist');
        return view('superadmin::quality_checklist.quality_checklist',compact('data','page_title','page_action','viewPage','viewPage1'))->with($page_data);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function show(QualityChecklist $checklist,$enc_id)
    {
        $id = ($enc_id!='') ? Crypt::decrypt($enc_id) : 0;
        Breadcrumbs::addBreadcrumb('Quality Checklist',  url('superadmin/quality-checklist'));
        Breadcrumbs::addBreadcrumb('Quality Checklist Details', '');
        $page_data = array('breadcrumbs' => Breadcrumbs::generate());
        $page_data['checklist_comps'] =  Helper::getCompanyChecklistMapping($id);
        $page_data['obj_companies'] =  Company::select('id', 'name')->where('is_deleted', '=', 0)->orderBy('name', 'asc')->get();
        $page_data['companies'] =  Helper::getCompanyList();
        $page_data['checklist'] = $checklist->where('is_deleted','=',0)->find($id);
        $page_data['checklist_id'] = $enc_id;
        $data['scripts'] = array('angularjs/angular.min','angularjs/angular-flash.min','angularjs/controllers/qualitychecklistcontroller','angularjs/ng-tags-input.min','quality_checklist');
        if (!empty($page_data['checklist'])) {
            return view('superadmin::quality_checklist.show_quality_checklist',compact('data','page_title','page_action','viewPage','viewPage1'))->with($page_data);
        } else {
            return redirect('superadmin/quality-checklist');
        }
    }

    /**
     * store new or update existing quality checklist details.
     *
     */
    public function saveChecklist(StoreChecklistPost $request)
    {
        //print_r(Input::all()); die;
        $id = (Input::get('id')) ? Input::get('id') : 0;
        $checklist = QualityChecklist::firstOrNew(array('id' => $id));
        $checklist->name = Input::get('name');
        $checklist->tags = json_encode(Input::get('tags'));
        $checklist->created_by = 0;
        $checklist->created_at = date('Y-m-d h:i:s');
        $success = $checklist->save();
        if($success){
            $success_alert = \Session::flash('flash_alert_notice', ($id!=0) ? 'Checklist updated successfully !' : 'Checklist created successfully !');
            return $success_alert;
        }
    }

    /**
     * get quality checklist details.
     *
     */
    public function editChecklist($enc_id)
    {
        $id = Crypt::decrypt($enc_id);
        $checklist = QualityChecklist::find($id);
        if(!empty($checklist)){
            $data = array();
            $data['id'] = $checklist->id;
            $data['name'] = $checklist->name;
            $data['tags'] = json_decode($checklist->tags);
            return json_encode($data);
        }
    }

    /**
     * soft delete existing quality checklist details.
     *
     */
    public function destroyChecklist(QualityChecklist $checklist,$enc_id)
    {
        $id = Crypt::decrypt($enc_id);
        if (!empty($id)) {
            $cur_checklist = $checklist->where('id', $id);
            if (!empty($cur_checklist)) {
                $cur_checklist->update(array('is_deleted' => 1));
                $checklistTasks = QualityChecklistTask::where('checklist_id','=',$id);
                if (!empty($checklistTask)) {
                    $checklistTask->update(array('is_deleted' => 1));
                }
            }
        }
        return redirect('superadmin/quality-checklist')->with('flash_alert_notice', 'Quality checklist deleted successfully.');
    }

    /**
     * store new or update existing quality checklist task details.
     *
     */
    public function saveChecklistTask(StoreChecklistTaskPost $request)
    {        
        $id = (Input::get('id')) ? Input::get('id') : 0;
        $checklistTask = QualityChecklistTask::firstOrNew(array('id' => $id));
        $checklistTask->checklist_id = Crypt::decrypt(Input::get('checklist_id'));
        $checklistTask->name = Input::get('name');
        $checklistTask->holdpoint = Input::get('holdpoint');
        $checklistTask->comment = Input::get('comment');
        $checklistTask->modified_by = 0;
        $checklistTask->created_by = 0;
        $checklistTask->created_at = date('Y-m-d h:i:s');
        $success = $checklistTask->save();
        
        if($success){
            $success_alert = \Session::flash('flash_alert_notice', ($id!=0) ? 'Checklist Task updated successfully !' : 'Checklist Task created successfully !');
            return $success_alert;
        }
    }

    /**
     * get quality checklist task details.
     *
     */
    public function editChecklistTask($enc_id)
    {
        $id = Crypt::decrypt($enc_id);
        $checklistTask = QualityChecklistTask::find($id);
        if(!empty($checklistTask)){
            $data = array();
            $data['id'] = $checklistTask->id;
            $data['name'] = $checklistTask->name;
            $data['holdpoint'] = (string)$checklistTask->holdpoint;
            $data['comment'] = $checklistTask->comment;
            $data['checklist_id'] = $checklistTask->checklist_id;
            return json_encode($data);
        }
    }

    /**
     * soft delete existing quality checklist task details.
     *
     */
    public function destroyChecklistTask(QualityChecklistTask $checklistTask,$enc_id)
    {
        if (!empty($enc_id)) {
            $id = Crypt::decrypt($enc_id);
            $cur_task = $checklistTask->where('id', $id);
            if(!empty($cur_task)){
                $cur_task->update(array('is_deleted' => 1));
            }
        }
        return redirect('superadmin/quality-checklist')->with('flash_alert_notice', 'Checklist task deleted successfully.');
    }

    /**
    * Check the specified checklist name is exist
    * And return as true and false
    */
    public function checklist_isexist()
    {
        $name = Input::get('checklist_name');
        $id = Input::get('id');
        if (isset($name) && !empty($name)) {
            if (!empty($id)) {
                $is_current_name = QualityChecklist::where('name',"=",$name)->where('is_deleted',"=",0)->where('id',"=",$id)->get();   
                if(isset($is_current_name[0]) && !empty($is_current_name[0]) && $is_current_name[0]->name == $name) {
                    echo "true";
                    die;
                }
            }
            $is_name = QualityChecklist::where('name',"=",$name)->where('is_deleted',"=",0)->get();
            if (isset($is_name[0]) && !empty($name[0])) {
                echo 'false';
            } else {
                echo 'true';
            }
        }
        die;
    }

    /**
    * Check the specified checklist task is exist
    * And return as true and false
    */
    public function checklist_task_isexist()
    {
        $name = Input::get('chk_task_name');
        $id = Input::get('id');
        $checklist_id = Input::get('checklist_id');
        if (isset($name) && !empty($name)) {
            if (!empty($id)) {
                $is_current_name = QualityChecklistTask::where('name',"=",$name)->where('is_deleted',"=",0)->where('id',"=",$id)->where('checklist_id',"=",$checklist_id)->get();   
                if(isset($is_current_name[0]) && !empty($is_current_name[0]) && $is_current_name[0]->name == $name) {
                    echo "true";
                    die;
                }
            }
            $is_name = QualityChecklistTask::where('name',"=",$name)->where('is_deleted',"=",0)->where('checklist_id',"=",$checklist_id)->get();
            if (isset($is_name[0]) && !empty($is_name[0])) {
                echo 'false';
            } else {
                echo 'true';
            }
        }
        die;
    }

    /**
    * List view by Data table for checklist data
    */
     public function getChecklistData(){
        $checklist1 = QualityChecklist::select(['id', 'name', 'tags','created_at'])->where('is_deleted', '=', 0)->orderBy('id', 'asc')->get();
        $checklist = Datatables::of($checklist1)->addColumn('tags', function ($qc) {
                        $tags=  json_decode($qc->tags);
                        #print_r($tags);
                        $data = array();
                        $tag = '';
                        if(!is_null($tags)){
                            foreach ($tags as $key => $value) {
                                $data[] = $value->text;
                            }
                            $tag = implode(" , ", $data);
                        }
                        return $tag;
        })->addColumn('modified_at', function ($cmp) {
                $date = $cmp->modified_at;
                $ts   = strtotime($date);
                return date('Y-m-d', $ts);
            })->addColumn('action', function ($qc) {
            $qc_id = Crypt::encrypt($qc->id);
            return '<a is-id="'.$qc_id.'" class="btn btn-warning btn-xs edit-ist" id="editChecklist" title="Edit"><i class="fa fa-pencil-square-o"></i></a>&nbsp;&nbsp;<a href="quality-checklist-details/'.$qc_id.'" class="btn btn-success btn-xs" title="View"><i class="fa fa-newspaper-o"></i></a>&nbsp;&nbsp;<a href="'.url('/').'/superadmin/delete-quality-checklist/'.$qc_id.'" title="Delete" class="btn btn-danger btn-xs"  onclick="return confirm('."'Are you sure, you want to delete this Checklist?'".');"><i class="glyphicon glyphicon-trash"></i></a>';
        })->make(true);
        return $checklist;
        die;
    }

    /**
    * List view by Data table for check list task data
    */
    public function getChecklistTaskData($enc_checklist_id)
    {
        $checklist_id = Crypt::decrypt($enc_checklist_id);
        $checklistTask1 = QualityChecklistTask::select(['id', 'name', 'holdpoint', 'comment', 'created_at'])->where('is_deleted', '=', 0)->where('checklist_id', '=', $checklist_id)->get();
        $checklistTask = Datatables::of($checklistTask1)->addColumn('created_at', function ($cmp) {
                $date = $cmp->created_at;
                $ts   = strtotime($date);
                return date('Y-m-d', $ts);
            })->addColumn('action', function ($qct) {
            $qct_id = Crypt::encrypt($qct->id);
                return '<a id="editT" class="btn btn-warning btn-xs edit_ius" IsUID="'.$qct_id.'" title="Edit"><i class="fa fa-pencil-square-o"></i></a>&nbsp;&nbsp;<a href="'.url('/').'/superadmin/delete-quality-checklist-task/'.$qct_id.'" title="Delete" class="btn btn-danger btn-xs""  onclick="return confirm('."'Are you sure, you want to delete this Task?'".');"><i class="glyphicon glyphicon-trash"></i></a>';
            })->make(true);
        return $checklistTask;die;
    }

    /**
    * List view by Data table for user Details
    */    
    public function getChecklistCompanies($checklistid)
    {        
        $companies = Company::select('id','name')->where('is_deleted', '=', 0)->get();
        $checklistCompniesMap = Helper::getCompanyChecklistMapping($checklistid);
        
        $data = array();
        foreach ($companies as $key => $value) {
            
            if(in_array($value->id, $checklistCompniesMap)){
                $data[$key]['companyId'] = $value->id;
                $data[$key]['name'] = $value->name;
            }
        }
        //print_r($data);
        return json_encode($data);
        exit;           
    }

    /**
     * Get associated Company list using DataTables.
     */
    public function getAllCompanies($enc_checklist_id){
        $checklist_id = $enc_checklist_id;
        $companies = Company::select('id','name')->where('is_deleted', '=', 0)->get();
        $checklistCompniesMap = Helper::getCompanyChecklistMapping($checklist_id);
        
        $data = array();
        foreach ($companies as $key => $value) {
            $data[$key]['companyId'] = $value->id;
            $data[$key]['checklistId'] = $enc_checklist_id;
            $data[$key]['name'] = $value->name;
            if(in_array($value->id, $checklistCompniesMap))
                $data[$key]['status'] = "linked";
            else
                $data[$key]['status'] = "unlinked";
        }
        //print_r($data);
        return json_encode($data);
        exit;
    }

    /**
     *  associate a Company to respective Quality Checklist.
     */
    public function associateCompany(){
        $companymap = new CompanyChecklistMapping;
        //$companymap->checklist_id = Crypt::decrypt(Input::get('checklistId'));
        $companymap->checklist_id = Input::get('checklistId');
        $companymap->company_id = Input::get('companyId');
        $success = $companymap->save();
        exit;
    }

    /**
     *  unlink Company to respective Quality Checklist.
     */
    public function unlinkCompany(){
        //$checklistId = Crypt::decrypt(Input::get('checklistId'));
        $checklistId = Input::get('checklistId');
        $companyId = Input::get('companyId');
        $companymapid = CompanyChecklistMapping::where('checklist_id','=',$checklistId)->where('company_id','=',$companyId)->delete();
        exit;
    }

    /**
     *  get location search query data.
     */
    public function locationTags(){
        $tag = Input::get('tag');
        $results = Location::select('label')->where('label','LIKE',$tag.'%')->get();
        $tags = array();
        foreach ($results as $key => $value) {
            $tags[]['text'] = $value->label;
        }
        //print_r($tags); die;
        return $tags;
    }
}
