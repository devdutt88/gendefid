<?php

namespace Modules\Superadmin\Http\Controllers;

use App\Models\superadmin\Role;
use App\Models\superadmin\Authentication;
use App\Models\superadmin\RoleAuthMap;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Yajra\Datatables\Facades\Datatables as Datatables;
use mjanssen\BreadcrumbsBundle\Breadcrumbs;
use Illuminate\Support\Facades\Input;
use Auth;
use Crypt;
use Helpers as Helper;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {        
        Breadcrumbs::addBreadcrumb('roles',  url('superadmin/role'));        
        $page_data = array('breadcrumbs' => Breadcrumbs::generate());
        $page_data['role'] = Role::where('is_deleted','=',0)->get();
        $data['scripts'] = array('angularjs/angular.min',
                                'jquery.dataTables.min',
                                'angularjs/angular-datatables.min',
                                'angularjs/angular-messages.min',
                                'angularjs/angular-flash.min',
                                'angularjs/controllers/rolecontroller'
                        );
        return view('superadmin::role.role_list',compact('data'))->with($page_data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {        
        $authentication = Authentication::where('is_deleted','=',0)->get();        
        $parent = array();
        $child = array();
        foreach ($authentication as $key => $value) {
            if ($value->module_id == 0) {
                $parent[$value->id] = $value->name;
            } else {
                $child[$value->module_id][$value->id] = $value->name;
            }
        }
        $data_tree = array();
        foreach ($parent as $key => $p_value) {
            if (!empty($child[$key])) {
                $data_tree[$key] = array("parent"=>$p_value,'child'=>$child[$key]);
            } else {
                $data_tree[$key] = array("parent"=>$p_value);
            }
        }        
        Breadcrumbs::addBreadcrumb('Role List',  url('superadmin/role'));        
        Breadcrumbs::addBreadcrumb('Add Role', '');
        $page_data = array('breadcrumbs' => Breadcrumbs::generate());
        $data['scripts'] = array('logger','treeview','role');
        return view('superadmin::role.add_role',compact('data','data_tree'))->with($page_data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {                
        $input = Input::all();        
        $sys_token = \Session::token();
        $token = Input::get('_token'); 
        if ($token != $sys_token) {
            return "there was some server error";
            die;
        }                
        $authentication = Authentication::where('is_deleted','=',0)->get();        
        $insertInRole = array(
            "name"  =>  $input["name"],
            "created_by" => 0,
            "created_at" => date('Y-m-d h:i:s')
            );
        $save = Role::create($insertInRole);  
        #$permission = Input::get("permission");        
        foreach ($authentication as $key => $value) {            
            $insertInRoleAuthMap['role_id'] = $save->id;
            $insertInRoleAuthMap["authorisation_id"] = $value->id;
            $insertInRoleAuthMap["status"] = 0;
            $company_details = RoleAuthMap::create($insertInRoleAuthMap);            
            $insertInRoleAuthMap = array();
        }
        $success = \Session::flash('flash_alert_notice', 'Role added successfully !');
        return $success;        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function show(Role $role,$id)
    {        
        $id = Crypt::decrypt($id);
        
        Breadcrumbs::addBreadcrumb('Roles',  url('superadmin/role'));       
        Breadcrumbs::addBreadcrumb('View',  url('superadmin/role'));        
        $page_data['breadcrumbs'] = Breadcrumbs::generate();
        $page_data['role'] = Role::where('id','=',$id)->first(); 
        //$page_data['role'] = Role::find($id); 
        $data['scripts'] = array('angularjs/angular.min',
                                'angularjs/angular-ui-tree',
                                'angularjs/controllers/permissionTree'
                        );
        return view('superadmin::role.show_role',compact('data','allparent','allchild'))->with($page_data);
    }

    /**
     * fetch the specified resource.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function getRole(Role $role,$id)
    {   
        $id = Crypt::decrypt($id);
        $role = Role::where('is_deleted','=',0)->where('id','=',$id)->first();
        $roles = $role->roleAuthDetails->where('status','=',1); 
        $auth_id = array();
        foreach($roles as $r) {
            $auth_id[] = (String)$r->authorisation_id;
        }
        return json_encode($auth_id);
        die;
    }

    /**
     * API REQUEST 
     * return permission of the role
     ***
     */
    public function getRolesMenu($roleid){
        
        $authentication = Authentication::where('is_deleted','=',0)->get(); 
        $element = array();       
        foreach ($authentication as $key => $value) {
            $element[$key]['id'] = $value->id;
            $element[$key]['name'] = $value->name;
            $element[$key]['parent_id'] = $value->module_id;
        }
        
        $permissionTree = Helper::buildTree($element);

        // $menuauth = RoleAuthMap::where('role_id', '=', $roleid)->get();
        // $menuauthArr = array();
        // foreach ($menuauth as $key => $value) {
            
        //     //if($value->roleAuthMapping[0]->module_id == 0){
        //         $menuauthArr[$key]['name'] = $value->rolePermissions[0]->name;
        //         //$menuauthArr[$key]['name'] = $value->name;
        //         $menuauthArr[$key]['status'] = $value->status;
        //         $menuauthArr[$key]['id'] = $value->authorisation_id;
        //     //} 
        // }

        //$menuauthArr = Helper::super_unique($menuauthArr); 
        //echo "<pre>"; print_r($permissionTree); die;
        return json_encode($permissionTree);
        exit;
    }

    

    /**
     * Store a newly created permission for roles in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeRolePermission(Request $request)
    {        
        $sys_token = \Session::token();
        $token = Input::get('_token'); 
        if ($token != $sys_token) {
            return "there was some server error";
            die;
        }        
        $id = Crypt::decrypt(Input::get('id'));
        $permission = Input::get('permissionData');
        foreach ($permission as $key => $perm_id) {
            $roleAuth = RoleAuthMap::firstOrNew(array("authorisation_id"=>$perm_id,'role_id'=>$id));
            $roleAuth->authorisation_id = $perm_id;
            $roleAuth->role_id = $id;
            $roleAuth->status = 1;
            $roleAuth->save();
        } 
        $uncheckData = Input::get('uncheckData');  
        $data = array();  
        foreach ($uncheckData as $key => $value) {
            if(!in_array($value, $permission)){
                $data[] = $value;
            }
        }
        foreach ($data as $key => $perm_id) {
            $roleAuth = RoleAuthMap::firstOrNew(array("authorisation_id"=>$perm_id,'role_id'=>$id));
            $roleAuth->authorisation_id = $perm_id;
            $roleAuth->role_id = $id;
            $roleAuth->status = 0;
            $roleAuth->save();
        }       
        return json_encode(array('msg'=>"Permission Added successfully!"));
    }

    /**
     * Get the specfic record according to give id in role
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Authentication  $authentication
     * @return \Illuminate\Http\Response in json
     */
    public function editRole($id)
    {
        $id = Crypt::decrypt($id);
        $role[0] = Role::find($id);
        $data = array();
        $i = 0;        
        foreach ($role as $key => $value) {
            $data['id'] = Crypt::encrypt($value->id);
            $data['name'] = $value->name;
            $data['description'] = $value->description;
            $i++;
        }
        return json_encode($data);        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Role $role)
    {                        
        $sys_token = \Session::token();
        $token = Input::get('_token'); 
        if ($token != $sys_token) {
            return "there was some server error";
            die;
        }            
        $input = Input::all();    
        $id = Crypt::decrypt($input['id']);
        $roles = Role::firstOrNew(array("id"=>$id));
        $roles->name = $input['name'];
        $roles->description = $input['description'];
        $roles->modified_by = 0;
        $roles->save();        
        $success = \Session::flash('flash_alert_notice', 'Role updated successfully !');
        return $success;        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function destroy(Role $role,$id)
    {
        $role = Role::where('id','=',$id)->first();
        $role->is_deleted = 1;
        $role->save();
        return redirect('superadmin/role')->with('flash_alert_notice', 'Role deleted successfully!');
    }
    /**
    * Check the specified Role is exist
    * And return as true and false
    */
    public function checkRole($id,$cname)
    {    
        #$cname = Input::get('name');            
        // echo $cname;die;
        // $id = Input::get('id');        
        if (isset($cname) && !empty($cname)) {
            if (!empty($id)) {
                $id = Crypt::decrypt($id);
                $is_current_name = Role::where('name',"=",$cname)->where('is_deleted',"=",0)->where('id',"=",$id)->get();   
                if (isset($is_current_name[0]) && !empty($is_current_name[0]) && $is_current_name[0]->name == $cname)
                {
                    return "false";
                    die;
                }
            }            
            $is_cname = Role::where('name',"=",$cname)->where('is_deleted',"=",0)->get();            
            if (isset($is_cname[0]) && !empty($is_cname[0])) {
                return 'true';
            } else {
                return 'false';
            }
        }        
    }

    /**
    * List view by Data table for Role Details
    */  
    public function getData(){
        $role = Role::select(['id', 'name'])->where('is_deleted', '=', 0)->where('is_deleted', '=', 0)->get();                          
        $company = Datatables::of($role)->addColumn('action', function ($role) {
            return '<a href="show-role/'.$role->id.'" class="btn btn-warning btn-xs"><i class="fa fa-newspaper-o" title="View"></i></a>&nbsp;&nbsp;<a href="delete-role/'.$role->id.'" title="Delete" class="btn btn-danger btn-xs"  onclick="return confirm('."'Are you sure, you want to delete this user?'".');"><i class="glyphicon glyphicon-trash"></i></a>';
        })->make(true);
        return $company;die;           
    }

    /**
    * List view by Data table for Assciated project Details
    */
    public function getRoleList()
    {         
        $role = Role::select(['id', 'name'])->where('is_deleted', '=', 0)->get(); 
        $data = array();
        $i = 0;
        foreach ($role as $key => $value) {
            $data[$i]['id'] = Crypt::encrypt($value->id);
            $data[$i]['name'] = $value->name;
            $i++;
        }
        return json_encode($data);
        exit;        
    }
}
