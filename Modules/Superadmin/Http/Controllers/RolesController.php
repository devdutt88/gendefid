<?php

namespace Modules\Superadmin\Http\Controllers;

use Kodeine\Acl\Models\Eloquent\Permission;
use Kodeine\Acl\Models\Eloquent\Role;
// use App\Models\superadmin\Roles;
// use App\Models\superadmin\Permissions;
use App\Models\superadmin\Authentication;
use App\Models\superadmin\RolesAuthMap;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Yajra\Datatables\Facades\Datatables as Datatables;
use mjanssen\BreadcrumbsBundle\Breadcrumbs;
use Illuminate\Support\Facades\Input;
use Auth;
use Crypt;
use Helpers as Helper;

class RolesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {        
        Breadcrumbs::addBreadcrumb('roles',  url('superadmin/roles'));        
        $page_data = array('breadcrumbs' => Breadcrumbs::generate());
        $page_data['roles'] = Roles::get();
        $data['scripts'] = array('angularjs/angular.min',
                                'jquery.dataTables.min',
                                'angularjs/angular-datatables.min',
                                'angularjs/angular-messages.min',
                                'angularjs/angular-flash.min',
                                'angularjs/controllers/rolecontroller',
                        );
        return view('superadmin::role.role_list',compact('data'))->with($page_data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {        
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeRole(Request $request)
    {                
        $input = Input::all();  
        $sys_token = \Session::token();
        $token = Input::get('_token'); 
        if ($token != $sys_token) {
            return "there was some server error";
            die;
        } 

        $roleObj = new Role();        
        $roleCreat = $roleObj->create([
            "name"  =>  $input["name"],
            "slug"  =>  str_slug($input["name"]),
            "description"  =>  $input["description"],
            "created_at" => date('Y-m-d h:i:s')
        ]);
       
        $success = \Session::flash('flash_alert_notice', 'Role has been added successfully !');
        return $success;        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Roles  $role
     * @return \Illuminate\Http\Response
     */
    public function show(Roles $role,$id)
    {        
        $id = Crypt::decrypt($id);
        
        Breadcrumbs::addBreadcrumb('Roles',  url('superadmin/roles'));       
        Breadcrumbs::addBreadcrumb('View',  url('superadmin/roles'));        
        $page_data['breadcrumbs'] = Breadcrumbs::generate();
        $page_data['role'] = Roles::where('id','=',$id)->first(); 
        $data['scripts'] = array('Treemodule/lib/angular.min',
                                'Treemodule/src/Controllers/TreeController',
                                'Treemodule/src/Services/TreeService',
                                'Treemodule/src/Config/URLConfig',
                                'Treemodule/src/Directives/TreeDirectives'
                        );
        return view('superadmin::role.show_role',compact('data','allparent','allchild'))->with($page_data);
    }

    /**
     * fetch the specified resource.
     *
     * @param  \App\Roles  $role
     * @return \Illuminate\Http\Response
     */
    public function getRoles(Roles $role,$id)
    {   
        $id = Crypt::decrypt($id);
        $role = Roles::where('id','=',$id)->first();
        $roles = $role->roleAuthDetails->where('status','=',1); 
        $auth_id = array();
        foreach($roles as $r) {
            $auth_id[] = (String)$r->authorisation_id;
        }
        return json_encode($auth_id);
        die;
    }

    /**
     * API REQUEST 
     * return permission of the role
     ***
     */
  public function getRolePermissions(){
        
        $authentication = Permissions::get(); 
        $element = array(); 
        foreach ($authentication as $key => $parent) {
            //$element[$pkey]['id'] = $parent->id;
            $element[$key]['name'] = $parent->name;
            $element[$key]['checked'] = false;
            $children = json_decode($parent->slug);
            $i = 0; 
            foreach ($children as $ckey => $child) {
                $element[$key]['children'][$i]['name'] = $ckey;
                $element[$key]['children'][$i]['checked'] = $child;
                $i++;
            }
        }
        
        //$permissionTree = Helper::buildTree($element);
        //$menuauthArr = Helper::super_unique($menuauthArr); 
        //echo "<pre>"; print_r($element); die;
        return json_encode($element);
        exit;
    }

    

    /**
     * Store a newly created permission for roles in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeRolesPermission(Request $request)
    {        
        $sys_token = \Session::token();
        $token = Input::get('_token'); 
        if ($token != $sys_token) {
            return "there was some server error";
            die;
        }        
        $id = Crypt::decrypt(Input::get('id'));
        $permission = Input::get('permissionData');
        foreach ($permission as $key => $perm_id) {
            $roleAuth = RolesAuthMap::firstOrNew(array("authorisation_id"=>$perm_id,'role_id'=>$id));
            $roleAuth->authorisation_id = $perm_id;
            $roleAuth->role_id = $id;
            $roleAuth->status = 1;
            $roleAuth->save();
        } 
        $uncheckData = Input::get('uncheckData');  
        $data = array();  
        foreach ($uncheckData as $key => $value) {
            if(!in_array($value, $permission)){
                $data[] = $value;
            }
        }
        foreach ($data as $key => $perm_id) {
            $roleAuth = RolesAuthMap::firstOrNew(array("authorisation_id"=>$perm_id,'role_id'=>$id));
            $roleAuth->authorisation_id = $perm_id;
            $roleAuth->role_id = $id;
            $roleAuth->status = 0;
            $roleAuth->save();
        }       
        return json_encode(array('msg'=>"Permission Added successfully!"));
    }

    /**
     * Get the specfic record according to give id in role
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Authentication  $authentication
     * @return \Illuminate\Http\Response in json
     */
    public function editRole($id)
    {
        $id = Crypt::decrypt($id);
        $role[0] = Roles::find($id);
        $data = array();
        $i = 0;        
        foreach ($role as $key => $value) {
            $data['id'] = Crypt::encrypt($value->id);
            $data['name'] = $value->name;
            $data['description'] = $value->description;
            $i++;
        }
        return json_encode($data);        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Roles  $role
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Roles $role)
    {                        
        $sys_token = \Session::token();
        $token = Input::get('_token'); 
        if ($token != $sys_token) {
            return "there was some server error";
            die;
        }            
        $input = Input::all();    
        $id = Crypt::decrypt($input['id']);
        $roles = Roles::firstOrNew(array("id"=>$id));
        $roles->name = $input['name'];
        $roles->slug = str_slug($input["name"]);
        $roles->description = $input['description'];
        //update role //
        $roles->save();        
        $success = \Session::flash('flash_alert_notice', 'Roles updated successfully !');
        return $success;        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Roles  $role
     * @return \Illuminate\Http\Response
     */
    public function destroy(Roles $role,$id)
    {
        print("under progress");
        // $role = Roles::where('id','=',$id)->first();
        // $role->is_deleted = 1;
        // $role->save();
        // return redirect('superadmin/role')->with('flash_alert_notice', 'Roles deleted successfully!');
    }
    /**
    * Check the specified Roles is exist
    * And return as true and false
    */
    public function checkRole($id,$cname)
    {    
        #$cname = Input::get('name');            
        // echo $cname;die;
        // $id = Input::get('id');        
        if (isset($cname) && !empty($cname)) {
            if (!empty($id)) {
                $id = Crypt::decrypt($id);
                $is_current_name = Roles::where('name',"=",$cname)->where('id',"=",$id)->get();   
                if (isset($is_current_name[0]) && !empty($is_current_name[0]) && $is_current_name[0]->name == $cname)
                {
                    return "false";
                    die;
                }
            }            
            $is_cname = Roles::where('name',"=",$cname)->get();            
            if (isset($is_cname[0]) && !empty($is_cname[0])) {
                return 'true';
            } else {
                return 'false';
            }
        }        
    }

    /**
    * List view by Data table for Roles Details
    */  
    public function getData(){
        $role = Roles::select(['id', 'name'])->get();                          
        $company = Datatables::of($role)->addColumn('action', function ($role) {
            return '<a href="show-role/'.$role->id.'" class="btn btn-warning btn-xs"><i class="fa fa-newspaper-o" title="View"></i></a>&nbsp;&nbsp;<a href="delete-role/'.$role->id.'" title="Delete" class="btn btn-danger btn-xs"  onclick="return confirm('."'Are you sure, you want to delete this user?'".');"><i class="glyphicon glyphicon-trash"></i></a>';
        })->make(true);
        return $company;die;           
    }

    /**
    * List view by Data table for Assciated project Details
    */
    public function getRolesList()
    {         
        $role = Roles::select(['id', 'name'])->get(); 
        $data = array();
        $i = 0;
        foreach ($role as $key => $value) {
            $data[$i]['id'] = Crypt::encrypt($value->id);
            $data[$i]['name'] = $value->name;
            $i++;
        }
        return json_encode($data);
        exit;        
    }

    public function getnodehtml(){
        return view('superadmin::role.node');
    }
}
