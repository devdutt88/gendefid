<?php

namespace Modules\Superadmin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use mjanssen\BreadcrumbsBundle\Breadcrumbs;
use Illuminate\Support\Facades\Input;
use Helpers as Helper;
use App\User;
use App\Superadmin;
use App\Models\superadmin\Company;
use App\Models\superadmin\Issueto;
use App\Models\superadmin\Project;
use App\Models\superadmin\UserDetails;
use File;
use Image;
use Auth;
use Hash;
use Carbon\Carbon;
   

class SuperadminController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {        
        $companies = Company::where('is_deleted', '=', 0)->count();        
        $users = User::where('is_deleted', '=', 0)->count();       
        $total_issueto = Issueto::where('is_deleted', '=', 0)->count();
        $project = Project::where('is_deleted', '=', 0)->count();
        $page_data = array('breadcrumbs' => Breadcrumbs::generate(), 'total_companies' => $companies, 'total_users' => $users, 'project' => $project, 'total_issueto' => $total_issueto);
        // $data['scripts'] = array('angularjs/fusioncharts/fusioncharts','angularjs/angular.min','angularjs/angular-fusioncharts.min','angularjs/controllers/dashboradcontroller');
        $data['scripts'] = array('angularjs/angular.min','angularjs/Chart','angularjs/angular-chart','angularjs/controllers/dashboradcontroller');
        return view('superadmin::dashboard',compact('data'))->with($page_data);
    }

    /**
     * Show superadmin profile
     * @return Response
     */
    public function profile()
    {
        $id = Auth::guard('admin')->user()->id;
        $admin = Superadmin::find($id);
        Breadcrumbs::addBreadcrumb('My Profile', '');
        $page_data = array('breadcrumbs' => Breadcrumbs::generate());

        //Get superadmin details according to id
        $page_data['admin'] = $admin;
        $user_det = Superadmin::where('id','=',$id)->find($id);        
        $page_data['udetail'][0] = $user_det;
        $page_data['superadmin_role'] = 'Superadmin';
        $page_data['option_state'] = array(
            ""=>'Select One',
            "NSW"=>'NSW',
            "QLD"=>'QLD',
            "SA"=>'SA',
            "VIC"=>'VIC',
            "WA"=>"WA");           
        if (!empty($page_data['admin'])) {
            $data['scripts'] = array('angularjs/angular.min','angularjs/angular-messages.min','angularjs/controllers/superadmincontroller');
            return view('superadmin::user.show_admin',compact('data'))->with($page_data);
        } else {
            return redirect('superadmin');
        } 
    }

    /**
     * API request for all count of graph
     * @return Response
     */
    public function getGrapthCounts($id){
        if($id == 0){
             $result = $this->getAllCountbyMonth();
        }else {
             $result = Helper::getCompanyCountbyMonth($id);  
        }      
       
        return $result;
        exit;      
    }

    public function getAllCountbyMonth(){
        
        $users = User::select('id', 'created_at')
            ->where('is_deleted','=','0')
            ->get()
            ->groupBy(function($date) {
                //return Carbon::parse($date->created_at)->format('Y'); // grouping by years
                return Carbon::parse($date->created_at)->format('m'); // grouping by months
            });

            $usermcount = [];
            $userArr = [];
            
            foreach ($users as $key => $value) {
                $usermcount[(int)$key] = count($value);
            }
            
            for($i = 1; $i <= 12; $i++){
                if(!empty($usermcount[$i])){
                    $userArr[$i] = $usermcount[$i];    
                }else{
                    $userArr[$i] = 0;    
                }
            }
            
        $projects = Project::select('id', 'created_at')
            ->where('is_deleted','=','0')
            ->get()
            ->groupBy(function($date) {
                //return Carbon::parse($date->created_at)->format('Y'); // grouping by years
                return Carbon::parse($date->created_at)->format('m'); // grouping by months
            });

            $projectmcount = [];
            $projectArr = [];
            
            foreach ($projects as $key => $value) {
                $projectmcount[(int)$key] = count($value);
            }
            
            for($i = 1; $i <= 12; $i++){
                if(!empty($projectmcount[$i])){
                    $projectArr[$i] = $projectmcount[$i];    
                }else{
                    $projectArr[$i] = 0;    
                }
            }

        $issuetos = Issueto::select('id', 'created_at')
            ->where('is_deleted','=','0')
            ->get()
            ->groupBy(function($date) {
                //return Carbon::parse($date->created_at)->format('Y'); // grouping by years
                return Carbon::parse($date->created_at)->format('m'); // grouping by months
            });

            $issuetomcount = [];
            $issuetoArr = [];
            
            foreach ($issuetos as $key => $value) {
                $issuetomcount[(int)$key] = count($value);
            }
            
            for($i = 1; $i <= 12; $i++){
                if(!empty($issuetomcount[$i])){
                    $issuetoArr[$i] = $issuetomcount[$i];    
                }else{
                    $issuetoArr[$i] = 0;    
                }
            }
        
        $totalArr = array($userArr,$projectArr,$issuetoArr);
        // echo "<pre>";
        // print_r($userArr);die;

        return $totalArr;
            
    }

    /**
     * API request for all compnies
     * @return Response
     */
    public function getCompanies(){
              
        $companies = Company::select('id', 'name')->where('is_deleted', '=', '0')->get();
        return $companies;
        exit;      
    }

    /**
     * API request to get superadmin table data
     * @return Response
     */
    public function edit()
    {
        $id = Auth::guard('admin')->user()->id;
        if ($id == 0) {
            die("error message");
        }
            $admin = Superadmin::find($id);
            $admindata = array();
            $admindata['username'] = $admin->name;
            $admindata['fullname'] = $admin->full_name;
            $admindata['password'] = $admin->plain_password; 
            $admindata['email'] = $admin->email; 
            $admindata['phone'] = (int)$admin->phone;                
            return json_encode($admindata);
       // return view('superadmin::edit');
    }

    /**
     * API request to update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request,Superadmin $admin)
    {        
        $user_id = Auth::guard('admin')->user()->id;
        $adminform = Input::get('adminform');
        $sys_token = \Session::token();
        $token = Input::get('_token'); 
        if ($token != $sys_token) { 
            return false;
        } 
        switch ($adminform) {
            case 'profile': 
                $admin = Superadmin::find($user_id);
                //$admin->name = Input::get('username');
                $admin->full_name = Input::get('fullname');
                $admin->email = Input::get('email'); 
                $admin->phone = Input::get('phone'); 
                $success = $admin->save();                

                $admin = \Session::get('adminData');
                $admin['fullname'] = Input::get('fullname');
                $admin['email'] = Input::get('email');
                $admin['phone'] = Input::get('phone');
                \Session::set('adminData',$admin);

                if($success){
                    $success = \Session::flash('flash_alert_notice', 'Profile data updated successfully !');
                    return $success;
                }else{
                    return "there was some server error";
                }
            break;

            case 'logo':
                $username = 'superadmin';
                $file = Input::file('logo');
                
                // Make directory if not exist
                $temp_path = public_path('uploads/user/temp/');
                if(!File::exists($temp_path)) {
                    File::makeDirectory($temp_path, 0777, true, true);
                }
                $path_200 = public_path('uploads/user/thumbnail_200x200/');
                if(!File::exists($path_200)) {
                    File::makeDirectory($path_200, 0777, true, true);
                }
                $path_29 = public_path('uploads/user/thumbnail_29x29/');
                if(!File::exists($path_29)) {
                    File::makeDirectory($path_29, 0777, true, true);
                }
                
                if ($file) {
                    $image_name = $file->getClientOriginalName();
                    $img_path = public_path('uploads/user/');
                    $extension = $file->getClientOriginalExtension(); // getting image extension
                    $fileName = $user_id . '_' .$username.".". $extension;
                    $uploaded_at = $file->move($temp_path, $fileName);
                    // Main Image
                    $img = Image::make($temp_path.$fileName)->resize(200,200);
                    if (file_exists($img_path.$fileName)) {
                            unlink($img_path.$fileName);
                    }                
                    $save_thumb = $img->save($img_path.$fileName);

                    // Resize image for 200x200
                    $img_thumb1 = Image::make($temp_path.$fileName)->resize(200,200);
                    if (file_exists($img_path. "thumbnail_200x200/" .$fileName)) {
                            unlink($img_path. "thumbnail_200x200/" .$fileName);
                    }                
                    $save_thumb = $img_thumb1->save($img_path. "thumbnail_200x200/" .$fileName);
                    
                    // Resize image for 138x30
                    $img_thumb2 = Image::make($temp_path.$fileName)->resize(29,29);
                    if (file_exists($img_path. "thumbnail_29x29/" .$fileName)) {
                            unlink($img_path. "thumbnail_29x29/" .$fileName);
                        }
                    $save_thumb = $img_thumb2->save($img_path . "thumbnail_29x29/" .$fileName);            
                    
                    // Remove temperory file
                    if (file_exists($temp_path.$fileName)) {
                        unlink($temp_path.$fileName);
                    }                    
                    //\Session::set('adminData', array('fullname'=>Auth::user()->full_name));
                    //\Session::set('adminData', array('logo_image'=>$fileName));
                    $admin = \Session::get('adminData');
                    $admin['logo_image'] = $fileName;
                    \Session::set('adminData',$admin);                    
                } else {
                    $fileName = '';
                } 

                //update user_details table data
                $user_details = Superadmin::firstOrNew(array('id' => $user_id));
                $user_details->id = $user_id;
                $user_details->logo_image = $fileName;
                $success = $user_details->save();
                if ($success) {
                    $success = \Session::flash('flash_alert_notice', 'Logo has been updated successfully !');
                    return $success;
                } else {
                    return "there was some server error";
                }
            break;

            default:
                return redirect('superadmin/user')->with('flash_alert_notice', 'There was nothing to update.');    
            break;
        }
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }

    /**
     *  API request to update password 
     * @return Response
     */
    public function changePassword(){
        $sys_token = \Session::token();
        $token = Input::get('_token'); 
        if ($token != $sys_token) { 
            return false;
        } 
        $user = Auth::guard('admin')->user();//Auth::user();
        $user->password = Hash::make(Input::get('password'));
        $user->save();
        return \Session::flash('flash_alert_notice', 'Your new password has been updated!');
        exit;
    }

    /**
     *  API request to check exist password 
     * @return Response
     */
    public function check_current_password($password){
        $current_password = Auth::guard('admin')->user()->password;
        if (Hash::check($password, $current_password)){
            echo "true";
        }else{
            echo "false";
        }
        exit;
    }

   
}
