<?php

namespace Modules\Superadmin\Http\Controllers;

use App\User;
use App\Models\superadmin\UserDetails;
use App\Models\superadmin\CompanyUserMapping;
use App\Models\superadmin\Role;
use App\Models\superadmin\Company;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Redirect;
use Modules\Superadmin\Http\Requests\StoreUserPost;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Validator;
use Auth;
use Paginate;
use Grids;
use HTML;
use Form;
use View;
use URL;
use Lang;
use DB;
use File;
use Image;
use Helpers as Helper;
use Yajra\Datatables\Facades\Datatables as Datatables;
use mjanssen\BreadcrumbsBundle\Breadcrumbs;


class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {         
        $data['scripts'] = array('user');
        Breadcrumbs::addBreadcrumb('User List',  url('superadmin/user'));
        $page_data = array('breadcrumbs' => Breadcrumbs::generate());
        return view('superadmin::user.user_list',compact('data'))->with($page_data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['scripts'] = array('user');
        $page_data = array();
        $page_data['roles'] = Helper::getUserRoles();
        $page_data['option_state'] = array(
            ""=>'Select One',
            "NSW"=>'NSW',
            "QLD"=>'QLD',
            "SA"=>'SA',
            "VIC"=>'VIC',
            "WA"=>"WA");
        $page_data['companies'] = Company::select('id', 'name')->where('is_deleted','=', 0)->get();
        $page_data['uc'] = array();
        $page_data['cur_user'] = '';
        Breadcrumbs::addBreadcrumb('User List',  url('superadmin/user'));
        Breadcrumbs::addBreadcrumb('Add User', '');
        $page_data['breadcrumbs'] = Breadcrumbs::generate();        
        return view('superadmin::user.add_user',compact('data'))->with($page_data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUserPost $request)
    {                    
        //Store value for User Details
        $user = new User;
        $user->name = Input::get('username');
        $user->phone = Input::get('phone');
        $user->full_name = Input::get('fullname');
        $user->password = bcrypt(Input::get('password')); 
        $user->email = Input::get('email'); 
        $user->role_id = Input::get('usertype');
        $user->plain_password = Input::get('password'); 
        $user->is_deleted = 0;
        $user->save();
        
        //Store value for User Address
        $user_details = new UserDetails;
        $user_details->busi_address1 = Input::get('bus_add1');
        $user_details->busi_address2 = Input::get('bus_add2');
        $user_details->busi_suburb = Input::get('bus_suburb');
        $user_details->busi_state = Input::get('bus_state');
        $user_details->busi_post = Input::get('bus_post');
        $user_details->busi_country = Input::get('bus_country');
        $user_details->bill_address1 = Input::get('buld_add1');
        $user_details->bill_address2 = Input::get('buld_add2');
        $user_details->bill_suburb = Input::get('buld_suburb');
        $user_details->bill_state = Input::get('buld_state');
        $user_details->bill_post = Input::get('buld_post');
        $user_details->bill_country = Input::get('buld_country');
                        
        if (!empty($user->id)) {
            $comps = Input::get('company');
            foreach($comps as $comp) {
                $arr = array('user_id' => $user->id, 'company_id'=> $comp);
                CompanyUserMapping::insert($arr);
            }
            $user_details->user_id = $user->id;
            $insert_user_details = $user_details->save();
            if (!empty($insert_user_details)) {           

                // Send Welcome Email Notif ication     
                $template = "superadmin::email.welcome";
                $link = url('/');
                $email = Input::get('email');
                $subject = "GendefId Welcome Notif ication";
                $data = array('name'=>Input::get('username'),'link'=>$link,'email'=>$email,'subject'=>$subject,'password'=>Input::get('password'));
                Helper::sendMessage($template,$data);                
                return redirect('superadmin/user')->with('flash_alert_notice', 'User created successfully.');        
            } else {
                return redirect('superadmin/user');
            }
        }
    }

    /**
     * Display the specif ied resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user,$id)
    {

        Breadcrumbs::addBreadcrumb('User List',  url('superadmin/user'));
        Breadcrumbs::addBreadcrumb('User Details', '');
        $page_data = array('breadcrumbs' => Breadcrumbs::generate());
        
        //Get specif ic user details according to id
        $page_data['user_compnies'] =  Helper::getCompanyUserMapping($id);
        $page_data['compnies'] =  Helper::getCompanyList();
        $page_data['logo_image'] = 'default-profile.png';
        $page_data['user'] = $user->where('is_deleted','=',0)->find($id);
        $page_data['roles'] = Helper::getUserRoles();
        $user_det = User::where('id','=',$id)->find($id);
        $udetail = $user_det->UserDetails;
        $cdetail = $user_det->CompanyUserMapping->where('is_deleted','=',0);                    
    
        if (empty($user_det)) {
            return redirect(url('superadmin/user'));
        } else {
            $page_data['logo_image'] = 'default-profile.png';
            $page_data['option_state'] = array(
                ""=>'Select One',
                "NSW"=>'NSW',
                "QLD"=>'QLD',
                "SA"=>'SA',
                "VIC"=>'VIC',
                "WA"=>"WA");
            $page_data['companies'] = Company::select('id', 'name')->where('is_deleted', '=', 0)->orderBy('name', 'asc')->get();
            $uc = array();
            foreach($cdetail as $comp) {
                $uc[] = $comp->company_id;
            }
            $page_data['user'] = $user_det;
            $page_data['udetail'] = $udetail;
            $page_data['cdetail'] = $cdetail;
            $page_data['uc'] = $uc;
        }           
        if (!empty($page_data['user'])) {
            $page_data['user_details'] = UserDetails::where('user_id', '=', $id)->first();            
            $data['scripts'] = array('angularjs/angular.min','angularjs/angular-flash.min','angularjs/controllers/usercontroller', 'user');
            return view('superadmin::user.show_user',compact('data'))->with($page_data);           
        } else {
            return redirect('superadmin/user');
        }
    }

    
    public function edit($id,$userform)
    {
        if ($id == 0) {
            die("error message");
        }
        switch ($userform) {
            case 'profile':
                $user = User::find($id);
                $userdata = array();
                $userdata['username'] = $user->name;
                $userdata['fullname'] = $user->full_name;
                $userdata['password'] = $user->plain_password; 
                $userdata['email'] = $user->email; 
                $userdata['phone'] = (int)$user->phone; 
                $userdata['usertype'] = $user->role_id;                
                return json_encode($userdata);
            break;

            case 'address':
                $userDetails = UserDetails::where('user_id','=',$id)->first();
                $addressData = array();
                $addressData['busi_address1'] = $userDetails->busi_address1;
                $addressData['busi_address2'] = $userDetails->busi_address2;
                $addressData['busi_suburb'] = $userDetails->busi_suburb; 
                $addressData['busi_state'] = $userDetails->busi_state; 
                $addressData['busi_post'] = $userDetails->busi_post; 
                $addressData['busi_country'] = $userDetails->busi_country;
                $addressData['bill_address1'] = $userDetails->bill_address1;
                $addressData['bill_address2'] = $userDetails->bill_address2;
                $addressData['bill_suburb'] = $userDetails->bill_suburb; 
                $addressData['bill_state'] = $userDetails->bill_state; 
                $addressData['bill_post'] = $userDetails->bill_post; 
                $addressData['bill_country'] = $userDetails->bill_country;                
                return json_encode($addressData);
            break;

            default:
                die("error message");
            break;
        }
        
    }

    /**
     * Update the specif ied resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update()
    {        
        $user_id = Input::get('id');
        $user_form = Input::get('userform');        
        switch ($user_form) {
            case 'profile':
                $user = User::firstOrNew(array('id' => Input::get('id')));                
                $user->full_name = Input::get('fullname');
                $user->password = bcrypt(Input::get('password')); 
                $user->email = Input::get('email'); 
                $user->phone = Input::get('phone'); 
                $user->role_id = Input::get('usertype');
                $user->plain_password = Input::get('password'); 
                $success = $user->save();
                if ($success) {
                    $success = \Session::flash('flash_alert_notice', 'User profile updated successfully !');
                    return $success;
                } else {
                    return false;
                }
            break;

            case 'address':
                $user_details = UserDetails::firstOrNew(array('user_id' => $user_id));
                $user_details->busi_address1 = Input::get('busi_address1');
                $user_details->busi_address2 = Input::get('busi_address2');
                $user_details->busi_suburb = Input::get('busi_suburb');
                $user_details->busi_state = Input::get('busi_state');
                $user_details->busi_post = Input::get('busi_post');
                $user_details->busi_country = Input::get('busi_country');
                $user_details->bill_address1 = Input::get('bill_address1');
                $user_details->bill_address2 = Input::get('bill_address2');
                $user_details->bill_suburb = Input::get('bill_suburb');
                $user_details->bill_state = Input::get('bill_state');
                $user_details->bill_post = Input::get('bill_post');
                $user_details->bill_country = Input::get('bill_country');
                $success = $user_details->save();
                if ($success) {
                    $success = \Session::flash('flash_alert_notice', 'Address data updated successfully !');
                    return $success;
                } else {
                    return "there was some server error";
                }
            break;

            case 'logo':                                
                $username = Input::get('username');
                $file = Input::file('logo'); 
                
                // Make directory if  not exist
                $temp_path = public_path('uploads/user/temp/');
                if (!File::exists($temp_path)) {
                    File::makeDirectory($temp_path, 0777, true, true);
                }
                $path_200 = public_path('uploads/user/thumbnail_200x200/');
                if (!File::exists($path_200)) {
                    File::makeDirectory($path_200, 0777, true, true);
                }
                $path_29 = public_path('uploads/user/thumbnail_29x29/');
                if (!File::exists($path_29)) {
                    File::makeDirectory($path_29, 0777, true, true);
                }                
                if ($file) {
                    $image_name = $file->getClientOriginalName();
                    $img_path = public_path('uploads/user/');
                    $extension = $file->getClientOriginalExtension(); // getting image extension
                    $fileName = $user_id . '_' .$username.".". $extension;
                    $uploaded_at = $file->move($temp_path, $fileName);
                    // Main image thumbnail
                    $img = Image::make($temp_path.$fileName)->resize(200,200);
                    if (file_exists($img_path.$fileName)) {
                            unlink($img_path.$fileName);
                    }                
                    $save_thumb = $img->save($img_path.$fileName);
                    // 200x200 image thumbnail
                    $img_thumb1 = Image::make($temp_path.$fileName)->resize(200,200);
                    if (file_exists($img_path. "thumbnail_200x200/" .$fileName)) {
                            unlink($img_path. "thumbnail_200x200/" .$fileName);
                    }                
                    $save_thumb = $img_thumb1->save($img_path. "thumbnail_200x200/" .$fileName);
                    // 29x29 image thumbnail
                    $img_thumb2 = Image::make($temp_path.$fileName)->resize(29,29);
                    if (file_exists($img_path. "thumbnail_29x29/" .$fileName)) {
                            unlink($img_path. "thumbnail_29x29/" .$fileName);
                        }
                    $save_thumb = $img_thumb2->save($img_path . "thumbnail_29x29/" .$fileName);            
                    // Remove temp folder image 
                    if (file_exists($temp_path.$fileName)) {
                        unlink($temp_path.$fileName);
                    }
                    
                } else {
                    $fileName = '';
                }
                // update user_details table data
                $user_details = UserDetails::firstOrNew(array('user_id' => $user_id));
                $user_details->user_id = $user_id;
                $user_details->logo_image = $fileName;
                $success = $user_details->save();
                if ($success) {
                    $success = \Session::flash('flash_alert_notice', 'Logo has been updated successfully !');
                    return $success;
                } else {
                    return "there was some server error";
                }
            break;

            case 'companies':    
                $cu_map = new CompanyUserMapping;
                $u_comps = $cu_map->select('company_id')->where('user_id', '=', $user_id)->where('is_deleted', '=', 0)->get();
                $uc = array();
                foreach($u_comps as $u_comp) {
                    $uc[] = $u_comp->company_id;
                }
                $u_comps_del = $cu_map->select("company_id")->where('user_id', '=', $user_id)->where('is_deleted', '=', 1)->get();
                $uc_del = array();
                foreach($u_comps_del as $ucd) {
                    $uc_del[] = $ucd->company_id;
                }
                $comps = Input::get('companies');                
                $delete_uc = array_diff($uc,$comps);                
                $to_del_comps = $cu_map->where('user_id', '=', $user_id)->whereIn('company_id', $delete_uc);
                if (!empty($to_del_comps)) {
                    $to_del_comps->update(array('is_deleted'=>1));
                }                
                $add_uc = array_diff($comps,$uc);
                foreach($add_uc as $comp) {
                    if (in_array($comp, $uc_del)) {
                        $to_add_comps = $cu_map->where('user_id', '=', $user_id)->where('company_id', '=', $comp);
                        if (!empty($to_add_comps)) {
                            $to_add_comps->update(array('is_deleted'=>0));
                        }
                    } else {
                        $cu_map->user_id = $user_id;
                        $cu_map->company_id = $comp;
                        $success = $cu_map->save();
                        $success = \Session::flash('flash_alert_notice', 'Assosiative companies data updated !');
                        return $success;
                    }
                }
            break;

            default:
                return redirect('superadmin/user')->with('flash_alert_notice', 'There was nothing to update.');    
            break;
        }
       
    }

    /**
     * Remove the specif ied resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user, $id)
    {
        if (!empty($id) && $id != 0){            
            $cur_user = User::where('id', $id);
            if (!empty($cur_user)){
                $cur_user->update(array('is_deleted' => 1));
                $companyUserMapping = CompanyUserMapping::where('user_id','=',$id);
                if (!empty($companyUserMapping))
                {
                    $companyUserMapping->update(array('is_deleted' => 1));
                }
            }
        }
        return redirect('superadmin/user')->with('flash_alert_notice', 'User deleted successfully.');
    }

    /**
    * Check the specified name and email is exist
    * And return as true and false
    */
    public function check_isexist(){    
        
        $username = Input::get('username');
        $email = Input::get('email');
        $id = Input::get('id');
        if (isset($username) && !empty($username)){
            if (!empty($id)){
                $is_current_name = User::where('name',"=",$username)->where('is_deleted',"=",0)->where('id',"=",$id)->get();   
                if (isset($is_current_name[0]) && !empty($is_current_name[0]) && $is_current_name[0]->name == $username)
                {
                    echo "true";
                    die;
                }
            }            
            $is_username = User::where('name',"=",$username)->where('is_deleted',"=",0)->get();            
            if (isset($is_username[0]) && !empty($is_username[0])){
                echo 'false';
            }else{
                echo 'true';
            }
        }
        if (isset($email) && !empty($email)){
            if (!empty($id)){
                $is_current_email = User::where('email',"=",$email)->where('is_deleted',"=",0)->where('id',"=",$id)->get();   
                if (isset($is_current_email[0]) && !empty($is_current_email[0]) && $is_current_email[0]->email == $email)
                {
                    echo "true";
                    die;
                }
            }
            $is_email = User::where('email',"=",$email)->where('is_deleted',"=",0)->get();
            if (isset($is_email[0]) && !empty($is_email[0])){
                echo 'false';
            }else{
                echo 'true';
            }
        }
        die;
    }

    /**
    * List view by Data table for user Details
    */
    public function getData()
    {
        $user = User::select(['id', 'name', 'full_name', 'email', 'role_id'])->where('is_deleted', '=', 0)->where('role_id', '!=', 0)->get();                          
        $company = Datatables::of($user)->addColumn('roles', function ($user) {   
                $role_name = Role::select(['name'])->where('id','=',$user->role_id)->get();
                return !empty($role_name[0]->name)?$role_name[0]->name:'';
            })->addColumn('action', function ($cmp) {
                return '<a href="user-details/'.$cmp->id.'" class="btn btn-warning btn-xs"><i class="fa fa-newspaper-o" title="View"></i></a>&nbsp;&nbsp;<a href="delete-user/'.$cmp->id.'" title="Delete" class="btn btn-danger btn-xs"  onclick="return confirm('."'Are you sure you want to delete this User?'".');"><i class="glyphicon glyphicon-trash"></i></a>';
            })->make(true);
        return $company;
        exit;        
    }

    /**
    * List view by Data table for user Details
    */    
    public function getUserCompanyData($id)
    {        
        $user_comps = Helper::getCompanyUserMapping($id);
        $user_company1 = Company::select([ 'name', 'type'])->where('is_deleted', '=', 0)->whereIn('id', $user_comps)->get();
        $user_company = Datatables::of($user_company1)->make(true);                          
        return $user_company;
        exit;        
    }
}