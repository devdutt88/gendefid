<?php

namespace Modules\Superadmin\Http\Controllers;

use App\User;
use App\Models\superadmin\UserDetails;
use App\Models\superadmin\CompanyUserMapping;
use App\Models\superadmin\ProjectUserRole;
use App\Models\superadmin\Project;
use Kodeine\Acl\Models\Eloquent\Role;
use App\Models\superadmin\Company;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Redirect;
use Modules\Superadmin\Http\Requests\StoreUserPost;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Validator;
use Auth;
use Paginate;
use Grids;
use HTML;
use Form;
use View;
use URL;
use Lang;
use DB;
use File;
use Image;
use Helpers as Helper;
use Crypt;
use Yajra\Datatables\Facades\Datatables as Datatables;
use mjanssen\BreadcrumbsBundle\Breadcrumbs;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {         
        Breadcrumbs::addBreadcrumb('User List',  url('superadmin/user'));
        $page_data = array('breadcrumbs' => Breadcrumbs::generate());
        $data['scripts'] = array('angularjs/angular.min',
                            'angularjs/angular-messages.min',
                            'angularjs/angular-flash.min',
                            'angularjs/controllers/userlistController',
                            'angularjs/user_directive',
                        );
        return view('superadmin::user.user_list',compact('data'))->with($page_data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['scripts'] = array('user');
        $page_data = array();
        $page_data['roles'] = Helper::getAllRoles();
        $page_data['option_state'] = array(
            ""=>'Select One',
            "NSW"=>'NSW',
            "QLD"=>'QLD',
            "SA"=>'SA',
            "VIC"=>'VIC',
            "WA"=>"WA");
        $page_data['option_country'] = Helper::countryList();
        $page_data['companies'] = Company::select('id', 'name')->where('is_deleted','=', 0)->get();
        $page_data['uc'] = array();
        $page_data['cur_user'] = '';
        //echo "<pre>"; print_r($page_data); die; 
        Breadcrumbs::addBreadcrumb('User List',  url('superadmin/user'));
        Breadcrumbs::addBreadcrumb('Add User', '');
        $page_data['breadcrumbs'] = Breadcrumbs::generate();        
        return view('superadmin::user.add_user',compact('data'))->with($page_data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUserPost $request)
    {
        $userRole = Input::get('usertype');      
        $user = new User;
        $user->name = Input::get('username');
        $user->phone = Input::get('phone');
        $user->full_name = Input::get('fullname');
        $user->password = bcrypt(Input::get('password')); 
        $user->email = Input::get('email'); 
        $user->role_id = Input::get('usertype');
        $user->plain_password =  Crypt::encrypt(Input::get('password'));
        $user->created_at = date('Y-m-d h:i:s');
        $user->created_by = 0;
        $user->is_deleted = 0;        
        $user->modified_by = 0;
        $user->created_at = date('Y-m-d h:i:s');
        $user->save();
        
        if (!empty($user->id)) {

            // Insert user role to mapping table
            $user->syncRoles($userRole);
                    
            //Send Welcome Email Notif ication     
            $template = "superadmin::email.welcome";
            $link = url('/');
            $email = Input::get('email');
            $subject = "GendefId Welcome Notif ication";
            $data = array('name'=>Input::get('username'),'link'=>$link,'email'=>$email,'subject'=>$subject,'password'=>Input::get('password'));
            Helper::sendMessage($template,$data); 
            $userid = Crypt::encrypt($user->id);
            return json_encode($userid);               
        }else{
            return false;
        }

        //$success = \Session::flash('flash_alert_notice', 'User created successfully !');                    

    }

    /**
     * Update user by angularjs action
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function updateUser(StoreUserPost $request){
        
        $id = Input::get('id');
        $useType = Input::get('usertype');
        $user = User::firstOrNew(array('id' => $id));                
        $user->full_name = Input::get('fullname');
        $user->password = bcrypt(Input::get('password')); 
        $user->email = Input::get('email'); 
        $user->phone = Input::get('phone'); 
        $user->role_id = Input::get('usertype');
        $user->plain_password = Crypt::encrypt(Input::get('password')); 
        $success = $user->save();
        if ($success) {
            // Update user role to mapping table
            $user->syncRoles($useType);
            $success = \Session::flash('flash_alert_notice', 'User profile updated successfully !');                    
            return $success;
        } else {
            return false;
        }
        exit;
    }

    /**
     * Display the specif ied resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user,$id)
    {        
        $id = Crypt::decrypt($id);
        Breadcrumbs::addBreadcrumb('User List',  url('superadmin/user'));
        Breadcrumbs::addBreadcrumb('User Details', '');
        $page_data = array('breadcrumbs' => Breadcrumbs::generate());
        
        //Get specif ic user details according to id
        $page_data['user_compnies'] =  Helper::getCompanyUserMapping($id);
        $page_data['compnies'] =  Helper::getCompanyList();
        $page_data['logo_image'] = 'default-profile.png';
        $page_data['user'] = $user->where('is_deleted','=',0)->find($id);
        $page_data['roles'] = Helper::getAllRoles();
        $user_det = User::where('id','=',$id)->first();
        $udetail = $user_det->UserDetails;
        $cdetail = $user_det->CompanyUserMapping->where('is_deleted','=',0);                    
    
        if (empty($user_det)) {
            return redirect(url('superadmin/user'));
        } else {
            $page_data['logo_image'] = 'default-profile.png';
            $page_data['option_country'] = Helper::countryList();
            $page_data['companies'] = Company::select('id', 'name')->where('is_deleted', '=', 0)->orderBy('name', 'asc')->get();
            $uc = array();
            foreach($cdetail as $comp) {
                $uc[] = $comp->company_id;
            }
            $page_data['user'] = $user_det;
            $page_data['udetail'] = $udetail;
            $page_data['cdetail'] = $cdetail;
            $page_data['uc'] = $uc;
        }           
        if (!empty($page_data['user'])) {            
            $page_data['user_details'] = UserDetails::where('user_id', '=', $id)->first();            
            $data['scripts'] = array('angularjs/angular.min',
                            'angularjs/angular-messages.min',
                            'angularjs/angular-flash.min',
                            'angularjs/controllers/usercontroller',
                            'angularjs/user_directive',
                             'user'
                            );
            return view('superadmin::user.show_user',compact('data'))->with($page_data);           
        } else {
            return redirect('superadmin/user');
        }
    }

    
    public function edit($id,$userform)
    {
        $id = Crypt::decrypt($id);
        if ($id == 0) {
            die("error message");
        }
        switch ($userform) {
            case 'profile':
                $user = User::find($id);
                $userdata = array();
                $userdata['id'] = $user->id;
                $userdata['username'] = $user->name;
                $userdata['fullname'] = $user->full_name;
                $userdata['password'] = Crypt::decrypt($user->plain_password); 
                $userdata['email'] = $user->email; 
                $userdata['phone'] = (int)$user->phone; 
                $userdata['usertype'] = (String)$user->role_id;  
                $userdata['roles'] = $this->getuserTypes();                
                return json_encode($userdata);
            break;

            case 'address':
                $addressData = array();
                $userDetails = UserDetails::where('user_id','=',$id)->first();
                if(empty($userDetails)){
                    return json_encode(false);
                }else{
                    $addressData['busi_address1'] = $userDetails->busi_address1;
                    $addressData['busi_address2'] = $userDetails->busi_address2;
                    $addressData['busi_suburb'] = $userDetails->busi_suburb; 
                    $addressData['busi_state'] = $userDetails->busi_state; 
                    $addressData['busi_post'] = $userDetails->busi_post; 
                    $addressData['busi_country'] = $userDetails->busi_country;
                    $addressData['bill_address1'] = $userDetails->bill_address1;
                    $addressData['bill_address2'] = $userDetails->bill_address2;
                    $addressData['bill_suburb'] = $userDetails->bill_suburb; 
                    $addressData['bill_state'] = $userDetails->bill_state; 
                    $addressData['bill_post'] = $userDetails->bill_post; 
                    $addressData['bill_country'] = $userDetails->bill_country;                
                    return json_encode($addressData);
                }
            break;

            default:
                die("userform is not defined");
            break;
        }
        
    }

    /**
     * Update the specif ied resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update()
    {
        ///////////////print_r(Input::get()); die;
        $id = Input::get('id'); 
        $user_id = Crypt::decrypt($id);
        $user_form = Input::get('userform');    
        $sys_token = \Session::token();
        $token = Input::get('_token'); 
        if ($token != $sys_token) { 
            return false;
        }   
        switch ($user_form) {
            case 'profile':
                $user = User::firstOrNew(array('id' => $user_id));                
                $user->full_name = Input::get('fullname');
                $user->password = bcrypt(Input::get('password')); 
                $user->email = Input::get('email'); 
                $user->phone = Input::get('phone'); 
                $user->role_id = Input::get('usertype');
                $user->plain_password = Crypt::encrypt(Input::get('password')); 
                $success = $user->save();
                if ($success) {
                    $success = \Session::flash('flash_alert_notice', 'User profile updated successfully !');                    
                    return $success;
                } else {
                    return false;
                }
            break;

            case 'address':
                $user_details = UserDetails::firstOrNew(array('user_id' => $user_id));
                $user_details->busi_address1 = Input::get('busi_address1');
                $user_details->busi_address2 = Input::get('busi_address2');
                $user_details->busi_suburb = Input::get('busi_suburb');
                $user_details->busi_state = Input::get('busi_state');
                $user_details->busi_post = Input::get('busi_post');
                $user_details->busi_country = Input::get('busi_country');
                $user_details->bill_address1 = Input::get('bill_address1');
                $user_details->bill_address2 = Input::get('bill_address2');
                $user_details->bill_suburb = Input::get('bill_suburb');
                $user_details->bill_state = Input::get('bill_state');
                $user_details->bill_post = Input::get('bill_post');
                $user_details->bill_country = Input::get('bill_country');
                $success = $user_details->save();
                if ($success) {
                    $success = \Session::flash('flash_alert_notice', 'Address data updated successfully !');
                    return $success;
                } else {
                    return "there was some server error";
                }
            break;

            case 'logo':
                $user = User::select('name')->where('id' ,'=' ,$user_id)->first();
                $file = Input::file('logo'); 
                
                // Make directory if  not exist
                $temp_path = public_path('uploads/user/temp/');
                if (!File::exists($temp_path)) {
                    File::makeDirectory($temp_path, 0777, true, true);
                }
                $path_200 = public_path('uploads/user/thumbnail_200x200/');
                if (!File::exists($path_200)) {
                    File::makeDirectory($path_200, 0777, true, true);
                }
                $path_29 = public_path('uploads/user/thumbnail_29x29/');
                if (!File::exists($path_29)) {
                    File::makeDirectory($path_29, 0777, true, true);
                }                
                if ($file) {
                    $image_name = $file->getClientOriginalName();
                    $img_path = public_path('uploads/user/');
                    $extension = $file->getClientOriginalExtension(); // getting image extension
                    $fileName = $user_id . '_' .$user->name.".". $extension;
                    $uploaded_at = $file->move($temp_path, $fileName);
                    // Main image thumbnail
                    $img = Image::make($temp_path.$fileName)->resize(500,400);
                    if (file_exists($img_path.$fileName)) {
                            unlink($img_path.$fileName);
                    }                
                    $save_thumb = $img->save($img_path.$fileName);
                    // 200x200 image thumbnail
                    $img_thumb1 = Image::make($temp_path.$fileName)->resize(200,200);
                    if (file_exists($img_path. "thumbnail_200x200/" .$fileName)) {
                            unlink($img_path. "thumbnail_200x200/" .$fileName);
                    }                
                    $save_thumb = $img_thumb1->save($img_path. "thumbnail_200x200/" .$fileName);
                    // 29x29 image thumbnail
                    $img_thumb2 = Image::make($temp_path.$fileName)->resize(29,29);
                    if (file_exists($img_path. "thumbnail_29x29/" .$fileName)) {
                            unlink($img_path. "thumbnail_29x29/" .$fileName);
                        }
                    $save_thumb = $img_thumb2->save($img_path . "thumbnail_29x29/" .$fileName);            
                    // Remove temp folder image 
                    if (file_exists($temp_path.$fileName)) {
                        unlink($temp_path.$fileName);
                    }
                    
                } else {
                    $fileName = '';
                }
                // update user_details table data
                $user_details = UserDetails::firstOrNew(array('user_id' => $user_id));
                $user_details->user_id = $user_id;
                $user_details->logo_image = $fileName;
                $success = $user_details->save();
                if ($success) {
                    $success = \Session::flash('flash_alert_notice_logo', 'Logo has been updated successfully !');
                    return $success;
                } else {
                    return false;
                }
            break;

            default:
                return redirect('superadmin/user')->with('flash_alert_notice', 'There was nothing to update.');    
            break;
        }
       
    }

    /**
     * Remove the specif ied resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user, $id)
    {
        $id = Crypt::decrypt($id);
        $delete = User::destroy($id);
        return redirect('superadmin/user')->with('flash_alert_notice', 'User deleted successfully.');
    }

    /**
    * Check the specified email is exist
    * And return as true and false to angular
    */
    public function check_email($id, $email){    
        
        if (!empty($email)){
            $is_email = User::where('email',"=",$email)->where('is_deleted',"=",0)->where('id',"!=",$id)->get();
            if (isset($is_email[0]) && !empty($is_email[0])){
                echo 'true';
            }else{
                echo 'false';
            }
        }
        exit;
    }

    /**
    * Check the specified name and email is exist
    * And return as true and false
    */
    public function check_isexist()
    {        
        $id = Input::get('id');
        $email = Input::get('email');
        $username = Input::get('username');

        if (!empty($email)){
            $is_email = User::where('email',"=",$email)->where('id',"!=",$id)->where('is_deleted',"=",0)->get();
            
            if (isset($is_email[0]) && !empty($is_email[0])){
                echo 'true';
            }else{
                echo 'false';
            }
        }
        elseif (!empty($username)){
            $is_email = User::where('name',"=",$username)->where('id',"!=",$id)->where('is_deleted',"=",0)->get();
            
            if (isset($is_email[0]) && !empty($is_email[0])){
                echo 'true';
            }else{
                echo 'false';
            }
        }else{
            echo "Please enter value";
        }
        exit;
    }

    /**
    * List view by Data table for user Details
    */
    public function getData()
    {
        $user = User::select(['id', 'name', 'full_name', 'email', 'role_id','created_at'])->where('is_deleted', '=', 0)->where('role_id', '!=', 0)->orderBy('id','DESC')->get();                                  
        $company = Datatables::of($user)->addColumn('roles', function ($user) {   
                $role_name = Role::select(['name'])->where('id','=',$user->role_id)->get();                
                return !empty($role_name[0])?$role_name[0]->name:'';
            })->addColumn('logo_image', function ($user) {   
                $logo = UserDetails::select(['logo_image'])->where('user_id','=',$user->id)->where('logo_image','!=',NULL)->get();
                $temp_path = public_path('uploads/user/thumbnail_29x29/');
                $path = '';
                $logo_image_src = '';                                
                if(!empty($logo) && !empty($logo[0])) {                    
                    $path = $temp_path.$logo[0]->logo_image;
                    if(file_exists($path)) {
                        $logo_image_src = '<img src="'.url("/").'/uploads/user/thumbnail_29x29/'.$logo[0]->logo_image.'" />';
                    }else{
                        $logo_image_src = '<img width="29" height="29" src="'.url("/").'/uploads/user_avatar.png" />';
                    }
                }else{
                    $logo_image_src = '<img width="29" height="29" src="'.url("/").'/uploads/user_avatar.png" />';
                }                      
                return $logo_image_src;
            })->addColumn('created_at', function ($cmp) {
                $date = $cmp->created_at;
                $ts   = strtotime($date);
                return date('Y-m-d', $ts);
            })->addColumn('action', function ($cmp) {
                $id = Crypt::encrypt($cmp->id);
                return '<a id="editUser" class="btn btn-warning btn-xs edit-user" userid="'.$id.'" title="Edit"><i class="fa fa-pencil-square-o"></i></a>&nbsp;&nbsp;<a href="user-details/'.$id.'" class="btn btn-success btn-xs"><i class="fa fa-newspaper-o" title="View"></i></a>&nbsp;&nbsp;<a href="delete-user/'.$id.'" title="Delete" class="btn btn-danger btn-xs"  onclick="return confirm('."'Are you sure, you want to delete this User?'".');"><i class="glyphicon glyphicon-trash"></i></a>';
            })->make(true);
        return $company;
        exit;        
    }
   
    /**
    * List view by Data table for user Details
    */    
    public function getUserCompanies($userid)
    {        
        $companies = Company::select('id','name')->where('is_deleted', '=', 0)->get();
        $userCompniesMap = Helper::getCompanyUserMapping($userid);
        
        $data = array();
        foreach ($companies as $key => $value) {
            
            if(in_array($value->id, $userCompniesMap)){
                $data[$key]['companyId'] = $value->id;
                $data[$key]['name'] = $value->name;
            }
        }
        //print_r($data);
        return json_encode($data);
        exit;           
    }

     /**
     * Get associated Company list using DataTables.
     */
    public function getAllCompanies($user_id){  
        $companies = Company::select('id','name')->where('is_deleted', '=', 0)->get();
        $userCompniesMap = Helper::getCompanyUserMapping($user_id);
        
        $data = array();
        foreach ($companies as $key => $value) {
            $data[$key]['companyId'] = $value->id;
            $data[$key]['userId'] = $user_id;
            $data[$key]['name'] = $value->name;
            if(in_array($value->id, $userCompniesMap))
                $data[$key]['status'] = "linked";
            else
                $data[$key]['status'] = "unlinked";
        }
        //print_r($data);die;
        return json_encode($data);
        exit;        
    }

    /**
     *  associated Company to respective Project.
     */
    public function associateCompany(){
        //print_r(Input::all()); die;
        $companymap = new CompanyUserMapping;
        $companymap->user_id = Input::get('userId');
        $companymap->company_id = Input::get('companyId');
        $success = $companymap->save();
        exit;
    }

    /**
     *  associated Company to respective Project.
     */
    public function unlinkCompany(){
        //print_r(Input::all()); die;
        $userId = Input::get('userId');
        $companyId = Input::get('companyId');
        $companymapid = CompanyUserMapping::where('user_id','=',$userId)->where('company_id','=',$companyId)->delete();
        exit;
    }

    /**
     *  get user type list .
     */
    public function getuserTypes(){
        //print_r(Input::all()); die;
        $userRoles = Role::select('id','name')->where('type', '=', 1)->get();
        foreach ($userRoles as $key => $value) {
            $roles[$key]['id'] = $value->id;
            $roles[$key]['name'] = $value->name;
        }
        return $roles;
        exit;
    }

    /**
    * List view by Data table for Assciated user project Details
    */
    public function getUserProjectData(Request $request, $userid)
    {         
        $project = Project::select([ 'id','name', 'type','created_at'])->where('created_by', $userid)->where('is_deleted', '=', 0)->get();
        $projects = Datatables::of($project)->addColumn('created_at', function ($project) {
                $date = $project->created_at;
                $ts   = strtotime($date);
                return date('Y-m-d', $ts);
            })->addColumn('action', function ($prj) {
                $id = Crypt::encrypt($prj->id);
                return '<a   data-toggle="tooltip" title="Project Configuration"  href='.url('/').'/superadmin/projectConfiguration/projectConfigId/'.$id.' class="btn btn-success btn-xs"><i class="fa fa-star-o"></i></a></a>';
            })->make(true);        
        return $projects;
        exit;        
    }

}