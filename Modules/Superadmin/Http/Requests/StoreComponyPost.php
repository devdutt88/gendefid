<?php

namespace Modules\Superadmin\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Input;
class StoreComponyPost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {       
        $company_form = Input::get('company_form');
        if($company_form == "company")
        {
            switch ( $this->method() ) {
                case 'GET':
                case 'DELETE': {
                        return [ ];
                    }
                case 'POST': {
                        return [                               
                            'name'=> "required|min:3" ,                              
                            'email'=> "required|email" ,
                            'phone' => 'required|digits_between:8,15',                                                                  
                        ];
                    }
                case 'PUT':
                case 'PATCH': 
                default:break;
            }
        }else{
            return [ ];
        }
        
    }
    
}