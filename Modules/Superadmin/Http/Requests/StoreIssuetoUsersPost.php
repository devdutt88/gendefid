<?php

namespace Modules\Superadmin\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreIssuetoUsersPost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ( $this->method() ) {
                case 'GET':
                case 'DELETE': {
                        return [ ];
                    }
                case 'POST': {
                        return [                               
                            'name'=> "required|min:3" ,
                            'phone'=> "required|digits_between:8,15" ,
                        ];
                    }
                case 'PUT':
                case 'PATCH': 
                default:break;
            }
    }
    
}