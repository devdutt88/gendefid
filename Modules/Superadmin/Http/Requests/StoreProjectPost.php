<?php

namespace Modules\Superadmin\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreProjectPost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ( $this->method() ) {
                case 'GET':
                case 'DELETE': {
                        return [ ];
                    }
                case 'POST': {
                        return [                               
                            'name'=> "required|min:3",                            
                            'type'=> "required",
                            'address1'=> "required",
                            'suburb'=> "required",
                            'company'=> "required",
                            'postal_code'=> "required",
                            'state' => "required",
                            'country' => "required",

                        ];
                    }
                case 'PUT':
                case 'PATCH': 
                default:break;
            }
    }

    public function messages()
    {
        return [
            'required' => 'This field is required',
            'email' => 'Please enter a valid email',
            'same' => 'Password did not match',

        ];
    }
}
