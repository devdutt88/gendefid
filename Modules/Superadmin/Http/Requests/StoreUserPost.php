<?php

namespace Modules\Superadmin\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreUserPost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ( $this->method() ) {
                case 'GET':
                case 'DELETE': {
                        return [ ];
                    }
                case 'POST': {
                        return [                               
                            'username'=> "required|min:3",
                            'fullname'=> "required|min:3",
                            'password'=> "required|min:6|regex:/^(?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!%*#?&])[A-Za-z\d$@$!%*#?&]{6,20}$/",
                            'CPassword'=> "same:password",
                            'email'=> "required|email",
                            'phone' => "required|digits_between:8,15",

                        ];
                    }
                case 'PUT':
                case 'PATCH': 
                default:break;
            }
    }

    public function messages()
    {
        return [
            'required' => 'This field is required',
            'email' => 'Please enter a valid email',
            'same' => 'Password did not match',
            'regex' => 'Password should be atleast 6 characters long and should contain one number,one character and one special character.',

        ];
    }
}
