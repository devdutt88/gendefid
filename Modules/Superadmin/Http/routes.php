<?php
use Illuminate\Support\Facades\Route;
Route::group(['middleware' => ['web','admin','SuperadminCheck'], 'prefix' => 'superadmin', 'namespace' => 'Modules\Superadmin\Http\Controllers'], function()

{    
    Route::get('/', 'SuperadminController@index');
    Route::get('profile', 'SuperadminController@profile');
    Route::get('edit-admin-profile', 'SuperadminController@edit');
    Route::post('update_admin', 'SuperadminController@update');
    Route::post('changeAdminPassword','SuperadminController@changePassword');
    Route::get('check_admin_current_password/{pwd}', 'SuperadminController@check_current_password');
    Route::get('comp-graph/{id}','SuperadminController@getGrapthCounts');
    Route::get('companies/','SuperadminController@getCompanies');
    
    //User
    Route::resource('user', 'UserController');
    Route::get('add-user', 'UserController@create');
    Route::post('save_user', 'UserController@store');
    Route::post('updateUser', 'UserController@updateUser');
    Route::get('user_types', 'UserController@getuserTypes');
    Route::get('edit-user/{id}/{userform}', 'UserController@edit');
    Route::post('update_user', 'UserController@update');
    Route::get('user-details/{id}', 'UserController@show');
    Route::get('delete-user/{id}', 'UserController@destroy');
    Route::get('check_isexist', 'UserController@check_isexist');
    Route::get('user_emailid/{id}/{email}', 'UserController@check_email');
    Route::get('usr_list', 'UserController@getData');
    Route::get('getUserProjectData/{id}', 'UserController@getUserProjectData');
    Route::get('getUserCompanies/{id}', 'UserController@getUserCompanies');
    Route::get('getAllUserCompanies/{id}','UserController@getAllCompanies');
    Route::post('associateUserCompany','UserController@associateCompany');
    Route::post('unlinkUserCompany','UserController@unlinkCompany');
    // Route::post('changeUserPassword/{user_id}','UserController@changePassword');
    // Route::get('check_current_password/{id}/{pwd}', 'UserController@check_current_password');
    
    //Company
    Route::resource('company', 'CompanyController');
    Route::get('add-company', 'CompanyController@create');
    Route::post('save_company', 'CompanyController@store');
    Route::get('edit-company-details/{id}/{userform}', 'CompanyController@edit');
    Route::post('update_company', 'CompanyController@update');
    Route::get('company-details/{id}/', 'CompanyController@show');
    Route::get('comp_check_isexist', 'CompanyController@check_isexist');
    Route::get('delete-company/{id}', 'CompanyController@destroy');
    Route::get('cmp_list', 'CompanyController@getData');
    Route::get('getCompanyUserData/{id}', 'CompanyController@getCompanyUserData');
    Route::get('getCompanyProjectData/{id}', 'CompanyController@getCompanyProjectData');
    Route::get('comp_check_emailexist/{id}/{email}', 'CompanyController@check_emailexist');
    Route::post('get_theme_colors/{cmp_id}', 'CompanyController@getThemeColors');
    
    //Permissions
    Route::resource('/permissions', 'AuthenticationController@permissionList');
    Route::post('save-permission', 'AuthenticationController@storePermission');
    Route::get('check-permission/{id}/{name}', 'AuthenticationController@checkPermission');
    Route::get('all-permissions', 'AuthenticationController@getPermissionData');
    Route::get('edit-permission/{id}', 'AuthenticationController@editPermission');
    Route::post('update-permission', 'AuthenticationController@updatePermission');
    Route::get('delete-permission/{id}', 'AuthenticationController@destroyPermission');

    //Roles
    Route::resource('/roles', 'AuthenticationController@roleList');
    Route::get('add-role', 'AuthenticationController@createRole');
    Route::get('edit-role/{id}', 'AuthenticationController@editRole');
    Route::post('save-role', 'AuthenticationController@storeRole');
    Route::get('delete-role/{id}', 'AuthenticationController@destroy');
    Route::get('role/{id}', 'AuthenticationController@showRole');
    Route::post('update-role', 'AuthenticationController@updateRole');
    Route::get('check-role/{id}/{rolename}', 'AuthenticationController@checkRole');
    Route::get('role_list', 'AuthenticationController@getRoleData');
    Route::get('get_roles', 'AuthenticationController@getRoles');
    Route::get('role-permissions/{roleid}', 'AuthenticationController@getRolePermissions');
    Route::post('store-rolePermision/{roleid}', 'AuthenticationController@storeRolePermission');
    Route::get('deleteRole/{id}', 'AuthenticationController@deleteRole');
    

    // IssuedTo
    Route::resource('issueto', 'IssuetoController');
    Route::post('save-issueto', 'IssuetoController@saveIssueto');
    Route::get('issueto-details/{id}', 'IssuetoController@show');
    Route::get('edit-issueto/{id}', 'IssuetoController@editIssueto');
    Route::get('issueto_check_isexist', 'IssuetoController@issueto_check_isexist');
    Route::get('delete-issueto/{id}', 'IssuetoController@destroyIssueto');
    Route::get('issueto_list', 'IssuetoController@getIssuetoData');
    Route::get('issueto-user-list/{issueto_id}', 'IssuetoController@getIssuetoUsersData');
    Route::post('save-issueto-user', 'IssuetoController@saveIssuetoUser');
    Route::post('edit-issueto-user/{id}', 'IssuetoController@editIssuetoUser');
    Route::post('update_issueto_user', 'IssuetoController@update_issueto_user');
    Route::get('delete-issueto-user/{id}', 'IssuetoController@destroyIssuetoUser');
    Route::get('issueto_user_check_isexist', 'IssuetoController@issueto_user_check_isexist');
    Route::post('issueto_user_details', 'IssuetoController@get_issueto_user_details');
    Route::get('getAllIssuetoCompanies/{id}','IssuetoController@getAllCompanies');
    Route::get('getIssuetoCompanies/{id}', 'IssuetoController@getIssuetoCompanies');
    Route::post('associateIssuetoCompany','IssuetoController@associateCompany');
    Route::post('unlinkIssuetoCompany','IssuetoController@unlinkCompany');
    Route::get('issueto_emailid/{id}/{email}', 'IssuetoController@checkIssuetoEmail');
    Route::get('issuetoUser_emailid/{id}/{issueto_id}/{email}', 'IssuetoController@checkIssuetoUserEmail');
    Route::get('ist-locations','IssuetoController@locationTags');

    //Quality Checklist
    Route::resource('quality-checklist', 'QualityChecklistController');
    Route::post('save-checklist', 'QualityChecklistController@saveChecklist');
    Route::get('get-quality-checklist', 'QualityChecklistController@getChecklistData');
    Route::get('quality-checklist-details/{id}', 'QualityChecklistController@show');
    Route::post('edit-quality-checklist/{id}', 'QualityChecklistController@editChecklist');
    Route::post('edit-checklist-task/{id}', 'QualityChecklistController@editChecklistTask');
    Route::get('getAllChecklistCompanies/{id}','QualityChecklistController@getAllCompanies');
    Route::get('getChecklistCompanies/{id}', 'QualityChecklistController@getChecklistCompanies');
    Route::post('associateChecklistCompany','QualityChecklistController@associateCompany');
    Route::post('unlinkChecklistCompany','QualityChecklistController@unlinkCompany');
    Route::post('save-checklist-task', 'QualityChecklistController@saveChecklistTask');
    Route::get('get-quality-checklist-tasks/{checklist_id}', 'QualityChecklistController@getChecklistTaskData');
    Route::get('delete-quality-checklist/{id}', 'QualityChecklistController@destroyChecklist');
    Route::get('delete-quality-checklist-task/{id}', 'QualityChecklistController@destroyChecklistTask');
    Route::get('qc-locations','QualityChecklistController@locationTags');

    //Project
    Route::get('project', 'ProjectController@index');
    Route::get('add-project', 'ProjectController@create');
    Route::post('save_project', 'ProjectController@store');
    Route::get('edit-project/{id}/{projectform}', 'ProjectController@edit');
    Route::get('project_list', 'ProjectController@getData');
    Route::get('project-details/{id}', 'ProjectController@show');
    Route::get('delete-project/{id}', 'ProjectController@destroy');
    Route::post('update_project', 'ProjectController@update');
    Route::get('getProjectCompanyData/{id}', 'ProjectController@getProjectCompanyData');
    Route::get('getProjectCompanies/{id}', 'ProjectController@getProjectCompanies');
    Route::get('getAllCompanies/{id}','ProjectController@getAllCompanies');
    Route::post('associateCompany','ProjectController@associateCompany');
    Route::post('unlinkCompany','ProjectController@unlinkCompany');
    Route::post('project_isexist/{id}','ProjectController@projectIsexist');

    // PDF 
    Route::get('generatepdf', 'ProjectController@generatePDF');

});