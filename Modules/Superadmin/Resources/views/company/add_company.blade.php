@extends('superadmin::layouts.master')

<!-- BEGIN CONTAINER -->
	
@section('content')
	
	
	<div class="page-content-wrapper">
		<div class="page-content">
			
			<!-- BEGIN PAGE HEADER-->
				
			@include('superadmin::partials.breadcrumb')
			
			<!-- END PAGE HEADER-->
			
			<!-- BEGIN CONTENT -->
			<div class="row">
				<div class="col-md-12">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-equalizer font-blue-hoki"></i>
								<span class="caption-subject font-blue-hoki bold uppercase">Add Company</span>
								<span class="caption-helper"></span>
							</div>
						</div>
						
						<div class="portlet-body form">
							<!-- BEGIN FORM-->
							
							{!! Form::open(array('url' => 'superadmin/save_company','method' => "post", 'class'=>"horizontal-form", 'id'=>"cmp_section", 'autocomplete'=>"on|off" ,'enctype'=>"multipart/form-data", 'accept-charset'=>"utf-8")) !!}
								@include('superadmin::company.form')
							{!! Form::close() !!}
							<!-- END FORM-->
						</div>
					</div>	
				</div>
			</div>
			<!-- END CONTENT -->
			<div class="clearfix">
			</div>
			
		</div>
	</div>
@stop
<!-- END CONTAINER -->