@extends('superadmin::layouts.master')

@section('content')
<!-- BEGIN CONTAINER -->

	<div class="page-content-wrapper">
		<div class="page-content">
		
			<!-- BEGIN PAGE HEADER-->			
			@include('superadmin::partials.breadcrumb')
			<!-- END PAGE HEADER-->
			
			<!-- BEGIN CONTENT -->
			<div class="row">
				<div class="col-md-12">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-equalizer font-blue-hoki"></i>
								<span class="caption-subject font-blue-hoki bold uppercase">Edit Company</span>
								<span class="caption-helper"></span>
							</div>
							<div class="tools"></div>
						</div>
						<div class="portlet-body form">
							<!-- BEGIN FORM-->										
							<form action="{{ url('superadmin/update_company') }}" method="post" class="horizontal-form" id="cmp_section" autocomplete="on|off" enctype="multipart/form-data" accept-charset="utf-8">

								{{ csrf_field() }}
								<input type="hidden" name="hid" id="hid" value="@if(!empty($company->id)){{$company->id}}@endif">											
								@include('superadmin::company.form')
							</form>
							<!-- END FORM-->
						</div>
					</div>
				</div>
			</div>
			<!-- END FORM CONTENT -->
			<div class="clearfix">
			</div>
			
		</div>
	</div>
	<!-- END CONTENT -->	
	@stop
<!-- END CONTAINER -->