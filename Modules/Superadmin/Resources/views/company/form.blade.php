<div class="form-body">
<!--<h3 class="form-section">Person Info</h3>-->
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">			
			{!! Form::label('Name', 'Name', array('class' => 'control-label')); !!} <span class="red">*</span>			
			@php $cname = '' @endphp
			@if(!empty($company->name))
				@php $cname = $company->name @endphp
			@endif<!--  -->

			{!! Form::text('name', $cname, array('class' => 'form-control','id'=>'name')) !!}

			@if ($errors->has('name'))
			    <span class=error>The company name field is required.</span>
			@endif
			<!--<span class="help-block">
			This is inline help </span>-->
			</div>
		</div>
		<!--/span-->
		<div class="col-md-6">
			<div class="form-group">				
			{!! Form::label('Registration Number', 'Registration Number', array('class' => 'control-label')); !!} <span class="red"></span>
			@php $cregis = '' @endphp
			@if(!empty($company->registration_number))
				@php $cregis = $company->registration_number @endphp
			@endif
			{!! Form::text('cregis', $cregis, array('class' => 'form-control','id'=>'cregis')) !!}
			<!--<span class="help-block">
			Select your gender </span>-->
			</div>
		</div>		
		<!--/span-->
	</div>
	<!--/row-->
	<div class="row">
		
	<!--/span-->
	<div class="col-md-6">
		<div class="form-group">		
		{!! Form::label('Email id', 'Email id', array('class' => 'control-label')); !!} <span class="red">*</span>
			@php $email = '' @endphp
			@if(!empty($company->email))
				@php $email = $company->email @endphp
			@endif
			{!! Form::text('email', $email, array('class' => 'form-control','id'=>'email')) !!}

		@if ($errors->has('email'))
			    <span class=error>{{ $errors->first('email') }}</span>
			@endif
		</div>
	</div>
	<div class="col-md-6">
			<div class="form-group">				
				{!! Form::label('Phone', 'Phone', array('class' => 'control-label')); !!} <span class="red">*</span>
				
				@php $phone = '' @endphp
				@if(!empty($company->phone))
					@php $phone = $company->phone @endphp
				@endif
				{!! Form::text('phone', $phone, array('class' => 'form-control','id'=>'phone')) !!}

				@if ($errors->has('phone'))
				    <span class=error>{{ $errors->first('phone') }}</span>
				@endif
			</div>
		</div>
	<!--/span-->
	</div>
	<!--/row-->
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">				
				{!! Form::label('Description', 'Description', array('class' => 'control-label')); !!} <span class="red"></span>				
				@php $description = '' @endphp
				@if(!empty($company->description))
					@php $description = $company->description @endphp
				@endif
				{!! Form::textarea('description', $description, array('class' => 'form-control','id'=>'description','cols'=>10,'rows'=>2)) !!}

				<div id="chars">120 Character Left</div>

				@if ($errors->has('description'))
				    <span class=error>{{ $errors->first('description') }}</span>
				@endif
			</div>
		</div>
	</div>
	<!--/row-->	
	<hr>
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<label class="control-label"><u><i><strong>Address Details:</strong></i></u></label>
			</div>
		</div>
	</div>
	<!--/row for buisness-->
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">			
				{!! Form::label('Business Address 1', 'Business Address 1', array('class' => 'control-label')); !!} <span class="red"></span>			
				@php $bus_add1 = '' @endphp
				@if(!empty($cmp_details[0]->busi_address1))
					@php $bus_add1 = $cmp_details[0]->busi_address1 @endphp
				@endif<!--  -->
				{!! Form::text('bus_add1', $bus_add1, array('class' => 'form-control','id'=>'bus_add1')) !!}
			</div>
		</div>
		<!--/span-->
		<div class="col-md-6">
			<div class="form-group">
				{!! Form::label('Business Address 2', 'Business Address 2', array('class' => 'control-label')); !!} <span class="red"></span>			
				@php $bus_add2 = '' @endphp
				@if(!empty($cmp_details[0]->busi_address2))
					@php $bus_add2 = $cmp_details[0]->busi_address2 @endphp
				@endif<!--  -->
				{!! Form::text('bus_add2', $bus_add2, array('class' => 'form-control','id'=>'bus_add2')) !!}
			</div>
		</div>
		<!--/span-->
	</div>

	<!--/row-->
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">

			{!! Form::label('Business Suburb', 'Business Suburb', array('class' => 'control-label')); !!} <span class="red"></span>			
			@php $bus_suburb = '' @endphp
			@if(!empty($cmp_details[0]->busi_suburb))
				@php $bus_suburb = $cmp_details[0]->busi_suburb @endphp
			@endif<!--  -->
			{!! Form::text('bus_suburb', $bus_suburb, array('class' => 'form-control','id'=>'bus_suburb')) !!}

			</div>
		</div>
		<!--/span-->
		<div class="col-md-6">
			<div class="form-group">			
			{!! Form::label('Business State', 'Business State', array('class' => 'control-label')); !!} <span class="red"></span>			
			@if(!empty($cmp_details[0]->busi_state))						
				{{ Form::select('bus_state', $option_state, $cmp_details[0]->busi_state,  ['class' => 'select2_category form-control','id'=>'bus_state']) }}
			@else
				{{ Form::select('bus_state', $option_state, null,  ['class' => 'select2_category form-control','id'=>'bus_state']) }}
			@endif
			
			</div>
		</div>
		<!--/span-->
	</div>
	<!--/row-->
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">			
			{!! Form::label('Business Post', 'Business Post', array('class' => 'control-label')); !!} <span class="red"></span>					
			@php $busi_post = '' @endphp
			@if(!empty($cmp_details[0]->busi_post))
				@php $busi_post = $cmp_details[0]->busi_post @endphp
			@endif<!--  -->
			{!! Form::text('bus_post', $busi_post, array('class' => 'form-control','id'=>'bus_post')) !!}

			</div>
		</div>
		<!--/span-->
		<div class="col-md-6">
			<div class="form-group">			
			{!! Form::label('Business Country', 'Business Country', array('class' => 'control-label')); !!} <span class="red"></span>
                {!! Form::text('bus_country', 'Australia', array('class' => 'form-control','id'=>'bus_country','readonly'=>'readonly')) !!}
			</div>
		</div>
		<!--/span-->
	</div>

	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<label class="control-label checkboxes">					
					<input name="chck1" id="chck1" class="chck1" type="checkbox"> Business Address is same as billing address.
				</label>
			</div>
		</div>
	</div>

	<!--/row for billing-->
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">			
				{!! Form::label('Billing Address 1', 'Billing Address 1', array('class' => 'control-label')); !!} <span class="red"></span>					
				@php $buld_add1 = '' @endphp
				@if(!empty($cmp_details[0]->bill_address1))
					@php $buld_add1 = $cmp_details[0]->bill_address1 @endphp
				@endif<!--  -->
				{!! Form::text('buld_add1', $buld_add1, array('class' => 'form-control','id'=>'buld_add1')) !!}
			</div>
		</div>
		<!--/span-->
		<div class="col-md-6">
			<div class="form-group">
				{!! Form::label('Billing Address 2', 'Billing Address 2', array('class' => 'control-label')); !!} <span class="red"></span>					
				@php $buld_add2 = '' @endphp
				@if(!empty($cmp_details[0]->bill_address2))
					@php $buld_add2 = $cmp_details[0]->bill_address2 @endphp
				@endif<!--  -->
				{!! Form::text('buld_add2', $buld_add2, array('class' => 'form-control','id'=>'buld_add2')) !!}
			</div>
		</div>
		<!--/span-->
	</div>
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">			
				{!! Form::label('Billing Suburb', 'Billing Suburb', array('class' => 'control-label')); !!} <span class="red"></span>					
				@php $buld_suburb = '' @endphp
				@if(!empty($cmp_details[0]->bill_suburb))
					@php $buld_suburb = $cmp_details[0]->bill_suburb @endphp
				@endif<!--  -->
				{!! Form::text('buld_suburb', $buld_suburb, array('class' => 'form-control','id'=>'buld_suburb')) !!}
			</div>
		</div>
		<!--/span-->
		<div class="col-md-6">
			<div class="form-group">			
			{!! Form::label('Billing State', 'Billing State', array('class' => 'control-label')); !!} <span class="red"></span>
			@if(!empty($cmp_details[0]->bill_state))
				{{ Form::select('buld_state', $option_state, $cmp_details[0]->bill_state,  ['class' => 'select2_category form-control','id'=>'buld_state']) }}
			@else
				{{ Form::select('buld_state', $option_state, null,  ['class' => 'select2_category form-control','id'=>'buld_state']) }}
			@endif
			</div>
		</div>
		<!--/span-->
	</div>
	<!--/row-->
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">			
				{!! Form::label('Billing Post', 'Billing Post', array('class' => 'control-label')); !!} <span class="red"></span>					
				@php $bill_post = '' @endphp
				@if(!empty($cmp_details[0]->bill_post))
					@php $bill_post = $cmp_details[0]->bill_post @endphp
				@endif<!--  -->
				{!! Form::text('buld_post', $bill_post, array('class' => 'form-control','id'=>'buld_post')) !!}
			</div>
		</div>
		<!--/span-->
		<div class="col-md-6">
			<div class="form-group">			
			{!! Form::label('Billing Country', 'Billing Country', array('class' => 'control-label')); !!} <span class="red"></span>			 	

                {!! Form::text('buld_country', 'Australia', array('class' => 'form-control','id'=>'buld_country','readonly'=>'readonly')) !!}

			</div>
		</div>
		<!--/span-->
	</div>
	<!--/row-->
	<?php /*
	<div class="row">		
		<!--/span-->
		<div class="col-md-6">
			<div class="form-group">
				<label class="btn btn-default btn-file">
				    Logo <input type="file" name="logo" id="logo">				    
				</label>
				@if(!empty($cmp_details[0]->logo_image))
				    <img class="logo_img" src="{{url('/')}}/uploads/company/{{$cmp_details[0]->logo_image}}">
				    @endif
			</div>
		</div>
		<!--/span-->
	</div>
	*/ ?>
<!--/row-->

</div>
<div class="form-actions">
	<button type="button" class="btn btn-default" onClick="history.go(-1); return false;"> Cancel </button>
	<button type="submit" class="btn blue pull-right"><i class="fa fa-check"></i> Submit</button>
</div>
