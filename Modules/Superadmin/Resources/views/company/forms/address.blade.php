<div class="modal fade" id="addressModal">
	<div class="modal-dialog">
	    <div class="modal-content">
			<!-- BEGIN FORM-->
		 	<form id="addressInfo" name="addressInfo" class="horizontal-form" ng-submit="saveCompanyDetail()">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true" ng-click="reset(addressInfo)">×</button>
					<h4 class="modal-title">Company Address Details</h4>
				</div>
				<div class="modal-body">
					{{ csrf_field() }}
					<input type="hidden" id="company_id" name="company_id" ng-model="cmpdetails.company_id"/>
					<div class="form-body">
						<!--/row for buisness-->
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
								
								{!! Form::label('Business Address 1', 'Business Address 1', array('class' => 'control-label')); !!} <span class="red"></span>

								{!! Form::text('bus_add1', '', array('class' => 'form-control','id'=>'bus_add1','ng-model'=>'cmpdetails.bus_add1')) !!}

								</div>
							</div>
							<!--/span-->
							<div class="col-md-6">
								<div class="form-group">								

								{!! Form::label('Business Address 2', 'Business Address 2', array('class' => 'control-label')); !!} <span class="red"></span>
								
								{!! Form::text('bus_add2', '', array('class' => 'form-control','id'=>'bus_add2','ng-model'=>'cmpdetails.bus_add2')) !!}

								</div>
							</div>
							<!--/span-->
						</div>

						<!--/row-->
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
								
								{!! Form::label('Business Suburb', 'Business Suburb', array('class' => 'control-label')); !!} <span class="red"></span>
								
								{!! Form::text('bus_suburb', '', array('class' => 'form-control','id'=>'bus_suburb','ng-model'=>'cmpdetails.bus_suburb')) !!}

								</div>
							</div>
							<!--/span-->
							<div class="col-md-6">
								<div class="form-group">								
								{!! Form::label('Business State', 'Business State', array('class' => 'control-label')); !!} <span class="red"></span>

								{{ Form::select('bus_state', $option_state, null,  ['class' => 'select2_category form-control','id'=>'bus_state', 'ng-model'=>"cmpdetails.bus_state"]) }}
								
								</div>
							</div>
							<!--/span-->
						</div>
						<!--/row-->
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
								
								{!! Form::label('Business Post', 'Business Post', array('class' => 'control-label')); !!} <span class="red"></span>
								
								{!! Form::text('bus_post', '', array('class' => 'form-control','id'=>'bus_post','ng-model'=>'cmpdetails.bus_post')) !!}

								</div>
							</div>
							<!--/span-->
							<div class="col-md-6">
								<div class="form-group">
								{!! Form::label('Business Country', 'Business Country', array('class' => 'control-label')); !!} <span class="red"></span>

								{!! Form::text('bus_country', '', array('class' => 'form-control','id'=>'bus_country','ng-model'=>'cmpdetails.bus_country','readonly'=>'readonly','ng-required'=>'true')) !!}

								</div>
							</div>
							<!--/span-->
						</div>

						<!-- <div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label"><input name="chck1" id="chck1" class="chck1" type="checkbox"> Business Address is same as billing address.</label>
								</div>
							</div>
						</div>
				 		-->
						<!--/row for billing-->
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
								{!! Form::label('Billing Address 1', 'Billing Address 1', array('class' => 'control-label')); !!} <span class="red"></span>
								
								{!! Form::text('billing_add1', '', array('class' => 'form-control','id'=>'billing_add1','ng-model'=>'cmpdetails.billing_add1')) !!}

								</div>
							</div>
							<!--/span-->
							<div class="col-md-6">
								<div class="form-group">								
								{!! Form::label('Billing Address 2', 'Billing Address 2', array('class' => 'control-label')); !!} <span class="red"></span>
								
								{!! Form::text('billing_add2', '', array('class' => 'form-control','id'=>'billing_add2','ng-model'=>'cmpdetails.billing_add2')) !!}
								</div>
							</div>
							<!--/span-->
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
								
								{!! Form::label('Billing Suburb', 'Billing Suburb', array('class' => 'control-label')); !!} <span class="red"></span>
								
								{!! Form::text('billing_suburb', '', array('class' => 'form-control','id'=>'billing_suburb','ng-model'=>'cmpdetails.billing_suburb')) !!}

								</div>
							</div>
							<!--/span-->
							<div class="col-md-6">
								<div class="form-group">								
								{!! Form::label('Billing State', 'Billing State', array('class' => 'control-label')); !!} <span class="red"></span>

								{{ Form::select('billing_state', $option_state, null,  ['class' => 'select2_category form-control','id'=>'billing_state','ng-model'=>"cmpdetails.billing_state"]) }}
								</div>
							</div>
							<!--/span-->
						</div>
						<!--/row-->
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
								{!! Form::label('Billing Post', 'Billing Post', array('class' => 'control-label')); !!} <span class="red"></span>
								
								{!! Form::text('billing_post', '', array('class' => 'form-control','id'=>'billing_post','ng-model'=>'cmpdetails.billing_post')) !!}
								</div>
							</div>
							<!--/span-->
							<div class="col-md-6">
								<div class="form-group">
								{!! Form::label('Billing Country', 'Billing Country', array('class' => 'control-label')); !!} <span class="red"></span>
								{!! Form::text('billing_country', '', array('class' => 'form-control','id'=>'billing_country','ng-model'=>'cmpdetails.billing_country','readonly'=>'readonly','ng-required'=>'true')) !!}
								</div>
							</div>
							<!--/span-->
						</div>
						<!--/row-->		
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal" ng-click="reset(addressInfo)">Cancel</button>
					<button type="submit" class="btn blue" ng-click="saveCompanyDetail()">Update</button>
					<span ng-show="showLoader">
	                	<img src="{{url('/')}}/assets/img/input-spinner.gif">
	                </span>
				</div>
			</form>
		</div>
	</div>
</div>