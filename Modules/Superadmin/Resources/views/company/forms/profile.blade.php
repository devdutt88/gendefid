<div class="modal-dialog">
	    <div class="modal-content">
			<form id="cmpInfo" name="cmpInfo" class="horizontal-form" ng-submit="saveComapny(@if(isset($cmp->id))'{{Crypt::encrypt($cmp->id)}}'@endif)" novalidate>
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true" ng-click="reset(cmpInfo)">×</button>
					<h4 class="modal-title">Company Personal Details</h4>					
				</div>				
				<div class="modal-body">
					<p class="error text-center" id="custom_error"></p>
					{{ csrf_field() }}
					<input type="hidden" id="cmp_id" name="cmp_id" ng-model="cmp_id" value="@if(isset($cmp->id)){{$cmp->id}}@endif" />
					<div class="form-body">
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									{!! Form::label('Company Name', 'Company Name', array('class' => 'control-label')); !!} <span class="red">*</span>					
									
									{!! Form::text('name', '', array('class' => 'form-control','id'=>'name','ng-model'=>'cmp.name','readonly'=>'readonly','ng-required'=>'true')) !!}
								</div>
							</div>
							<!--/span-->
							<div class="col-md-6">
								<div class="form-group">
									{!! Form::label('Company Registration Number', 'Company Registration Number', array('class' => 'control-label')); !!} <span class="red"></span>				
									{!! Form::text('cregis', '', array('class' => 'form-control','id'=>'cregis','ng-model'=>'cmp.cregis')) !!}

								</div>
							</div>
							<!--/span-->
						</div>	
						<div class="row">
							<!--/span-->
							
							<!--/span-->

							<div class="col-md-6">
								<div class="form-group">
									{!! Form::label('Email', 'Email', array('class' => 'control-label')); !!} <span class="red">*</span>
									<input type="email" id="email" name="email" class="form-control" placeholder="" ng-model="cmp.email" email-exists ng-model-options="{ updateOn: 'blur' }" ng-required="true"/>
								    <p ng-messages="cmpInfo.email.$error"  ng-if="cmpInfo.email.$dirty" role="alert">
									    <span class="error" ng-message="required">Email address is required.</span>
									    <span class="error" ng-message="email">This field must be a valid email address.</span>
									    <span class="error" ng-message="emailExists">Email address already exist.</span>
								  	</p>

								  	

									@if ($errors->has('email'))
									    <span class=error>{{ $errors->first('email') }}</span>
									@endif
								</div>
							</div>
							<div class="col-md-6">
									<div class="form-group">
										
										{!! Form::label('Phone', 'Phone', array('class' => 'control-label')); !!} <span class="red">*</span>

										{!! Form::number('phone', '', array('class' => 'form-control','id'=>'phone','ng-model'=>'cmp.phone','ng-minlength'=>'8','ng-maxlength'=>'15')) !!}

										<span style="color:red" ng-show="cmpInfo.phone.$dirty && cmpInfo.phone.$invalid">
											<span ng-show="cmpInfo.phone.$error.required || cmpInfo.phone.$error.number">Valid phone number is required.</span>
											<span ng-show="((cmpInfo.phone.$error.minlength ||
	                           	cmpInfo.phone.$error.maxlength) && cmpInfo.phone.$dirty)">phone number between 8 to 15 digits.</span>
										</span>
										@if ($errors->has('phone'))
										    <span class=error>{{ $errors->first('phone') }}</span>
										@endif
									</div>
								</div>

						</div>
						<div class="row">
							<div class="form-group">
								<!-- <div class="col-md-6">
									<div class="form-group">
										<label class="control-label">Phone <span class="red">*</span></label>
										
										<input type="number" id="phone" name="phone" ng-minlength="8" ng-maxlength="15"  class="form-control" placeholder=""  ng-model="cmp.phone" required>

										<span style="color:red" ng-show="cmpInfo.phone.$dirty && cmpInfo.phone.$invalid">
											<span ng-show="cmpInfo.phone.$error.required || cmpInfo.phone.$error.number">Valid phone number is required.</span>
											<span ng-show="((cmpInfo.phone.$error.minlength ||
	                           	cmpInfo.phone.$error.maxlength) && cmpInfo.phone.$dirty)">phone number between 8 to 15 digits.</span>
										</span>
										@if ($errors->has('phone'))
										    <span class=error>{{ $errors->first('phone') }}</span>
										@endif
									</div>
								</div>
 -->
								<div class="col-md-12">
									<div class="form-group">										
										{!! Form::label('Description', 'Description', array('class' => 'control-label')); !!} <span class="red"></span>
														
										{!! Form::textarea('description', '', array('class' => 'form-control','id'=>'description', 'ng-trim'=>'false', 'maxlength'=>'120','ng-model'=>'cmp.description','rows'=>2,'cols'=>6)) !!}
										<span>@{{120 - cmp.description.length}} Character Left</span>
									</div>
								</div>							
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal" ng-click="reset(cmpInfo)">Cancel</button>
					<button type="submit" class="btn blue" ng-disabled="cmpInfo.name.$dirty && cmpInfo.name.$invalid ||  
					cmpInfo.email.$dirty && cmpInfo.email.$invalid ||  
					cmpInfo.ctype.$dirty && cmpInfo.ctype.$invalid ||  
					cmpInfo.phone.$dirty && cmpInfo.phone.$invalid" id="company_form" >Update</button>
					<span ng-show="showLoader">
	                	<img src="{{url('/')}}/assets/img/input-spinner.gif">
	                </span>
				</div>
			</form>
		</div>
	</div>