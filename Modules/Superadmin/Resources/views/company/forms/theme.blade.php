<div class="modal-dialog">
	<div class="modal-content min-height-450">
	
		<form id="themeSettingsFrm" name="themeSettingsFrm" class="horizontal-form" ng-submit="saveThemeSettings(@if(isset($cmp->id))'{{Crypt::encrypt($cmp->id)}}'@endif,'themeColors')">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h4 class="modal-title">User Personal Details</h4>
			</div>
			<div class="modal-body">
				
				{{ csrf_field() }}
				<div class="form-body">
				    <div class="row">
						<div class="col-md-6">
							<div class="portlet-title tabbable-line">
							    <ul class="nav nav-tabs pull-left">
									<li class="bold col-md-12 theme_settings active" ng-class="tab_active1">
										<a href="#tab_1_1" data-toggle="tab" ng-click="setColorPicker('bar')">Header Footer Bars</a>
									</li>
									<li class="bold col-md-12 theme_settings" ng-class="tab_active3">
										<a href="#tab_1_3" data-toggle="tab" ng-click="setColorPicker('label')">Text Color</a>
									</li>
									<li class="bold col-md-12 theme_settings" ng-class="tab_active2">
										<a href="#tab_1_2" data-toggle="tab" ng-click="setColorPicker('btn')">Buttons Background</a>
									</li>
									<li class="bold col-md-12 theme_settings" ng-class="tab_active4">
										<a href="#tab_1_4" data-toggle="tab" ng-click="setColorPicker('btn_text')">Button Text Color</a>
									</li>
								</ul>
							</div>
						</div>
				    		<?php /*<div class="tab-pane active col-md-12" id="tab_1_1">
				    			Header Footer Bars
				    		</div>
				    		<div class="tab-pane active col-md-12" id="tab_1_2">
				    			Buttons Background
				    		</div>
				    		<div class="tab-pane active col-md-12" id="tab_1_3">
				    			Text Colors
				    		</div>
				    	</div>*/ ?>
						<div class="col-md-6">
						
							<div class="" id="">
								<div class="row">
									<color-picker ng-model="theme.cur_val" ng-change="changeTabValue(theme.cur_tab)"></color-picker>
									<input type="text" readonly="readonly" class="input_color" ng-model="theme.cur_val">
									<span class="show_color" style="background-color: @{{theme.cur_val}}"></span>
								</div>
							</div>
							<input type="hidden" readonly="readonly" ng-model="theme.cst_bar">
							<input type="hidden" readonly="readonly" ng-model="theme.cst_btn">
							<input type="hidden" readonly="readonly" ng-model="theme.cst_label">
							<input type="hidden" readonly="readonly" ng-model="theme.cst_btn_text">
							<?php /*
							<div class="tab-content">
								<div class="tab-pane active" id="tab_1_1">
									<div class="row">
										<color-picker ng-model="theme.cst_bar"></color-picker>
										<input type="text" readonly="readonly" class="input_color" ng-model="theme.cst_bar">
										<span class="show_color" style="background-color: @{{theme.cst_bar}}"></span>
									</div>
								</div>
								<!-- END PERSONAL INFO TAB -->
								<!-- Profile Dialog -->
								<!-- USER ADDRESS TAB -->
								<div class="tab-pane" id="tab_1_2">
									<div class="row">
										<color-picker ng-model="theme.cst_btn"></color-picker>
										<input type="text" readonly="readonly" class="input_color" ng-model="theme.cst_btn">
										<span class="show_color" style="background-color: @{{theme.cst_btn}}"></span>
									</div>
								</div>

								<div class="tab-pane" id="tab_1_3">
									<div class="row">
										<color-picker ng-model="theme.cst_label"></color-picker>
										<input type="text" readonly="readonly" class="input_color" ng-model="theme.cst_label">
										<span class="show_color" style="background-color: @{{theme.cst_label}}"></span>
									</div>
								</div>
							</div>
				    		*/ ?>
				    	</div> 
				    </div>
	    		</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" style="float:left" ng-click="saveThemeSettings(@if(isset($cmp->id))'{{Crypt::encrypt($cmp->id)}}'@endif,'setToDefault')">Set to Default</button>
				<button type="button" class="btn btn-default" data-dismiss="modal" ng-click="reset(themeSettingsFrm)">Cancel</button>
				<button type="submit" class="btn blue" ng-disabled="theme.cst_bar.$dirty && theme.cst_bar.$invalid ||  
				theme.cst_btn.$dirty && theme.cst_btn.$invalid ||  
				theme.cst_label.$dirty && theme.cst_label.$invalid" id="theme_settings_form" >Save</button>
				<span ng-show="showLoader">
                	<img src="{{url('/')}}/assets/img/input-spinner.gif">
                </span>
			</div>
		</form>

	</div>
</div>