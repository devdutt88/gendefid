@extends('superadmin::layouts.master')
<!-- BEGIN CONTAINER -->
@section('content')
<!-- BEGIN CONTENT -->
<link href="{{url('css/angular-color-picker.css')}}" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="{{url('js/angular-color-picker.js')}}"></script>
<div class="page-content-wrapper" ng-app="companyModule">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content" ng-controller="companyController">
        <!-- BEGIN PAGE HEADER-->
        @include('superadmin::partials.breadcrumb')
        <!-- END PAGE HEADER-->

		<a href="javascript:;" class="btn-theme_settings" ng-click="themeSettings(@if(isset($cmp->id))'{{Crypt::encrypt($cmp->id)}}'@endif)">
			<i class="icon-settings"></i>
		</a>

        <div class="clearfix"></div>
        <!-- END DASHBOARD STATS 1-->
        	<div class="row">
			    <div class="col-md-6 col-xs-12">
			    <!-- Alert Section -->
					@if(Session::has('flash_alert_notice'))
				        <div class="alert alert-success alert-dismissable cst-alert cst-normal-alert">
				          	<!-- <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button> -->
				          	<i class="icon fa fa-check"></i> 
				           	{{ Session::get('flash_alert_notice') }} 
				        </div>
				   	@endif
				<!-- End Alert Section -->
			        <div class="portlet light bordered topbox-min-height ">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="icon-microphone font-dark hide"></i>
                                <span class="caption-subject bold font-dark uppercase"> Company Details</span>
                                <span class="caption-helper"></span>
                            </div>
		                    <div class="actions">
								<a href="" class="btn btn-circle btn-default btn-sm"" data-toggle="tooltip" title="Edit Details" ng-click="showModelFrm('{{Crypt::encrypt($cmp->id)}}', 'company')">
								<i class="fa fa-pencil"></i> Edit </a>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="row">
                            	<input type="hidden" name="hid" id="hid" value="{{Crypt::encrypt($cmp->id)}}" />
                            	<div class="col-md-12">	
									<div class="col-sm-4 col-md-12 col-lg-4 text-center cst-logo-section">
										<div class="form-group">
											@if(!empty($cmp_details[0]->logo_image))
												<img id="logo_image" src="{{url('/')}}/uploads/company/{{$cmp_details[0]->logo_image}}" alt="Company Logo" />
											@else
												<img id="logo_image" src="{{url('/')}}/uploads/company_icon.png" alt="Company Logo" />
											@endif 
					                	</div>
						                <div class="form-group">
											<span class="btn btn-circle cst-yellow fileinput-button">
												    @if(!empty($cmp_details[0]->logo_image))
								                    	<span>Change logo</span>
								                    @else
								                    	<span>Add Logo</span>
								                    @endif
								                    <input type='file' name="logo" id="imgInp" ng-files="getTheFiles($files)"/>
											</span>
										</div>
										<div class="form-group">
											<button type="submit" id="upload_btn" style="display:none;" class="btn btn-success btn-circle start" ng-click="uploadLogo('{{Crypt::encrypt($cmp->id)}}','logo','{{$cmp->name}}')">
							                    <i class="glyphicon glyphicon-upload"></i>
							                    <span>Start upload</span>
							                </button>
							                <span ng-show="showLogoLoader">
							                	<img src="{{url('/')}}/assets/img/ajax-loader.gif">
							                </span>
								        </div>
									</div>
					                <div class="col-sm-8 col-md-12 col-lg-8">
				                	    <div class="row">
											<label class="form-control-static col-xs-4"><strong>Name:</strong></label>
											<div class="col-xs-8 form-control-static">
													{{ucfirst($cmp->name)}}
											</div>
										</div>
	                                    <div class="row">
											<label class="form-control-static col-xs-4"><strong>Reg Number:</strong></label>
											<div class="col-xs-8 form-control-static">
													{{$cmp->registration_number}}
											</div>
										</div>
	                                    <div class="row">
											<label class="form-control-static col-xs-4"><strong>Email-id:</strong></label>
											<div class="col-xs-8 form-control-static">
												{{$cmp->email}}
										 	</div>
										</div>
	                                    <div class="row">
											<label class="form-control-static col-xs-4"><strong>Phone:</strong></label>
											<div class="col-xs-8 form-control-static">
												{{$cmp->phone}}
										 	</div>
										</div>
					                </div>
                                </div>
                            </div>
                            <hr>
							<div class="row">
								<label class="col-xs-12"><strong>Description</strong></label>
								<div class="col-xs-12">
										 {{$cmp->description}}
								</div> 
							</div>
                        </div>
                    </div>
			    </div>
			    <div class="col-md-6 col-xs-12">
			    <!-- Alert Section -->
					@if(Session::has('flash_alert_notice_address'))
				        <div class="alert alert-success alert-dismissable cst-alert cst-normal-alert">
				          	<!-- <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button> -->
				          	<i class="icon fa fa-check"></i>
				           	{{ Session::get('flash_alert_notice_address') }} 
				        </div>
				   	@endif
				<!-- End Alert Section -->
			        <div class="portlet light bordered topbox-min-height">
			            <div class="portlet-title tabbable-line">
			                <div class="caption">
			                    <i class=" icon-social-twitter font-dark hide"></i>
			                    <span class="caption-subject font-dark bold uppercase">Company Address</span>
			                </div>
                            <div class="actions">
									<a href="" class="btn btn-circle btn-default btn-sm"" data-toggle="tooltip" title="Edit Address" ng-click="showModelFrm('{{Crypt::encrypt($cmp->id)}}', 'address')">
									<i class="fa fa-pencil"></i> Edit </a>
                            </div>

			            </div>
			            <!-- Address Dialog -->
							@include('superadmin::company.forms.address')
	    				<!-- End of Address Dialog -->
			            <div class="portlet-body">
	                        <!-- BEGIN: Actions -->
	                        <h5 class="caption-subject font-blue-steel bold uppercase">Business Address</h5>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<label class="control-label col-xs-12 col-sm-3 col-md-5"><strong>Address:</strong></label>
										<div class="col-md-7">
											@if(!empty($cmp_details[0]->busi_address1))
												 {{$cmp_details[0]->busi_address1}}
											@endif
											@if(!empty($cmp_details[0]->busi_address2))
												 {{$cmp_details[0]->busi_address2}}
											@endif
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<label class="control-label col-xs-12 col-sm-3 col-md-5"><strong>Suburb:</strong></label>
										<div class="col-xs-12 col-sm-9 col-md-7">
											@if(!empty($cmp_details[0]->busi_suburb))
												 {{$cmp_details[0]->busi_suburb}}
											@endif
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<label class="control-label col-xs-12 col-sm-3 col-md-5"><strong>State:</strong></label>
										<div class="col-xs-12 col-sm-9 col-md-7">
											@if(!empty($cmp_details[0]->busi_state))
												 {{$cmp_details[0]->busi_state}}
											@endif
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<label class="control-label col-xs-12 col-sm-3 col-md-5"><strong>Post:</strong></label>
										<div class="col-xs-12 col-sm-9 col-md-7">
											@if(!empty($cmp_details[0]->busi_post))
												 {{$cmp_details[0]->busi_post}}
											@endif
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<label class="control-label col-xs-12 col-sm-3 col-md-5"><strong>Country:</strong></label>
										<div class="col-xs-12 col-sm-9 col-md-7">
											@if(!empty($cmp_details[0]->busi_country))
												 {{$cmp_details[0]->busi_country}}
											@endif
										</div>
									</div>
								</div>
							</div>
							<h5 class="caption-subject font-blue-steel bold uppercase">Billing Address</h5>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<label class="control-label col-xs-12 col-sm-3 col-md-5"><strong>Address:</strong></label>
										<div class="col-xs-12 col-sm-9 col-md-7">
											@if(!empty($cmp_details[0]->bill_address1))
												 {{$cmp_details[0]->bill_address1}}
											@endif		
											@if(!empty($cmp_details[0]->bill_address2))
												 {{$cmp_details[0]->bill_address2}}
											@endif
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<label class="control-label col-xs-12 col-sm-3 col-md-5"><strong>Suburb:</strong></label>
										<div class="col-xs-12 col-sm-9 col-md-7">
											@if(!empty($cmp_details[0]->bill_suburb))
												 {{$cmp_details[0]->bill_suburb}}
											@endif
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<label class="control-label col-xs-12 col-sm-3 col-md-5"><strong>State:</strong></label>
										<div class="col-xs-12 col-sm-9 col-md-7">
											@if(!empty($cmp_details[0]->bill_state))
												 {{$cmp_details[0]->bill_state}}
											@endif
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<label class="control-label col-xs-12 col-sm-3 col-md-5"><strong>Post:</strong></label>
										<div class="col-xs-12 col-sm-9 col-md-7">
											@if(!empty($cmp_details[0]->bill_post))
												 {{$cmp_details[0]->bill_post}}
											@endif
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<label class="control-label col-xs-12 col-sm-3 col-md-5"><strong>Country:</strong></label>
										<div class="col-xs-12 col-sm-9 col-md-7">
											@if(!empty($cmp_details[0]->bill_country))
												 {{$cmp_details[0]->bill_country}}
											@endif
										</div>
									</div>
								</div>
							</div>
	                        <!-- END: Actions -->
			            </div>
			        </div>
			    </div>
			</div>
			<!--Assoicative users-->
			<div class="row">
			    <div class="col-lg-12 col-md-12 col-xs-12">
			        <div class="portlet light bordered box-min-height">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="icon-microphone font-dark hide"></i>
                                <span class="caption-subject bold font-dark uppercase">Users</span>
                                <span class="caption-helper"></span>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="row">
                            	<div class="col-md-12">
									<div class="form-group">
                                        <table class="table" id="assoc_user_list">
										<thead>
											<tr>
												<th> Name </th>
												<th> Email ID </th>
												<th> Phone </th>
												<th> Role </th>
												<th> Created Date </th>
											</tr>
										</thead>
									</table>
									</div>
								</div>
                        	</div>
                        </div>
                    </div>
			    </div>
		    </div>
		    <div class="row">
			    <div class="col-lg-12 col-md-12 col-xs-12">
			        <div class="portlet light bordered box-min-height">
			            <div class="portlet-title tabbable-line">
			                <div class="caption">
			                    <i class=" icon-social-twitter font-dark hide"></i>
			                    <span class="caption-subject font-dark bold uppercase">Projects</span>
			                </div> 
			                <!-- <div class="actions">
                                <i class="icon-bubbles font-dark hide"></i>
			                    <span class="caption-subject font-dark bold uppercase">
			                    	<a class="btn btn-icon-only cst-yellow" ng-click="showModelFrm({{$cmp->id}},'address')"><i class="fa fa-edit"></i> </a>
			                    </span>
                            </div> -->
			            </div>
			            <div class="portlet-body">
			                <div class="tab-content">
			                    <div class="row">
	                            	<div class="col-md-12">
										<div class="form-group">
	                                        <table class="table" id="assoc_project_list">
											<thead>
												<tr>
													<th> Name </th>
													<th> Type </th>	
													<th> Author </th>
													<th> Created Date </th>
												</tr>
											</thead>
										</table>
										</div>
									</div>
	                        	</div>
			                </div>
			            </div>
			        </div>
			    </div>
			</div>
			<!-- Company Dialog -->
		    <div class="modal fade" id="companyModal" close="cancel()">
				@include('superadmin::company.forms.profile')
		    </div>
		    <!-- Address Dialog -->
				@include('superadmin::company.forms.address')
			<!-- End of Address Dialog -->
			<!-- End of Company Dialog -->
			<div class="modal fade" id="themeSettingsModal" close="cancel()">
				@include('superadmin::company.forms.theme')
			</div>
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->                         
@stop
<!-- END CONTAINER -->
