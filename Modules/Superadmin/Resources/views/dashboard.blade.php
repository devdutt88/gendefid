@extends('superadmin::layouts.master')

@section('content')
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper" ng-app="dashboardModule">
	<div class="page-content " ng-controller="dashboardController">
		
		<div class="page-head">
			<div class="page-title">
				<h1>Dashboard <small>company statistics</small></h1>
			</div>
		</div>
		<!-- END PAGE HEADER-->
		<!-- BEGIN DASHBOARD STATS -->
		<div class="row margin-top-10">
			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<div class="dashboard-stat blue-madison">
					<div class="visual">
						<i class="fa fa-bar-chart-o"></i>
					</div>
					<div class="details">
						<div class="number">
							 {{$total_companies}}
						</div>
						<div class="desc">
							 Companies
						</div>
					</div>
					<a class="more" href="{{url('superadmin/company')}}">
					View more <i class="m-icon-swapright m-icon-white"></i>
					</a>
				</div>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<div class="dashboard-stat red-intense">
					<div class="visual">
						<i class="fa fa-users"></i>
					</div>
					<div class="details">
						<div class="number">
							 {{$total_users}}
						</div>
						<div class="desc">
							 Users
						</div>
					</div>
					<a class="more" href="{{url('superadmin/user')}}">
					View more <i class="m-icon-swapright m-icon-white"></i>
					</a>
				</div>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<div class="dashboard-stat green-haze">
					<div class="visual">
						<i class="fa fa fa-film"></i>
					</div>
					<div class="details">
						<div class="number">
							 {{$project}}
						</div>
						<div class="desc">
							 Projects
						</div>
					</div>
					<a class="more" href="{{url('superadmin/project')}}">
					View more <i class="m-icon-swapright m-icon-white"></i>
					</a>
				</div>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<div class="dashboard-stat purple-plum">
					<div class="visual">
						<i class="fa fa-handshake-o"></i>
					</div>
					<div class="details">
						<div class="number">
							 {{$total_issueto}}
						</div>
						<div class="desc">
							 Issue To
						</div>
					</div>
					<a class="more" href="{{url('superadmin/issueto')}}">
					View more <i class="m-icon-swapright m-icon-white"></i>
					</a>
				</div>
			</div>
		</div>
		<!-- End Row -->
		<div class="row" ng-init="getCompnies()">
			<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 pull-right">
			<select name="company" class="form-control" ng-model="company" ng-change="cmpGraphData()">
				<option value="">Select Company</option>
				<option ng-repeat="x in companies" value="@{{x.id}}">@{{x.name}}</option>
			</select>
			</div>
		</div>
		<!-- End Row -->
		<div class="row margin-top-10" ng-init="cmpGraphData()">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<canvas id="bar" height="100" class="chart chart-bar" chart-data="data" chart-labels="labels" chart-series="series" chart-dataset-override="datasetOverride" chart-colors="colors"></canvas>
			</div>
		</div>
		<!-- End Row -->
		<div class="clearfix">
		</div>
	</div>
</div>
<!-- END CONTENT -->
@stop