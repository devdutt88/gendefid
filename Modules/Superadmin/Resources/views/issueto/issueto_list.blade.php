@extends('superadmin::layouts.master')

@section('content')
<!-- BEGIN CONTAINER -->
	<div class="page-content-wrapper" ng-app="issuetoModule">
		<div class="page-content" ng-controller="issuetoController">
			@include('superadmin::partials.breadcrumb')
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN EXAMPLE TABLE PORTLET-->
					<div class="portlet box grey-cascade">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-globe"></i>Issueto List
							</div>
							<div class="actions">
								<div class="portlet-input input-inline input-small">
									<div class="input-icon text-right">  
										<button class="btn btn-circle btn-sm btn-default" id="" ng-click="showIssueto('add', 0)"> <i class="fa fa-plus"></i> Add New </button>
									</div>
								</div>
							</div>
						</div>
						<div class="portlet-body">
							<!-- Alert Section -->
								@if(Session::has('flash_alert_notice'))
							        <div class="alert alert-success alert-dismissable cst-alert cst-list-alert">
							          	<!-- <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button> -->
							          	<i class="icon fa fa-check"></i>  
							           	{{ Session::get('flash_alert_notice') }} 
							        </div>
							   	@endif
							<!-- End Alert Section -->
						<!-- BEGIN CONTENT -->
	                       	@include('superadmin::issueto.issueto_form')
							<table class="table table-striped table-bordered table-hover" id="IssuedtoList">
								<thead>
									<tr>
										<th> Company </th>
										<th> Author </th>
										<th> Phone </th>
										<th> Email </th>
										<th> Activity </th>
										<th width="120"> Created </th>
										<th width="100"> Action </th>
									</tr>
								</thead>
							</table>
						</div>
					</div>
					<!-- END EXAMPLE TABLE PORTLET-->
				</div>
			</div>
			<!-- END CONTENT -->
			<div class="clearfix">
			</div>
		</div>
	</div>
	
<!-- END CONTAINER -->
@stop