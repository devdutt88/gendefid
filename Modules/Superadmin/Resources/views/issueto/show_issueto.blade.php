@extends('superadmin::layouts.master')

@section('content')
<!-- BEGIN CONTAINER -->
<link href="{{url('/')}}/css/ng-tags-input.min.css" rel="stylesheet" type="text/css"> 
<link href="{{url('/')}}/assets/css/tasks.css" rel="stylesheet" type="text/css"/>
<div class="page-content-wrapper" ng-app="issuetoModule">
	<div class="page-content" ng-controller="issuetoController">
		<!-- {!! $breadcrumbs !!} -->
		@include('superadmin::partials.breadcrumb')
		<!-- BEGIN CONTENT -->
		<div class="row">
			<div class="col-lg-8">
				<!-- Alert Section -->
					@if(Session::has('flash_alert_notice'))
				        <div class="alert alert-success alert-dismissable cst-alert cst-normal-alert">
				          	<!-- <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button> -->
				          	<i class="icon fa fa-check"></i>  
				           	{{ Session::get('flash_alert_notice') }} 
				        </div>
				   	@endif
				<!-- End Alert Section -->
				<div class="portlet light bordered box-min-height">
					<div class="portlet-title">
                        <div class="caption">
                            <i class="icon-users"></i>
                            <span class="caption-subject bold uppercase"> {{$issueto->name}} </span>
                            <span class="caption-helper">users list ...</span>
                        </div>
                       	<div class="actions">
							<a href="" class="btn btn-circle btn-default btn-md" ng-click="showIssuetoUser('add', {{$issueto_id}}, 0);">
							<i class="fa fa-plus"></i> Add New User </a>
							<!-- <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title="">
							</a> -->
						</div>
                    </div>
					<div class="portlet-body">

						@include('superadmin::issueto.issueto_user_form') <!-- Edit Issueto User Form -->
						
						<div class="form-horizontal">
							<div class="form-body">
								<table class="table table-striped table-bordered table-hover" id="IssuedUserstoList">
									<thead>
										<tr>
											<th> Name </th>
											<th> Email </th>
											<th> Phone </th>
											<th> Tags </th>
											<th width="80"> Action </th>
										</tr>
									</thead>
								</table>
							</div>
						</div>
					</div>
				</div>	
			</div>

			@include('superadmin::issueto.forms.companies')
			<div class="col-lg-4" ng-init="issuetoCompanyList({{$issueto->id}})">
				<div class="portlet light bordered tasks-widget bordered">
                    <div class="portlet-title caption font-green-sharp">
                        <div class="caption caption-md">
                            <!-- <i class="icon-screen-desktop font-green-sharp"></i> -->
                            <span class="caption-subject bold font-green-sharp uppercase"> Associated Compnies </span>
                        </div>
                   		<div class="actions">
							<a href="" class="btn btn-circle btn-default btn-sm" ng-click="showModelFrm({{$issueto->id}} , 'companies')">
							<i class="fa fa-pencil"></i> </a>
						</div>
                    </div>
                    <div class="portlet-body" >
		                <div class="task-content">
		                	
							<div class="scroller portlet-height">
								<!-- START TASK LIST -->
		                        <ul class="task-list">
		                            <li class="" ng-repeat="list in iclists">
										<div class="task-title">
											<span class="task-title-sp"> @{{list.name | uppercase }}</span>
										</div>
		                            </li>
		                        </ul>
		                    </div>
		                </div>
		            </div>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
		<!-- END CONTENT -->
	</div>
</div>
<!-- END CONTAINER -->
@stop