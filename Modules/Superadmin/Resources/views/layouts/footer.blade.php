</div>
<!-- BEGIN FOOTER -->
<div class="page-footer">
	<div class="page-footer-inner pull-right">
		 Copyright © 2017 All Rights Reserved - Wiseworking
	</div>
	<div class="scroll-to-top">
		<i class="icon-arrow-up"></i>
	</div>
</div>
<!-- END FOOTER -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<![endif]-->
<script type="text/javascript" src="{{ elixir('js/backend.js') }}"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- custom js -->
<script type="text/javascript">
var baseUrl='{{url('/')}}'
</script>
@if(!empty($data['scripts']))
	@foreach ($data['scripts'] as $js)
		<script src="{{url('/')}}/js/{{$js}}.js"></script>
	@endforeach
@endif
<script>
jQuery(document).ready(function() {    
	//Metronic.init(); // init metronic core componets      
	Layout.init(); // init current layout
	$('[data-toggle="tooltip"]').tooltip(); 
});
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>