<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8"/>
<title> Wiseworking | Superadmin</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<meta content="" name="description"/>
<meta content="" name="author"/>
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" href="{{ elixir('css/backend.css') }}">
<link rel="stylesheet" href="{{url('/')}}/css/custom.css">
<!-- END GLOBAL MANDATORY STYLES -->
<link rel="shortcut icon" href="{{url('/')}}/img/favicon.ico"/>
</head>
<!-- END HEAD -->

<!-- BEGIN BODY -->
<body class="page-footer-fixed page-header-fixed page-quick-sidebar-over-content page-sidebar-closed-hide-logo page-container-bg-solid">
<!-- BEGIN HEADER -->
@php $admin = \Session::get('adminData');
@endphp
<div class="page-header -i navbar navbar-fixed-top">
	<!-- BEGIN HEADER INNER -->
	<div class="page-header-inner">
		<!-- BEGIN LOGO -->
		<div class="page-logo">
			<a href="{{url('/')}}">
			<img src="{{url('/')}}/assets/img/logo_middle.png" alt="logo" class="logo-default"/>
			</a>
			<div class="menu-toggler sidebar-toggler">
			</div>
		</div>
		<!-- END LOGO -->
		<!-- BEGIN RESPONSIVE MENU TOGGLER -->
		<!-- END RESPONSIVE MENU TOGGLER -->
		<!-- BEGIN TOP NAVIGATION MENU -->
		<div class="page-top">
			<a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse"></a>
			<div class="top-menu">
				<ul class="nav navbar-nav pull-right">
					<!-- BEGIN NOTIFICATION DROPDOWN -->
					
					<li class="dropdown dropdown-user dropdown-dark">
						<a data-close-others="true" data-hover="dropdown" data-toggle="dropdown" class="dropdown-toggle" href="javascript:;">
						<span class="username username-hide-on-mobile">
							@if(!empty($admin['fullname']))
								{{$admin['fullname']}}
							@else
								Superadmin
							@endif
							 
						</span>
						<!-- DOC: Do not remove below empty space(&nbsp;) as its purposely used -->
						
						@if(!empty($admin['logo_image']))
							<img class="img-circle" src="{{url('/')}}/uploads/user/thumbnail_29x29/{{$admin['logo_image']}}" alt="your image" />
						@else
							<img alt="default" class="img-circle" src="{{url('/')}}/assets/img/avatar.png"/>
						@endif
						<!-- <img src="../../assets/admin/layout4/img/avatar9.jpg" class="img-circle" alt=""> -->
						</a>
						<ul class="dropdown-menu dropdown-menu-default">
							<li>
								<a href="{{ url('/superadmin/profile') }}">
								<i class="icon-user"></i> My Profile </a>
							</li>						
							<li>
								<a href="{{ url('/logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
								<i class="icon-key"></i> Log Out </a>
								<form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
	                                {{ csrf_field() }}
	                            </form>
							</li>
						</ul>
					</li>
				</ul>
			</div>
		</div>
		<!-- END TOP NAVIGATION MENU -->
	</div>
	<!-- END HEADER INNER -->
</div>
<!-- END HEADER -->
<div class="clearfix">
</div>
<div class="page-container">
