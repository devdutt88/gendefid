<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8"/>
<title> Wiseworking | Superadmin</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<meta content="" name="description"/>
<meta content="" name="author"/>
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
<link href="{{url('/')}}/assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link href="{{url('/')}}/assets/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css"/>
<link href="{{url('/')}}/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="{{url('/')}}/assets/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
<link href="{{url('/')}}/assets/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css"/>
<link href="{{url('/')}}/assets/js/jstree/dist/themes/default/style.min.css" rel="stylesheet" type="text/css"/>
<link href="{{url('/')}}/assets/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css" rel="stylesheet" type="text/css"/>
<link href="{{url('/')}}/assets/css/components-rounded.css" id="style_components" rel="stylesheet" type="text/css"/>
<link href="{{url('/')}}/assets/css/plugins.css" rel="stylesheet" type="text/css"/>
<link href="{{url('/')}}/assets/css/layout.css" rel="stylesheet" type="text/css"/>
<!-- <link href="{{url('/')}}/assets/css/grey.css" rel="stylesheet" type="text/css" id="style_color"/> -->
<link href="{{url('/')}}/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
<link href="{{url('/')}}/css/jquery.contextMenu.css" rel="stylesheet" type="text/css"/>
<link href="{{url('/')}}/assets/css/custom.css" rel="stylesheet" type="text/css"/>
<link href="{{url('/')}}/assets/css/light.css" rel="stylesheet" type="text/css"/>
<!-- END GLOBAL MANDATORY STYLES -->
<link rel="shortcut icon" href="{{url('/')}}/assets/img/favicon.ico"/>
<link href="{{url('/')}}/assets/dist/sweetalert.css" rel="stylesheet" type="text/css"/>
</head>
<!-- END HEAD -->

<!-- BEGIN BODY -->
<body class="page-header-fixed page-quick-sidebar-over-content page-sidebar-closed-hide-logo page-container-bg-solid page-footer-fixed">
	<!-- BEGIN HEADER -->
<div class="page-header -i navbar navbar-fixed-top">
	<!-- BEGIN HEADER INNER -->
	<div class="page-header-inner">
		<!-- BEGIN LOGO -->
		<div class="page-logo">
			<a href="{{url('/')}}">
			<img src="{{url('/')}}/assets/img/logo_middle.png" alt="logo" class="logo-default"/>
			</a>
			<div class="menu-toggler sidebar-toggler">
			</div>
		</div>
		<!-- END LOGO -->
		<!-- BEGIN HORIZANTAL MENU -->
		<!-- DOC: Remove "hor-menu-light" class to have a horizontal menu with theme background instead of white background -->
		<!-- DOC: This is desktop version of the horizontal menu. The mobile version is defined(duplicated) in the responsive menu below along with sidebar menu. So the horizontal menu has 2 seperate versions -->
		<div class="hor-menu hor-menu-light hidden-sm hidden-xs">
			<ul class="nav navbar-nav">
				<!-- DOC: Remove data-hover="megadropdown" and data-close-others="true" attributes below to disable the horizontal opening on mouse hover -->
				<li class="classic-menu-dropdown">
					<a href="{{url('superadmin')}}">
					<i class="icon-home"></i>
					Dashboard <span class="">
					</span>
					</a>
				</li>				

				<!-- <li class="classic-menu-dropdown">
					<a data-toggle="dropdown" href="{{url('superadmin/company')}}">
					<i class="fa fa-building-o"></i>
					Company <i class="fa fa-angle-down"></i>
					</a>
					<ul class="dropdown-menu pull-left">
						<li>
							<a href="{{url('superadmin/company')}}">
							<i class="icon-list"></i> Company List </a>
						</li>
						<li>
							<a href="{{url('superadmin/add-company')}}">
							<i class="icon-plus"></i> Add Company </a>
						</li>						
					</ul>
				</li>

				<li class="classic-menu-dropdown">
					<a data-toggle="dropdown" href="{{url('superadmin/user')}}">
					<i class="icon-user"></i>
					User <i class="fa fa-angle-down"></i>
					</a>
					<ul class="dropdown-menu pull-left">
						<li>
							<a href="{{url('superadmin/user')}}">
							<i class="fa fa-users"></i> User List </a>
						</li>
						<li>
							<a href="{{url('superadmin/add-user')}}">
							<i class="fa fa-user"></i> Add User </a>
						</li>						
					</ul>
				</li>
				
				<li class="classic-menu-dropdown">
					<a data-toggle="dropdown" href="{{url('superadmin/project')}}">
					<i class="icon-film"></i>
					Project <i class="fa fa-angle-down"></i>
					</a>
					<ul class="dropdown-menu pull-left">
						<li>
							<a href="{{url('superadmin/project')}}">
							<i class="icon-list"></i> Project List </a>
						</li>
						<li>
							<a href="{{url('superadmin/add-project')}}">
							<i class="icon-plus"></i> Add Project </a>
						</li>						
					</ul>
				</li>

				<li class="classic-menu-dropdown">
					<a data-toggle="dropdown" href="{{url('superadmin/permision')}}">
					<i class="fa fa-bell" aria-hidden="true"></i>
					Permision <i class="fa fa-angle-down"></i>
					</a>
					<ul class="dropdown-menu pull-left">
						<li>
							<a href="{{url('superadmin/permision')}}">
							<i class="icon-list"></i> Permision List </a>
						</li>
						<li>
							<a href="{{url('superadmin/add-permission')}}">
							<i class="icon-plus"></i> Add Permision </a>
						</li>						
					</ul>
				</li>				

				<!-- <li class="classic-menu-dropdown">
					<a href="{{url('superadmin/quality-checklist')}}">
					<i class="fa fa-thumbs-up"></i>
					Quality Checklist
					</a>					
				</li> -->

				<!--<li class="classic-menu-dropdown">
					<a data-toggle="dropdown" href="{{url('superadmin/role')}}">
					<i class="fa fa-tasks" aria-hidden="true"></i>
					Role <i class="fa fa-angle-down"></i>
					</a>	
					<ul class="dropdown-menu pull-left">
						<li>
							<a href="{{url('superadmin/role')}}">
							<i class="icon-list"></i> Role List </a>
						</li>
						<li>
							<a href="{{url('superadmin/add-role')}}">
							<i class="icon-plus"></i> Add Role </a>
						</li>						
					</ul>				
				</li>

				<li class="classic-menu-dropdown">
					<a href="{{url('superadmin/issueto')}}">
					<i class="fa fa-handshake-o"></i>
					Issueto
					</a>					
				</li> -->

			</ul>				
		</div>
		<!-- END HORIZANTAL MENU -->	
		<!-- BEGIN RESPONSIVE MENU TOGGLER -->
		<a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
		</a>
		<!-- END RESPONSIVE MENU TOGGLER -->
		<!-- BEGIN TOP NAVIGATION MENU -->
		<div class="page-top">
			<div class="top-menu">
				<ul class="nav navbar-nav pull-right">
					<!-- BEGIN NOTIFICATION DROPDOWN -->
					<li class="dropdown dropdown-user dropdown-dark">
						<a data-close-others="true" data-hover="dropdown" data-toggle="dropdown" class="dropdown-toggle" href="javascript:;">
						<span class="username username-hide-on-mobile">
							@if(!empty($admin['fullname']))
								{{$admin['fullname']}}
							@else
								Superadmin
							@endif
							 
						</span>
						<!-- DOC: Do not remove below empty space(&nbsp;) as its purposely used -->
						
						@if(!empty($admin['logo_image']))
							<img class="img-circle" src="{{url('/')}}/uploads/user/thumbnail_29x29/{{$admin['logo_image']}}" alt="your image" />
						@else
							<img alt="default" class="img-circle" src="{{url('/')}}/assets/img/avatar.png"/>
						@endif
						<!-- <img src="../../assets/admin/layout4/img/avatar9.jpg" class="img-circle" alt=""> -->
						</a>
						<ul class="dropdown-menu dropdown-menu-default">
							<li>
								<a href="{{ url('/superadmin/profile') }}">
								<i class="icon-user"></i> My Profile </a>
							</li>						
							<li>
								<a href="{{ url('/logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
								<i class="icon-key"></i> Log Out </a>
								<form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
	                                {{ csrf_field() }}
	                            </form>
							</li>
						</ul>
					</li>
				</ul>
			</div>
		</div>
		<!-- END TOP NAVIGATION MENU -->
	</div>
	<!-- END HEADER INNER -->
</div>
<!-- END HEADER -->
<div class="clearfix">
</div>
<div class="page-container">