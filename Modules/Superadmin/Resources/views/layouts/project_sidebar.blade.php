<div class="page-sidebar-wrapper">
   <div class="page-sidebar navbar-collapse collapse">
      <!-- BEGIN SIDEBAR MENU -->
      <ul class="page-sidebar-menu " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
         <!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
         <li class="sidebar-toggler-wrapper">
            <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
            <div class="sidebar-toggler">
            </div>
            <!-- END SIDEBAR TOGGLER BUTTON -->
         </li>
         <!-- DOC: To remove the search box from the sidebar you just need to completely remove the below "sidebar-search-wrapper" LI element -->
                
         
            <li>
         
            <a href="javascript:;">
               <i class="icon-film"></i>
               <span class="title">
                  @if(!empty($project[0]))
                     {{$project[0]->name}}
                  @endif
               </span>
            </a>           
         </li>

         @if(Request::segment(3) == "location")
            <li class="start active open">
         @else
            <li>
         @endif
            <a href="{{url('superadmin/projectConfiguration/location')}}">
            <i class="fa fa-map-marker"></i>
            <span class="title">Location</span>
            @if(Request::segment(3) == "location")
               <span class="arrow open"></span>
            @else
               <span class="arrow "></span>
            @endif            
            </a>           
         </li>

         @if(Request::segment(3) == "dfdf")
            <li class="start active open">
         @else
            <li>
         @endif
            <a href="{{url('superadmin/projectConfiguration/issueto')}}">
            <i class="fa fa-handshake-o"></i>
            <span class="title">Issuedto</span>
            @if(Request::segment(3) == "locatfdfdion")
               <span class="arrow open"></span>
            @else
               <span class="arrow "></span>
            @endif            
            </a>           
         </li>

         <!-- @if(Request::segment(3) == "dfdf")
            <li class="start active open">
         @else
            <li>
         @endif
            <a href="{{url('superadmin/projectConfiguration/location')}}">
            <i class="fa fa-gift"></i>
            <span class="title">Standard Items</span>
            @if(Request::segment(3) == "locatfdfdion")
               <span class="arrow open"></span>
            @else
               <span class="arrow "></span>
            @endif            
            </a>           
         </li>

         @if(Request::segment(3) == "dfdf")
            <li class="start active open">
         @else
            <li>
         @endif
            <a href="{{url('superadmin/projectConfiguration/location')}}">
            <i class="fa fa-check"></i>
            <span class="title">Checklist</span>
            @if(Request::segment(3) == "locatfdfdion")
               <span class="arrow open"></span>
            @else
               <span class="arrow "></span>
            @endif            
            </a>           
         </li>

         @if(Request::segment(3) == "dfdf")
            <li class="start active open">
         @else
            <li>
         @endif
            <a href="{{url('superadmin/projectConfiguration/location')}}">
            <i class="fa fa-eye"></i>
            <span class="title">Progress Monitoring</span>
            @if(Request::segment(3) == "locatfdfdion")
               <span class="arrow open"></span>
            @else
               <span class="arrow "></span>
            @endif            
            </a>           
         </li>

         @if(Request::segment(3) == "dfdf")
            <li class="start active open">
         @else
            <li>
         @endif
            <a href="{{url('superadmin/projectConfiguration/location')}}">
            <i class="fa fa-thumbs-up"></i>
            <span class="title">Quality</span>
            @if(Request::segment(3) == "locatfdfdion")
               <span class="arrow open"></span>
            @else
               <span class="arrow "></span>
            @endif            
            </a>           
         </li>

         @if(Request::segment(3) == "dfdf")
            <li class="start active open">
         @else
            <li>
         @endif
            <a href="{{url('superadmin/projectConfiguration/location')}}">
            <i class="icon-calendar"></i>
            <span class="title">Project Calendar</span>
            @if(Request::segment(3) == "locatfdfdion")
               <span class="arrow open"></span>
            @else
               <span class="arrow "></span>
            @endif            
            </a>           
         </li> -->
      </ul>
      <!-- END SIDEBAR MENU -->
   </div>
</div>