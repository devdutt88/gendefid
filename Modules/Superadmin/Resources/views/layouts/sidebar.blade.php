<div class="page-sidebar-wrapper">
   <div class="page-sidebar navbar-collapse collapse">
      <!-- BEGIN SIDEBAR MENU -->
      <ul class="page-sidebar-menu " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
         <!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
         <li class="sidebar-toggler-wrapper">
            <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
            <div class="sidebar-toggler">
            </div>
            <!-- END SIDEBAR TOGGLER BUTTON -->
         </li>
         <!-- DOC: To remove the search box from the sidebar you just need to completely remove the below "sidebar-search-wrapper" LI element -->
         @if(Request::segment(2) == "")
            <li class="start active open">
         @else
         <li>
         @endif
            <a href="{{url('superadmin/')}}">
            <i class="icon-home"></i>
            <span class="title">Dashboard</span>
            <span class="selected"></span>
            <!-- <span class="arrow open"></span> -->
            </a>             
         </li> 
        
         @if(Request::segment(2) == "company" || Request::segment(2) == "add-company" || Request::segment(2) == "edit-company-details" || Request::segment(2) == "company-details")
            <li class="start active open">
         @else
            <li>
         @endif
            <a href="{{url('superadmin/company')}}">
            <i class="icon-screen-desktop"></i>
            <span class="title">Company</span>
            @if(Request::segment(2) == "company" || Request::segment(2) == "add-company" || Request::segment(2) == "edit-company-details" || Request::segment(2) == "company-details")
               <span class="arrow open"></span>
            @else
               <span class="arrow "></span>
            @endif
            
            </a>
            <ul class="sub-menu">             
               @if(Request::segment(2) == "company" || Request::segment(2) == "company-details")
                  <li class="active">
               @else
                  <li>
               @endif               
                  <a href="{{url('superadmin/company')}}">
                  <i class="icon-list"></i>
                  Company List</a>
               </li>
               @if(Request::segment(2) == "add-company")
                  <li class="active">
               @else
                  <li>
               @endif 
                  <a href="{{url('superadmin/add-company')}}">
                  <i class="icon-plus"></i>
                  Add Company</a>
               </li>                  
            </ul>
         </li>

         @if(Request::segment(2) == "user" || Request::segment(2) == "add-user" || Request::segment(2) == "edit-user-details" || Request::segment(2) == "user-details")
            <li class="start active open">
         @else
            <li>
         @endif
            <a href="{{url('superadmin/user')}}">
            <i class="icon-user"></i>
            <span class="title">User</span>
            @if(Request::segment(2) == "user" || Request::segment(2) == "add-user" || Request::segment(2) == "edit-user-details" || Request::segment(2) == "user-details")
               <span class="arrow open"></span>
            @else
               <span class="arrow "></span>
            @endif            
            </a>
            <ul class="sub-menu">
               @if(Request::segment(2) == "user" || Request::segment(2) == "user-details")
                  <li class="active">
               @else
                  <li>
               @endif  
                  <a href="{{url('superadmin/user')}}">
                  <i class="icon-list"></i>
                  User List</a>
               </li>
            </ul>
         </li>

         @if(Request::segment(2) == "project" || Request::segment(2) == "add-project" || Request::segment(2) == "project-details" || Request::segment(3) == "location")
            <li class="start active open">
         @else
            <li>
         @endif
            <a href="{{url('superadmin/project')}}">
            <i class="icon-film"></i>
            <span class="title">Project</span>
            @if(Request::segment(2) == "project" || Request::segment(2) == "add-project" || Request::segment(2) == "project-details" || Request::segment(3) == "location")
               <span class="arrow open"></span>
            @else
               <span class="arrow "></span>
            @endif
            
            </a>
            <ul class="sub-menu">
               @if(Request::segment(2) == "project" || Request::segment(2) == "project-details")
                  <li class="active">
               @else
                  <li>
               @endif  
                  <a href="{{url('superadmin/project')}}">
                  <i class="icon-list"></i>
                  Project List</a>
               </li>
               @if(Request::segment(2) == "add-project")
                  <li class="active">
               @else
                  <li>
               @endif
                  <a href="{{url('superadmin/add-project')}}">
                  <i class="icon-plus"></i>
                  Add Project</a>
               </li>
            </ul>
         </li>

         @if(Request::segment(2) == "permissions" || Request::segment(2) == "roles")
            <li class="start active open">
         @else
            <li>
         @endif
            <a href="{{url('superadmin/permission')}}">
            <i class="icon-lock" aria-hidden="true"></i>
            <span class="title">Authentication</span>
            @if(Request::segment(2) == "permission" || Request::segment(2) == "roles")
               <span class="arrow open"></span>
            @else
               <span class="arrow "></span>
            @endif
            
            </a>
            <ul class="sub-menu">
               @if(Request::segment(2) == "roles")
                  <li class="active">
               @else
                  <li>
               @endif 
                  <a href="{{url('superadmin/roles')}}">
                  <i class="icon-list"></i>
                  Roles</a>
               </li> 
               @if(Request::segment(2) == "permissions")
                  <li class="active">
               @else
                  <li>
               @endif 
                  <a href="{{url('superadmin/permissions')}}">
                  <i class="icon-list"></i>
                  Permissions</a>
               </li>                    
            </ul>
         </li>
        
         @if(Request::segment(2) == "issueto" || Request::segment(2) == "issueto-details")
            <li class="start active open">
         @else
            <li>
         @endif
            <a href="{{url('superadmin/issueto')}}">
            <i class="icon-users"></i>
            <span class="title">Issuedto</span>
            </a>
         </li>

         @if(Request::segment(2) == "quality-checklist" || Request::segment(2) == "quality-checklist-details")
            <li class="start active open">
         @else
            <li>
         @endif
            <a href="{{url('superadmin/quality-checklist')}}">
            <i class="icon-check"></i>
            <span class="title">Quality Checklist</span>
            </a>
         </li>
          
      </ul>
      <!-- END SIDEBAR MENU -->
   </div>
</div>