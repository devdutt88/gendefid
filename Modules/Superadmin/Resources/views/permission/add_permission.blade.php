@extends('superadmin::layouts.master')

<!-- BEGIN CONTAINER -->
	
@section('content')
	
	
	<div class="page-content-wrapper">
		<div class="page-content">
			
			<!-- BEGIN PAGE HEADER-->			
			@include('superadmin::partials.breadcrumb')
			<!-- END PAGE HEADER-->
			
			<!-- BEGIN CONTENT -->
			<div class="row">
				<div class="col-md-12">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-equalizer font-blue-hoki"></i>
								<span class="caption-subject font-blue-hoki bold uppercase">Add Permission</span>
								<span class="caption-helper"></span>
							</div>
						</div>
						
						<div class="portlet-body form">
							<!-- BEGIN FORM-->
							
							<form action="{{ url('superadmin/save-permission') }}" method="post" class="form-horizontal" id="add_permission" autocomplete="on|off" enctype="multipart/form-data" accept-charset="utf-8" novalidate="novalidate">
								{{ csrf_field() }}
                                @include('superadmin::permission.form')
                            </form>
							<!-- END FORM-->
						</div>
					</div>	
				</div>
			</div>
			<!-- END CONTENT -->
			<div class="clearfix">
			</div>
			
		</div>
	</div>
@stop
<!-- END CONTAINER -->