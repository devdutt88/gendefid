<div class="modal fade" id="permissionEditModel" close="cancel()">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="permissionEditInfo" name="permissionEditInfo" class="form-horizontal form-row-seperated" ng-submit="savePermission(label)" novalidate>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" ng-click="cancel(permissionEditInfo)">×</button>
                    <h4 class="modal-title">@{{heading}} Permission</h4>
                </div>
                <div class="modal-body">
                    {{ csrf_field() }}
                    <input type="hidden" id="cmp_id" name="cmp_id" ng-model="cmp_id" value="@if(isset($cmp->id)){{$cmp->id}}@endif" />                    
                    <input type="hidden" name="id" ng-model="permission.id" ng-init="permission.id=permission.id">
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">Name
                                <span class="required" aria-required="true"> * </span>
                            </label>
                            <div class="col-md-6">
                                <div class="input-icon right">
                                    <i class="fa"></i>
                                   <input type="text" id="name" class="form-control" name="name" placeholder="" ng-model="permission.name" permission-exists ng-model-options="{ updateOn: 'blur' }" ng-required="true"/>
                                    <p ng-messages="permissionEditInfo.name.$error"  ng-if="permissionEditInfo.name.$touched" role="alert">
                                        <span class="error" ng-message="required">Permission name is required.</span>
                                        <span class="error" ng-message="permissionExists">Permission name already exist.</span>
                                    </p>
                                </div>
                            </div>
                        </div>                        
                        <div class="form-group">
                            <label class="control-label col-md-3">Type
                                <span class="required" aria-required="true"> * </span>
                            </label>
                            <div class="col-md-6">
                                <div class="input-icon right">
                                    <i class="fa"></i>                                                                       
                                    <select  ng-disabled="isDisabled" name="type" class="form-control" ng-model="permission.type" ng-change="checkRule()" ng-required="true">
                                        <option value="">Please select type</option>
                                        <option ng-repeat="x in types" value="@{{x}}">@{{x}}</option>
                                    </select>
                                    <p ng-messages="permissionEditInfo.type.$error.required"  ng-if="permissionEditInfo.type.$touched" role="alert">
                                        <span class="error" ng-message="required">Permission type is required.</span>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="form-group" ng-show="show_module" >
                            <label class="control-label col-md-3">
                                <span class="required" aria-required="true">  </span>
                            </label>
                            <div class="col-md-6">
                                <div class="input-icon right">
                                    <i class="fa"></i>                                                                       
                                    <select name="module_id" class="form-control" ng-model="permission.module_id" ng-required="label_validation">
                                        <option value="">Please select Menu</option>
                                        <option ng-repeat="(key, x) in module_names" value="@{{key}}">@{{x}}</option>
                                    </select>
                                    <span style="color:red">@{{msg}}</span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Description 
                                <span class="required" aria-required="true">  </span>
                            </label>
                            <div class="col-md-6">
                                <div class="input-icon right">
                                    <i class="fa"></i>                             
                                    <textarea class="form-control" ng-trim="false" maxlength="120" ng-model="permission.desc" name="desc" id="desc" ></textarea>
                                    <span>@{{120 - permission.desc.length}} Character Left</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"  ng-click="cancel(permissionEditInfo)">Cancel</button>
                    <button type="submit" class="edit_permission btn blue" ng-disabled="permissionEditInfo.$invalid  || isaddPermissionDisabled">@{{button}}</button>
                    <span ng-show="showLoader">
                        <img src="{{url('/')}}/assets/img/input-spinner.gif">
                    </span>
                </div>
            </form>
        </div>
    </div>
</div>