@extends('superadmin::layouts.master')

<!-- BEGIN CONTAINER -->
	<!-- BEGIN SIDEBAR -->
	@section('content')

	<!-- END SIDEBAR -->

	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
							<h4 class="modal-title">Modal title</h4>
						</div>
						<div class="modal-body">
							 Widget settings form goes here
						</div>
						<div class="modal-footer">
							<button type="button" class="btn blue">Save changes</button>
							<button type="button" class="btn default" data-dismiss="modal">Close</button>
						</div>
					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			</div>
			<!-- /.modal -->
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
		
			<!-- BEGIN PAGE HEADER-->			
			@include('superadmin::partials.breadcrumb')
			
			<!-- END PAGE HEADER-->
			<!-- BEGIN CONTENT -->
			<div class="row">
				<div class="tab-pane active" id="tab_3">
					<div class="portlet box grey-cascade">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-gift"></i>Show Company Details
							</div>
							<!-- <div class="tools">
								<a href="javascript:;" class="collapse" data-original-title="" title="">
								</a>
								<a href="#portlet-config" data-toggle="modal" class="config" data-original-title="" title="">
								</a>
								<a href="javascript:;" class="reload" data-original-title="" title="">
								</a>											
							</div> -->
						</div>
						<div class="portlet-body form">
							<!-- BEGIN FORM-->

							<div class="edit_btn">
									<div class="row">
										<div class="col-md-12">
											<div class="row">
												<div class="col-md-offset-3 col-md-9">						
													<!-- <a href="#"><button type="button" class="btn default" onclick="history.go(-1); return false;"><i class="fa fa-chevron-left "></i> Back</button></a> -->
													<a href="/superadmin/edit-company-details/{{ $cmp->id }}" class="btn cst-yellow"><i class="fa fa-pencil"></i> Edit</a>
												</div>
											</div>
										</div>
										<div class="col-md-6">
										</div>
									</div>
								</div>

							<form class="form-horizontal" role="form">
								<div class="form-body">									
									<h3 class="form-section">Company Info</h3>
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-5"><strong>Company Name:</strong></label>
												<div class="col-md-7">
													<p class="form-control-static">
														 {{$cmp->name}}
													</p>
												</div>
											</div>
										</div>
										<!--/span-->
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-5"><strong>Comapny Type:</strong></label>
												<div class="col-md-7">
													<p class="form-control-static">
														 {{$cmp->type}}
													</p>
												</div>
											</div>
										</div>
										<!--/span-->
									</div>
									<!--/row-->
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-5"><strong>Company Registration Number:</strong></label>
												<div class="col-md-7">
													<p class="form-control-static">
														 {{$cmp->registration_number}}
													</p>
												</div>
											</div>
										</div>
										<!--/span-->
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-5"><strong>Email-id:</strong></label>
												<div class="col-md-7">
													<p class="form-control-static">
														{{$cmp->email}}
													</p>
												</div>
											</div>
										</div>
										<!--/span-->
									</div>
									<!--/row-->
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-5"><strong>Phone:</strong></label>
												<div class="col-md-7">
													<p class="form-control-static">
														{{$cmp->phone}}
													</p>
												</div>
											</div>
										</div>
										<!--/span-->
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-5"></label>
												<div class="col-md-7">
													<p class="form-control-static">
														 
													</p>
												</div>
											</div>
										</div>
										<!--/span-->
									</div>
									<!--/row-->
									<h3 class="form-section">Business Details</h3>
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-5"><strong>Business Address 1:</strong></label>
												<div class="col-md-7">
													<p class="form-control-static">
														 {{$cmp_details[0]->busi_address1}}
													</p>
												</div>
											</div>
										</div>
									
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-5"><strong>Business Address 2:</strong></label>
												<div class="col-md-7">
													<p class="form-control-static">
														 {{$cmp_details[0]->busi_address2}}
													</p>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<!--/span-->
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-5"><strong>Business Suburb:</strong></label>
												<div class="col-md-7">
													<p class="form-control-static">
														 {{$cmp_details[0]->busi_suburb}}
													</p>
												</div>
											</div>
										</div>
										<!--/span-->
									
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-5"><strong>Business State:</strong></label>
												<div class="col-md-7">
													<p class="form-control-static">
														{{$cmp_details[0]->busi_state}}
													</p>
												</div>
											</div>
										</div>
									</div>
									<!--/row-->
									<div class="row">
										<!--/span-->
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-5"><strong>Business Post:</strong></label>
												<div class="col-md-7">
													<p class="form-control-static">
														{{$cmp_details[0]->busi_post}}
													</p>
												</div>
											</div>
										</div>
										<!--/span-->
										<!--/span-->
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-5"><strong>Business Country:</strong></label>
												<div class="col-md-7">
													<p class="form-control-static">
														{{$cmp_details[0]->busi_country}}
													</p>
												</div>
											</div>
										</div>
										<!--/span-->
									</div>
									<!--Billing addr-->
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-5"><strong>Billing Address 1:</strong></label>
												<div class="col-md-7">
													<p class="form-control-static">
														 {{$cmp_details[0]->bill_address1}}
													</p>
												</div>
											</div>
										</div>
									
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-5"><strong>Billing Address 2:</strong></label>
												<div class="col-md-7">
													<p class="form-control-static">
														 {{$cmp_details[0]->bill_address2}}
													</p>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<!--/span-->
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-5"><strong>Billing Suburb:</strong></label>
												<div class="col-md-7">
													<p class="form-control-static">
														 {{$cmp_details[0]->bill_suburb}}
													</p>
												</div>
											</div>
										</div>
										<!--/span-->
									
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-5"><strong>Billing State:</strong></label>
												<div class="col-md-7">
													<p class="form-control-static">
														{{$cmp_details[0]->bill_state}}
													</p>
												</div>
											</div>
										</div>
									</div>
									<!--/row-->
									<div class="row">
										<!--/span-->
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-5"><strong>Billing Post:</strong></label>
												<div class="col-md-7">
													<p class="form-control-static">
														{{$cmp_details[0]->bill_post}}
													</p>
												</div>
											</div>
										</div>
										<!--/span-->
										<!--/span-->
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-5"><strong>Billing Country:</strong></label>
												<div class="col-md-7">
													<p class="form-control-static">
														{{$cmp_details[0]->bill_country}}
													</p>
												</div>
											</div>
										</div>
										<!--/span-->
									</div>
									<div class="row">
										<!--/span-->
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-5"><strong>Logo:</strong></label>
												<div class="col-md-7">
													<p class="form-control-static">
														@if(!empty($cmp_details[0]->logo_image))
												    <img class="logo_img" src="{{url('/')}}/assets/uploads/company/{{$cmp_details[0]->logo_image}}">
												    @endif
													</p>
												</div>
											</div>
										</div>
										<!--/span-->
										<!--/span-->
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-5"><strong></strong></label>
												<div class="col-md-7">
													<p class="form-control-static">
														
													</p>
												</div>
											</div>
										</div>
										<!--/span-->
									</div>									
								</div>								
							</form>
							<!-- END FORM-->
						</div>
					</div>																
				</div>
			</div>
		</div>
	</div>
	<!-- END CONTENT -->	
<!-- END CONTAINER -->
@stop