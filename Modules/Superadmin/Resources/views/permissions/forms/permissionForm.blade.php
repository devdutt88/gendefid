<div class="modal fade" id="permissionModel" close="cancel()">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="permissionForm" name="permissionForm" class="form-horizontal form-row-seperated" ng-submit="savePermission(label)" novalidate>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" ng-click="cancel(permissionForm)">×</button>
                    <h4 class="modal-title">@{{heading}} Permission</h4>
                </div>
                <div class="modal-body">
                    {{ csrf_field() }}
                    <input type="hidden" name="id" ng-model="permission.id" ng-init="permission.id=permission.id">
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">Name
                                <span class="required" aria-required="true"> * </span>
                            </label>
                            <div class="col-md-6">
                                <div class="input-icon right">
                                    <i class="fa"></i>
                                   <input type="text" id="name" class="form-control" name="name" placeholder="" ng-model="permission.name" permission-exists ng-model-options="{ updateOn: 'blur' }" ng-required="true"/>
                                    <p ng-messages="permissionForm.name.$error"  ng-if="permissionForm.name.$touched" role="alert">
                                        <span class="error" ng-message="required">Permission name is required.</span>
                                        <span class="error" ng-message="permissionExists">Permission name already exist.</span>
                                    </p>
                                </div>
                            </div>
                        </div>                        
                        <div class="form-group">
                            <label class="control-label col-md-3">Description 
                                <span class="required" aria-required="true">  </span>
                            </label>
                            <div class="col-md-6">
                                <div class="input-icon right">
                                    <i class="fa"></i>                             
                                    <textarea class="form-control" ng-trim="false" maxlength="120" ng-model="permission.description" name="description" id="description" ></textarea>
                                    <span>@{{120 - permission.description.length}} Character Left</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"  ng-click="cancel(permissionForm)">Cancel</button>
                    <button type="submit" class="btn blue" ng-disabled="permissionForm.$invalid  || isDisabled">@{{button}}</button>
                    <span ng-show="showLoader">
                        <img src="{{url('/')}}/assets/img/input-spinner.gif">
                    </span>
                </div>
            </form>
        </div>
    </div>
</div>