@extends('superadmin::layouts.master')

@section('content')
<!-- BEGIN CONTAINER -->

	<div class="page-content-wrapper" ng-app="permissApp">
		<div class="page-content" ng-controller="permissController">
			
			@include('superadmin::partials.breadcrumb')
			
			<!-- BEGIN CONTENT -->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN EXAMPLE TABLE PORTLET-->
					<div class="portlet box grey-cascade">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-globe"></i>Permission List
							</div>
							<div class="tools"></div>
						</div>
						<div class="portlet-body">
							@if(Session::has('flash_alert_notice'))
		                        <div class="alert alert-success alert-dismissable cst-alert cst-list-alert">
		                          	<!-- <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button> -->
		                          	<i class="icon fa fa-check"></i>  
		                           	{{ Session::get('flash_alert_notice') }} 
		                        </div>
	                       	@endif
							<!-- Permission add Dialog -->				    
								@include('superadmin::permissions.forms.permissionForm')
							<!-- End of Permission add Dialog -->
							<div class="table-toolbar">
								<div class="row">
									<div class="col-md-12 text-right">
										<div class="btn-group">
										<button class="btn cst-yellow" id="" ng-click="showModelFrm('add',0)"> <i class="fa fa-plus"></i> Add New </button>
										</div>
									</div>
								</div>
							</div>
							
							<table class="table table-striped table-bordered table-hover" id="permissionDataTable">
								<thead>
									<tr>
										<th> Name </th>
										<th> Description </th>
										<th> Created at </th>
										<th width="70"> Action </th>
									</tr>
								</thead>
							</table>							
						</div>
					</div>
					<!-- END EXAMPLE TABLE PORTLET-->
				</div>
			</div>
			<!-- END CONTENT -->
			<div class="clearfix">
			</div>

		</div>
	</div>
	
<!-- END CONTAINER -->
@stop