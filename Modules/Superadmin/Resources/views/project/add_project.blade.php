@extends('superadmin::layouts.master')

@section('content')
<!-- BEGIN CONTAINER -->
	<div class="page-content-wrapper aaaaaaq">
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
				@include('superadmin::partials.breadcrumb')
			<!-- END PAGE HEADER-->
			<!-- BEGIN CONTENT -->
			<div class="row">
				<div class="col-md-12">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-equalizer font-blue-hoki"></i>
								<span class="caption-subject font-blue-hoki bold uppercase">Add Project</span> 
								<span class="caption-helper"></span>
							</div>

							<div class="tools"></div>
						</div>
						<div class="portlet-body form">
							<!-- BEGIN FORM-->
								@include('superadmin::project.form')
							<!-- END FORM-->
						</div>
					</div>
				</div>
			</div>
			<!-- END CONTENT -->
			<div class="clearfix">
			</div>
		</div>
	</div>
<!-- END CONTAINER -->
@stop
