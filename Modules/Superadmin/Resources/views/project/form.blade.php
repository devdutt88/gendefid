<form action="{{ url('superadmin/save_project') }}" id="add_project_form" class="horizontal-form" method="post" enctype="multipart/form-data">
	{{ csrf_field() }}
	<div class="form-body">
		<!--<h3 class="form-section">Person Info</h3>-->
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label">Project Name <span class="red">*</span></label>
					<input type="text" id="name" class="form-control" name="name" placeholder="" value="@if(isset($project->name)){{$project->name}}@endif" />
					@if ($errors->has('name'))
					    <span class=error>{{ $errors->first('name') }}</span>
					@endif
				</div>
			</div>
			<div class="col-md-6">
				<!-- <div class="form-group">
					<label class="control-label">Project Code <span class="red">*</span></label>
					<input type="text" id="code" name="code" class="form-control" placeholder=""  value="@if(isset($project->code)){{$project->code}}@endif">
					@if ($errors->has('code'))
					    <span class=error>{{ $errors->first('code') }}</span>
					@endif
				</div>
			</div> -->
			<div class="form-group">
				<label class="control-label">Project Type <span class="red">*</span></label>
				@if(!empty($project))						
					{{ Form::select('type', $option_type, $project->type,  ['class' => 'select2_category form-control','id'=>'type']) }}
				@else
					{{ Form::select('type', $option_type, null,  ['class' => 'select2_category form-control','id'=>'type']) }}
				@endif

				@if ($errors->has('type'))
				    <span class=error>{{ $errors->first('type') }}</span>
				@endif
			</div>
		</div>
		</div>
		<!--/row-->
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label">Company <span class="red">*</span></label>
					@php $proj_comps = array($proj_comps) @endphp
					<select name="company" id="company" class="select2_category form-control" data-placeholder="Choose Companies" tabindex="1">
						<option value="">Select Any One</option>
						
						@foreach($companies as $company)
							<option value="{{$company->id}}" @if(in_array($company->id, $proj_comps)) selected @endif >{{$company->name}}</option>
						@endforeach
						
					</select>
					@if ($errors->has('company'))
					    <span class=error>{{ $errors->first('company') }}</span>
					@endif
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label">Description </label>
					<textarea type="text" id="description" name="description" class="form-control" rows = "3" >@if(isset($project->description)){{$project->description}}@endif</textarea>
					<div id="chars">120 Character Left</div>
					@if ($errors->has('description'))
					    <span class=error>{{ $errors->first('description') }}</span>
					@endif
				</div>
			</div>
		</div>
		<!--/row-->
		<!-- <div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label">Project Type <span class="red">*</span></label>
					@if(!empty($project))						
						{{ Form::select('type', $option_type, $project->type,  ['class' => 'select2_category form-control','id'=>'type']) }}
					@else
						{{ Form::select('type', $option_type, null,  ['class' => 'select2_category form-control','id'=>'type']) }}
					@endif

					@if ($errors->has('type'))
					    <span class=error>{{ $errors->first('type') }}</span>
					@endif
				</div>
			</div>
		</div> -->
		<!--/row-->
		<hr>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label"><u><i><strong>Address Details:</strong></i></u></label>
				</div>
			</div>
		</div>
		<!--/row for buisness-->
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label">Address 1 <span class="red">*</span></label>
					<input type="text" id="address1" name="address1" class="form-control" placeholder="" value="@if(isset($project->address1)){{$project->address1}}@endif">
					@if ($errors->has('address1'))
					    <span class=error>{{ $errors->first('address1') }}</span>
					@endif
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label">Address 2 </label>
					<input type="text" id="address2" name="address2" class="form-control" placeholder="" value="@if(isset($project->address2)){{$project->address2}}@endif">
					@if ($errors->has('address2'))
					    <span class=error>{{ $errors->first('address2') }}</span>
					@endif
				</div>
			</div>
		</div>
		<!--/row-->
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label">Suburb <span class="red">*</span></label>
					<input type="text" id="suburb" name="suburb" class="form-control" placeholder="" value="@if(isset($project->suburb)){{$project->suburb}}@endif" />
					@if ($errors->has('suburb'))
					    <span class=error>{{ $errors->first('suburb') }}</span>
					@endif
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label">Post Code <span class="red">*</span></label>
					<input type="text" id="postal_code" name="postal_code" class="form-control" placeholder="" value="@if(isset($project->postal_code)){{$project->postal_code}}@endif" />
					@if ($errors->has('postal_code'))
					    <span class=error>{{ $errors->first('postal_code') }}</span>
					@endif
				
				</div>
			</div>
		</div>
		<!--/row-->
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label">State <span class="red">*</span></label>
					@if(!empty($project))						
						{{ Form::select('state', $option_state, $project->state,  ['class' => 'select2_category form-control','id'=>'state']) }}
					@else
						{{ Form::select('state', $option_state, null,  ['class' => 'select2_category form-control','id'=>'state']) }}
					@endif
					@if ($errors->has('state'))
					    <span class=error>{{ $errors->first('state') }}</span>
					@endif
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label">Country <span class="red">*</span></label>				
					{!! Form::text('country', 'Australia', array('class' => 'form-control','id'=>'country','readonly'=>'readonly')) !!}
					@if ($errors->has('country'))
					    <span class=error>{{ $errors->first('country') }}</span>
					@endif
				</div>
			</div>
		</div>		
	
	<div class="form-actions text-right">
		<button type="button" class="btn btn-default" onClick="history.go(-1); return false;"> Cancel </button>
		<button type="submit" class="btn blue"><i class="fa fa-check"></i> Submit</button>
	</div>
</form>