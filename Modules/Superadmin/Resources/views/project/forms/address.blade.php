<div class="modal fade" id="addressModal" close="cancel()" style="display:none;">
    <!-- <div class="modal fade" id="showModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"> -->
    <div class="modal-dialog">
        <div class="modal-content">
    	<!-- BEGIN FORM-->
        <form id="add_project_form1" name="projectAddress" class="horizontal-form" ng-submit="update_details({{$project->id}},'address')" novalidate>
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h4 class="modal-title">Project Address</h4>
			</div>
			<div class="modal-body">
				{{ csrf_field() }}
				<input type="hidden" id="project_id" name="project_id" value="@if(isset($project->id)){{$project->id}}@endif" />
				<div class="form-body">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label">Address 1 <span class="red">*</span></label>
								<input type="text" id="address1" class="form-control" name="address1" placeholder="" ng-model="project.address1" ng-required="true"/>
								<span class="error" ng-show="projectAddress.address1.$invalid && projectDetails.address1.$touched">address1 is required</span>
								@if ($errors->has('address1'))
								    <span class=error>{{ $errors->first('address1') }}</span>
								@endif
								<!--<span class="help-block">
								This is inline help </span>-->
							</div>
						</div>
						<!--/span-->
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label">Address 2 <span class="red"></span></label>
								<input type="text" id="address2" name="address2" class="form-control" placeholder="" ng-model="project.address2" ng-required="true"/>
								<span class="error" ng-show="projectAddress.address2.$invalid && projectDetails.address2.$touched">address2 is required</span>
								@if ($errors->has('address2'))
								    <span class=error>{{ $errors->first('address2') }}</span>
								@endif
							</div>
						</div>
						<!--/span-->
						<div class="col-md-6">
							<div class="form-group">

							<label class="control-label">Suburb <span class="red"></span></label>
								<input type="text" id="suburb" name="suburb" class="form-control" placeholder="" ng-model="project.suburb" ng-required="true"/>
								<span class="error" ng-show="projectAddress.suburb.$invalid && projectDetails.suburb.$touched">suburb is required</span>
								@if ($errors->has('suburb'))
								    <span class=error>{{ $errors->first('suburb') }}</span>
								@endif

							</div>
						</div>
						<!--/span-->
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label">Post Code <span class="red"></span></label>
								<input type="text" id="postal_code" name="postal_code" class="form-control" placeholder="" ng-model="project.postal_code" ng-required="true"/>
								<span class="error" ng-show="projectAddress.postal_code.$invalid && projectDetails.postal_code.$touched">project postal_code is required</span>
								@if ($errors->has('postal_code'))
								    <span class=error>{{ $errors->first('postal_code') }}</span>
								@endif
							</div>
						</div>
						<!--/span-->
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label">State <span class="red">*</span></label>
								@if(!empty($project))
									{{ Form::select('state', $option_state, $project->state,  ['class' => 'select2_category form-control','id'=>'state', 'ng-model'=>"project.state", 'ng-required'=>"true"]) }}
								@else
									{{ Form::select('state', $option_state, null,  ['class' => 'select2_category form-control','id'=>'state', 'ng-model'=>"project.state", 'ng-required'=>"true"]) }}
								@endif
								
								@if ($errors->has('state'))
								    <span class=error>{{ $errors->first('state') }}</span>
								@endif
							</div>
						</div>
						<!--/span-->
						<div class="col-md-6">
							<div class="form-group">

								<label class="control-label">Country <span class="red">*</span></label>
								@if(!empty($project))
									{{ Form::select('country', $option_country, $project->country,  ['class' => 'select2_category form-control','id'=>'country', 'ng-model'=>"project.country", 'ng-required'=>"true"]) }}
								@else
									{{ Form::select('country', $option_type, null,  ['class' => 'select2_category form-control','id'=>'country', 'ng-model'=>"project.country", 'ng-required'=>"true"]) }}
								@endif
								
								@if ($errors->has('country'))
								    <span class=error>{{ $errors->first('country') }}</span>
								@endif
							</div>
						</div>
						<!--/span-->

						<!-- <div class="col-md-6">
							<div class="form-group">

								<label class="control-label">GPS Coordinates <span class="red"></span></label>
								<input type="text" id="geo_code" name="geo_code" class="form-control" placeholder="" ng-model="project.geo_code" ng-required="true"/>
								<span class="error" ng-show="projectAddress.geo_code.$invalid && projectDetails.geo_code.$touched">project geo_code is required</span>
								@if ($errors->has('geo_code'))
								    <span class=error>{{ $errors->first('geo_code') }}</span>
								@endif
							</div>
						</div> -->
						<!--/span-->
						
					</div>
					<!--/row-->
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal" ng-click="cancel()">Cancel</button>
				<button type="submit" class="btn blue" id="address_form" >Update</button>
			</div>
    	</form>
		<!-- END FORM-->
        </div>
    </div>
</div>