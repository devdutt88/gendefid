<div class="modal fade" id="companyModal" data-backdrop="static" data-keyboard="false"> 
	<div class="modal-dialog">
	    <div class="modal-content">
			<!-- BEGIN FORM-->
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h4 class="modal-title">Assign company to project</h4>
			</div>
			<div class="modal-body">
				<flash-message class="cst-alert cst-company-alert" show-close="false" on-dismiss="myCallback(flash)"></flash-message>
				<div class="portlet light bordered tasks-widget bordered">
                    <div class="portlet-title caption font-green-sharp">
                        <div class="caption caption-md">
                            <!-- <i class="icon-screen-desktop font-green-sharp"></i> -->
                            <span class="caption-subject bold font-green-sharp uppercase"> Company List </span>
                        </div>
						<div class="inputs">
		                    <div class="portlet-input input-inline input-small">
		                        <div class="input-icon right">
		                            <i class="icon-magnifier"></i>
		                            <input type="text" id="query" class="form-control input-circle" ng-model="query" onfocus="pxtrack.emit('counter', '1')" placeholder="search..."/>
		                        </div>
		                    </div>
		                </div>
                    </div>
                    <div class="portlet-body" >
		                <div class="task-content">
							<div class="scroller portlet-height">
								<!-- START TASK LIST -->
		                        <ul class="task-list">
		                            <li class="" ng-repeat="list in clists | filter:query | orderBy: orderList">
										<div class="task-title">
											<span class="task-title-sp"> @{{list.name | uppercase}}</span>
											<span ng-show="showLoader"><img src="{{url('/')}}/assets/img/input-spinner.gif"></span>
											<span class="label label-lg label-danger task-right" ng-if="list.status == 'linked'" ng-click="unlinkCompany(list.companyId,list.projectId)">Remove</span>
											<span class="label label-lg label-success task-right" ng-if="list.status == 'unlinked'" ng-click="associateCompany(list.companyId,list.projectId)">Add</span>
										</div>
		                            </li>
		                        </ul>
		                    </div>
		                </div>
		            </div>
				</div>
			</div>
			<!-- <div class="modal-footer">
				<button type="submit" class="btn btn-primary" data-dismiss="modal">Save & Close</button>
			</div> -->
		</div>
	</div>
</div>
