<div class="modal fade" id="detailsModal" close="cancel()" style="display:none;">
	<!-- <div class="modal fade" id="showModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"> -->
	<div class="modal-dialog">
		<div class="modal-content">
			<!-- BEGIN FORM-->
			<form id="projectInfo" name="projectInfo" class="horizontal-form" ng-submit="update_details('{{Crypt::encrypt($project->id)}}','details')" novalidate>
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					<h4 class="modal-title">Project Details</h4>
				</div>
				<div class="modal-body">
					{{ csrf_field() }}
					<input type="hidden" id="project_id" name="project_id" value="@if(isset($project->id)){{$project->id}}@endif" />
					<div class="form-body">
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Project Name <span class="red">*</span></label>
									
									<input type="text" id="name" name="name" class="form-control" placeholder="" ng-model="project.name" project-exists ng-model-options="{ updateOn: 'blur' }" ng-required="true"/>
								    <div ng-messages="projectInfo.name.$error" role="alert">
									    <span class="error" ng-message="required">Please enter a value for this field.</span>
									    <span class="error" ng-message="projectExists">Project already exists</span>
								  	</div>
									 
									<!--<span class="help-block">
									This is inline help </span>-->
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Project Type <span class="red">*</span></label>
									<select name="type" class="form-control" ng-model="project.type">
										<option value="">Select any one</option>
										<option ng-repeat="x in types" value="@{{x}}">@{{x}}</option>
									</select>
									<span class="error" ng-show="projectInfo.type.$invalid && projectInfo.type.$touched">
								       Project type is required.
								    </span>
								</div>
							</div>
							<!-- <div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Project Code <span class="red">*</span></label>
									<input type="text" id="code" name="code" class="form-control" placeholder="" ng-model="project.code" ng-required="true"/>
									<span class="error" ng-show="projectInfo.code.$invalid && projectInfo.code.$touched">Project code is required</span>
									
								</div>
							</div> -->
						</div>
						<!--/row-->
						<div class="row">							
							<!--/span-->
							<div class="col-md-12">
								<div class="form-group">
									<label class="control-label">Description <span class="red"></span></label>
									<textarea id="description" name="description" class="form-control" placeholder="" ng-model="project.description" , ng-trim='false' maxlength=120 rows="2"/></textarea>
									<span>@{{120 - project.description.length}} Character Left</span> 
								</div>
							</div>
						</div>
						<!--/row-->
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Address 1 <span class="red">*</span></label>
									<input type="text" id="address1" class="form-control" name="address1" placeholder="" ng-model="project.address1" ng-required="true"/>
									<span class="error" ng-show="projectInfo.address1.$invalid && projectInfo.address1.$touched">address1 is required</span>
								</div>
							</div>
							<!--/span-->
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Address 2 </label>
									<input type="text" id="address2" name="address2" class="form-control" placeholder="" ng-model="project.address2" />
								</div>
							</div>
						</div>
						<!--/row-->
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
								<label class="control-label">Suburb <span class="red"></span></label>
									<input type="text" id="suburb" name="suburb" class="form-control" placeholder="" ng-model="project.suburb" ng-required="true"/>
								</div>
							</div>
							<!--/span-->
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Post Code <span class="red">*</span></label>
									<input type="text" id="postal_code" name="postal_code" class="form-control" placeholder="" ng-model="project.postal_code" ng-required="true"/>
									<span class="error" ng-show="projectInfo.postal_code.$invalid && projectInfo.postal_code.$touched">Project postal_code is required</span>
								</div>
							</div>
						</div>
						<!--/row-->
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">State <span class="red">*</span></label>
									@if(!empty($project))
										{{ Form::select('state', $option_state, $project->state,  ['class' => 'select2_category form-control','id'=>'state', 'ng-model'=>"project.state", 'ng-required'=>"true"]) }}
									@else
										{{ Form::select('state', $option_state, null,  ['class' => 'select2_category form-control','id'=>'state', 'ng-model'=>"project.state", 'ng-required'=>"true"]) }}
									@endif
									
									<span class="error" ng-show="projectInfo.state.$invalid && projectInfo.state.$touched">state is required</span>
								</div>
							</div>
							<!--/span-->
							<div class="col-md-6">
								<div class="form-group">

									<label class="control-label">Country <span class="red">*</span></label>
									{!! Form::text('country', '', array('class' => 'form-control','id'=>'country','ng-model'=>"project.country",'readonly'=>'readonly', 'ng-required'=>"true")) !!}
									<span class="error" ng-show="projectInfo.country.$invalid && projectInfo.country.$touched">country is required</span>
								</div>
							</div>
						</div>
						<!--/row-->
						<!-- <div class="row">
							<div class="col-md-6">
								<div class="form-group">

									<label class="control-label">GPS Coordinates <span class="red"></span></label>
									<input type="text" id="geo_code" name="geo_code" class="form-control" placeholder="" ng-model="project.geo_code" />
								</div>
							</div>
						</div> -->
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal" ng-click="cancel(projectInfo)">Cancel</button>
					<button type="submit" class="btn blue" id="profile_form" ng-disabled="projectInfo.$invalid">Update</button>
					<span ng-show="showLoader">
	                	<img src="{{url('/')}}/assets/img/input-spinner.gif">
	                </span>
				</div>
			</form>
			<!-- END FORM-->
	    </div>
	</div>
</div>