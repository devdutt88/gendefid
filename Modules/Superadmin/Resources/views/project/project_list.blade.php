@extends('superadmin::layouts.master')

@section('content')
<!-- BEGIN CONTAINER -->

	<div class="page-content-wrapper">
		<div class="page-content">
			
			@include('superadmin::partials.breadcrumb')
			
			<!-- BEGIN CONTENT -->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN EXAMPLE TABLE PORTLET-->
					<div class="portlet box grey-cascade">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-globe"></i>Project List
							</div>
							<div class="actions">
								<div class="portlet-input input-inline input-small">
									<div class="input-icon text-right">
										<a class="btn btn-circle btn-sm btn-default" id="" href="{{url('/')}}/superadmin/add-project"> <i class="fa fa-plus"></i> Add New Project</a>
									</div>
								</div>
							</div>
						</div>
						<div class="portlet-body">
							<!-- Alert Section -->
								@if(Session::has('flash_alert_notice'))
							        <div class="alert alert-success alert-dismissable cst-alert cst-list-alert">
							          	<!-- <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button> -->
							          	<i class="icon fa fa-check"></i>  
							           	{{ Session::get('flash_alert_notice') }} 
							        </div>
							   	@endif
							<!-- End Alert Section -->
							 
							<table class="table table-striped table-bordered table-hover" id="sample_1">
								<thead>
									<tr>
										<th> Name </th>										
										<th> Type </th>
										<th> Author </th>
										<th> Company </th>
										<th width="110"> Created </th>
										<th width="80"> Action </th>
									</tr>
								</thead>

							</table> 
						</div>
					</div>
					<!-- END EXAMPLE TABLE PORTLET-->
				</div>
			</div>
			<!-- END CONTENT -->
			<div class="clearfix">
			</div>

		</div>
	</div>
	
<!-- END CONTAINER -->
@stop