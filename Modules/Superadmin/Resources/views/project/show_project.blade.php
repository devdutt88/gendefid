@extends('superadmin::layouts.master')

@section('content')
<!-- BEGIN CONTAINER -->
<link href="{{url('/')}}/assets/css/tasks.css" rel="stylesheet" type="text/css"/>
<div class="page-content-wrapper"  ng-app="projectModule">
	<div class="page-content" ng-controller="projectController">
		<!-- BEGIN PAGE HEADER-->
		@include('superadmin::partials.breadcrumb')
		<!-- {!! $breadcrumbs !!} -->
		<!-- END PAGE HEADER-->
       	@include('superadmin::project.forms.details') <!-- Project Info Form Modal -->
       	
		<!-- BEGIN CONTENT -->
		<div class="row">
			<div class="col-lg-12">
				<!-- Alert Section -->
					@if(Session::has('flash_alert_notice'))
				        <div class="alert alert-success alert-dismissable cst-alert cst-normal-alert">
				          	<!-- <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button> -->
				          	<i class="icon fa fa-check"></i>  
				           	{{ Session::get('flash_alert_notice') }} 
				        </div>
				   	@endif
				<!-- End Alert Section -->
		       	<div class="portlet light bordered box-min-height">
                    <div class="portlet-title caption font-green-sharp">
                        <div class="caption">
                            <i class="icon-microphone font-dark hide"></i>
                            <span class="caption-subject bold font-dark uppercase"> Project Details </span>
                            <span class="caption-helper"></span>
                        </div>
                       	<div class="actions">
							<a href="" class="btn btn-circle btn-default btn-sm" ng-click="show_details('{{Crypt::encrypt($project->id)}}', 'details')">
							<i class="fa fa-pencil"></i> Edit </a>
							<!-- <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title="">
							</a> -->
						</div>
                    </div>
					<div class="portlet-body">
						<div class="row">
							<div class="col-md-12">
								<div class="col-sm-4 text-center cst-logo-section">
									<form id="projectLogoFrm" runat="server">
										<div class="form-group">
											@if(!empty($project->logo))
												<img id="logo_image"  width="200" height="200"  src="{{url('/')}}/uploads/project/{{$project->logo}}" alt="project image" />
											@else
												<img id="logo_image"  width="200" height="200"  src="{{url('/')}}/uploads/project_icon.png" alt="project image" />
											@endif
										</div>
										<div class="form-group">
											<span class="btn btn-circle cst-yellow fileinput-button">
							                    @if(!empty($project->logo))
							                    	<span>Change Logo</span>
							                    @else
							                    	<span>Add Logo</span>
							                    @endif
												 	<input type='file' name="logo" id="projectImg"/>
											</span>
										</div>
										<div class="form-group">
											<button type="submit" id="upload_btn" style="display:none;" class="btn btn-success btn-circle start" ng-click="update_details('{{Crypt::encrypt($project->id)}}','logo','{{$project->name}}')"><i class="glyphicon glyphicon-upload"></i>Start upload</button>

											<span ng-show="showLogoLoader">
												<img src="{{url('/')}}/assets/img/ajax-loader.gif">
											</span>
										</div>
									</form>
								</div>
								<div class="col-sm-8">
									<div class="row">
										<label class="form-control-static col-xs-4"><strong>Name:</strong></label>
										<div class="form-control-static col-xs-8">
												 {{$project->name}} 												 
										</div>
									</div>
									<div class="row">
										<label class="form-control-static col-xs-4"><strong>Type:</strong></label>
										<div class="form-control-static col-xs-8">
												 {{$project->type}} 												 
										</div>
									</div>
									<div class="row">
										<label class="form-control-static col-xs-4"><strong>Address:</strong></label>
										<div class="form-control-static col-xs-8">
												 {{$project->address1}} 
												 {{$project->address2}}
										</div>
									</div>
									<div class="row">
										<label class="form-control-static col-xs-4"><strong>Suburb :</strong></label>
										<div class="form-control-static col-xs-8">
												 {{$project->suburb}}
										</div>
									</div>
									<div class="row">
										<label class="form-control-static col-xs-4"><strong>Postal Code :</strong></label>
										<div class="form-control-static col-xs-8">
												{{$project->postal_code}}
										</div>
									</div>
									<div class="row">
										<label class="form-control-static col-xs-4"><strong>State :</strong></label>
										<div class="form-control-static col-xs-8">
												{{$project->state}}
										</div>
									</div>
									<div class="row">
										<label class="form-control-static col-xs-4"><strong>Country :</strong></label>
										<div class="form-control-static col-xs-8">
												{{$project->country}}
										</div>
									</div>
									<!-- <div class="row">
										<label class="form-control-static col-xs-4"><strong>GPS Coordinates :</strong></label>
										<div class="form-control-static col-xs-8">
												 {{$project->geo_code}}
										</div>
									</div> -->
								</div>
							</div>
						</div>
						<hr>
						<div class="row">
								<!-- <div class="form-group">
									<label class="control-label col-md-5"><strong>Project Type :</strong></label>
									<div class="col-md-7">
										<p class="form-control-static">
											 {{$project->type}}
										</p>
									</div>
								</div> -->
							<label class="col-xs-12"><strong>Description</strong></label>
							<div class="col-xs-12">
									 {{$project->description}}
							</div> 
						</div>
					</div><!--End Portlet Body -->
				</div><!--End Portlet -->
			</div><!--End First Column -->
			
			@include('superadmin::project.forms.companies')
			<!-- <div class="col-lg-4" ng-init="projectCompanyList({{$project->id}})">
				<div class="portlet light bordered tasks-widget bordered">
                    <div class="portlet-title caption font-green-sharp">
                        <div class="caption caption-md"> -->
                            <!-- <i class="icon-screen-desktop font-green-sharp"></i> -->
                            <!-- <span class="caption-subject bold font-green-sharp uppercase"> Associated Compnies </span>
                        </div>
                   		<div class="actions">
							<a href="" class="btn btn-circle btn-default btn-sm" ng-click="showModelFrm({{$project->id}} , 'companies')">
							<i class="fa fa-pencil"></i> </a>
						</div>
                    </div>
                    <div class="portlet-body" >
		                <div class="task-content">
		                	
							<div class="scroller portlet-height"> -->
								<!-- START TASK LIST -->
		                        <!-- <ul class="task-list">
		                            <li class="" ng-repeat="list in pclists">
										<div class="task-title">
											<span class="task-title-sp"> @{{list.name | uppercase }}</span>
										</div>
		                            </li>
		                        </ul>
		                    </div>
		                </div>
		            </div>
				</div>
			</div> -->
			<div class="clearfix"></div>
		</div><!--End Row Main -->
		<!-- END CONTENT -->
	</div>
</div>
<!-- END CONTAINER -->
@stop