<div class="modal fade" id="checklistTaskModal" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog">
	    <div class="modal-content">
			<!-- BEGIN FORM-->
		 	<form id="checklistTaskFrm" name="checklistTaskFrm" class="horizontal-form" method="post" ng-submit="saveChecklistTask('{{$checklist_id}}')" nonvalidate>
				<div class="modal-header">
					{{ Form::button("×", $attributes = array("class"=>"close","ng-click"=>"reset(checklistTaskFrm)", "data-dismiss" => "modal", "aria-hidden"=>"true")) }}
					<h4 class="modal-title">@{{checklistTask.form_title}}</h4>
				</div>
				<div class="modal-body">
					{{ csrf_field() }}
					{{ Form::hidden("new_checklist_id", $checklist_id, $attributes = array("id" => "new_checklist_id")) }}
					{{ Form::hidden("checklist_task_id", "checklistTask.id", $attributes = array("id" => "checklist_task_id","ng-model"=>"checklistTask.id", "ng-init" => "checklistTask.id=checklistTask.id")) }}
					<div class="form-body">
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Name <span class="red">*</span></label>
									{{ Form::text("name", "", $attributes = array("class"=>"form-control", "id"=>"name","ng-model"=>"checklistTask.name", "ng-required" => "true")) }}
									<span class="error" ng-show="checklistTaskFrm.name.$invalid && checklistTaskFrm.name.$touched">Name is required</span>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Hold Point <span class="red">*</span></label></br>
									<!-- <label class="radio-inline"><input type="radio" ng-model="holdpoint" value="Yes">Yes</label>
									<label class="radio-inline"><input type="radio" ng-model="holdpoint" value="No">No</label> -->

									<label data-ng-repeat="choice in holdpoints" class="radio-inline control-label">
									  <input type="radio" name="holdpoint" data-ng-model="checklistTask.holdpoint" data-ng-value="choice" />
									  @{{choice}}
									</label>

									<span class="error" ng-show="checklistTaskFrm.status.$invalid && checklistTaskFrm.status.$touched">Hold Point is required</span>
								</div>
							</div>
						</div>
						<!--/row-->
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label class="control-label">Comment</label>
									{{ Form::textarea('comment', '', $attributes = array('class'=>'form-control', 'id' => 'comment', 'rows' => '3', 'ng-model'=>'checklistTask.comment')) }}
								</div>
							</div>
						</div>
						<!--/row-->
					</div>
				</div>
				<div class="modal-footer">
					{{ Form::button("Cancel", $attributes = array("class"=>"btn btn-default","ng-click"=>"reset(checklistTaskFrm)", "data-dismiss" => "modal")) }}
					<button type="submit" class="btn btn-primary" id="details_form" ng-disabled="checklistTaskFrm.$invalid">Save</button>
					<span ng-show="showLoader">
	                	<img src="{{url('/')}}/assets/img/input-spinner.gif">
	                </span>
				</div>
			</form>
			<!-- END FORM-->
		</div>
	</div>
</div>
