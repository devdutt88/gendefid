@extends('superadmin::layouts.master')

@section('content')
<!-- BEGIN CONTAINER -->
	<link href="{{url('/')}}/css/ng-tags-input.min.css" rel="stylesheet" type="text/css"> 
	<div class="page-content-wrapper" ng-app="checklistModule">
		<div class="page-content" ng-controller="checklistController">
			@include('superadmin::partials.breadcrumb')
			<!-- BEGIN CONTENT -->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN EXAMPLE TABLE PORTLET-->
					<div class="portlet box grey-cascade">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-globe"></i>Quality Checklist
							</div>
							<div class="actions">
								<div class="portlet-input input-inline input-small">
									<div class="input-icon text-right"> 
										<button class="btn btn-circle btn-sm btn-default" id="" ng-click="showChecklist('add', 0)"> <i class="fa fa-plus"></i> Add New </button>
									</div>
								</div>
							</div>
						</div>
						<div class="portlet-body">
							<!-- Alert Section -->
								@if(Session::has('flash_alert_notice'))
							        <div class="alert alert-success alert-dismissable cst-alert cst-list-alert">
							          	<!-- <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button> -->
							          	<i class="icon fa fa-check"></i>  
							           	{{ Session::get('flash_alert_notice') }} 
							        </div>
							   	@endif
							<!-- End Alert Section -->
							@include('superadmin::quality_checklist.quality_checklist_form')
							<div class="row">
								<div class="col-md-12">
									<table class="table table-striped table-bordered table-hover" id="Checklist">
										<thead>
											<tr>
												<th> Name </th>
												<th> Tags </th>
												<th width="120"> Created </th>
												<th width="120"> Action </th>
											</tr>
										</thead>
									</table>
								</div>
							</div>
						</div>
					</div>
					<!-- END EXAMPLE TABLE PORTLET-->
				</div>
			</div>
			<!-- END CONTENT -->
			<div class="clearfix">
			</div>
		</div>
	</div>
	<!-- END CONTAINER -->
@stop