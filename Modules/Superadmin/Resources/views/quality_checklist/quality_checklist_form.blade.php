
<div class="modal fade" id="checklistModal" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog">
	    <div class="modal-content">
			<!-- BEGIN FORM-->
			<form id="checklistInfo" name="checklistInfo" class="horizontal-form" ng-submit="saveChecklist()">
				<div class="modal-header">
					{{ Form::button("×", $attributes = array("class"=>"close","ng-click"=>"reset(checklistInfo)", "data-dismiss" => "modal", "aria-hidden"=>"true")) }}
					<h4 class="modal-title">@{{checklist.form_title}}</h4>
				</div>
				<div class="modal-body">
					{{ csrf_field() }}
					{{ Form::hidden("id", "checklist.id", $attributes = array("id" => "id","ng-model"=>"checklist.id", "ng-init" => "checklist.id=checklist.id")) }}
					<div class="form-body">
						<div class="row">
							<div class="form-group col-md-6">
								<label class="control-label">Name <span class="red">*</span></label>
								{{ Form::text("name", "", $attributes = array("class"=>"form-control", "id"=>"name","ng-model"=>"checklist.name", "ng-required" => "true")) }}
								<span class="error" ng-show="checklistInfo.name.$invalid && checklistInfo.name.$touched">Checklist name is required</span>
							</div>
						</div>
						<div class="row">
							<div class="form-group col-md-12">
								<label class="control-label">Tags</label>
								 <tags-input ng-model="checklist.tags">
							        <auto-complete source="loadTags($query)"></auto-complete>
							    </tags-input>
							</div>
						</div>
						<div class="row">
							<div class="form-group col-md-12 text-right">
								{{ Form::button("Cancel", $attributes = array("class"=>"btn btn-default","ng-click"=>"reset(checklistInfo)", "data-dismiss" => "modal")) }}
								<button type="submit" class="btn btn-primary" id="details_form" ng-disabled="checklistInfo.$invalid">Save</button>
								<span ng-show="showLoader">
				                	<img src="{{url('/')}}/assets/img/input-spinner.gif">
				                </span>
							</div>
						</div>
					</div>
				</div>
			</form>
			<!-- END FORM-->
		</div>
	</div>
</div>
