@extends('superadmin::layouts.master')

<!-- BEGIN CONTAINER -->
	
@section('content')
	
	
	<div class="page-content-wrapper">
		<div class="page-content">
			
			<!-- BEGIN PAGE HEADER-->			
			@include('superadmin::partials.breadcrumb')
			<!-- END PAGE HEADER-->
			
			<!-- BEGIN CONTENT -->
			<div class="row">
				<div class="col-md-12">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-equalizer font-blue-hoki"></i>
								<span class="caption-subject font-blue-hoki bold uppercase">Add Role</span>
								<span class="caption-helper"></span>
							</div>
						</div>
						
						<div class="portlet-body form">
							<!-- BEGIN FORM-->
							{{Form::open(array('url' => '','name'=>"role" ,'id'=>"role" ,'method' => 'get'))}}
							<div class="row">								
							    <div class="col-md-6">							    
							    @include('superadmin::role.form')
							    <button type="submit" class="btn blue" id="show-values">Save Permission</button>
							        <!-- <pre id="values"></pre> -->
							   </div>
  							</div>
  							{{ Form::close() }}
							<!-- END FORM-->
						</div>
					</div>	
				</div>
			</div>
			<!-- END CONTENT -->
			<div class="clearfix">
			</div>
			
		</div>
	</div>
@stop
<!-- END CONTAINER -->