<div class="form-group">
	<label class="control-label">Role Name <span class="red">*</span></label>
	@if(!empty($role->name))
		{{Form::text('name',$role->name,array('class'=>'form-control','id'=>'name','ng-model'=>'name','required'=>'required'))}}		
	@else
		{{Form::text('name','',array('class'=>'form-control','id'=>'name','ng-model'=>'name','required'=>'required'))}}		
	@endif
</div>
  <div id="treeview-checkbox-demo">							            
        <ul>
        	<li data-value="selected">Permission:-
        		<ul>
	            @foreach($data_tree as $key => $tree)
	            	<li  data-value="{{$key}}">
	            		{{$tree['parent']}}
	            		@if(!empty($tree['child']))
	            		<ul>
	            			@foreach($tree['child'] as $key => $tree_child)
	            				<li data-value="{{$key}}">{{$tree_child}}</li>
	            			@endforeach
	            		</ul>
	            		@endif
	            	</li>
	            @endforeach
        	</ul>
        	</li>
        </ul>
    </div>
    