<div class="modal fade" id="roleModel" close="cancel()">
<div class="modal-dialog">
        <div class="modal-content">
            <form id="rolenInfo" name="rolenInfo" class="form-horizontal form-row-seperated" ng-submit="submitForm('','profile')" novalidate>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">@{{heading}} Role</h4>
                </div>
                <div class="modal-body">
                    {{ csrf_field() }}                    
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">Name
                                <span class="required" aria-required="true"> * </span>
                            </label>
                            <div class="col-md-6">
                                <div class="input-icon right">
                                    <i class="fa"></i>                                   
                                    <input type="hidden" name="id" ng-model="role.id" ng-init="role.id=role.id">
                                   <input type="text" id="name" name="name" class="form-control" placeholder="" ng-model="role.name" role-exists ng-model-options="{ updateOn: 'blur' }" ng-required="true"/>
                                    <p ng-messages="rolenInfo.name.$error"  ng-if="rolenInfo.name.$dirty" role="alert">
                                        <span class="error" ng-message="required">Role name is required.</span>
                                        <span class="error" ng-message="roleExists">Role name already exist.</span>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3"> Description</label>
                            <div class="col-md-6">
                                <div class="input-icon right">
                                    <i class="fa"></i>                                   
                                    <input type="text" id="description" name="description" class="form-control" placeholder="" ng-model="role.description" />
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">User Type</label>
                            <div class="col-md-6">
                                <div class="input-icon right">
                                    <i class="fa"></i>                                   
                                    <input type="checkbox" name="type" value="1" ng-model="role.type">
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default addRole" data-dismiss="modal" ng-click="cancel('rolenInfo')">Cancel</button>
                    <button type="submit" ng-disabled="rolenInfo.$invalid || isaddRoleDisabled" class="add_role btn blue" ng-click="saveRole(label)">@{{button}}</button>
                    <span ng-show="showLoader">
                        <img src="{{url('/')}}/assets/img/input-spinner.gif">
                    </span>
                </div>
            </form>
        </div>
    </div>
</div>