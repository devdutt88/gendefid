<div class="modal fade" id="roleEditModel" close="cancel()">
<div class="modal-dialog">
        <div class="modal-content">
            <form id="editRolenInfo" name="editRolenInfo" class="form-horizontal form-row-seperated" ng-submit="submitForm('','profile')" novalidate>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Edit Role</h4>
                </div>
                <div class="modal-body">
                    {{ csrf_field() }}                    
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">Name
                                <span class="required" aria-required="true"> * </span>
                            </label>
                            <div class="col-md-6">
                                <div class="input-icon right">
                                    <i class="fa"></i>
                                    <input type="hidden" name="id" ng-model="role.id">
                                  <input type="text" id="name" name="name" class="form-control" placeholder="" ng-model="role.name" role-exists ng-model-options="{ updateOn: 'blur' }" ng-required="true"/>
                                    <p ng-messages="editRolenInfo.name.$error"  ng-if="editRolenInfo.name.$dirty" role="alert">
                                        <span class="error" ng-message="required">Role name is required.</span>
                                        <span class="error" ng-message="roleExists">Role name already exist.</span>
                                    </p>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default addRole" data-dismiss="modal" ng-click="cancel('editRolenInfo')">Cancel</button>
                    <button type="submit"  ng-disabled="isaddRoleDisabled"  class="add_role btn blue" ng-click="saveRole('edit')" ng-disabled="editRolenInfo.$invalid">Edit Role</button>
                    <span ng-show="showLoader">
                        <img src="{{url('/')}}/assets/img/input-spinner.gif">
                    </span>
                </div>
            </form>
        </div>
    </div>
</div>