<div class="modal fade" id="permissionModel" close="cancel()">
<div class="col-lg-12 col-md-12" ng-controller="TreeController as tc">
<div class="modal-dialog">
        <div class="modal-content">
            <form id="rolenInfo" name="rolenInfo" class="form-horizontal form-row-seperated" ng-submit="submitForm('','profile')" novalidate>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">{{roleName}} Permission</h4>
                </div>
                <div class="modal-body">
                    {{ csrf_field() }}                    
                    <div class="form-body">
                        
                        <ul class="tree">
                            <node-tree children="tc.tree"></node-tree>
                        </ul>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default addRole" data-dismiss="modal" ng-click="cancel('rolenInfo')">Cancel</button>
                    <button type="submit" ng-disabled="rolenInfo.$invalid || isaddRoleDisabled" class="add_role btn blue" ng-disabled="rolenInfo.$invalid" ng-click="storRolePermission(roleid)">Save Permission</button>
                    <span ng-show="showLoader">
                        <img src="{{url('/')}}/assets/img/input-spinner.gif">
                    </span>
                </div>
            </form>
        </div>
    </div>
    </div>
</div>