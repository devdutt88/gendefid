@extends('superadmin::layouts.master')

@section('content')
<!-- BEGIN CONTAINER -->
<link href="{{url('/')}}/assets/css/tasks.css" rel="stylesheet" type="text/css"/>
<div class="page-content-wrapper" ng-app="roleModule">
	<div class="page-content" ng-controller="roleController">
		<!-- {!! $breadcrumbs !!} -->
		@include('superadmin::partials.breadcrumb')
		<!-- BEGIN CONTENT -->
		<div class="row">
			<div class="col-lg-8">
				<div class="portlet light bordered box-min-height">
					<div class="portlet-title caption font-green-sharp">
						<div class="caption">
							<i class="icon-microphone font-dark hide"></i>
							<span class="caption-subject bold theme-font-color uppercase"> Permission List </span>
						</div>
						<div class="actions">
							<!-- <a href="" class="btn btn-circle btn-default btn-sm" ng-click="showIssueto('edit', );">
							<i class="fa fa-pencil"></i> Edit </a> -->
							<!-- <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title="">
						</a> -->
					</div>
				</div>
				<div class="portlet-body">
					<div class="row">							
						<div class="row">
							<div class="col-md-12">
								<label class="control-label col-md-3"><strong></strong></label>
								<div class="col-md-9">

								</div>
							</div>
						</div>
					</div>
					<div class="edit_btn">								
						<div class="text-right">
							<div class="row">
								<div class="col-md-12">
									<!-- <a href="#"><button type="button" onClick="history.go(-1); return false;" class="btn default"><i class="fa fa-chevron-left "></i> Back</button></a> -->
									<!-- <a href="javascript:;" id="" ng-click="showIssuetoUser('add');" class="btn cst-yellow"><i class="fa fa-plus"></i> Add New User</a> -->
								</div>
							</div>
						</div>								
					</div>

					<div class="form-horizontal">
						<div class="form-body">								
							<!-- permission list -->
							<input type="hidden" name="hid" id="hid" value="">
								<div class="portlet-body">
									<div class="panel-group accordion" id="accordion1">
										@php $i = 0 @endphp
										@php $j = 0 @endphp
										@foreach($allparent as $key=>$parent)
											@if($i == 0)
												<div class="panel panel-default">
												<form id="form{{$i}}">
													<div class="panel-heading">
														<h4 class="panel-title">
														<input id="perm{{$key}}" name="role[]" class="role" type="checkbox" value="{{$key}}" >
														<a class="accordion-toggle accord" data-toggle="collapse" data-parent="#accordion1" href="#collapse1" aria-expanded="true">
															{{$parent}} 
														</a>
														</h4>
													</div>
													<div id="collapse1" class="panel-collapse collapse in" aria-expanded="true" style="">
														<div class="panel-body">
														 <div class="alert alert-success alert-dismissable {{$i}}" style="display: none;">
									                          	<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
									                          	<i class="icon fa fa-check"></i>  
									                           	Permission Added successfully ! 
									                        </div>
															<div class="row">
																<div class="col-md-6">
																	<label class="control-label bold">Submenu</label>
																		<div class="checkbox-list">
																			@if(!empty($allchild[$key]['Submenu']))
																				@foreach($allchild[$key]['Submenu'] as $key1=>$child)
																					<label class="checkbox-inline">
																					
																					<input name="role[]" class="role"  id="perm{{$key1}}" value="{{$key1}}" type="checkbox">
																					{{$child}} </label>		
																				@endforeach
																			@endif
																		</div>
																</div>
																
																	<div class="col-md-6">
																		<label class="control-label bold">Internal permission</label>
																			<div class="checkbox-list">
																				@if(!empty($allchild[$key]['Internal permission']))
																					@foreach($allchild[$key]['Internal permission'] as $key2=>$child)
																						<label class="checkbox-inline">
																						
																						<input name="role[]" class="role" id="perm{{$key2}}" value="{{$key2}}" type="checkbox">
																						{{$child}} </label>		
																					@endforeach
																				@endif
																			</div>
																	</div>
															</div>
															<div class="row pull-right">
																<div class="col-md-6">
																	<label class="control-label bold"></label>
																	<a class="btn blue" ng-click="form({{$i}})" href="javascript:;">
																	Save </a>
																</div>
															</div>
														</div>
													</div>
													</form>
												</div>
											@else
												<div class="panel panel-default">
												<form id="form{{$i}}">
													<div class="panel-heading">
														<h4 class="panel-title">
														<input id="perm{{$key}}" name="role[]" class="role"  type="checkbox" value={{$key}} >
														<a class="accordion-toggle accord collapsed" data-toggle="collapse" data-parent="#accordion1" href="#collapse_{{$i}}" aria-expanded="false">
															{{$parent}} </label>
															 </a>
														</h4>
													</div>
													<div id="collapse_{{$i}}" class="panel-collapse collapse" aria-expanded="false" style="">
														<div class="panel-body">
														<div class="alert alert-success alert-dismissable {{$i}}" style="display: none;">
									                          	<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
									                          	<i class="icon fa fa-check"></i>  
									                           	Permission Added successfully ! 
									                        </div>
															<div class="row">
																<div class="col-md-6">
																	<label class="control-label bold">Submenu</label>
																		<div class="checkbox-list">
																			@if(!empty($allchild[$key]['Submenu']))
																				@foreach($allchild[$key]['Submenu'] as $key3=>$child)
																					<label class="checkbox-inline">
																					
																					<input name="role[]" class="role" id="perm{{$key3}}" value="{{$key3}}" type="checkbox">
																					{{$child}} </label>		
																				@endforeach
																			@endif
																		</div>
																</div>
																
																	<div class="col-md-6">
																		<label class="control-label bold">Internal permission</label>
																			<div class="checkbox-list">
																				@if(!empty($allchild[$key]['Internal permission']))
																					@foreach($allchild[$key]['Internal permission'] as $key4=>$child)
																						<label class="checkbox-inline">
																						
																						<input name="role[]" class="role"  id="perm{{$key4}}" value="{{$key4}}" type="checkbox">
																						{{$child}} </label>		
																					@endforeach
																				@endif
																			</div>
																	</div>
															</div>
															<div class="row pull-right">
																<div class="col-md-6">
																	<label class="control-label bold"></label>
																	<a class="btn blue" ng-click="form({{$i}})" href="javascript:;">
																	Save </a>
																</div>
															</div>

														</div>
													</div>
													</form>
												</div>
											@endif
											@php $i = $i+1 @endphp
										@endforeach
									</div>
								</div>
							<!-- end permission list -->
						</div>
					</div>
				</div>
			</div>	
		</div>
		<div class="col-lg-4">
			<flash-message duration="2000" class="cst-alert cst-company-alert" show-close="true" on-dismiss="myCallback(flash)"></flash-message>
			@if(Session::has('flash_alert_notice'))
				<div class="alert alert-success alert-dismissable cst-alert cst-normal-alert">
					<!-- <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button> -->
					<i class="icon fa fa-check"></i>  
					{{ Session::get('flash_alert_notice') }} 
				</div>
			@endif
			<div class="portlet light bordered tasks-widget">
				<div class="portlet-title caption font-green-sharp">
					<div class="caption">
						<i class="icon-microphone font-dark hide"></i>
						<span class="caption-subject bold font-dark uppercase"> Role List </span>
					</div>                       
					<div class="inputs">
						<div class="portlet-input input-inline input-small">
							<div class="input-icon text-right">                            
								<!--  <i class="icon-magnifier"></i>     -->                        
								<!-- <input type="text" id="query" class="form-control input-circle" ng-model="query" onfocus="pxtrack.emit('counter', '1')" placeholder="search..."/> -->
								<button class="btn btn-circle btn-sm btn-default" id=""  ng-click="showModelFrm('add-role',0)"> <i class="fa fa-plus"></i> Add New </button>
							</div>
						</div>
					</div>		               
				</div>
				<!-- Permission add Dialog -->				    
					@include('superadmin::role.forms.add_role')
				<!-- End of Permission add Dialog -->
				<!-- Permission edit Dialog -->				    					
				<!-- End of Permission edit Dialog -->
				
				<div class="portlet-body" >
					<div class="task-content">
						<div class="scroller portlet-height">
							<!-- START TASK LIST -->                       
							<ul class="task-list">
								<li class="" ng-repeat="list in plists | filter:query | orderBy: orderList">
									<div class="task-checkbox">
										<!-- <input type="checkbox" name="status" class="liChild" ng-model="companymap.status" ng-click="associateCompany(list.id)" ng-if="list.status == 'linked'"> -->
									</div>
									<div class="task-title">
									<span class="task-title-sp text-capitalize" ng-click="getRole(list.id)"><span href="javascript:;" class="role_list" id="role_list @{{list.id}}">@{{list.name}}</span></span>
									<div ng-show="loader@{{$index}}"></div>
									<span class="label label-sm label-success task-left ng-scope" ng-show="false" ng-click="associateCompany(list.companyId,list.projectId)">Unselect</span>										
										<span class="btn btn-warning btn-xs task-right" ng-click="showModelFrm('edit',list.id)"><i class="fa fa-edit" title="Edit"></i></span>
									</div>
								</li>
							</ul>                        
						</div>                                           
					</div>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>															
	</div>
	<!-- END CONTENT -->
</div>
</div>
<!-- END CONTAINER -->
@stop