@extends('superadmin::layouts.master')

@section('content')
<!-- BEGIN CONTAINER -->
<link href="{{url('/')}}/js/Treemodule/css/tree.css" rel="stylesheet">
<div class="page-content-wrapper" ng-app="roleModule">
	<div class="page-content" >
		<!-- {!! $breadcrumbs !!} -->
		@include('superadmin::partials.breadcrumb')
		<!-- BEGIN CONTENT -->
		<div class="row">						
			<div class="col-lg-12 col-md-12" ng-controller="roleController">
				<div class="portlet box blue tabbable">
					<div class="portlet-title caption font-green-sharp">
						<div class="caption">
							<i class="icon-microphone font-dark hide"></i>
							<span class="caption-subject bold font-dark uppercase"> Role List </span>
						</div>                       
						<div class="actions">
							<div class="portlet-input input-inline input-small">
								<div class="input-icon text-right">                            
									<button class="btn btn-circle btn-sm btn-default" id=""  ng-click="showModelFrm('add-role',0)"> <i class="fa fa-plus"></i> Add New </button>
								</div>
							</div>
						</div>		               
					</div>
					<!-- Permission add Dialog -->				    
						@include('superadmin::role.forms.add_role')

					<!-- End of Permission add Dialog -->
					<div class="portlet-body" ng-init="roleList()">
						@if(Session::has('flash_alert_notice'))
							<div class="alert alert-success alert-dismissable cst-alert cst-normal-alert">
								<!-- <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button> -->
								<i class="icon fa fa-check"></i>  
								{{ Session::get('flash_alert_notice') }} 
							</div>
						@endif
						<flash-message duration="2000" show-close="true" on-dismiss="myCallback(flash)"></flash-message>
						<div class="table-responsive ">
							<table class="table table-striped table-bordered table-hover" datatable="ng" dt-options="vm.dtOptions">
							    <thead>
							      	<tr>
										<!-- <th width="40">SR</th> -->
										<th>Name</th>
										<th width="60%">Description</th>
										<th width="90">Action</th>
							      	</tr>
							    </thead>
							    <tbody>
							      	<tr ng-repeat="role in roleList">
								        <!-- <td>@{{$index + 1}}</td> -->
								        <td ng-click="showPermissions(role.id)" class="" style="cursor: pointer;">@{{role.name}}</td>
								        <td>@{{role.description}}</td>
								        <td>
							                <button type="button" class="btn btn-warning btn-xs" ng-click="showModelFrm('edit', role.id,'');"><i class="fa fa-pencil-square-o"></i></button>  
							                <button type="button" class="btn btn-success btn-xs" ng-click="showModelFrm('permission',role.id,role.name)"><i class="fa fa-newspaper-o"></i></button> 
							                <button type="button" class="btn btn-danger btn-xs" ng-confirm-click="Are you sure to delete this record ?" confirmed-click="delete(role.id)"><i class="glyphicon glyphicon-trash"></i></button> 
						                </td>
							      	</tr>
							    </tbody>
					  		</table>
					  		<!-- End table -->
				  		</div>
					</div>
				</div>
			</div>
			<!-- <div class="col-lg-8 col-md-8" ng-controller="TreeController as tc">
				<div class="portlet box blue tabbable">
					<div class="portlet-title caption font-green-sharp">
						<div class="caption">
							<i class="icon-microphone font-dark hide"></i>
							<span class="caption-subject bold font-dark uppercase"> Authentications </span>
						</div>                       
						<div class="actions">
							<div class="portlet-input input-inline input-small">
								<div class="input-icon text-right">                            
									<button class="btn btn-circle btn-sm btn-default" id=""  ng-click="showModelFrm('add-role',0)"> <i class="fa fa-plus"></i> Add New </button>
								</div>
							</div>
						</div>		               
					</div>
					<div class="portlet-body">
						<flash-message show-close="false" on-dismiss="myCallback(flash)"></flash-message>
						<ul class="tree">
				        	<node-tree children="tc.tree"></node-tree>
					    </ul>
					    <p class="text-right">
							<button class="btn blue" ng-click="storRolePermission(roleid)">Save Permissions</button>
							<span ng-show="showLoader">
			                	<img src="{{url('/')}}/assets/img/loading.gif">
			                </span>
						</p>
					</div>
				</div>
			</div> -->
		</div>
		<!-- End Row -->
		<div class="clearfix"></div>			
		@include('superadmin::role.forms.permission')														
	</div>
	<!-- END CONTENT -->
</div>
</div>
<!-- END CONTAINER -->
@stop