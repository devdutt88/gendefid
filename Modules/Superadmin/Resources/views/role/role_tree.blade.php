<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
  
<title>A demo of using treeview plug-in with checkbox</title>
<!-- <link rel="canonical" href="http://www.jquery-az.com/jquery-treeview-with-checkboxes-2-examples-with-bootstrap" />
 -->  <link rel="stylesheet" href="{{url('/')}}/css/style.css">
 
<link href="http://www.gendefid.com/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link type="text/css" rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" />
</head>
<body>
<div class="topdiv">
Demo by jquery-az.com
</div>
<div>
<table> 
<tr>
<td class="indtd">
<div class="container">
<div class="row">
    <div class="col-md-6">
      <div id="treeview-checkbox-demo">
            <ul>
                <li>HTML
                <ul>
                        <li data-value="table">HTML table</li>
                        <li data-value="links">HTML links</li>
                 </ul>
                </li>
                <li>PHP
                    <ul>
                        <li data-value="PHP if..else">PHP if..else</li>
                        <li>PHP Loops
                            <ul>
                                <li data-value="For loop">For loop</li>
                                <li data-value="While loop">While loop</li>
                                <li data-value="Do WHile loop">Do WHile loop</li>
                            </ul>
                        </li>
                        <li>PHP arrays</li>
                    </ul>
                </li>
                <li>jQuery
                    <ul>
                        <li data-value="jQuery append">jQuery append</li>
                        <li data-value="jQuery prepend">jQuery prepend</li>
                    </ul>
                </li>
            </ul>
        </div>
        <button type="button" class="btn btn-success" id="show-values">Get Values</button>
        <pre id="values"></pre>
   </div>
  </div>
 </div>       
      	<script src="{{url('/')}}/assets/js/jquery.min.js" type="text/javascript"></script>
		<script src="{{url('/')}}/assets/js/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="{{url('/')}}/js/logger.js"></script>
        <script src="{{url('/')}}/js/treeview.js"></script>
        
        <script>
            $('#treeview-checkbox-demo').treeview({
                debug : true,
                data : ['links', 'Do WHile loop']
            });
            $('#show-values').on('click', function(){ 
                console.log($('#treeview-checkbox-demo').treeview('selectedValues'));
                $('#values').text(
                    $('#treeview-checkbox-demo').treeview('selectedValues')
                );
            });
        </script>
</td>

        
  </tr>
 </table></div><hr />
<div style="text-align: center;margin: 5px; height: 250px; width:90%;">



</div>

<hr />
<!-- <button id="enabletxtarea">Enable textarea to copy/paste code</button> -->

<script>

// $(document).ready(function() {
// $("textarea").attr("disabled", "disabled");

//    $("#enabletxtarea").on("click",function(){
//     $("textarea").removeAttr('disabled');
//    });


// });
</script>

    </body>
</html>