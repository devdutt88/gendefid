@extends('superadmin::layouts.master')

@section('content')
<!-- BEGIN CONTAINER -->
<link href="{{url('/')}}/js/Treemodule/css/tree.css" rel="stylesheet">
<div class="page-content-wrapper" ng-app="tree">
	<div class="page-content" ng-controller="TreeController as tc">
		<!-- {!! $breadcrumbs !!} -->
		@include('superadmin::partials.breadcrumb')
		<!-- BEGIN TAB PORTLET-->
		<div class="col-lg-9 col-md-12">
			<div class="portlet box blue tabbable">
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-gift"></i>Role Authentications
					</div>
				</div>
				<div class="portlet-body">
					<ul class="tree">
				        <node-tree children="tc.tree"></node-tree>
				    </ul>
				    <p class="text-right">
						<button class="btn blue" ng-click="storRolePermission()">Save Permissions</button>
					</p>
				</div>
			</div>
		</div>
		<!-- END TAB PORTLET-->
		<div class="clearfix"></div>															
	</div>
	<!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
@stop