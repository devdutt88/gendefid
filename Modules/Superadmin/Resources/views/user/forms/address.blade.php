<div class="modal fade" id="addressModal" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog">
	    <div class="modal-content">
			<!-- BEGIN FORM-->
		 	<form id="addressInfo" name="addressInfo" class="horizontal-form" ng-submit="submitForm('{{Crypt::encrypt($user->id)}}','address')">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					<h4 class="modal-title">User Personal Details</h4>
				</div>
				<div class="modal-body">
					{{ csrf_field() }}
					<div class="form-body">
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
								<label class="control-label">Business Address 1</label>
								<input type="text" id="busi_address1" name="busi_address1" class="form-control" placeholder="" ng-model="address.busi_address1">
								</div>
							</div>
							<!--/span-->
							<div class="col-md-6">
								<div class="form-group">
								<label class="control-label">Business Address 2</label>
								<input type="text" id="busi_address2" name="busi_address2" class="form-control" placeholder="" ng-model="address.busi_address2">
								</div>
							</div>
							<!--/span-->
						</div>

						<!--/row-->
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
								<label class="control-label">Business Suburb</label>
								<input type="text" id="busi_suburb" name="busi_suburb" class="form-control" placeholder="" ng-model="address.busi_suburb">
								</div>
							</div>
							<!--/span-->
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Business State</label>
									<select name="busi_state" class="form-control" ng-model="address.busi_state">
										<option value="">Select any one</option>
										<option ng-repeat="x in states" value="@{{x}}">@{{x}}</option>
									</select>
								</div>
							</div>
							<!--/span-->
						</div>
						<!--/row-->
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
								<label class="control-label">Business Post</label>
								<input type="text" id="busi_post" name="busi_post" class="form-control" placeholder="" ng-model="address.busi_post">
								</div>
							</div>
							<!--/span-->
							<div class="col-md-6">
								<div class="form-group">
								<label class="control-label">Business Country</label>								
								{!! Form::text('busi_country', 'Australia', array('class' => 'form-control','id'=>'busi_country','ng-model'=>'address.busi_country','readonly'=>'readonly','ng-required'=>'true')) !!}
								</div>
							</div>
							<!--/span-->
						</div>

						<!-- <div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label"><input name="chck1" id="chck1" class="chck1" type="checkbox"> Business Address is same as billing address.</label>
								</div>
							</div>
						</div> -->

						<!--/row for billing-->
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
								<label class="control-label">Billing Address 1</label>
								<input type="text" id="bill_address1" name="bill_address1" class="form-control" placeholder="" ng-model="address.bill_address1">
								</div>
							</div>
							<!--/span-->
							<div class="col-md-6">
								<div class="form-group">
								<label class="control-label">Billing Address 2</label>
								<input type="text" id="bill_address2" name="bill_address2" class="form-control" placeholder="" ng-model="address.bill_address2">
								</div>
							</div>
							<!--/span-->
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
								<label class="control-label">Billing Suburb</label>
								<input type="text" id="bill_suburb" name="bill_suburb" class="form-control" placeholder="" ng-model="address.bill_suburb">
								</div>
							</div>
							<!--/span-->
							<div class="col-md-6">
								<div class="form-group">
								<label class="control-label">Billing State</label>
									<select name="bill_state" class="form-control" ng-model="address.bill_state">
										<option value="">Select any one</option>
										<option ng-repeat="x in states" value="@{{x}}">@{{x}}</option>
									</select>
								</div>
							</div>
							<!--/span-->
						</div>
						<!--/row-->
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
								<label class="control-label">Billing Post</label>
								<input type="text" id="bill_post" name="bill_post" class="form-control" placeholder="" ng-model="address.bill_post">
								</div>
							</div>
							<!--/span-->
							<div class="col-md-6">
								<div class="form-group">
								<label class="control-label">Billing Country</label>								
								{!! Form::text('bill_country', 'Australia', array('class' => 'form-control','id'=>'bill_country','ng-model'=>'address.bill_country','readonly'=>'readonly','ng-required'=>'true')) !!}
								</div>
							</div>
							<!--/span-->
						</div>
						<!--/row-->
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal" ng-click="cancel()">Cancel</button>
					<button type="submit" class="btn btn-primary" id="address_form">Update</button>
					<span ng-show="showLoader"><img src="{{url('/')}}/assets/img/input-spinner.gif"></span>
				</div>
			</form>
			<!-- END FORM-->
		</div>
	</div>
</div>
