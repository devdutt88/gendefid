<form name="changePasswordFrm" ng-submit="changePassword()">
	{{ csrf_field() }}
    <div class="col-md-12">
      	<div class="form-group" ng-class="{ 'has-error' : changePasswordFrm.current_psw.$dirty && changePasswordFrm.current_psw.$invalid }">
			<label class="control-label col-md-5">Current Password</label>
			<div class="col-md-5">
				<input type="password" class="form-control" name="current_psw" id="current_psw" ng-model="adminPassword.current_psw" password-exists ng-model-options="{ updateOn: 'blur' }" required />
				<!-- <span class="error" ng-show="changePasswordFrm.$error.passwordExists">Invalid password please try again</span> -->
				<div class="help-block" ng-messages="changePasswordFrm.current_psw.$error" ng-if="changePasswordFrm.current_psw.$dirty">
					<p class="error" ng-message="required">This field is required</p>
					<p class="error" ng-message="passwordExists">Invalid password please try again</p>
				</div>
			</div>
		</div>
      	<div class="form-group" ng-class="{ 'has-error' : changePasswordFrm.password.$dirty && changePasswordFrm.password.$invalid }">
        	<label class="control-label col-md-5">New Password</label>
        	<div class="col-md-5">
	        	<input type="password" class="form-control" id="password" name="password" placeholder="password" required ng-model="adminPassword.password" password-verify="@{{adminPassword.confirm_password}}" ng-pattern="/^(?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!%*#?&])[A-Za-z\d$@$!%*#?&]{8,}$/">
				<div class="help-block" ng-messages="changePasswordFrm.password.$error" ng-if="changePasswordFrm.password.$dirty">
					<p class="error" ng-message="required">This field is required</p>
					<p class="error" ng-message="minlength">This field is too short</p>
					<p class="error" ng-message="maxlength">This field is too long</p>
					<p ng-message="pattern">Password should be atleast 8 characters long and should contain one number,one character and one special character
		          	</p>
					<!-- <p class="error" ng-message="passwordVerify">No match!</p> -->
				</div>
			</div>
      	</div>
      	<div class="form-group" ng-class="{ 'has-error' : changePasswordFrm.confirm_password.$dirty && changePasswordFrm.confirm_password.$invalid }">
        	<label class="control-label col-md-5">Confirm New Password</label>
        	<div class="col-md-5">
	        	<input class="form-control" id="confirm_password" ng-model="adminPassword.confirm_password" name="confirm_password" type="password" placeholder="confirm password" required password-verify="@{{adminPassword.password}}">
	        	<div class="help-block" ng-messages="changePasswordFrm.confirm_password.$error" ng-if="changePasswordFrm.confirm_password.$dirty">
		          <p class="error" ng-message="required">This field is required</p>
		          <p class="error" ng-message="minlength">This field is too short</p>
		          <p class="error" ng-message="maxlength">This field is too long</p>
		          <p class="error" ng-message="required">This field is required</p>
		          <p class="error" ng-message="passwordVerify">password did not match!</p>
		        </div>
	        </div>
      	</div>
      	<hr><!-- Fotter Line -->
		<div class="form-group">
			<div class="text-right">
				<button type="button" ng-click="reset(changePasswordFrm)" class="btn default"> Cancel</button>
				<button type="submit" class="btn btn-success" id="password_form" ng-disabled="changePasswordFrm.$invalid"> Update </button>
				<span ng-show="showLoader"><img src="{{url('/')}}/assets/img/input-spinner.gif"></span>
			</div>
		</div>
    </div>
</form>
