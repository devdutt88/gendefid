<div class="modal fade" id="userformModel" close="reset(userForm)">
	<div class="modal-dialog">
        <div class="modal-content">
			<form name="userForm" id="userFormId" class="horizontal-form" ng-submit="submitForm(action)" novalidate>
				<div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" ng-click="reset(userForm)">×</button>
                    <h4 class="modal-title">@{{formheader}} User </h4>
                </div>
				<div class="modal-body">
					{{ csrf_field() }}
					{{ Form::hidden("user_id", "user.id", $attributes = array("id" => "user","ng-model"=>"user.id", "ng-init" => "user.id=user.id")) }}
					<div class="form-body">
						<div class="row">
							<div class="col-md-6">
								<div class="form-group" ng-if=" action === 'edit' " >
									<label class="control-label">Username <span class="red">*</span></label>
									<input type="text" id="username" class="form-control" name="username" ng-model="user.username" ng-readonly="true" />
								</div>
								<div class="form-group" ng-if=" action === 'add' ">
									<label class="control-label">Username <span class="red">*</span></label>
								  	<input type="text" id="username" class="form-control" name="username" ng-minlength="3" ng-maxlength="15" ng-model="user.username"  username-exists ng-model-options="{ updateOn: 'blur' }" required />
									<div class="help-block" ng-messages="userForm.username.$error">
									    <p class="error" ng-message="required">Username is required.</p>
									    <p class="error" ng-message="minlength || maxlength">Length should be between 3 to 8 char</p>
									    <p class="error" ng-message="usernameExists">Username address already exist.</p>
								  	</div>
								</div>
							</div>
							<!--/span-->
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Full Name <span class="red">*</span></label>
									<input type="text" id="fullname" name="fullname" class="form-control" ng-model="user.fullname" ng-required="true" />
									<span class="error" ng-show="userForm.fullname.$error.required">Fullname is required.</span>
								</div>
							</div>
							<!--/span-->
							
						</div>
						<!--/row-->
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Email id <span class="red">*</span></label>
									<input  type="text" id="email" name="email" class="form-control" ng-model="user.email" email-exists ng-model-options="{ updateOn: 'blur' }" ng-required="true" />
									<div class="help-block" ng-messages="userForm.email.$error">
									    <p class="error" ng-message="required">Email address is required.</p>
									    <p class="error" ng-message="email">This field must be a valid email address.</p>
									    <p class="error" ng-message="emailExists">Email address already exist.</p>
								  	</div>
							  	</div>
							</div>
							<!--/span-->
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Password <span class="red">*</span></label>
									<input type="text" id="password" name="password" class="form-control" ng-model="user.password" ng-pattern="/^(?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!%*#?&])[A-Za-z\d$@$!%*#?&]{6,20}$/" ng-required="true" />
									<div ng-show="userForm.password.$error">
										<span class="error" ng-show="userForm.password.$error.required">Password is required.</span>
									    <span class="error" ng-show="userForm.password.$error.pattern">Password should be atleast 6 characters long and should contain 1 number,1 character and 1 special character.
								    </div>
									<!--<span class="help-block">
									This field has error. </span>-->
								</div>
							</div>
							<!--/span-->
						</div>
						<!--/row-->
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Contact Number <span class="red">*</span></label>
									<input type="number" id="phone" name="phone" class="form-control" ng-model="user.phone" ng-required="true" />
									<div ng-show="userForm.phone.$error">
										<span class="error" ng-show="userForm.phone.$error.required">Contact number is required.</span>
										<span class="error" ng-show="userForm.phone.$error.number">Please enter only numeric value.</span>
									    <span class="error" ng-show="userForm.phone.$error.minlength || userForm.phone.$error.maxlength">Contact number must be in b/w 8 to 15.</span>
								    </div>
									@if ($errors->has('phone'))
									    <span class=error>{{ $errors->first('phone') }}</span>
									@endif
								</div>
							</div>
							<!--/span-->
							
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">User Type <span class="red">*</span></label>
									<select name="usertype" class="form-control" ng-model="user.usertype" ng-required="true">
										<option value="">Select any one</option>
										<option ng-repeat="x in roles" value="@{{x.id}}">@{{x.name}}</option>
									</select>
									<span class="error" ng-show="userForm.usertype.$invalid">Usertype is required.</span>
								</div>
							</div>
							<!--/span-->
						</div>
						<!--/row-->
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal" ng-click="reset(userForm)"> Cancel </button>
					<button type="submit" class="btn blue" ng-disabled="userForm.$invalid"> Submit </button>
					<span ng-show="showLoader"><img src="{{url('/')}}/assets/img/input-spinner.gif"></span>
				</div>
			</form>
        </div>
    </div>
</div>
