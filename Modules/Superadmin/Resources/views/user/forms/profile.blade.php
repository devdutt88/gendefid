<div class="modal fade" id="profileModal" data-backdrop="static" data-keyboard="false"> 
	<div class="modal-dialog">
	    <div class="modal-content">
			<!-- BEGIN FORM-->
		 	<form id="userInfo" name="userInfo" class="horizontal-form" ng-submit="submitForm('{{Crypt::encrypt($user->id)}}','profile')" novalidate>
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					<h4 class="modal-title">User Personal Details</h4>
				</div>
				<div class="modal-body">
					{{ csrf_field() }}
					{{ Form::hidden("user_id", "user.id", $attributes = array("id" => "user","ng-model"=>"user.id", "ng-init" => "user.id=user.id")) }}
					<div class="form-body">
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Username <span class="red">*</span></label>
									<input type="text" id="username" class="form-control" name="username" placeholder="" ng-model="user.username" ng-readonly="true" />
									<span class="error" ng-show="userInfo.username.$invalid && userInfo.username.$touched">Username is required.</span>
								</div>
							</div>
							<!--/span-->
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Full Name <span class="red">*</span></label>
									<input type="text" id="fullname" name="fullname" class="form-control" placeholder="" ng-model="user.fullname" ng-required="true"/>
									<span class="error" ng-show="userInfo.fullname.$invalid && userInfo.fullname.$touched">Fullname is required.</span>
								</div>
							</div>
							<!--/span-->
						</div><!-- End Row -->
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Email id <span class="red">*</span></label>
									<input type="text" id="email" name="email" class="form-control" placeholder="" ng-model="user.email" email-exists ng-model-options="{ updateOn: 'blur' }" ng-required="true"/>
							    	<div class="help-block" ng-messages="userInfo.email.$error" ng-if="userInfo.email.$dirty">
									    <span class="error" ng-message="required">Email address is required.</span>
									    <span class="error" ng-message="email">This field must be a valid email address.</span>
									    <span class="error" ng-message="emailExists">Email address already exist.</span>
								  	</div>
								</div>
							</div>
							<!--/span-->
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Password <span class="red">*</span></label>
									<input type="text" id="password" name="password" class="form-control" ng-model="user.password" ng-pattern="/^(?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!%*#?&])[A-Za-z\d$@$!%*#?&]{6,20}$/" required />
									<div ng-show="userInfo.password.$dirty && userInfo.password.$error">
										<span class="error" ng-show="userInfo.password.$error.required">Password is required.</span>
									    <span class="error" ng-show="userInfo.password.$error.pattern">Password should be atleast 6 characters long and should contain 1 number,1 character and 1 special character.
								    </div>
								</div>
							</div>
							<!--/span-->
						</div><!-- End Row -->
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Contact Number <span class="red">*</span></label>
									<input type="number" id="phone" name="phone" class="form-control" placeholder="" ng-model="user.phone"  ng-minlength="8" ng-maxlength="15" required/>
									<span class="error" ng-show="userInfo.phone.$error.required">Contact number is required.</span>
									<span class="error" ng-show="userInfo.phone.$error.number">Please enter only numeric value.</span>
								    <span class="error" ng-show="userInfo.phone.$error.minlength || userInfo.phone.$error.maxlength">Contact number must be in b/w 8 to 15.</span>
								    
								</div>
							</div>
							<!--/span-->
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">User Type <span class="red">*</span></label>
									<select name="usertype" class="form-control" ng-model="user.usertype">
										<option value="">Select any one</option>
										<option ng-repeat="x in user.roles" value="@{{x.id}}">@{{x.name}}</option>
									</select>
								</div>
							</div> 
							<!--/span-->
						</div>
						<!--/row-->
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal" ng-click="cancel()">Cancel</button>
					<button type="submit" class="btn btn-primary" id="profile_form" ng-disabled="userInfo.$invalid">Update</button>
					<span ng-show="showLoader"><img src="{{url('/')}}/assets/img/input-spinner.gif"></span>
				</div>
			</form>
			<!-- END FORM-->
		</div>
	</div>
</div>
