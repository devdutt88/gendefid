@extends('superadmin::layouts.master')

@section('content')
<!-- BEGIN CONTAINER -->
	<link href="{{url('/')}}/assets/css/profile.css" rel="stylesheet" type="text/css"/>
	<div class="page-content-wrapper" ng-app="adminModule">
		<div class="page-content" ng-controller="superadminController">
			<!-- BEGIN PAGE HEADER-->
			@include('superadmin::partials.breadcrumb')
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<!-- BEGIN PROFILE SIDEBAR -->
				<div class="col-md-4">
					<!-- Alert Section -->
						@if(Session::has('flash_alert_notice_logo'))
					        <div class="alert alert-success alert-dismissable cst-alert cst-normal-alert">
					          	<!-- <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button> -->
					          	<i class="icon fa fa-check"></i>  
					           	{{ Session::get('flash_alert_notice_logo') }} 
					        </div>
					   	@endif
					<!-- End Alert Section -->	
					<!-- PORTLET MAIN -->
					<div class="portlet light min-height-450">
						<div class="profile-userpic text-center">
							<form id="adminLogoFrm" runat="server">
								<div class="form-group">
									<!-- SIDEBAR USERPIC -->
									@if(!empty($udetail[0]->logo_image))
										<img id="logo_image"  src="{{url('/')}}/uploads/user/{{$udetail[0]->logo_image}}"  class="img-responsive" alt="your image" />
									@else
										<img id="logo_image"  src="{{url('/')}}/uploads/user_icon.jpg" alt="user image"  class="img-responsive" />
									@endif
									<!-- END SIDEBAR USERPIC -->
								</div>	
								<div class="profile-userbuttons">
									<div class="col-xs-12">
										<span class="btn cst-yellow fileinput-button btn-circle btn-sm">
						                    @if(!empty($udetail[0]->logo_image))
						                    	<i class="fa fa-pencil"></i>
						                    	<span>Change Photo</span>
						                    @else
						                    	<i class="glyphicon glyphicon-plus"></i>
						                    	<span>Add Photo</span>
						                    @endif
											 	<input type='file' name="logo" id="userImg"/>
										</span>
									</div>
									<div class="col-xs-12">
										<p class="form-control-static">
											<button type="submit" id="upload_btn" style="display:none;" class="btn btn-circle btn-sm btn-success start" ng-click="submitForm('logo')">
							                    <i class="glyphicon glyphicon-upload"></i>
							                    <span>Start upload</span>
							                </button>
							                <span ng-show="showLoader">
							                	<img src="{{url('/')}}/assets/img/loading.gif">
							                </span>
										</p>
									</div>
								</div>
							</form>
						</div>
						<!-- SIDEBAR USER TITLE -->
						<div class="profile-usertitle profile-stat">
							<div class="profile-usertitle-name">
								 {{$admin->full_name}}
							</div>
							<div class="profile-usertitle-job">
								 {{$superadmin_role}}
							</div>
						</div>
						<!-- END SIDEBAR USER TITLE -->
					</div>
					<!-- END PORTLET MAIN -->
				</div>
				<!-- END BEGIN PROFILE SIDEBAR -->
				<!-- BEGIN PROFILE CONTENT -->
				<div class="profile-content col-md-8">
					<div class="row">
						<div class="col-md-12">
							<!-- Alert Section -->
								@if(Session::has('flash_alert_notice'))
							        <div class="alert alert-success alert-dismissable cst-alert cst-normal-alert">
							          	<!-- <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button> -->
							          	<i class="icon fa fa-check"></i>  
							           	{{ Session::get('flash_alert_notice') }} 
							        </div>
							   	@endif
							<!-- End Alert Section -->
							<div class="portlet light bordered min-height-450">
								<div class="portlet-title tabbable-line">
									<div class="actions">
										<a href="" class="btn btn-circle btn-default btn-sm" ng-click="showModal()">
										<i class="fa fa-pencil"></i> Edit </a>
									</div>
									<ul class="nav nav-tabs pull-left">
										<li class="active bold">
											<a href="#tab_1_1" data-toggle="tab">Personal Info</a>
										</li>
										<li class="bold">
											<a href="#tab_1_2" data-toggle="tab">Change Password</a>
										</li>
									</ul>
								</div>
								<div class="portlet-body">
									<div class="tab-content">
										<!-- PERSONAL INFO TAB -->
										<div class="tab-pane active" id="tab_1_1">
											<div class="row">
												<div class="col-md-12">
													<div class="form-group">
														<div class="row">
															<label class="control-label col-md-3"><strong>Name :</strong></label>
															<div class="col-md-9">
																	 {{$admin->full_name}}
															</div>
														</div>
														<div class="row">
															<label class="control-label col-md-3"><strong>Username :</strong></label>
															<div class="col-md-9">
																	 {{$admin->name}}
															</div>
														</div>
														<div class="row">
															<label class="control-label col-md-3"><strong>Email :</strong></label>
															<div class="col-md-9">
																	 {{$admin->email}}
															</div>
														</div>
														<div class="row">
															<label class="control-label col-md-3"><strong>Contact :</strong></label>
															<div class="col-md-9">
																	{{$admin->phone}}
															</div>
														</div>
													</div>
												</div>
												<!--/span-->
											</div>
										</div>
										<!-- Profile Dialog -->
										@include('superadmin::user.forms.admin-profile') <!-- UPDATE PROFILE FORM -->
										<!-- END PERSONAL INFO TAB -->
										<!-- CHANGE AVATAR TAB -->
										<div class="tab-pane" id="tab_1_2">
											<div class="form-horizontal">
												<div class="form-body">
													@include('superadmin::user.forms.admin-password') <!-- CHANGE PASSWORD FORM -->
												</div>
											</div>
										</div>
										<!-- END CHANGE AVATAR TAB -->
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- END PROFILE CONTENT -->
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
<!-- END CONTAINER -->
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/angular-messages/1.5.7/angular-messages.min.js"></script> -->
@stop