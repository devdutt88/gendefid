@extends('superadmin::layouts.master')

@section('content')
<!-- BEGIN CONTAINER -->
	<div class="page-content-wrapper" ng-app="userModule">
		<div class="page-content" ng-controller="userController">
			<!-- BEGIN PAGE HEADER-->
			@include('superadmin::partials.breadcrumb')
			<!-- {!! $breadcrumbs !!} -->
			<!-- END PAGE HEADER-->

			<!-- BEGIN CONTENT -->
			<div class="row">
				<div class="col-md-12">
					<div class="portlet box grey-cascade">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-gift"></i>User Details
							</div>
							
						</div>
						<div class="portlet-body form">
						@if(Session::has('flash_alert_notice'))
	                        <div class="alert alert-success alert-dismissable">
	                          	<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
	                          	<i class="icon fa fa-check"></i>  
	                           	{{ Session::get('flash_alert_notice') }} 
	                        </div>
	                   	@endif

							<div class="edit_btn">
								<div class="row">
									<div class="col-md-12 text-right">
										<div class="row">
											<div class="col-md-12">
												<!-- <a href="#"><button type="button" onClick="history.go(-1); return false;" class="btn default"><i class="fa fa-chevron-left "></i> Back</button></a> -->
												<button class="btn cst-yellow" ng-click="showModelFrm({{$user->id}}, 'profile')"><i class="fa fa-pencil"></i> Update Profile</button>
											</div>
										</div>
									</div>
									<div class="col-md-6">
									</div>
								</div>
							</div>
							<!-- Profile Dialog -->
								@include('superadmin::user.forms.profile')
					    	<!-- End of Profile Dialog -->
							<!-- BEGIN FORM-->
							<div class="form-horizontal">
								<div class="form-body">									
									<h3 class="form-section">User Info</h3>								
									<div class="row">
										<div class="col-md-6 text-center">
											<form id="userLogoFrm" runat="server">
												<div class="form-group">
													<span class="btn cst-yellow fileinput-button">
									                    <i class="glyphicon glyphicon-plus"></i>

									                    @if(!empty($udetail[0]->logo_image))
									                    	<span>Change Photo</span>
									                    @else
									                    	<span>Add Photo</span>
									                    @endif
														 	<input type='file' name="logo" id="userImg"/>
													</span><br><br>
													@if(!empty($udetail[0]->logo_image))
														<img id="logo_image"  width="200" height="200"  src="{{url('/')}}/uploads/user/{{$udetail[0]->logo_image}}" alt="your image" />
													@else
														<img id="logo_image"  width="200" height="200"  src="{{url('/')}}/uploads/user_icon.jpg" alt="user image" />
													@endif
												</div>	
												<p class="form-control-static">
													<button type="submit" id="upload_btn" style="display:none;" class="btn btn-success start" ng-click="submitForm({{$user->id}},'logo','{{$user->name}}')">
									                    <i class="glyphicon glyphicon-upload"></i>
									                    <span>Start upload</span>
									                </button>
									                <span ng-show="showLoader">
									                	<img src="{{url('/')}}/assets/img/ajax-loader.gif">
									                </span>
												</p>
											</form>
												

										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-3"><strong>Username :</strong></label>
												<div class="col-md-7">
													<p class="form-control-static">
														 {{$user->name}}
													</p>
												</div>

												<label class="control-label col-md-3"><strong>Password :</strong></label>
												<div class="col-md-7">
													<p class="form-control-static">
														 {{$user->plain_password}}
													</p>
												</div>

												<label class="control-label col-md-3"><strong>User Role :</strong></label>
												<div class="col-md-7">
													<p class="form-control-static">
														{{((isset($roles[$user->role_id])) ? $roles[$user->role_id] : 'NA')}}
													</p>
												</div>

												<label class="control-label col-md-3"><strong>Name :</strong></label>
												<div class="col-md-7">
													<p class="form-control-static">
														 {{$user->full_name}}
													</p>
												</div>

												<label class="control-label col-md-3"><strong>Email :</strong></label>
												<div class="col-md-7">
													<p class="form-control-static">
														 {{$user->email}}
													</p>
												</div>

												<label class="control-label col-md-3"><strong>Mobile :</strong></label>
												<div class="col-md-7">
													<p class="form-control-static">
														{{$user->phone}}
													</p>
												</div>
											
												
											<?php /*
												<label class="control-label col-md-3"><strong>Company :</strong></label>
												<div class="col-md-7">
													<p class="form-control-static">
														@foreach($user_compnies as $uc)
														{{$compnies[$uc]}}<br>
														@endforeach
													</p>
												</div> */?>
											</div>
										</div>
										<!--/span-->
									</div>
								</div>							
							</div>
							<!-- END USERINFO FORM-->
							<div class="edit_btn">
								<div class="row">
									<div class="col-md-12 text-right">
										<div class="row">
											<div class="col-md-12">
												<button class="btn cst-yellow" ng-click="showModelFrm({{$user->id}}, 'address')"><i class="fa fa-pencil"></i> Update Address</button>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!-- Address Dialog -->
								@include('superadmin::user.forms.address')
					    	<!-- End of Address Dialog -->
							<div class="form-horizontal">
								<div class="form-body">									
									<h3 class="form-section">User Address</h3>		
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-5"><strong>Business Address 1 :</strong></label>
												<div class="col-md-7">
													<p class="form-control-static">
														 @if(!empty($user_details)){{$user_details->busi_address1}}@endif
													</p>
												</div>
											</div>
										</div>
									
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-5"><strong>Business Address 2 :</strong></label>
												<div class="col-md-7">
													<p class="form-control-static">
														 @if(!empty($user_details)){{$user_details->busi_address2}}@endif
													</p>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<!--/span-->
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-5"><strong>Business Suburb :</strong></label>
												<div class="col-md-7">
													<p class="form-control-static">
														 @if(!empty($user_details)){{$user_details->busi_suburb}}@endif
													</p>
												</div>
											</div>
										</div>
										<!--/span-->
									
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-5"><strong>Business State :</strong></label>
												<div class="col-md-7">
													<p class="form-control-static">
														@if(!empty($user_details)){{$user_details->busi_state}}@endif
													</p>
												</div>
											</div>
										</div>
									</div>
									<!--/row-->
									<div class="row">
										<!--/span-->
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-5"><strong>Business Post :</strong></label>
												<div class="col-md-7">
													<p class="form-control-static">
														@if(!empty($user_details)){{$user_details->busi_post}}@endif
													</p>
												</div>
											</div>
										</div>

										<!--/span-->
										<!--/span-->
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-5"><strong>Business Country :</strong></label>
												<div class="col-md-7">
													<p class="form-control-static">
														@if(!empty($user_details)){{$user_details->busi_country}}@endif
													</p>
												</div>
											</div>
										</div>
										<!--/span-->
									</div>
									<!--Billing addr-->
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-5"><strong>Billing Address 1 :</strong></label>
												<div class="col-md-7">
													<p class="form-control-static">
														 @if(!empty($user_details)){{$user_details->bill_address1}}@endif
													</p>
												</div>
											</div>
										</div>
									
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-5"><strong>Billing Address 2 :</strong></label>
												<div class="col-md-7">
													<p class="form-control-static">
														 @if(!empty($user_details)){{$user_details->bill_address2}}@endif
													</p>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<!--/span-->
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-5"><strong>Billing Suburb :</strong></label>
												<div class="col-md-7">
													<p class="form-control-static">
														 @if(!empty($user_details)){{$user_details->bill_suburb}}@endif
													</p>
												</div>
											</div>
										</div>
										<!--/span-->
									
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-5"><strong>Billing State :</strong></label>
												<div class="col-md-7">
													<p class="form-control-static">
														@if(!empty($user_details)){{$user_details->bill_state}}@endif
													</p>
												</div>
											</div>
										</div>
									</div>
									<!--/row-->
									<div class="row">
										<!--/span-->
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-5"><strong>Billing Post :</strong></label>
												<div class="col-md-7">
													<p class="form-control-static">
														@if(!empty($user_details)){{$user_details->bill_post}}@endif
													</p>
												</div>
											</div>
										</div>
										<!--/span-->
										<!--/span-->
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-md-5"><strong>Billing Country :</strong></label>
												<div class="col-md-7">
													<p class="form-control-static">
														@if(!empty($user_details)){{$user_details->bill_country}}@endif
													</p>
												</div>
											</div>
										</div>
										<!--/span-->
									</div>
								</div>							
							</div>
							
							<div class="edit_btn">
								<div class="row">
									<div class="col-md-12 text-right">
										<div class="row">
											<div class="col-md-12">
												<button class="btn cst-yellow" ng-click="showModelFrm({{$user->id}}, 'companies')"> <i class="fa fa-plus"></i> Add More Companies </button>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!-- Address Dialog -->
								@include('superadmin::user.forms.companies')
					    	<!-- End of Address Dialog -->
							<div class="form-horizontal" >
								<div class="form-body">									
									<h3 class="form-section">Associated Companies</h3>
									<table class="table table-striped table-bordered table-hover" id="assoc_comp_list">
										<thead>
											<tr>
												<th> Company Name </th>
												<th> Company Type </th>
											</tr>
										</thead>														
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>																
			</div>
			<!-- END CONTENT -->
		</div>
	</div>
<!-- END CONTAINER -->
@stop