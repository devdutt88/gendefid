@extends('superadmin::layouts.master')

@section('content')
<!-- BEGIN CONTAINER -->
<link href="{{url('/')}}/assets/css/tasks.css" rel="stylesheet" type="text/css"/>
<link href="{{url('/')}}/assets/css/profile.css" rel="stylesheet" type="text/css"/>
<div class="page-content-wrapper" ng-app="userModule">
	<div class="page-content" ng-controller="showuserController">
		<!-- BEGIN PAGE HEADER-->
		@include('superadmin::partials.breadcrumb')
		<!-- {!! $breadcrumbs !!} -->
		<!-- END PAGE HEADER-->
		
		<!-- BEGIN ROW 1 CONTENT-->
		<div class="row">
			<!-- BEGIN PROFILE SIDEBAR -->
			<div class="col-md-3">
				<!-- PORTLET MAIN -->			
				<!-- Alert Section -->
					@if(Session::has('flash_alert_notice_logo'))
				        <div class="alert alert-success alert-dismissable cst-alert cst-normal-alert">
				          	<!-- <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button> -->
				          	<i class="icon fa fa-check"></i>  
				           	{{ Session::get('flash_alert_notice_logo') }} 
				        </div>
				   	@endif
				<!-- End Alert Section -->	
				<div class="portlet light min-height-450">
					<div class="profile-userpic text-center">
						<form id="userLogoFrm" runat="server">
							<div class="form-group">
								<!-- SIDEBAR USERPIC -->
								@php
									$path =public_path('uploads/user/thumbnail_200x200/');
								@endphp
								@if(!empty($udetail[0]) && !empty($udetail[0]->logo_image) && file_exists($path.$udetail[0]->logo_image))
									<img id="logo_image"  src="{{url('/')}}/uploads/user/thumbnail_200x200/{{$udetail[0]->logo_image}}"  class="img-responsive" alt="your image" />
								@else
									<img id="logo_image"  src="{{url('/')}}/uploads/user_icon.jpg" alt="user image"  class="img-responsive" />
								@endif
								<!-- END SIDEBAR USERPIC -->
							</div>	
							<div class="profile-userbuttons">
								<div class="col-xs-12">
									<span class="btn cst-yellow fileinput-button btn-circle btn-sm">
					                    @if(!empty($udetail[0]))
					                    	<i class="fa fa-pencil"></i>
					                    	<span>Change Photo</span>
					                    @else
					                    	<i class="glyphicon glyphicon-plus"></i>
					                    	<span>Add Photo</span>
					                    @endif
										 	<input type='file' name="logo" id="userImg"/>
									</span>
								</div>
								<div class="col-xs-12">
									<p class="form-control-static">
										<button type="submit" id="upload_btn" style="display:none;" class="btn btn-circle btn-sm btn-success start" ng-click="submitForm('{{Crypt::encrypt($user->id)}}','logo','{{$user->name}}')">
						                    <i class="glyphicon glyphicon-upload"></i>
						                    <span>Start upload</span>
						                </button>
						                <span ng-show="showLoader">
						                	<img src="{{url('/')}}/assets/img/loading.gif">
						                </span>
									</p>
								</div>
							</div>
						</form>
					</div>
					<!-- SIDEBAR USER TITLE -->
					<div class="profile-usertitle profile-stat">
						<div class="profile-usertitle-name">
							 {{$user->full_name}}
						</div>
						<div class="profile-usertitle-job">
							 {{((isset($roles[$user->role_id])) ? $roles[$user->role_id] : 'NA')}}
						</div>
					</div>
					<!-- END SIDEBAR USER TITLE -->
					<!-- SIDEBAR BUTTONS -->
					<div class="row list-separated profile-stat">
						<div class="col-md-6 col-sm-6 col-xs-6">
							<div class="uppercase profile-stat-title">
								 <?php echo count($uc); ?>
							</div>
							<div class="uppercase profile-stat-text">
								 Companies
							</div>
						</div>
						<div class="col-md-6 col-sm-6 col-xs-6">
							<div class="uppercase profile-stat-title">
								 51
							</div>
							<div class="uppercase profile-stat-text">
								 Projects
							</div>
						</div>
						<!-- <div class="col-md-4 col-sm-4 col-xs-6">
							<div class="uppercase profile-stat-title">
								 61
							</div>
							<div class="uppercase profile-stat-text">
								 Permission
							</div>
						</div> -->
					</div>
				</div>
				<!-- END PORTLET MAIN -->
				<!-- PORTLET MAIN -->
			</div>
			<!-- END BEGIN PROFILE SIDEBAR -->
			<!-- BEGIN PROFILE CONTENT -->
			<div class="profile-content col-md-6">
				<div class="row">				
					<div class="col-md-12">
					<!-- Alert Section -->
						@if(Session::has('flash_alert_notice'))
					        <div class="alert alert-success alert-dismissable cst-alert cst-normal-alert">
					          	<!-- <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button> -->
					          	<i class="icon fa fa-check"></i>  
					           	{{ Session::get('flash_alert_notice') }} 
					        </div>
					   	@endif
					<!-- End Alert Section -->
						<div class="portlet light bordered min-height-450">
							<div class="portlet-title tabbable-line">
								<!-- <div class="caption">
									<i class="icon-globe theme-font hide"></i>
									<span class="caption-subject font-blue-madison bold uppercase">User Account</span>
								</div> -->
								<div class="actions">
									<a href="" class="btn btn-circle btn-default btn-sm" ng-click="showModelFrm('{{Crypt::encrypt($user->id)}}', tabstate)">
									<i class="fa fa-pencil"></i> Edit </a>
								</div>
								<ul class="nav nav-tabs pull-left">
									<li class="bold" ng-class="tab_active1">
										<a href="#tab_1_1" data-toggle="tab" ng-click="setTabstate('profile')">Personal Info</a>
									</li>
									<li class="bold" ng-class="tab_active2">
										<a href="#tab_1_2" data-toggle="tab" ng-click="setTabstate('address')">Addresses</a>
									</li>
								</ul>
							</div>
							<div class="portlet-body">
								<div class="tab-content">
									<!-- PERSONAL INFO TAB -->
									<div class="tab-pane active" id="tab_1_1">
										<div class="row">
											<div class="col-md-12">
												<div class="form-group">
													
													<div class="row">
														<label class="control-label col-md-5"><strong>Name :</strong></label>
														<div class="col-md-7">
																 {{$user->full_name}}
														</div>
													</div>
													<div class="row">
														<label class="control-label col-md-5"><strong>User Type :</strong></label>
														<div class="col-md-7">
																{{((isset($roles[$user->role_id])) ? $roles[$user->role_id] : 'NA')}}
														</div>
													</div>
													<div class="row">
														<label class="control-label col-md-5"><strong>Username :</strong></label>
														<div class="col-md-7">
																 {{$user->name}}
														</div>
													</div>
													<div class="row">
														<label class="control-label col-md-5"><strong>Password :</strong></label>
														<div class="col-md-7">
															XXXXXXXXX
															<!-- @if(!empty($user->plain_password))
															 {{Crypt::decrypt($user->plain_password)}}
															@endif -->
														</div>
													</div>
													<div class="row">
														<label class="control-label col-md-5"><strong>Email :</strong></label>
														<div class="col-md-7">
																 {{$user->email}}
														</div>
													</div>
													<div class="row">
														<label class="control-label col-md-5"><strong>Contact :</strong></label>
														<div class="col-md-7">
																{{$user->phone}}
														</div>
													</div>
												</div>
											</div>
											<!--/span-->
										</div>
									</div>
									<!-- END PERSONAL INFO TAB -->
									<!-- Profile Dialog -->
									@include('superadmin::user.forms.profile')
									<!-- USER ADDRESS TAB -->
									<div class="tab-pane" id="tab_1_2">
										<div class="row">
											<div class="col-md-12">
												<div class="form-group">
													<h5 class="caption-subject font-blue-steel bold uppercase">Business Details</h5>
													<div class="row">
														<label class="control-label col-md-5"><strong> Address :</strong></label>
														<div class="col-md-7">
																 @if(!empty($user_details)){{$user_details->busi_address1}}@endif
																 @if(!empty($user_details)){{$user_details->busi_address2}}@endif
														</div>
													</div>
													<div class="row">
														<label class="control-label col-md-5"><strong> Postcode :</strong></label>
														<div class="col-md-7">
																@if(!empty($user_details)){{$user_details->busi_post}}@endif
														</div>
													</div>
													<div class="row">
														<label class="control-label col-md-5"><strong> Suburb(City) :</strong></label>
														<div class="col-md-7">
																 @if(!empty($user_details)){{$user_details->busi_suburb}}@endif
														</div>
													</div>
													<div class="row">
														<label class="control-label col-md-5"><strong> Postcode :</strong></label>
														<div class="col-md-7">
																@if(!empty($user_details)){{$user_details->busi_post}}@endif
														</div>
													</div>
													<div class="row">
														<label class="control-label col-md-5"><strong> State :</strong></label>
														<div class="col-md-7">
																@if(!empty($user_details)){{$user_details->busi_state}}@endif
														</div>
													</div>
													<div class="row">
														<label class="control-label col-md-5"><strong> Country :</strong></label>
														<div class="col-md-7">
																@if(!empty($user_details)){{$user_details->busi_country}}@endif
														</div>
													</div>
													<h5 class="caption-subject font-blue-steel bold uppercase">Billing Details</h5>
													<div class="row">
														<label class="control-label col-md-5"><strong> Address :</strong></label>
														<div class="col-md-7">
																 @if(!empty($user_details)){{$user_details->bill_address1}}@endif
																 @if(!empty($user_details)){{$user_details->bill_address2}}@endif
														</div>
													</div>
													<div class="row">
														<label class="control-label col-md-5"><strong> Postcode :</strong></label>
														<div class="col-md-7">
																@if(!empty($user_details)){{$user_details->bill_post}}@endif
														</div>
													</div>
													<div class="row">
														<label class="control-label col-md-5"><strong> Suburb(City) :</strong></label>
														<div class="col-md-7">
																 @if(!empty($user_details)){{$user_details->bill_suburb}}@endif
														</div>
													</div>
													<div class="row">
														<label class="control-label col-md-5"><strong> State :</strong></label>
														<div class="col-md-7">
																@if(!empty($user_details)){{$user_details->bill_state}}@endif
														</div>
													</div>
													<div class="row">
														<label class="control-label col-md-5"><strong> Country :</strong></label>
														<div class="col-md-7">
																@if(!empty($user_details)){{$user_details->bill_country}}@endif
														</div>
													</div>
												</div>
											</div>
											<!--/span-->
										</div>
									</div>
									<!-- END PERSONAL INFO TAB -->
									<!-- Profile Dialog -->
									@include('superadmin::user.forms.address')
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- END PROFILE CONTENT -->
			@include('superadmin::user.forms.companies')
			<div class="col-md-3" ng-init="userCompanyList({{$user->id}})">
				<div class="portlet light bordered tasks-widget bordered">
                    <div class="portlet-title caption font-green-sharp">
                        <div class="caption caption-md">
                            <!-- <i class="icon-screen-desktop font-green-sharp"></i> -->
                            <span class="caption-subject bold font-green-sharp uppercase"> Associated Compnies </span>
                        </div>
                   		<div class="actions">
							<a href="" class="btn btn-circle btn-default btn-sm" ng-click="showModelFrm({{$user->id}} , 'companies')">
							<i class="fa fa-pencil"></i> </a>
						</div>
                    </div>
                    <div class="portlet-body" >
		                <div class="task-content">
		                	
							<div class="scroller portlet-height">
								<!-- START TASK LIST -->                       
		                        <ul class="task-list">
		                            <li class="" ng-repeat="list in uclists">
										<div class="task-title">
											<span class="task-title-sp"> @{{list.name | uppercase }}</span>
										</div>
		                            </li>
		                        </ul>                        
		                    </div>                                           
		                </div>
		            </div>
				</div>
			</div>
		</div>
		<!-- END ROW CONTENT-->
		<!-- BEGIN ROW 2 CONTENT-->
		<div class="row">
		 	<div class="col-lg-12 col-md-12 col-xs-12" ng-init="userProjects({{$user->id}})">
		       <div class="portlet light bordered">
					<div class="portlet-title caption font-green-sharp">
						<div class="caption">
							<i class="icon-microphone font-dark hide"></i>
							<span class="caption-subject bold font-dark uppercase"> Project List </span>
						</div>                       
						<div class="actions">
							<div class="portlet-input input-inline input-small">
								<div class="input-icon text-right">                            
									<a class="btn btn-circle btn-sm btn-default" href="{{url('/')}}/superadmin/add-project"> <i class="fa fa-plus"></i> Add New </a>
								</div>
							</div>
						</div>		               
					</div>
		            <div class="portlet-body">
		                <div class="tab-content">
		                    <div class="row">
		                    	<div class="col-md-12">
									<div class="form-group">
	                                <table class="table" id="user_projects">
										<thead>
											<tr>
												<th> Name </th>													
												<th> Type </th>	
												<th> Author </th>	
												<th width="60"> Action </th>												
											</tr>
										</thead>
									</table>
									</div>
								</div>
		                	</div>		                                                               
		                </div>
		            </div>
		        </div>
		    </div>
		</div>
	</div>
</div>
<!-- END CONTAINER -->
@stop