<?php

namespace App\Helpers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Mail;
use Auth;
use Config;
use View;
use Input;
use session;
use Crypt;
use Hash;
use Menu;
use Html;
use App\User;
use App\Superadmin;
//use App\Models\superadmin\Role;
use App\Models\superadmin\Company;
use App\Models\superadmin\CompanyDetails;
use App\Models\superadmin\Project;
use App\Models\superadmin\Issueto;
use App\Models\superadmin\CompanyUserMapping;
use App\Models\superadmin\CompanyIssuetoMapping;
use App\Models\superadmin\CompanyProjectMapping;
use App\Models\superadmin\CompanyChecklistMapping;
use App\Models\projectConfiguration\Location;
use App\Models\superadmin\UserDetails;
use Kodeine\Acl\Models\Eloquent\Role;
use Carbon\Carbon;

class Helper {

    /**
     * function used to check stock in kit
     *
     * @param = null
     */

    public static function getUserRoles(){
    	$data = array();
    	$roles = Role::where('is_deleted', '=', 0)->where('id', '!=', 0)->select('id','name')->get();
    	foreach($roles as $role){
    		$data[$role->id] = $role->name;
    	}
        asort($data);
    	return $data;
    }

    public static function getAllRoles(){
        $data = array();
        $roles = Role::get();
        foreach($roles as $role){
            $data[$role->id] = $role->name;
        }
        asort($data);
        return $data;
    }

    public static function angularUserRoles(){
        $data = array();
        $roles = Role::select('id','name')->get();
        foreach($roles as $key=> $role){
            $data[$key]['id'] = $role->id;
            $data[$key]['name'] = $role->name;
        }
        asort($data);
        return $data;
    }

    public static function getProjectUserRoles(){
        $data = array();
        $roles = Role::select('id','name')->where('type','!=',1)->get();
        foreach($roles as $key=> $role){
            $data[$key]['id'] = $role->id;
            $data[$key]['name'] = $role->name;
        }
        asort($data);
        return $data;
    }

    public static function getCompanyList(){
        $data = array();
        $comps = Company::where('is_deleted', '=', 0)->select('id','name')->get();
        foreach($comps as $comp){
            $data[$comp->id] = $comp->name;
        }
        asort($data);
        return $data;
    }

    public static function getCompanyUserMapping($user_id){
        $data = array();
        $usr_comps = CompanyUserMapping::select('company_id')->where('user_id', '=', $user_id)->get();
            $data = array();
            foreach($usr_comps as $comp){
                $data[] = $comp->company_id;
            }
        asort($data);
        return $data;
    }

    public static function getCompanyIssuetoMapping($issueto_id){
        $data = array();
        $issueto_comps = CompanyIssuetoMapping::select('company_id')->where('issueto_id', '=', $issueto_id)->get();
            $data = array();
            foreach($issueto_comps as $comp){
                $data[] = $comp->company_id;
            }
        asort($data);
        return $data;
    }

    public static function getCompanyChecklistMapping($checklist_id){
        $data = array();
        $checklist_comps = CompanyChecklistMapping::select('company_id')->where('checklist_id', '=', $checklist_id)->get();
            $data = array();
            foreach($checklist_comps as $comp){
                $data[] = $comp->company_id;
            }
        asort($data);
        return $data;
    }

    public static function getCompanyProjectMapping($project_id){
        $data = array();
        $prj_comps = CompanyProjectMapping::select('company_id')->where('project_id', '=', $project_id)->get();
        $data = array();
        foreach($prj_comps as $comp){
            $data[] = $comp->company_id;
        }
        return $data;
    }

    public static function sendMessage($template,$data){
         Mail::send($template, $data, function($message) use ($data) {
             $message->to($data['email'], $data['name'])->subject
                ($data['subject']);
             $message->from('gendefid@fxbytes.com','GendefId');
        });
        return "1";
    }

    public static function getCompanyUsers($company_id){
        $data = array();
        $comp_users = CompanyUserMapping::select('user_id')->where('company_id', '=', $company_id)->get();
            $data = array();
            foreach($comp_users as $user){
                $data[] = $user->user_id;
            }
        asort($data);
        return $data;
    }

    public static function getCompanyProject($company_id){
        $data = array();
        $prj_comps = CompanyProjectMapping::select('project_id')->where('company_id', '=', $company_id)->get();
            $data = array();
            foreach($prj_comps as $pro){
                $data[] = $pro->project_id;
            }
        asort($data);
        return $data;
    }
    
    public static function getCompanyIssuedTo($company_id){
        $data = array();
        $prj_comps = CompanyIssuetoMapping::select('issueto_id')->where('company_id', '=', $company_id)->get();
            $data = array();
            foreach($prj_comps as $pro){
                $data[] = $pro->project_id;
            }
        asort($data);
        return $data;
    }

    public static function getLoginUserDetails(){
        $data = array();
        if(Auth::guard(null)->check()){
            $id = Auth::user()->id; 
            $admin = User::find($id);  
            $userdetail = $admin->UserDetails; 
        }else{
            $auth = Auth::guard('admin');
            $id = $auth->user()->id;   
            $admin = Superadmin::find($id); 
        }
        #print_r($userdetail); die;
        //$userdetail = $admin-> UserDetails;
        $adminData = array();
        $adminData['username'] = $admin->name;
        $adminData['fullname'] = $admin->full_name;
        $adminData['email'] = $admin->email; 
        $adminData['phone'] = $admin->phone;
        $adminData['logo_image'] = !empty($userdetail[0])?$userdetail[0]->logo_image:(!empty($admin->logo_image)?$admin->logo_image:'');
        //echo "<pre>";print_r($userdetail[0]->logo_image); //die;
        \Session::set('adminData', $adminData);
    }
    
    public static function generatePageTree($datas, $parent = 0, $depth=0){
        if($depth > 100) return ''; // Make sure not to have an endless recursion
        $tree = '<ul class="fa-ul">';
        for($i=0, $ni=count($datas); $i < $ni; $i++){
            if($datas[$i]['parent_id'] == $parent){
                $tree .= '<li data-value="'.$datas[$i]['id'].'"><i class="fa fa-plus ckeckedT" data-id="'.$datas[$i]['id'].'">&nbsp;&nbsp;</i>';
                $tree .= $datas[$i]['name'];
                $tree .= Helper::generatePageTree($datas, $datas[$i]['id'], $depth+1);
                $tree .= '</li>';
            }
        }
        $tree .= '</ul>';
        return $tree;
    }

    public static function getAllLocation(){
        $locations = Location::get();
        exit;
    }

    public static function countryList(){
        $country = array(''=>'Select One','Australia'=>'Australia');
        return $country;
    }

    public static function getCompanyCountbyMonth($company_id){
        
        $comp_users = Self::getCompanyUsers($company_id);  
        $users = User::select('id', 'created_at')
            ->where('is_deleted','=','0')
            ->whereIn('id', $comp_users)
            ->get()
            ->groupBy(function($date) {
                //return Carbon::parse($date->created_at)->format('Y'); // grouping by years
                return Carbon::parse($date->created_at)->format('m'); // grouping by months
            });

            $usermcount = [];
            $userArr = [];
            
            foreach ($users as $key => $value) {
                $usermcount[(int)$key] = count($value);
            }
            
            for($i = 1; $i <= 12; $i++){
                if(!empty($usermcount[$i])){
                    $userArr[$i] = $usermcount[$i];    
                }else{
                    $userArr[$i] = 0;    
                }
            }
            
        $comp_projects = Self::getCompanyProject($company_id); 
        $projects = Project::select('id', 'created_at')
            ->where('is_deleted','=','0')
            ->whereIn('id', $comp_projects)
            ->get()
            ->groupBy(function($date) {
                //return Carbon::parse($date->created_at)->format('Y'); // grouping by years
                return Carbon::parse($date->created_at)->format('m'); // grouping by months
            });

            $projectmcount = [];
            $projectArr = [];
            
            foreach ($projects as $key => $value) {
                $projectmcount[(int)$key] = count($value);
            }
            
            for($i = 1; $i <= 12; $i++){
                if(!empty($projectmcount[$i])){
                    $projectArr[$i] = $projectmcount[$i];    
                }else{
                    $projectArr[$i] = 0;    
                }
            }

        $comp_issuedtos = Self::getCompanyIssuedTo($company_id);
        $issuetos = Issueto::select('id', 'created_at')
            ->where('is_deleted','=','0')
            ->whereIn('id', $comp_issuedtos)
            ->get()
            ->groupBy(function($date) {
                //return Carbon::parse($date->created_at)->format('Y'); // grouping by years
                return Carbon::parse($date->created_at)->format('m'); // grouping by months
            });

            $issuetomcount = [];
            $issuetoArr = [];
            
            foreach ($issuetos as $key => $value) {
                $issuetomcount[(int)$key] = count($value);
            }
            
            for($i = 1; $i <= 12; $i++){
                if(!empty($issuetomcount[$i])){
                    $issuetoArr[$i] = $issuetomcount[$i];    
                }else{
                    $issuetoArr[$i] = 0;    
                }
            }
        
        $totalArr = array($userArr,$projectArr,$issuetoArr);
        // echo "<pre>";
        // print_r($userArr);die;

        return $totalArr;
            
    }

    public static function super_unique($array)
    {
        $result = array_map("unserialize", array_unique(array_map("serialize", $array)));

        foreach ($result as $key => $value)
        {
            if ( is_array($value) )
            {
              $result[$key] = Self::super_unique($value);
            }
        }
        return $result;
    }

    public static function buildTree(array $elements, $parentId = 0) {
        $branch = array();
        $result = array();
        foreach ($elements as $element) {
            if ($element['parent_id'] == $parentId) {
                $result[] = $element['id'];
                $children = Self::buildTree($elements, $element['id']);
                if ($children) {
                    foreach($children as $chRows) {
                        $result[] = $chRows;
                    }
                    $element['children'] = $children;
                }
                $branch[] = $element;
            }
        }
        //return $result; //For ids
        return $branch; //For parent/child relation tree.
    }

    public static function findRoleName($id){     
        $data = array();
        $roles = Role::where('id','=',$id)->select('id','name')->get();
        return $roles;
    }

    public static function getCompanyDetails($cmp_id){
        $comps = Company::where('is_deleted', '=', 0)->where('id', '=', $cmp_id)->first();
        $cmp_details = $comps->CompanyDetails;  
        $data['name'] = $comps->name;
        $data['logo'] = $cmp_details[0]->logo_image;
        \Session::set('company_details', $data);
    }

    /**
    * Fetch column value of any model
    * accept 3 parameter, select comun name, primary key of table, Object of model
    * return Collaction object
    */
    public static function getColumnValById($key,$id,$model){
        $result = $model->select($key)->where('is_deleted',0)->where('id',$id)->first();
        return $result;
    }

}