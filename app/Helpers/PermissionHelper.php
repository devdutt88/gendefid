<?php

namespace App\Helpers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Mail;
use Auth;
use Config;
use View;
use Input;
use session;
use Crypt;
use Hash;
use Menu;
use Html;
use App\User;
use App\Superadmin;
use App\Models\superadmin\ProjectUserRole;
use Kodeine\Acl\Models\Eloquent\Role;
use Carbon\Carbon;

class PermissionHelper {    

    public static function hasProjectRole($user_id,$project_id) {        
        $project_id = Crypt::decrypt($project_id);
        $prj = ProjectUserRole::select('role_id')->where('user_id',$user_id)->where('project_id',$project_id)->first();
        $role = Role::all();
        $r_id = array();
        foreach ($role as $key => $value) {
        	$r_id[] = $value->id;
        }
        if(in_array($prj->role_id, $r_id)) {        	
        	return true;
        }else {
        	return false;
        }
    }

    public static function getProjectPermission($user_id,$project_id){
    	$project_id = Crypt::decrypt($project_id);
        $prj = ProjectUserRole::select('role_id')->where('user_id',$user_id)->where('project_id',$project_id)->first();
        $roleObj = Role::where('id','=',$prj->role_id)->first();
        $permissions = $roleObj->getPermissions();
        echo "<pre>";
        print_r($permissions);
    }
}