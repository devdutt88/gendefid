<?php

namespace App\Helpers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Mail;
use Auth;
use Config;
use View;
use Input;
use session;
use Crypt;
use Hash;
use Menu;
use Html;
use App\User;
use App\Superadmin;
use App\Models\superadmin\Role;
use App\Models\superadmin\Authentication;
// use App\Models\superadmin\Company;
// use App\Models\superadmin\Project;
// use App\Models\superadmin\Issueto;
// use App\Models\superadmin\CompanyUserMapping;
// use App\Models\superadmin\CompanyIssuetoMapping;
// use App\Models\superadmin\CompanyProjectMapping;
// use App\Models\superadmin\CompanyChecklistMapping;
// use App\Models\projectConfiguration\Location;
use Carbon\Carbon;

class TreeHelper {

    /**
     * function used to check stock in kit
     *
     * @param = elements, parentId
     */

    public static function buildTree(array $elements, $parentId = 0) {
        $branch = array();
        $result = array();
        foreach ($elements as $element) {
            if ($element['module_id'] == $parentId) {
                $result[] = $element['id'];
                $children = Self::buildTree($elements, $element['id']);
                if ($children) {
                    foreach($children as $chRows) {
                        $result[] = $chRows;
                    }
                    $element['nodes'] = $children;
                }
                $branch[] = $element;
            }
        }
        return $result; //For locations ids
        //return $branch; //For parent/child relation tree.
    }

}