<?php

namespace App\Http\Controllers\Auth;

use authenticate;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Helpers as Helper;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\MessageBag;
use HTML;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = 'superadmin/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {                   
        $this->middleware(['guest'], ['except' => 'logout']);
    }

    public function username()
    {   
        return 'name';
    }

    protected function authenticated($request, $user)
    {                
        if($user->role_id === 0) {
            Helper::getLoginUserDetails();            
            return redirect()->intended('superadmin/');
        }
        if($user->role_id !== 0) {
            Helper::getLoginUserDetails();            
            return redirect()->intended('frontend/');
        }       
        return redirect()->intended('/');
    }
    
    public function adminLogin(){
        $input = Input::all();        
        if(count($input) > 0){
            $auth = auth()->guard('admin');
            $credentials = [
                'name' =>  $input['name'],
                'password' =>  $input['password'],
            ];
            if ($auth->attempt($credentials)) {               
                return redirect()->intended('superadmin');
            } else {
                $err = "These credentials do not match our records.";
                $errors = new MessageBag(['error' => [$err]]);
                return \Redirect::back()->withErrors($errors);
            }
        } else {
            return view('admin.login');
        }
    }

    public function getBackLogin(){
        $err = "These credentials do not match our records.";
        $errors = new MessageBag(['name' => [$err]]);
        return redirect('admin/login')->withErrors($errors);
        die;
    }

}
