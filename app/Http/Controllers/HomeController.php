<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Mail;
use App\User;
use App\Superadmin;
use Helpers as Helper;
use Crypt;

use App\Http\Requests\Change_Password;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Redirect::to('/login');
    }

    public function sendResetLink(){
        $input = Input::except('_token');
        $email = $input['email'];
        $user = User::where('email','=',$email)->get();        
         if(!empty($user[0]))
         {
            $key = config('app.KEY');
            $link = url('/')."/resetLink/".base64_encode(base64_encode($email.$key."####".$user[0]->id));
            $data = array('name'=>$user[0]->name,'link'=>$link,'email'=>$email);
            Mail::send('email.reset', $data, function($message) use ($data) {
                 $message->to($data['email'], 'GendefID2')->subject
                    ('GendefId Reset Password');
                 $message->from('gendefid@fxbytes.com','GendefId');
            });
            echo json_encode(array('msg'=>"Reset link has been sent to your register email-id,please check and reset password.",'status'=>0));
        }
        else{            
            echo json_encode(array('msg'=>"Email-id does not exist,please try again.",'status'=>1));
        }
        die;
    }

    public function sendResetLinkAdmin(){
        $input = Input::except('_token');
        $email = $input['email'];
        $admin = Superadmin::where('email','=',$email)->get();        
         if(!empty($admin[0]))
         {
            $key = config('app.KEY');
            $link = url('/')."/resetAdminLink/".Crypt::encrypt(base64_encode(base64_encode($email.$key."####".$admin[0]->id)));
            $data = array('name'=>$admin[0]->name,'link'=>$link,'email'=>$email);
            Mail::send('email.reset', $data, function($message) use ($data) {
                 $message->to($data['email'], 'GendefID2')->subject
                    ('GendefId Reset Password');
                 $message->from('ismael.ansari@fxbytes.com','GendefId');
            });
            echo json_encode(array('msg'=>"Reset link has been sent to your register email-id,please check and reset password.",'status'=>0));
        }
        else{            
            echo json_encode(array('msg'=>"Email-id does not exist,please try again.",'status'=>1));
        }
        die;
    }



    public function resetLink($email){
        if(!Auth::user())
        {
            $email = base64_decode(base64_decode($email));
            $email = explode("####", $email);
            $token = config('app.KEY');
            if("####".$email[1] == $token){
                $email_id = $email[0];
                $id = $email[2];
            }else{
                $data['msg'] = "Invalid Token.";
            }        
            return view("auth.passwords.reset",compact('token','email_id','id'));
        }
        else{
            return Redirect::to('/');
        }        
    }

    public function resetAdminLink($email){
        if(!Auth::user())
        {
            $email = Crypt::decrypt($email);
            $email = base64_decode(base64_decode($email));
            $email = explode("####", $email);
            $token = config('app.KEY');
            if("####".$email[1] == $token){
                $email_id = $email[0];
                $id = $email[2];
            }else{
                $data['msg'] = "Invalid Token.";
            }        
            return view("auth.passwords.resetAdmin",compact('token','email_id','id'));
        }
        else{
            return Redirect::to('/');
        }        
    }

    public function resetPassword(Change_Password $Request){
        $input = Input::except('_token');
        $user = User::where('id','=',$input['hid'])->where('email','=',$input['email'])->first();
        $user->password = bcrypt($input['password']);
        $user->plain_password = Crypt::encrypt($input['password']);
        $user->save();
        return redirect('login')->with('flash_alert_notice', 'Your password has been changed successfully,please login again!');
    }  

    public function resetAdminPassword(Change_Password $Request){
        $input = Input::except('_token');        
        $user = Superadmin::where('id','=',$input['hid'])->where('email','=',$input['email'])->first();
        $user->password = bcrypt($input['password']);        
        $user->save();
        return redirect('admin/login')->with('flash_alert_notice', 'Your password has been changed successfully,please login again!');
    }  
}
