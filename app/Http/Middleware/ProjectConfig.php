<?php

namespace App\Http\Middleware;
use Closure;
use Crypt;

class ProjectConfig
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {      
        $sess_set = \Session::get('projectConfig');
        if($request->segment(3) == "projectConfigId"){   
            $id = Crypt::decrypt($request->segment(4));
            \Session::set('projectConfig', $id);         
            return redirect('superadmin/projectConfiguration/location');
        }
        else if(empty($request->segment(3))){            
            \Session::set('projectConfig', 0);            
        }
        else if($sess_set == 0){
            return redirect('superadmin/');   
        }
        return $next($request);
    }
}
