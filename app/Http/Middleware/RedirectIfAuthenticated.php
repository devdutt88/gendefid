<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Helpers as Helper;
use Illuminate\Support\MessageBag;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if(filter_var($request->get('name'), FILTER_VALIDATE_EMAIL)) {        
            $input = $request->all();
            $auth = auth()->guard(null);
            $credentials = [
                'email' =>  $input['name'],
                'password' =>  $input['password'],
            ];
            if ($auth->attempt($credentials)) {
                $admin = Helper::getLoginUserDetails();
            }
        }
        if (Auth::guard('admin')->check()) {                           
            if(Auth::guard('admin')->user()->role_id == 0)
            {
                $admin = Helper::getLoginUserDetails();
                return redirect('/superadmin');
            }
            else{                
                return redirect('admin/login');   
            }      
        } 
        else if (Auth::guard(null)->check()) {

                $admin = Helper::getLoginUserDetails();
                return redirect('/dashboard');   
            }             
             
        return $next($request);
    }
}
