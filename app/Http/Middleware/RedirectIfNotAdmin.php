<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Helpers as Helper;

class RedirectIfNotAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = 'admin')
    {                                
        if(Auth::guard(null)->check()){
            if ($request->segment(1) == "superadmin")
            {
                return response()->view('errors.404', [ ], 404);
            }            
        }
        else if(!Auth::guard('admin')->check()){                 
            return redirect('/admin/login');             
        }     
        $admin = Helper::getLoginUserDetails();
        return $next($request);
    }
}
