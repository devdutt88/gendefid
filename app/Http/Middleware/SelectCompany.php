<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\superadmin\CompanyUserMapping;
use Helpers as Helper;
use App\Models\superadmin\Company;


class SelectCompany
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $company = $request->session()->get('company');
        if(!$company && $request->segment(1) != 'setCompany'){            
            $user_id = \Auth::user()->id;
            $user_comps = Helper::getCompanyUserMapping($user_id);
            $user_company = Company::select([ 'id','name'])->where('is_deleted', '=', 0)->whereIn('id', $user_comps)->get();
            $arr = array();
            foreach ($user_company as $key => $value) {
                $arr[$value->id] = $value->name;
            }             
            \Session::set('company_count', count($arr));
            if(count($arr) == 1){
                $id = array_keys($arr);   
                Helper::getCompanyDetails($id[0]);                
                \Session::set('company', $id[0]);
            }else{
                $arr = json_encode($arr);
                return response()->view('frontend::company.index',compact('arr')); 
            }            
        }
        return $next($request);
    }
}
