<?php

namespace App\Http\Middleware;

use Closure;

class SuperadminCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        \Session::set('projectConfig', 0);
        return $next($request);
    }
}
