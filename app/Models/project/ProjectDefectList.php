<?php

namespace App\Models\project;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProjectDefectList extends Model
{
	use SoftDeletes;
    protected $table = 'project_defect_list';
}
