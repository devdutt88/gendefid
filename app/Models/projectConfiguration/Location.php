<?php

namespace App\Models\projectConfiguration;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    //
    protected $table = 'project_location';
    protected $fillable = ['label','project_id','parent_id','company_id','order_id','created_at','created_by','modified_by'];
    public $timestamps = false;

    public function childs() {
       return $this->hasMany('App\Models\projectConfiguration\Location','parent_id','id') ;
   }

}
