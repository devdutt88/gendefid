<?php

namespace App\Models\projectConfiguration;

use Illuminate\Database\Eloquent\Model;

class ProjectChecklist extends Model
{
    //
    protected $table = 'project_checklist';
    protected $fillable = ['id','name'];
    public $timestamps = false;

}
