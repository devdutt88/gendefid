<?php

namespace App\Models\projectConfiguration;

use Illuminate\Database\Eloquent\Model;

class ProjectChecklistTasks extends Model
{
    //
    protected $table = 'project_checklist_task';
    protected $fillable = ['id','checklist_id','name','status'];
    public $timestamps = false;

}
