<?php

namespace App\Models\projectConfiguration;

use Illuminate\Database\Eloquent\Model;

class ProjectIssueto extends Model
{
    //
    protected $table = 'project_issueto';
    protected $fillable = ['id','name'];
    public $timestamps = false;

}
