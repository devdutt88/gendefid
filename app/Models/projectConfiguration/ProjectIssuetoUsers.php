<?php

namespace App\Models\projectConfiguration;

use Illuminate\Database\Eloquent\Model;

class ProjectIssuetoUsers extends Model
{
    //
    protected $table = 'project_issueto_users';
    protected $fillable = ['id','issuto_id','name','tags','trade'];
    public $timestamps = false;

}
