<?php

namespace App\Models\superadmin;
 
use Illuminate\Database\Eloquent\Model;

class Authentication extends Model
{
    //
    protected $table = 'authentication';
    protected $fillable = ['name','type','module_id','description','created_at','created_by'];    
 	public $timestamps = false;
 	
}
