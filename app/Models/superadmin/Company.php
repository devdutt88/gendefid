<?php

namespace App\Models\superadmin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Company extends Model
{
    use SoftDeletes;
    
    protected $table = 'company';
    protected $fillable = ['name','code','registration_number','email','phone','description','created_at','modified_by'];
    public $timestamps = false;

 	public function CompanyDetails()
    {
        return $this->hasMany('App\Models\superadmin\CompanyDetails');        
    }
}
