<?php

namespace App\Models\superadmin;

use Illuminate\Database\Eloquent\Model;

class CompanyChecklistMapping extends Model
{
    //
    protected $table = 'company_checklist_mapping';
    protected $fillable = ['checklist_id', 'company_id'];
    public $timestamps = false;
}
