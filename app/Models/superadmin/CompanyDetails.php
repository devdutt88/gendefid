<?php

namespace App\Models\superadmin;

use Illuminate\Database\Eloquent\Model;

class CompanyDetails extends Model
{
    //
    protected $table = 'company_details';
    protected $fillable = ['company_id','logo_image','busi_address1','busi_address2','busi_suburb','busi_state','busi_post','busi_country','bill_address1','bill_address2','bill_suburb','bill_state','bill_post','bill_country','modified_by'];
    public $timestamps = false;    
}