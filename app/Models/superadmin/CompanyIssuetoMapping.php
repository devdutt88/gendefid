<?php

namespace App\Models\superadmin;

use Illuminate\Database\Eloquent\Model;

class CompanyIssuetoMapping extends Model
{
    //
    protected $table = 'company_issueto_mapping';
    protected $fillable = ['issueto_id'];
    public $timestamps = false;
}
