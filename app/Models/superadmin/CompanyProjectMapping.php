<?php

namespace App\Models\superadmin;

use Illuminate\Database\Eloquent\Model;

class CompanyProjectMapping extends Model
{
    //
    protected $table = 'company_project_mapping';
    protected $fillable = ['project_id','company_id'];
    public $timestamps = false;

    public function CompanyDetails()
    {
        return $this->hasMany('App\Models\superadmin\Company');        
    }
}
