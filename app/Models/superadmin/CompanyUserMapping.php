<?php

namespace App\Models\superadmin;

use Illuminate\Database\Eloquent\Model;

class CompanyUserMapping extends Model
{
    //
    protected $table = 'company_user_mapping';
    protected $fillable = ['user_id'];
    public $timestamps = false;

    public function CompanyUser()
    {
        return $this->hasMany('App\Models\superadmin\Company');        
    }

    
}
