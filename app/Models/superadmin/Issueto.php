<?php

namespace App\Models\superadmin;

use Illuminate\Database\Eloquent\Model;

class Issueto extends Model
{
    //
    protected $table = 'issueTo';
    protected $fillable = ['id','name','description'];
    public $timestamps = false;

    public function issueToCompany()
    {
        return $this->hasMany('App\Models\superadmin\ProjectIssueto','company_issueto_id','id');
    }
}
