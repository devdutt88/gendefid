<?php

namespace App\Models\superadmin;

use Illuminate\Database\Eloquent\Model;

class IssuetoUsers extends Model
{
    //
    protected $table = 'issueTo_users';
    protected $fillable = ['id','issueto_id','name','activity','phone','email'];
    public $timestamps = false;

}
