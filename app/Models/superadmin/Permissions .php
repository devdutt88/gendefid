<?php

namespace App\Models\superadmin;
 
use Illuminate\Database\Eloquent\Model;

class Permissions extends Model
{
    //
    protected $table = 'permissions';
    protected $fillable = ['name','slug','description','created_at'];    
 	public $timestamps = false;
 	
}
