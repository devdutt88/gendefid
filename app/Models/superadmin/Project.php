<?php

namespace App\Models\superadmin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Project extends Model
{
    //
    use SoftDeletes;

    protected $table = 'project';
    protected $fillable = ['name', 'code', 'type','description', 'address1', 'suburb', 'postal_code ','state', 'country',];
    public $timestamps = false;
    protected $dates = ['deleted_at'];
    
    public function projectCompany()
    {
        return $this->hasMany('App\Models\superadmin\CompanyProjectMapping','project_id','id');        
    }

}
