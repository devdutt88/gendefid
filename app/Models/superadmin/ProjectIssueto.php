<?php

namespace App\Models\superadmin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProjectIssueto extends Model
{
    //
    use SoftDeletes;
    
    protected $table = 'project_issueto';
    protected $fillable = ['id','name'];    
    public $timestamps = false;
    protected $dates = ['deleted_at'];

}
