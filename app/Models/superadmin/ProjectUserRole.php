<?php

namespace App\Models\superadmin;

use Illuminate\Database\Eloquent\Model;

class ProjectUserRole extends Model
{
    //
    protected $table = 'project_user_role';
    protected $fillable = ['project_id', 'user_id'];
    public $timestamps = false;

    public function ProjectDetails()
    {
        return $this->hasMany('App\Models\superadmin\Project','id','project_id');
    }

    public function userDetails()
    {
        return $this->hasMany('App\User','id','created_by');
    }

    public function roleDetails()
    {
        return $this->hasMany('App\Models\superadmin\Role','id','role_id');
    }
}
