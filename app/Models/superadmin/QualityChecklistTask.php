<?php

namespace App\Models\superadmin;

use Illuminate\Database\Eloquent\Model;

class QualityChecklistTask extends Model
{
    //
    protected $table = 'quality_checklist_task';
    protected $fillable = ['id','checklist_id','name','status','comment','email'];
    public $timestamps = false;

}
