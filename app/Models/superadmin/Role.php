<?php

namespace App\Models\superadmin;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    //
    protected $table = 'roles';
    protected $fillable = ['name','slug','description','created_at','created_by','updated_at'];
    public $timestamps = false;    

    public function roleAuthDetails()
    {
        return $this->hasMany('App\Models\superadmin\RoleAuthMap');        
    }
}