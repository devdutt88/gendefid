<?php

namespace App\Models\superadmin;

use Illuminate\Database\Eloquent\Model;

class RoleAuthMap extends Model
{
    //
    protected $table = 'role_authentication_mapping';
    protected $fillable = ['role_id','authorisation_id','status'];
    public $timestamps = false;

    public function rolePermissions()
    {
        return $this->hasMany('App\Models\superadmin\Authentication', 'id', 'authorisation_id');    
    }
        
}
