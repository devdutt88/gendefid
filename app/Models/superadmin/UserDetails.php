<?php

namespace App\Models\superadmin;

use Illuminate\Database\Eloquent\Model;

class UserDetails extends Model
{
    protected $table = 'user_details';
    protected $fillable = ['user_id'];
    //$this->timestamp('created_at')->nullable();
    //const UPDATED_AT = 'modified_at';
    public $timestamps = false;
}
