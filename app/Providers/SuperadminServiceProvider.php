<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class SuperadminServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        $this->app['auth']->extend('session',function()
        {
            return new CustomUserProvider(new Superadmin);
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
