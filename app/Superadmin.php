<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use App\Scopes\AuthorizedScope;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Superadmin extends Authenticatable
{
    //table name
    protected $table = 'superadmin';
}
