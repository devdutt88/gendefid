<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Kodeine\Acl\Traits\HasRole;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use Notifiable;
    use HasRole;
    use SoftDeletes;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    public $timestamps = false;

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    public function UserDetails()
    {
        return $this->hasMany('App\Models\superadmin\UserDetails');
    }
    
    public function CompanyUserMapping()
    {
        return $this->hasMany('App\Models\superadmin\CompanyUserMapping','user_id','id');
    }
    
    public function userRole()
    {
        return $this->hasMany('App\Models\superadmin\Role');
    }
    
    public function userProjectRole()
    {
        return $this->hasMany('App\Models\superadmin\ProjectUserRole','user_id','id');
    }
}
