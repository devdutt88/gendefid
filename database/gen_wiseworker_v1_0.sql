-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 31, 2017 at 05:52 PM
-- Server version: 5.7.18-0ubuntu0.16.04.1
-- PHP Version: 7.0.18-1+deb.sury.org~xenial+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gen_wiseworker_v1.0`
--

-- --------------------------------------------------------

--
-- Table structure for table `address`
--

CREATE TABLE `address` (
  `id` int(11) NOT NULL,
  `type_id` varchar(32) NOT NULL,
  `address1` varchar(128) NOT NULL,
  `address2` varchar(128) DEFAULT NULL,
  `suburb` varchar(64) NOT NULL,
  `state` varchar(32) NOT NULL,
  `post_code` varchar(16) NOT NULL,
  `geo_code` varchar(16) DEFAULT NULL,
  `country_code` int(11) NOT NULL,
  `row_status` int(11) NOT NULL DEFAULT '0',
  `modified_by` int(11) DEFAULT NULL,
  `modified_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `address`
--

INSERT INTO `address` (`id`, `type_id`, `address1`, `address2`, `suburb`, `state`, `post_code`, `geo_code`, `country_code`, `row_status`, `modified_by`, `modified_on`) VALUES
(0, 'postal', '4 Grant Street', NULL, 'Fitzroy North', '303', '3068', NULL, 61, 0, 0, '2016-05-18 11:18:48'),
(1, 'site', 'test 1', '', 'san jose', '17', '1234', '321564', 82, 0, 0, '2016-05-24 23:42:39'),
(2, 'billing', 'address 1 p', 'address 2 p', 'sub p', '10', '452022', '452022', 82, 0, 0, '2016-05-25 04:08:54'),
(3, 'street', '4 Grant St.', 'North Fitzroy', 'Melbourne', '12', '3068', 'EF765CD760', 82, 0, 0, '2016-05-25 22:39:42'),
(4, 'street', 'test', 'test', 'test', '10', 'test', 'test', 82, 0, 0, '2016-05-26 12:41:27'),
(5, 'street', 'test address 1', '', 'sub rub', '13', '14256', '', 82, 0, 0, '2016-05-31 11:20:08'),
(6, 'street', 'test address 1', '', 'sub rub', '12', '14256', '', 82, 0, 0, '2016-07-28 13:00:31'),
(7, 'street', 'test address 1', '', 'sub rub', '13', '14256', '14256', 82, 0, 0, '2016-06-08 08:44:18'),
(8, 'street', 'test address 1', '', 'sub rub', '13', '14256', '14256', 82, 0, 0, '2016-06-08 08:48:21'),
(9, 'street', '4 Grant St', '', 'Fitzroy North', '12', '3068', '', 82, 0, 0, '2016-07-26 22:14:13'),
(10, 'street', 'test', 'test', 'test', '17', 'test', 'test', 82, 0, 0, '2016-07-28 16:34:56'),
(11, 'street', '130/Block C', 'SOUTH lane street', '123', '10', '456', '789', 82, 0, 0, '2016-07-29 06:57:40'),
(12, 'street', '1234 Bourke St', '', 'Melbourne', '12', '3000', '', 82, 0, 0, '2016-08-03 23:11:02'),
(13, 'street', '398 Elizabeth St', '', 'Melbourne', '12', '3000', '', 82, 0, 0, '2016-08-09 07:47:12');

-- --------------------------------------------------------

--
-- Table structure for table `authorisation`
--

CREATE TABLE `authorisation` (
  `id` int(11) NOT NULL,
  `code` varchar(32) NOT NULL,
  `description` varchar(128) NOT NULL,
  `row_status` varchar(32) NOT NULL DEFAULT 'active',
  `modified_by` int(11) DEFAULT NULL,
  `modified_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `authorisation`
--

INSERT INTO `authorisation` (`id`, `code`, `description`, `row_status`, `modified_by`, `modified_on`) VALUES
(1, 'iPad_add_inspection             ', 'add some description here', 'active', NULL, '2016-05-18 11:18:47'),
(2, 'iPad_close_inspection           ', 'add some description here', 'active', NULL, '2016-05-18 11:18:47'),
(3, 'iPad_delete_inspection          ', 'add some description here', 'active', NULL, '2016-05-18 11:18:47'),
(4, 'iPad_edit_inspection            ', 'add some description here', 'active', NULL, '2016-05-18 11:18:47'),
(5, 'iPad_my_projects                ', 'add some description here', 'active', NULL, '2016-05-18 11:18:47'),
(6, 'iPad_progress_monitoring        ', 'add some description here', 'active', NULL, '2016-05-18 11:18:47'),
(7, 'iPad_quality_control            ', 'add some description here', 'active', NULL, '2016-05-18 11:18:47'),
(8, 'iPad_reports                    ', 'add some description here', 'active', NULL, '2016-05-18 11:18:47'),
(9, 'iPad_settings                   ', 'add some description here', 'active', NULL, '2016-05-18 11:18:47'),
(10, 'iPad_sign_off_image             ', 'add some description here', 'active', NULL, '2016-05-18 11:18:47'),
(11, 'iPhone_add_inspection           ', 'add some description here', 'active', NULL, '2016-05-18 11:18:47'),
(12, 'iPhone_close_inspection         ', 'add some description here', 'active', NULL, '2016-05-18 11:18:47'),
(13, 'iPhone_delete_inspection        ', 'add some description here', 'active', NULL, '2016-05-18 11:18:47'),
(14, 'iPhone_edit_inspection          ', 'add some description here', 'active', NULL, '2016-05-18 11:18:47'),
(15, 'iPhone_edit_inspection_partial  ', 'add some description here', 'active', NULL, '2016-05-18 11:18:47'),
(16, 'iPhone_my_projects              ', 'add some description here', 'active', NULL, '2016-05-18 11:18:47'),
(17, 'iPhone_progress_monitoring      ', 'add some description here', 'active', NULL, '2016-05-18 11:18:47'),
(18, 'iPhone_quality_control          ', 'add some description here', 'active', NULL, '2016-05-18 11:18:47'),
(19, 'iPhone_reports                  ', 'add some description here', 'active', NULL, '2016-05-18 11:18:47'),
(20, 'iPhone_settings                 ', 'add some description here', 'active', NULL, '2016-05-18 11:18:47'),
(21, 'iPhone_sign_off_image           ', 'add some description here', 'active', NULL, '2016-05-18 11:18:47'),
(22, 'location_level_sync_pemission   ', 'add some description here', 'active', NULL, '2016-05-18 11:18:47'),
(23, 'set_user_permission             ', 'add some description here', 'active', NULL, '2016-05-18 11:18:47'),
(24, 'web_add_project                 ', 'add some description here', 'active', NULL, '2016-05-18 11:18:47'),
(25, 'web_archive_permission          ', 'add some description here', 'active', NULL, '2016-05-18 11:18:47'),
(26, 'web_checklist                   ', 'add some description here', 'active', NULL, '2016-05-18 11:18:47'),
(27, 'web_close_inspection            ', 'add some description here', 'active', NULL, '2016-05-18 11:18:47'),
(28, 'web_delete_inspection           ', 'add some description here', 'active', NULL, '2016-05-18 11:18:47'),
(29, 'web_delete_project              ', 'add some description here', 'active', NULL, '2016-05-18 11:18:47'),
(30, 'web_drawingManagement_permission', 'add some description here', 'active', NULL, '2016-05-18 11:18:47'),
(31, 'web_edit_inspection             ', 'add some description here', 'active', NULL, '2016-05-18 11:18:47'),
(32, 'web_edit_profile                ', 'add some description here', 'active', NULL, '2016-05-18 11:18:47'),
(33, 'web_edit_project                ', 'add some description here', 'active', NULL, '2016-05-18 11:18:47'),
(34, 'web_location_level_pemission    ', 'add some description here', 'active', NULL, '2016-05-18 11:18:47'),
(35, 'web_menu_checklist              ', 'add some description here', 'active', NULL, '2016-05-18 11:18:47'),
(36, 'web_menu_health_checkup         ', 'add some description here', 'active', NULL, '2016-05-18 11:18:47'),
(37, 'web_menu_my_profile             ', 'add some description here', 'active', NULL, '2016-05-18 11:18:47'),
(38, 'web_menu_permission             ', 'add some description here', 'active', NULL, '2016-05-18 11:18:47'),
(39, 'web_menu_progress_monitoring    ', 'add some description here', 'active', NULL, '2016-05-18 11:18:47'),
(40, 'web_menu_projects               ', 'add some description here', 'active', NULL, '2016-05-18 11:18:47'),
(41, 'web_menu_qa_task                ', 'add some description here', 'active', NULL, '2016-05-18 11:18:47'),
(42, 'web_menu_quality_control        ', 'add some description here', 'active', NULL, '2016-05-18 11:18:47'),
(43, 'web_menu_reports                ', 'add some description here', 'active', NULL, '2016-05-18 11:18:47'),
(44, 'web_project_configuration       ', 'add some description here', 'active', NULL, '2016-05-18 11:18:47'),
(45, 'web_report_checklist            ', 'add some description here', 'active', NULL, '2016-05-18 11:18:47'),
(46, 'web_report_csv                  ', 'add some description here', 'active', NULL, '2016-05-18 11:18:47'),
(47, 'web_report_detail_report        ', 'add some description here', 'active', NULL, '2016-05-18 11:18:47'),
(48, 'web_report_door_sheet           ', 'add some description here', 'active', NULL, '2016-05-18 11:18:47'),
(49, 'web_report_executive_report     ', 'add some description here', 'active', NULL, '2016-05-18 11:18:47'),
(50, 'web_report_non_conformance      ', 'add some description here', 'active', NULL, '2016-05-18 11:18:47'),
(51, 'web_report_onsite               ', 'add some description here', 'active', NULL, '2016-05-18 11:18:47'),
(52, 'web_report_progress_monitoring  ', 'add some description here', 'active', NULL, '2016-05-18 11:18:47'),
(53, 'web_report_qa_status            ', 'add some description here', 'active', NULL, '2016-05-18 11:18:47'),
(54, 'web_report_qa_task              ', 'add some description here', 'active', NULL, '2016-05-18 11:18:47'),
(55, 'web_report_qa_wallchart         ', 'add some description here', 'active', NULL, '2016-05-18 11:18:47'),
(56, 'web_report_quality_assuarance   ', 'add some description here', 'active', NULL, '2016-05-18 11:18:47'),
(57, 'web_report_quality_control      ', 'add some description here', 'active', NULL, '2016-05-18 11:18:47'),
(58, 'web_report_sub_contractor_report', 'add some description here', 'active', NULL, '2016-05-18 11:18:47'),
(59, 'web_report_summay_with_images   ', 'add some description here', 'active', NULL, '2016-05-18 11:18:47'),
(60, 'web_report_summay_with_out_image', 'add some description here', 'active', NULL, '2016-05-18 11:18:47'),
(61, 'web_report_wall_chart           ', 'add some description here', 'active', NULL, '2016-05-18 11:18:47'),
(62, 'web_assign_user_to_project', 'add some description here', 'active', NULL, '2016-05-18 11:18:47');

-- --------------------------------------------------------

--
-- Table structure for table `client_properties`
--

CREATE TABLE `client_properties` (
  `id` int(11) NOT NULL,
  `company_id` int(11) DEFAULT NULL,
  `property_key` varchar(256) NOT NULL,
  `property_value` varchar(512) NOT NULL,
  `row_status` int(11) NOT NULL DEFAULT '0',
  `modified_by` int(11) DEFAULT '0',
  `modified_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `client_properties`
--

INSERT INTO `client_properties` (`id`, `company_id`, `property_key`, `property_value`, `row_status`, `modified_by`, `modified_on`) VALUES
(1, 0, 'company_logo', '//wiseworking.com.au/wp-content/uploads/WW-Logo-Header-Desktop.png', 0, 0, '2016-05-18 11:18:49'),
(2, 1, 'company_logo', 'http://wiseworking.com.au/wp-content/uploads/WW-Logo-Header-Desktop.png', 0, 0, '2016-06-03 11:57:37'),
(3, 2, 'company_logo', 'http:', 0, 0, '2016-06-05 23:34:18'),
(7, NULL, 'login_attempts', '144.139.231.90', 0, 0, '2016-08-29 02:53:57'),
(9, NULL, 'login_attempts', '124.188.18.207', 0, 0, '2016-07-20 19:49:44'),
(10, NULL, 'login_attempts', '183.182.87.238', 0, 0, '2016-10-03 02:46:30'),
(11, NULL, 'login_attempts', '124.189.142.27', 0, 0, '2016-08-04 13:50:36'),
(12, 0, 'company_logo', '//wiseworking.com.au/wp-content/uploads/WW-Logo-Header-Desktop.png', 0, 0, '2016-05-18 11:18:49'),
(13, NULL, 'login_attempts', '1.23.121.39', 0, 0, '2016-09-30 02:17:11');

-- --------------------------------------------------------

--
-- Table structure for table `company`
--

CREATE TABLE `company` (
  `id` int(11) NOT NULL,
  `code` varchar(16) DEFAULT NULL,
  `type_id` varchar(32) NOT NULL,
  `registration_number` varchar(128) DEFAULT NULL,
  `name` varchar(100) NOT NULL,
  `row_status` int(11) NOT NULL DEFAULT '0',
  `modified_by` int(11) DEFAULT NULL,
  `modified_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `company`
--

INSERT INTO `company` (`id`, `code`, `type_id`, `registration_number`, `name`, `row_status`, `modified_by`, `modified_on`) VALUES
(0, 'WISE', 'wiseworking', '52 154 200 487', 'WiseWorking Pty. Ltd.', 0, 0, '2016-05-18 11:18:48'),
(1, '125156', 'sub_contractor', '174575', 'WiseWorking Test Project', 0, 0, '2016-06-03 11:57:53'),
(2, 'OZC', 'sub_contractor', '234525467543', 'OZ Ceramics', 0, 0, '2016-06-05 23:34:18');

-- --------------------------------------------------------

--
-- Table structure for table `company_address_mapping`
--

CREATE TABLE `company_address_mapping` (
  `id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `address_id` int(11) NOT NULL,
  `row_status` int(11) NOT NULL DEFAULT '0',
  `modified_by` int(11) DEFAULT NULL,
  `modified_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `company_address_mapping`
--

INSERT INTO `company_address_mapping` (`id`, `company_id`, `address_id`, `row_status`, `modified_by`, `modified_on`) VALUES
(0, 0, 0, 0, 0, '2016-05-18 11:18:48');

-- --------------------------------------------------------

--
-- Table structure for table `company_company_mapping`
--

CREATE TABLE `company_company_mapping` (
  `id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `company2_id` int(11) NOT NULL,
  `row_status` int(11) NOT NULL DEFAULT '0',
  `modified_by` int(11) DEFAULT NULL,
  `modified_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `company_contact_details_mapping`
--

CREATE TABLE `company_contact_details_mapping` (
  `id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `contact_details_id` int(11) NOT NULL,
  `row_status` int(11) NOT NULL DEFAULT '0',
  `modified_by` int(11) DEFAULT NULL,
  `modified_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `company_contact_details_mapping`
--

INSERT INTO `company_contact_details_mapping` (`id`, `company_id`, `contact_details_id`, `row_status`, `modified_by`, `modified_on`) VALUES
(0, 0, 0, 0, 0, '2016-05-18 11:18:48');

-- --------------------------------------------------------

--
-- Table structure for table `company_project_mapping`
--

CREATE TABLE `company_project_mapping` (
  `id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `default_flag` int(11) NOT NULL DEFAULT '1' COMMENT '1 means this is the default mapping',
  `row_status` varchar(32) NOT NULL DEFAULT 'active',
  `modified_by` int(11) DEFAULT NULL,
  `modified_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `company_project_mapping`
--

INSERT INTO `company_project_mapping` (`id`, `project_id`, `company_id`, `default_flag`, `row_status`, `modified_by`, `modified_on`) VALUES
(1, 2, 1, 1, 'active', NULL, '2016-06-07 11:23:02'),
(2, 1, 1, 1, 'deleted', NULL, '2016-06-06 22:39:19'),
(3, 1, 2, 1, 'active', NULL, '2016-06-06 22:51:03'),
(4, 2, 2, 1, 'active', NULL, '2016-06-07 11:21:47'),
(5, 3, 0, 1, 'active', NULL, '2016-06-08 08:47:47'),
(6, 2, 0, 1, 'active', NULL, '2016-07-14 11:14:42'),
(7, 4, 2, 1, 'active', NULL, '2016-06-14 06:35:12'),
(8, 5, 0, 1, 'active', NULL, '2016-07-26 22:14:13'),
(9, 6, 0, 1, 'active', NULL, '2016-07-28 16:34:56'),
(10, 7, 0, 1, 'active', NULL, '2016-07-29 06:37:36'),
(11, 8, 0, 1, 'active', NULL, '2016-07-29 13:06:21'),
(12, 9, 0, 1, 'active', NULL, '2016-08-09 07:47:12');

-- --------------------------------------------------------

--
-- Table structure for table `contact_details`
--

CREATE TABLE `contact_details` (
  `id` int(11) NOT NULL,
  `contact_type` varchar(32) NOT NULL,
  `details` varchar(255) NOT NULL,
  `row_status` varchar(50) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `contact_details`
--

INSERT INTO `contact_details` (`id`, `contact_type`, `details`, `row_status`, `modified_by`, `modified_on`) VALUES
(0, 'email', 'jwilliams@wiseworking.com.au', 'active', 0, '2016-07-28 12:16:35'),
(1, 'mobile', '123456785', 'active', 0, '2016-07-28 12:16:42');

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `project_id` int(11) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `event_type` varchar(32) NOT NULL,
  `notes` varchar(640) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `image`
--

CREATE TABLE `image` (
  `id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `image_type` varchar(100) NOT NULL,
  `image_id` bigint(20) NOT NULL,
  `image_gallery_id` bigint(20) NOT NULL DEFAULT '0',
  `image_file` varchar(256) NOT NULL COMMENT 'this is the full file path and name of the image on the filesystem. the file names should be unique and the dir should reflect the company',
  `label` varchar(64) DEFAULT NULL COMMENT 'this is an optional image title or label',
  `description` varchar(512) DEFAULT NULL,
  `row_status` varchar(50) NOT NULL DEFAULT 'active',
  `modified_by` int(11) NOT NULL DEFAULT '0',
  `modified_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `image`
--

INSERT INTO `image` (`id`, `project_id`, `image_type`, `image_id`, `image_gallery_id`, `image_file`, `label`, `description`, `row_status`, `modified_by`, `modified_on`) VALUES
(1, 5, 'inspections', 52, 0, 'upload/images/inspection/medium/1472129259.jpe', '', '', 'active', 0, '2016-08-25 12:47:11'),
(2, 5, 'inspections', 52, 0, 'upload/images/inspection/medium/1472129293.jpg', '', '', 'active', 0, '2016-08-25 12:47:45'),
(3, 5, 'inspections', 52, 0, 'upload/images/inspection/medium/1472129316.png', '', '', 'active', 0, '2016-08-25 12:48:08'),
(4, 5, 'inspections', 54, 0, 'upload/images/inspection/medium/5501472190483.jpg', '', '', 'active', 0, '2016-08-26 07:11:53'),
(6, 5, 'inspections', 37, 0, 'upload/images/inspection/medium/1472204767.jpg', '', '', 'active', 0, '2016-08-26 09:45:37'),
(7, 5, 'inspections', 59, 0, 'upload/images/inspection/medium/1472204849.jpg\n\n', '', '', 'active', 0, '2016-08-26 09:47:16'),
(13, 5, 'inspections', 58, 0, 'upload/images/inspection/medium/1472213124.jpg', '', '', 'active', 0, '2016-08-26 12:04:55'),
(14, 5, 'inspections', 58, 0, 'upload/images/inspection/medium/1472213179.jpg', '', '', 'active', 0, '2016-08-26 12:05:49'),
(15, 5, 'inspections', 58, 0, 'upload/images/inspection/medium/1472213161.jpg', '', '', 'active', 0, '2016-08-26 12:05:32'),
(16, 5, 'inspections', 35, 0, 'upload/images/inspection/medium/1472213787.jpg', '', '', 'active', 0, '2016-08-26 12:15:57'),
(17, 5, 'inspections', 60, 0, 'upload/images/inspection/medium/1472392407.jpg\n\n', '', '', 'active', 0, '2016-08-28 13:53:25'),
(18, 5, 'inspections', 61, 0, 'upload/images/inspection/medium/1472393254.jpg\n\n', '', '', 'active', 0, '2016-08-28 14:07:32'),
(19, 5, 'inspections', 65, 0, 'upload/images/inspection/medium/1473145162.jpg\n\n', '', '', 'active', 0, '2016-09-06 06:58:38'),
(20, 5, 'inspections', 66, 0, 'upload/images/inspection/medium/1473200851.jpg\n\n', '', '', 'active', 0, '2016-09-06 22:26:53'),
(21, 5, 'inspections', 68, 0, 'upload/images/inspection/medium/1473249315.jpg\n\n', '', '', 'active', 0, '2016-09-07 11:54:39'),
(22, 5, 'inspections', 71, 0, 'upload/images/inspection/medium/1473394383.jpg\n\n', '', '', 'active', 0, '2016-09-09 04:12:13'),
(23, 5, 'inspections', 72, 0, 'upload/images/inspection/medium/1473403542.jpg\n\n', '', '', 'active', 0, '2016-09-09 06:44:53'),
(24, 5, 'inspections', 77, 0, 'upload/images/inspection/medium/1473428566.jpg\n\n', '', '', 'active', 0, '2016-09-09 13:42:03'),
(25, 5, 'inspections', 77, 0, 'upload/images/inspection/medium/1473428680.jpg', '', '', 'active', 0, '2016-09-09 13:43:46'),
(26, 5, 'inspections', 78, 0, 'upload/images/inspection/medium/1473650014.jpg\n\n', '', '', 'active', 0, '2016-09-12 03:13:07'),
(27, 5, 'inspections', 82, 0, 'upload/images/inspection/medium/1473769406.jpg\n\n', '', '', 'active', 0, '2016-09-13 12:22:30'),
(28, 5, 'inspections', 17, 0, 'upload/images/inspection/medium/1474623305.png', '', '', 'active', 0, '2016-09-23 09:35:03'),
(29, 5, 'inspections', 17, 0, 'upload/images/inspection/medium/1474623305.jpg', '', '', 'active', 0, '2016-09-23 09:35:03'),
(30, 5, 'inspections', 98, 0, 'genidpublice/upload/inspection/medium/1476352625.png', NULL, NULL, 'active', 0, '2016-10-13 10:01:33'),
(31, 5, 'inspections', 99, 0, 'genidpublice/upload/inspection/medium/1476352625.png', NULL, NULL, 'active', 0, '2016-10-13 10:02:22'),
(32, 5, 'inspections', 99, 0, 'genidpublice/upload/inspection/medium/1476352622.png', NULL, NULL, 'active', 0, '2016-10-13 10:02:22'),
(33, 5, 'inspections', 100, 0, 'genidpublice/upload/inspection/medium/1476352625.png', NULL, NULL, 'active', 0, '2016-10-13 10:05:34'),
(34, 5, 'inspections', 100, 0, 'genidpublice/upload/inspection/medium/1476352622.png', NULL, NULL, 'active', 0, '2016-10-13 10:05:34'),
(35, 5, 'inspections', 101, 0, 'genidpublice/upload/inspection/medium/1476418902.png', NULL, NULL, 'active', 0, '2016-10-14 04:21:42'),
(36, 5, 'inspections', 101, 0, 'genidpublice/upload/inspection/medium/1476419740.png', NULL, NULL, 'active', 0, '2016-10-14 04:35:40'),
(37, 5, 'inspections', 101, 0, 'genidpublice/upload/inspection/medium/1476419745.jpg', NULL, NULL, 'active', 0, '2016-10-14 04:35:45'),
(38, 5, 'inspections', 101, 0, 'genidpublice/upload/inspection/medium/1476419750.jpg', NULL, NULL, 'active', 0, '2016-10-14 04:35:50'),
(39, 5, 'inspections', 101, 0, 'genidpublice/upload/inspection/medium/1476420895.jpg', NULL, NULL, 'active', 0, '2016-10-14 04:54:55'),
(40, 5, 'inspections', 101, 0, 'genidpublice/upload/inspection/medium/1476420954.jpg', NULL, NULL, 'active', 0, '2016-10-14 04:55:54'),
(41, 5, 'inspections', 101, 0, 'genidpublice/upload/inspection/medium/1476420956.jpg', NULL, NULL, 'active', 0, '2016-10-14 04:55:56'),
(42, 5, 'inspections', 101, 0, 'genidpublice/upload/inspection/medium/1476420967.jpg', NULL, NULL, 'active', 0, '2016-10-14 04:56:07');

-- --------------------------------------------------------

--
-- Table structure for table `image_gallery`
--

CREATE TABLE `image_gallery` (
  `id` int(11) NOT NULL,
  `category` varchar(16) NOT NULL COMMENT 'like defects, hazzard, safety_inspection ...',
  `label` varchar(64) DEFAULT NULL COMMENT 'this is an optional image title or label',
  `row_status` int(11) NOT NULL DEFAULT '0',
  `modified_by` int(11) DEFAULT '0',
  `modified_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `inspection`
--

CREATE TABLE `inspection` (
  `id` bigint(20) NOT NULL,
  `project_id` int(11) NOT NULL,
  `inspection_location_id` int(11) NOT NULL,
  `inspection_label` varchar(255) NOT NULL,
  `inspection_description` varchar(512) NOT NULL,
  `inspection_note` text,
  `inspection_type` varchar(32) NOT NULL,
  `inspection_qrcode` varchar(100) DEFAULT NULL,
  `inspection_images` varchar(255) DEFAULT NULL,
  `inspection_date` datetime NOT NULL,
  `inspected_by` int(11) NOT NULL DEFAULT '0',
  `inspection_raised_by` varchar(100) NOT NULL,
  `inspection_activitymain` varchar(100) NOT NULL,
  `inspection_status` varchar(32) NOT NULL DEFAULT 'open',
  `inspection_activity` varchar(100) NOT NULL,
  `row_status` varchar(20) DEFAULT NULL,
  `modified_by` int(11) DEFAULT '0',
  `modified_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `inspection`
--

INSERT INTO `inspection` (`id`, `project_id`, `inspection_location_id`, `inspection_label`, `inspection_description`, `inspection_note`, `inspection_type`, `inspection_qrcode`, `inspection_images`, `inspection_date`, `inspected_by`, `inspection_raised_by`, `inspection_activitymain`, `inspection_status`, `inspection_activity`, `row_status`, `modified_by`, `modified_on`) VALUES
(17, 5, 2, 'Ground > Apartment G.01', 'Arm broken', 'asfafasf', 'defect', '0.01042900_1475206459_text.png', NULL, '2016-08-24 00:00:00', 0, 'structural_engineer', '', 'open', '', 'active', 0, '2016-10-14 03:52:46'),
(26, 5, 2, 'Ground > Apartment G.01', 'test by haresh', 'test by haresh', 'defect', '0.00057500_1473412901_insp.png', NULL, '2016-08-24 00:00:00', 0, 'builder', '', 'open', '', 'active', 0, '2016-09-09 09:37:32'),
(27, 5, 2, 'Ground > Apartment G.01', 'test 1', '', 'defect', '0.00057500_1473412901_insp.png', NULL, '2016-08-24 00:00:00', 0, 'builder', '', 'open', '', 'deleted', 0, '2016-10-13 11:09:40'),
(28, 5, 2, 'Ground > Apartment G.01', 'haresh test 2', 'haresh test 2', 'defect', '0.00057500_1473412901_insp.png', NULL, '2016-08-24 00:00:00', 0, 'builder', '', 'open', '', 'active', 0, '2016-09-09 09:37:32'),
(29, 5, 27, 'Level 2 > Apartment 2.01 > Ensuite', 'checked', '', 'defect', '0.00057500_1473412901_insp.png', NULL, '2016-08-24 00:00:00', 0, 'builder', '', 'open', '', 'active', 0, '2016-09-12 06:33:29'),
(30, 5, 2, 'Ground > Apartment G.01', 'This is another Justin Test', '', 'defect', '0.00057500_1473412901_insp.png', NULL, '2016-08-24 00:00:00', 0, 'builder', '', 'open', '', 'active', 0, '2016-09-09 09:37:32'),
(31, 5, 69, 'Level 3 > Apartment 3.01', 'Level 3 Test', '', 'defect', '0.00057500_1473412901_insp.png', NULL, '2016-08-24 00:00:00', 0, 'builder', '', 'open', '', 'active', 0, '2016-09-09 09:37:32'),
(32, 5, 69, 'Level 3 > Apartment 3.01', 'Justin testing standard defects', '', 'defect', '0.00057500_1473412901_insp.png', NULL, '2016-08-24 00:00:00', 0, 'builder', '', 'open', '', 'active', 0, '2016-09-09 09:37:32'),
(33, 5, 2, 'Ground > Apartment G.01', 'test 24 ', '', 'defect', '0.00057500_1473412901_insp.png', NULL, '2016-08-24 00:00:00', 0, 'builder', '', 'open', '', 'active', 0, '2016-09-09 09:37:32'),
(34, 5, 2, 'Ground > Apartment G.01', 'test me', '', 'defect', '0.00057500_1473412901_insp.png', NULL, '2016-08-25 00:00:00', 0, 'builder', '', 'open', '', 'active', 0, '2016-09-09 09:37:32'),
(35, 5, 17, 'Level 1 > Apartment 1.1 > Bedroom 1', 'Fix Mark on paint', '', 'defect', '0.00057500_1473412901_insp.png', NULL, '2016-08-25 00:00:00', 0, 'builder', '', 'open', '', 'active', 0, '2016-09-09 09:37:32'),
(36, 5, 17, 'Level 1 > Apartment 1.1 > Bedroom 1', 'Testing to see if it works', '', 'defect', '0.00057500_1473412901_insp.png', NULL, '2016-08-25 00:00:00', 0, 'builder', '', 'open', '', 'active', 0, '2016-09-09 09:37:32'),
(37, 5, 0, 'Level 3 > Apartment 3.01 > Select', 'This is a test', '', 'defect', '0.00057500_1473412901_insp.png', NULL, '2016-08-25 00:00:00', 0, 'builder', '', 'open', '', 'active', 0, '2016-09-09 09:37:32'),
(38, 5, 69, 'Level 3 > Apartment 3.01', 'We are writing this to test the functionality', '', 'defect', '0.00057500_1473412901_insp.png', NULL, '2016-08-25 00:00:00', 0, 'builder', '', 'open', '', 'active', 0, '2016-09-09 09:37:32'),
(39, 5, 69, 'Level 3 > Apartment 3.01', 'Does the filter panel still work?', '', 'defect', '0.00057500_1473412901_insp.png', NULL, '2016-08-25 00:00:00', 0, 'builder', '', 'open', '', 'active', 0, '2016-09-09 09:37:32'),
(49, 5, 3, 'Ground > Apartment G.01 > Bedroom 1', 'haresh test 25-08', 'test note here', 'defect', '0.00057500_1473412901_insp.png', NULL, '2016-08-25 00:00:00', 0, 'builder', '', 'open', '', 'active', 0, '2016-09-09 09:37:32'),
(50, 5, 2, 'Ground > Apartment G.01', 'test images', 'test images', 'defect', '0.00057500_1473412901_insp.png', NULL, '2016-08-25 00:00:00', 0, 'builder', '', 'open', '', 'active', 0, '2016-09-09 09:37:32'),
(51, 5, 70, 'Level 3 > Apartment 3.01 > Apartment 3.02', 'We need to do this', '', 'defect', '0.00057500_1473412901_insp.png', NULL, '2016-08-25 00:00:00', 0, 'builder', '', 'open', '', 'active', 0, '2016-09-09 09:37:32'),
(52, 5, 70, 'Level 3 > Apartment 3.02', 'No One Standard Defect Found !', 'ttttt', 'defect', '0.00057500_1473412901_insp.png', NULL, '2016-08-25 00:00:00', 0, 'architect', '', 'open', '', 'active', 0, '2016-09-09 09:37:32'),
(53, 5, 71, 'Level 4 > Common Areas', 'test common', 'common note', 'defect', '0.00057500_1473412901_insp.png', NULL, '2016-08-26 00:00:00', 0, 'builder', '', 'open', '', 'active', 0, '2016-09-09 09:37:32'),
(54, 5, 15, 'Level 1', 'h test', '', 'defect', '0.00057500_1473412901_insp.png', NULL, '2016-08-26 00:00:00', 0, 'builder', '', 'open', '', 'active', 0, '2016-09-09 09:37:32'),
(55, 5, 22, 'Level 2 > Apartment 2.01', 'h test 1', '', 'defect', '0.00057500_1473412901_insp.png', NULL, '2016-08-26 00:00:00', 0, 'architect', '', 'open', '', 'active', 0, '2016-09-09 09:37:32'),
(56, 5, 16, 'Level 1 > Apartment 1.1', 'h desc', '', 'defect', '0.00057500_1473412901_insp.png', NULL, '2016-08-26 00:00:00', 0, 'builder', '', 'open', '', 'active', 0, '2016-09-09 09:37:32'),
(57, 5, 22, 'Level 2 > Apartment 2.01', 'desc', '', 'defect', '0.00057500_1473412901_insp.png', NULL, '2016-08-26 00:00:00', 0, 'architect', '', 'open', '', 'active', 0, '2016-09-09 09:37:32'),
(58, 5, 71, 'Level 4 > Common Areas', 'No One Standard Defect Found !', '', 'defect', '0.00057500_1473412901_insp.png', NULL, '2016-08-26 00:00:00', 0, 'structural_engineer', '', 'open', '', 'active', 0, '2016-09-09 09:37:32'),
(59, 5, 22, 'Level 2 > Apartment 2.01', 'This is a new test image adding working', '', 'defect', '0.00057500_1473412901_insp.png', NULL, '2016-08-26 00:00:00', 0, 'builder', '', 'open', '', 'active', 0, '2016-09-09 09:37:32'),
(60, 5, 71, 'Level 4 > Common Areas', 'my testing', 'testing', 'defect', '0.00057500_1473412901_insp.png', NULL, '2016-08-28 00:00:00', 0, 'builder', '', 'open', '', 'active', 0, '2016-09-09 09:37:32'),
(61, 5, 44, ' > Apartment 1.2', 'testing21', 'testing21', 'defect', '0.00057500_1473412901_insp.png', NULL, '2016-08-29 00:00:00', 0, 'builder', '', 'open', '', 'active', 0, '2016-09-09 09:37:32'),
(62, 5, 69, 'Level 3 > Apartment 3.01', 'testing 212', 'testing 212', 'defect', '0.00057500_1473412901_insp.png', NULL, '2016-08-29 00:00:00', 0, 'builder', '', 'open', '', 'active', 0, '2016-09-09 09:37:32'),
(63, 5, 2, ' > Apartment G.01', 'Desc', '', 'defect', '0.00057500_1473412901_insp.png', NULL, '2016-09-02 00:00:00', 0, 'builder', '', 'open', 'brickwork', 'active', 0, '2016-09-09 09:37:32'),
(64, 5, 2, 'Ground > Apartment G.01', 'Scratch in paint', '', 'defect', '0.00057500_1473412901_insp.png', NULL, '2016-09-06 00:00:00', 0, 'architect', '', 'open', 'brickwork', 'active', 0, '2016-09-09 09:37:32'),
(65, 5, 71, 'Level 4 > Common Areas', 'Scratch in paint', '', 'defect', '0.00057500_1473412901_insp.png', NULL, '2016-09-06 00:00:00', 0, 'builder', '', 'open', 'concreting', 'active', 0, '2016-09-09 09:37:32'),
(66, 5, 69, 'Level 3 > Apartment 3.01', 'Scratch in paint', '', 'defect', '0.00057500_1473412901_insp.png', NULL, '2016-09-07 00:00:00', 0, 'builder', '', 'open', 'brickwork', 'active', 0, '2016-09-09 09:37:32'),
(67, 5, 2, 'Ground > Apartment G.01', 'test1', 'test', 'defect', '0.00057500_1473412901_insp.png', NULL, '2016-09-07 00:00:00', 0, 'builder', '', 'open', 'carpentry', 'active', 0, '2016-09-09 09:37:32'),
(68, 5, 69, 'Level 3 > Apartment 3.01', 'Scratch in paint', '', 'defect', '0.00057500_1473412901_insp.png', NULL, '2016-09-07 00:00:00', 0, 'builder', '', 'open', '', 'active', 0, '2016-09-09 09:37:32'),
(69, 5, 69, ' > Apartment 3.01', 'Desc2', 'test', 'defect', '0.00057500_1473412901_insp.png', NULL, '2016-09-07 00:00:00', 0, 'builder', 'electrical', 'open', 'carpentry', 'active', 0, '2016-09-09 09:37:32'),
(70, 5, 16, 'Level 1 > Apartment 1.1', 'Test desc1', '', 'Warranty Issue', '0.00057500_1473412901_insp.png', NULL, '2016-09-09 00:00:00', 0, 'superintendant', '', 'open', '', 'active', 0, '2016-09-09 09:37:32'),
(71, 5, 71, 'Level 4 > Common Areas', 'Broken pipe leaking', '', 'defect', '0.00057500_1473412901_insp.png', NULL, '2016-09-09 00:00:00', 0, 'builder', '', 'open', '', 'active', 0, '2016-09-09 09:37:32'),
(72, 5, 71, 'Level 4 > Common Areas', 'Broken pipe leaking', '', 'defect', '0.00057500_1473412901_insp.png', NULL, '2016-09-09 00:00:00', 0, 'builder', '', 'open', '', 'active', 0, '2016-09-09 09:37:15'),
(73, 5, 69, 'Level 3 > Apartment 3.01', 'test haresh', 'test haresh', 'defect', '0.00057500_1473412901_insp.png', NULL, '2016-09-09 00:00:00', 0, 'builder', '', 'open', '', 'active', 0, '2016-09-09 09:20:47'),
(74, 5, 70, ' > Apartment 3.02', 'test me 11', '', 'defect', '0.88982000_1473414416_insp.png', NULL, '2016-09-09 00:00:00', 0, 'builder', '', 'open', '', 'active', 0, '2016-09-09 09:46:04'),
(75, 5, 16, 'Level 1 > Apartment 1.1', 'add new test', '', 'defect', '0.85267600_1473422550_insp.png', NULL, '2016-09-09 00:00:00', 0, 'builder', '', 'open', '', 'active', 0, '2016-09-09 12:01:37'),
(76, 5, 71, 'Level 4 > Common Areas', 'Test Desc', 'last test', 'defect', '0.74363100_1473426896_insp.png', NULL, '2016-09-09 00:00:00', 0, 'builder', '', 'open', '', 'active', 0, '2016-09-12 02:59:27'),
(77, 5, 71, 'Level 4 > Common Areas', 'Broken pipe leaking', '', 'defect', '0.95849100_1473428575_insp.png', NULL, '2016-09-09 00:00:00', 0, 'builder', '', 'open', '', 'active', 0, '2016-09-09 13:42:03'),
(78, 5, 27, 'Level 2 > Apartment 2.01 > Ensuite', 'Broken pipe leaking', '', 'defect', '0.12796900_1473650045_insp.png', NULL, '2016-09-12 00:00:00', 0, 'builder', '', 'open', '', 'active', 0, '2016-09-12 03:13:07'),
(79, 5, 71, 'Level 4 > Common Areas', 'Broken pipe leaking', '', 'defect', '0.81340600_1473683787_insp.png', NULL, '2016-09-12 00:00:00', 0, 'builder', '', 'open', '', 'active', 0, '2016-09-12 12:35:29'),
(80, 5, 2, 'Ground > Apartment G.01', 'test', '', 'defect', '0.39583100_1473740119_insp.png', NULL, '2016-09-13 00:00:00', 0, 'builder', '', 'open', '', 'active', 0, '2016-09-13 04:14:19'),
(81, 5, 16, 'Level 1 > Apartment 1.1', 'Test Desc', '', 'defect', '0.54739900_1473744808_insp.png', NULL, '2016-09-13 00:00:00', 0, 'builder', '', 'open', '', 'active', 0, '2016-09-13 05:32:29'),
(82, 5, 27, 'Level 2 > Apartment 2.01 > Ensuite', 'Broken pipe leaking', '', 'defect', '0.25083700_1473769410_insp.png', NULL, '2016-09-13 00:00:00', 0, 'builder', '', 'open', '', 'active', 0, '2016-09-13 12:22:30'),
(83, 5, 22, 'Level 2 > Apartment 2.01', 'Blah', '', 'defect', '0.18808200_1473769907_insp.png', NULL, '2016-09-13 00:00:00', 0, 'builder', '', 'open', '', 'active', 0, '2016-09-13 12:30:47'),
(84, 5, 44, 'Level 1 > Apartment 1.1 > Apartment 1.2', 'haresg1212', '', 'defect', NULL, NULL, '2016-10-13 00:00:00', 1, 'builder', '', 'open', '', 'active', 0, '2016-10-13 06:40:16'),
(85, 5, 44, 'Level 1 > Apartment 1.1 > Apartment 1.2', 'haresg1212', '', 'defect', NULL, NULL, '2016-10-13 00:00:00', 1, 'builder', '', 'open', '', 'active', 0, '2016-10-13 06:42:10'),
(86, 5, 44, 'Level 1 > Apartment 1.1 > Apartment 1.2', 'haresg1212sssssss', '', 'defect', '0.24864400_1476340965_insp.png', NULL, '2016-10-13 00:00:00', 1, 'builder', '', 'open', '', 'active', 0, '2016-10-13 06:42:45'),
(87, 5, 44, 'Level 1 > Apartment 1.1 > Apartment 1.2', 'haresg1212sssssss', '', 'defect', '0.35015800_1476341144_insp.png', NULL, '2016-10-13 00:00:00', 1, 'builder', '', 'open', '', 'active', 0, '2016-10-13 06:45:44'),
(88, 5, 44, 'Level 1 > Apartment 1.1 > Apartment 1.2', 'haresg1212sssssss', '', 'defect', '0.77029300_1476341206_insp.png', NULL, '2016-10-13 00:00:00', 1, 'builder', '', 'open', '', 'active', 0, '2016-10-13 06:46:47'),
(89, 5, 44, 'Level 1 > Apartment 1.1 > Apartment 1.2', 'haresg1212sssssss', '', 'defect', '0.35024600_1476341242_insp.png', NULL, '2016-10-13 00:00:00', 1, 'builder', '', 'open', '', 'active', 0, '2016-10-13 06:47:22'),
(90, 5, 44, 'Level 1 > Apartment 1.1 > Apartment 1.2', 'haresg1212sssssss', '', 'defect', '0.12405300_1476341294_insp.png', NULL, '2016-10-13 00:00:00', 1, 'builder', '', 'open', '', 'active', 0, '2016-10-13 06:48:14'),
(91, 5, 44, 'Level 1 > Apartment 1.1 > Apartment 1.2', 'haresg1212sssssss', '', 'defect', '0.45618500_1476341407_insp.png', NULL, '2016-10-13 00:00:00', 1, 'builder', '', 'open', '', 'active', 0, '2016-10-13 06:50:07'),
(92, 5, 44, 'Level 1 > Apartment 1.1 > Apartment 1.2', 'haresg1212sssssss', '', 'defect', '0.00568900_1476341811_insp.png', NULL, '2016-10-13 00:00:00', 1, 'builder', '', 'open', '', 'active', 0, '2016-10-13 06:56:52'),
(93, 5, 44, 'Level 1 > Apartment 1.1 > Apartment 1.2', 'haresg1212sssssss', '', 'defect', '0.44880100_1476341854_insp.png', NULL, '2016-10-13 00:00:00', 1, 'builder', '', 'open', '', 'active', 0, '2016-10-13 06:57:35'),
(94, 5, 44, 'Level 1 > Apartment 1.1 > Apartment 1.2', 'haresg1212sssssss', '', 'defect', '0.23068500_1476341910_insp.png', NULL, '2016-10-13 00:00:00', 1, 'builder', '', 'open', '', 'active', 0, '2016-10-13 06:58:31'),
(95, 5, 44, 'Level 1 > Apartment 1.1 > Apartment 1.2', 'haresg1212sssssss', '', 'defect', '0.38388600_1476341932_insp.png', NULL, '2016-10-13 00:00:00', 1, 'builder', '', 'open', '', 'active', 0, '2016-10-13 06:58:52'),
(96, 5, 2, 'Ground > Apartment G.01', 'test1234', '', 'defect', '0.29948200_1476352646_insp.png', NULL, '2016-10-13 00:00:00', 1, 'builder', '', 'open', '', 'active', 0, '2016-10-13 09:57:27'),
(97, 5, 2, 'Ground > Apartment G.01', 'test1234', '', 'defect', '0.77418400_1476352696_insp.png', NULL, '2016-10-13 00:00:00', 1, 'builder', '', 'open', '', 'active', 0, '2016-10-13 09:58:17'),
(98, 5, 2, 'Ground > Apartment G.01', 'test1234', '', 'defect', '0.74295300_1476352729_insp.png', NULL, '2016-10-13 00:00:00', 1, 'builder', '', 'open', '', 'active', 0, '2016-10-13 09:58:50'),
(99, 5, 2, 'Ground > Apartment G.01', 'test1234', '', 'defect', '0.78702300_1476352941_insp.png', NULL, '2016-10-13 00:00:00', 1, 'builder', '', 'open', '', 'active', 0, '2016-10-13 10:02:22'),
(100, 5, 2, 'Ground > Apartment G.01', 'test1234', '', 'defect', '0.72514500_1476353134_insp.png', NULL, '2016-10-13 00:00:00', 1, 'builder', '', 'open', '', 'active', 0, '2016-10-13 10:05:35'),
(101, 5, 2, 'Ground > Apartment G.01', 'test1234', '', 'defect', '0.33088900_1476353213_insp.png', '{"image1":"genidpublice\\/upload\\/inspection\\/medium\\/1476352625.png","image2":"genidpublice\\/upload\\/inspection\\/medium\\/1476352622.png"}', '2016-10-13 00:00:00', 1, 'builder', '', 'open', '', 'active', 0, '2016-10-13 10:06:53');

-- --------------------------------------------------------

--
-- Table structure for table `inspection_activity`
--

CREATE TABLE `inspection_activity` (
  `id` bigint(25) NOT NULL,
  `inspection_id` bigint(25) NOT NULL,
  `project_id` bigint(25) NOT NULL,
  `activity_main` varchar(255) NOT NULL,
  `row_status` varchar(100) NOT NULL DEFAULT 'active',
  `modified_by` bigint(25) NOT NULL,
  `modified_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `original_modified_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` bigint(25) NOT NULL,
  `resource_type` varchar(100) NOT NULL DEFAULT 'Webserver'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `inspection_activity`
--

INSERT INTO `inspection_activity` (`id`, `inspection_id`, `project_id`, `activity_main`, `row_status`, `modified_by`, `modified_on`, `original_modified_date`, `created_date`, `created_by`, `resource_type`) VALUES
(1, 70, 5, 'cleaning', 'active', 0, '2016-09-09 01:37:59', '2016-09-09 01:37:59', '2016-09-09 01:37:59', 0, 'Webserver'),
(2, 71, 5, 'plumbing', 'active', 0, '2016-09-09 02:13:06', '2016-09-09 02:13:06', '2016-09-09 02:13:06', 0, 'Webserver'),
(3, 71, 5, 'carpentry', 'active', 0, '2016-09-09 02:13:06', '2016-09-09 02:13:06', '2016-09-09 02:13:06', 0, 'Webserver'),
(4, 72, 5, 'plumbing', 'active', 0, '2016-09-09 04:45:46', '2016-09-09 04:45:46', '2016-09-09 04:45:46', 0, 'Webserver'),
(5, 73, 5, 'carpentry', 'active', 0, '2016-09-09 07:21:41', '2016-09-09 07:21:41', '2016-09-09 07:21:41', 0, 'Webserver'),
(6, 74, 5, 'carpentry', 'active', 0, '2016-09-09 07:46:57', '2016-09-09 07:46:57', '2016-09-09 07:46:57', 0, 'Webserver'),
(9, 75, 5, 'brickwork', 'active', 0, '2016-09-09 10:03:13', '2016-09-09 10:03:13', '2016-09-09 10:03:13', 0, 'Webserver'),
(10, 75, 5, 'metalwork', 'active', 0, '2016-09-09 10:03:13', '2016-09-09 10:03:13', '2016-09-09 10:03:13', 0, 'Webserver'),
(14, 77, 5, 'plumbing', 'active', 0, '2016-09-09 11:44:52', '2016-09-09 11:44:52', '2016-09-09 11:44:52', 0, 'Webserver'),
(15, 76, 5, 'carpentry', 'active', 0, '2016-09-12 01:00:25', '2016-09-12 01:00:25', '2016-09-12 01:00:25', 0, 'Webserver'),
(16, 78, 5, 'plumbing', 'active', 0, '2016-09-12 01:14:05', '2016-09-12 01:14:05', '2016-09-12 01:14:05', 0, 'Webserver'),
(17, 79, 5, 'Structural steel', 'active', 0, '2016-09-12 10:36:28', '2016-09-12 10:36:28', '2016-09-12 10:36:28', 0, 'Webserver'),
(18, 79, 5, 'plumbing', 'active', 0, '2016-09-12 10:36:28', '2016-09-12 10:36:28', '2016-09-12 10:36:28', 0, 'Webserver'),
(19, 80, 5, 'concreting', 'active', 0, '2016-09-13 02:15:19', '2016-09-13 02:15:19', '2016-09-13 02:15:19', 0, 'Webserver'),
(20, 80, 5, 'carpentry', 'active', 0, '2016-09-13 02:15:19', '2016-09-13 02:15:19', '2016-09-13 02:15:19', 0, 'Webserver'),
(21, 81, 5, 'carpentry', 'active', 0, '2016-09-13 03:33:29', '2016-09-13 03:33:29', '2016-09-13 03:33:29', 0, 'Webserver'),
(22, 82, 5, 'plumbing', 'active', 0, '2016-09-13 10:23:30', '2016-09-13 10:23:30', '2016-09-13 10:23:30', 0, 'Webserver'),
(23, 82, 5, 'Structural steel', 'active', 0, '2016-09-13 10:23:30', '2016-09-13 10:23:30', '2016-09-13 10:23:30', 0, 'Webserver'),
(24, 83, 5, '', 'active', 0, '2016-09-13 10:31:48', '2016-09-13 10:31:48', '2016-09-13 10:31:48', 0, 'Webserver'),
(25, 49, 5, 'plumbing', 'active', 0, '2016-09-13 10:33:36', '2016-09-13 10:33:36', '2016-09-13 10:33:36', 0, 'Webserver'),
(26, 49, 5, 'Structural steel', 'active', 0, '2016-09-13 10:33:36', '2016-09-13 10:33:36', '2016-09-13 10:33:36', 0, 'Webserver'),
(27, 0, 5, '', 'active', 1, '2016-10-13 05:13:33', '2016-10-13 05:13:33', '2016-10-13 05:13:33', 1, 'Webserver'),
(28, 0, 5, '', 'active', 1, '2016-10-13 05:14:54', '2016-10-13 05:14:54', '2016-10-13 05:14:54', 1, 'Webserver'),
(29, 86, 5, '', 'active', 1, '2016-10-13 05:42:45', '2016-10-13 05:42:45', '2016-10-13 05:42:45', 1, 'Webserver'),
(30, 87, 5, '', 'active', 1, '2016-10-13 05:45:44', '2016-10-13 05:45:44', '2016-10-13 05:45:44', 1, 'Webserver'),
(31, 88, 5, '', 'active', 1, '2016-10-13 05:46:47', '2016-10-13 05:46:47', '2016-10-13 05:46:47', 1, 'Webserver'),
(32, 89, 5, '', 'active', 1, '2016-10-13 05:47:22', '2016-10-13 05:47:22', '2016-10-13 05:47:22', 1, 'Webserver'),
(33, 90, 5, '', 'active', 1, '2016-10-13 05:48:14', '2016-10-13 05:48:14', '2016-10-13 05:48:14', 1, 'Webserver'),
(34, 91, 5, '', 'active', 1, '2016-10-13 05:50:07', '2016-10-13 05:50:07', '2016-10-13 05:50:07', 1, 'Webserver'),
(35, 92, 5, '', 'active', 1, '2016-10-13 05:56:52', '2016-10-13 05:56:52', '2016-10-13 05:56:52', 1, 'Webserver'),
(36, 93, 5, '', 'active', 1, '2016-10-13 05:57:35', '2016-10-13 05:57:35', '2016-10-13 05:57:35', 1, 'Webserver'),
(37, 94, 5, '', 'active', 1, '2016-10-13 05:58:31', '2016-10-13 05:58:31', '2016-10-13 05:58:31', 1, 'Webserver'),
(38, 95, 5, '', 'active', 1, '2016-10-13 05:58:52', '2016-10-13 05:58:52', '2016-10-13 05:58:52', 1, 'Webserver'),
(39, 99, 5, '', 'active', 1, '2016-10-13 09:02:22', '2016-10-13 09:02:22', '2016-10-13 09:02:22', 1, 'Webserver'),
(40, 100, 5, '', 'active', 1, '2016-10-13 09:05:35', '2016-10-13 09:05:35', '2016-10-13 09:05:35', 1, 'Webserver');

-- --------------------------------------------------------

--
-- Table structure for table `inspection_item`
--

CREATE TABLE `inspection_item` (
  `id` bigint(20) NOT NULL,
  `inspection_id` bigint(20) NOT NULL,
  `location_id` int(11) NOT NULL,
  `description` varchar(512) NOT NULL,
  `label` text NOT NULL,
  `priority` varchar(32) NOT NULL DEFAULT 'medium' COMMENT 'key_type=inspection_item_priority',
  `image_gallery` bigint(20) DEFAULT NULL,
  `raised_on` datetime NOT NULL,
  `raised_by` varchar(100) NOT NULL,
  `item_status` varchar(32) NOT NULL DEFAULT 'open' COMMENT 'key_type=inspection_item_status',
  `expected_by` date DEFAULT NULL,
  `fixed_by` date DEFAULT NULL,
  `cost_attribute` varchar(100) NOT NULL,
  `cost_impact` varchar(100) DEFAULT NULL,
  `issue_to` bigint(25) NOT NULL,
  `inspection_activity` varchar(100) DEFAULT NULL,
  `blocked_inspection_id` bigint(20) DEFAULT NULL,
  `row_status` varchar(50) NOT NULL DEFAULT 'active',
  `modified_by` int(11) DEFAULT '0',
  `modified_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `inspection_item`
--

INSERT INTO `inspection_item` (`id`, `inspection_id`, `location_id`, `description`, `label`, `priority`, `image_gallery`, `raised_on`, `raised_by`, `item_status`, `expected_by`, `fixed_by`, `cost_attribute`, `cost_impact`, `issue_to`, `inspection_activity`, `blocked_inspection_id`, `row_status`, `modified_by`, `modified_on`) VALUES
(26, 17, 2, 'Arm broken', 'Ground > Apartment G.01', 'medium', NULL, '2016-08-24 00:00:00', 'builder', 'open', '0000-00-00', '2016-09-08', 'none', '', 147515362130, '', NULL, 'active', 0, '2016-09-23 09:35:03'),
(27, 17, 2, 'Arm broken', 'Ground > Apartment G.01', 'medium', NULL, '2016-08-24 00:00:00', 'builder', 'open', '0000-00-00', '2016-09-01', 'none', '', 147544302520, '', NULL, 'active', 0, '2016-09-23 09:35:03'),
(33, 26, 2, 'test by haresh', 'Ground > Apartment G.01', 'medium', NULL, '2016-08-24 00:00:00', 'builder', 'disputed', '0000-00-00', '2016-08-25', 'none', 'low', 147544302520, '', NULL, 'active', 0, '2016-09-07 06:55:25'),
(34, 27, 2, 'test 1', 'Ground > Apartment G.01', 'medium', NULL, '2016-08-24 00:00:00', 'builder', 'open', '0000-00-00', '2016-08-25', 'none', 'low', 147544302520, '', NULL, 'deleted', 0, '2016-10-13 11:09:49'),
(35, 27, 2, 'test 1', 'Ground > Apartment G.01', 'medium', NULL, '2016-08-24 00:00:00', 'builder', 'disputed', '0000-00-00', '2016-08-26', 'backcharge', 'medium', 147544302520, '', NULL, 'deleted', 0, '2016-10-13 11:09:49'),
(36, 28, 2, 'haresh test 2', 'Ground > Apartment G.01', 'medium', NULL, '2016-08-24 00:00:00', 'builder', 'open', '0000-00-00', '2016-08-25', 'none', 'low', 147544302520, '', NULL, 'active', 0, '2016-09-07 06:55:25'),
(37, 28, 2, 'haresh test 2', 'Ground > Apartment G.01', 'medium', NULL, '2016-08-24 00:00:00', 'builder', 'open', '0000-00-00', '2016-08-27', 'backcharge', 'medium', 147544302520, '', NULL, 'active', 0, '2016-09-07 06:55:25'),
(38, 28, 2, 'haresh test 2', 'Ground > Apartment G.01', 'medium', NULL, '2016-08-24 00:00:00', 'builder', 'open', '0000-00-00', '2016-08-28', 'variation', 'high', 147544302520, '', NULL, 'active', 0, '2016-09-07 06:55:25'),
(39, 29, 27, 'There is a light globe missing', 'Level 2 > Apartment 2.01 > Ensuite', 'medium', NULL, '2016-08-24 00:00:00', 'builder', 'open', '0000-00-00', '2016-10-25', 'none', '', 147544302520, '', NULL, 'active', 0, '2016-09-12 06:33:29'),
(40, 30, 2, 'This is another Justin Test', 'Ground > Apartment G.01', 'medium', NULL, '2016-08-24 00:00:00', 'builder', 'open', '0000-00-00', '2016-08-24', 'none', '', 147544302520, '', NULL, 'active', 0, '2016-09-07 06:55:25'),
(41, 31, 69, 'Level 3 Test', 'Level 3 > Apartment 3.01', 'medium', NULL, '2016-08-24 00:00:00', 'builder', 'open', '0000-00-00', '2016-08-24', 'none', '', 147544302520, '', NULL, 'active', 0, '2016-09-07 06:55:25'),
(42, 32, 69, 'Justin testing standard defects', 'Level 3 > Apartment 3.01', 'medium', NULL, '2016-08-24 00:00:00', 'builder', 'closed', '0000-00-00', '2016-08-24', 'none', '', 147544302520, '', NULL, 'active', 0, '2016-09-07 06:55:25'),
(43, 33, 2, 'test 24 ', 'Ground > Apartment G.01', 'medium', NULL, '2016-08-24 00:00:00', 'builder', 'open', '0000-00-00', '2016-08-24', 'backcharge', 'low', 147544302520, '', NULL, 'active', 0, '2016-09-07 06:55:25'),
(44, 34, 2, 'test me', 'Ground > Apartment G.01', 'medium', NULL, '2016-08-25 00:00:00', 'builder', 'disputed', '0000-00-00', '2016-08-25', 'backcharge', 'low', 147544302520, 'carpentry', NULL, 'active', 0, '2016-09-07 06:56:14'),
(45, 35, 17, 'Fix Mark on paint', 'Level 1 > Apartment 1.1 > Bedroom 1', 'medium', NULL, '2016-08-25 00:00:00', 'builder', 'open', '0000-00-00', '2016-08-25', 'none', '', 147544302520, '', NULL, 'active', 0, '2016-09-07 06:55:25'),
(46, 35, 17, 'Fix Mark on paint', 'Level 1 > Apartment 1.1 > Bedroom 1', 'medium', NULL, '2016-08-25 00:00:00', 'builder', 'open', '0000-00-00', '2016-08-26', 'none', '', 147544302520, '', NULL, 'active', 0, '2016-09-07 06:55:25'),
(47, 36, 17, 'Testing to see if it works', 'Level 1 > Apartment 1.1 > Bedroom 1', 'medium', NULL, '2016-08-25 00:00:00', 'builder', 'open', '0000-00-00', '2016-08-25', 'none', '', 147544302520, '', NULL, 'active', 0, '2016-09-07 06:55:25'),
(48, 36, 17, 'Testing to see if it works', 'Level 1 > Apartment 1.1 > Bedroom 1', 'medium', NULL, '2016-08-25 00:00:00', 'builder', 'open', '0000-00-00', '2016-08-26', 'none', 'select', 147544302520, '', NULL, 'active', 0, '2016-09-07 06:55:25'),
(49, 37, 0, 'This is a test', 'Level 3 > Apartment 3.01 > Select', 'medium', NULL, '2016-08-25 00:00:00', 'builder', 'open', '0000-00-00', '2016-08-25', 'none', '', 147544302520, '', NULL, 'active', 0, '2016-09-07 06:55:25'),
(50, 38, 69, 'We are writing this to test the functionality', 'Level 3 > Apartment 3.01', 'medium', NULL, '2016-08-25 00:00:00', 'builder', 'open', '0000-00-00', '2016-08-30', 'none', '', 147544302520, '', NULL, 'active', 0, '2016-09-07 06:55:25'),
(51, 39, 69, 'Does the filter panel still work?', 'Level 3 > Apartment 3.01', 'medium', NULL, '2016-08-25 00:00:00', 'builder', 'open', '0000-00-00', '2016-08-25', 'none', '', 147544302520, '', NULL, 'active', 0, '2016-09-07 06:55:25'),
(59, 49, 3, 'haresh test 25-08', 'Ground > Apartment G.01 > Bedroom 1', 'medium', NULL, '2016-08-25 00:00:00', 'builder', 'open', '0000-00-00', '2016-08-25', 'backcharge', 'low', 147448262990, '', NULL, 'active', 0, '2016-09-13 12:32:35'),
(60, 50, 2, 'test images', 'Ground > Apartment G.01', 'medium', NULL, '2016-08-25 00:00:00', 'builder', 'open', '0000-00-00', '2016-08-25', 'backcharge', 'medium', 147544302520, '', NULL, 'active', 0, '2016-09-07 06:55:25'),
(61, 51, 70, 'We need to do this', 'Level 3 > Apartment 3.01 > Apartment 3.02', 'medium', NULL, '2016-08-25 00:00:00', 'builder', 'Fixed', '0000-00-00', '2016-08-25', 'none', '', 147544302520, '', NULL, 'active', 0, '2016-09-07 06:55:25'),
(62, 52, 70, 'Test', 'Level 3 > Apartment 3.02', 'medium', NULL, '2016-08-25 00:00:00', 'builder', 're_opened', '0000-00-00', '2016-08-25', 'backcharge', 'low', 147544302520, '', NULL, 'active', 0, '2016-09-07 06:55:25'),
(63, 52, 70, 'Test', 'Level 3 > Apartment 3.02', 'medium', NULL, '2016-08-25 00:00:00', 'builder', 'open', '0000-00-00', '2016-08-25', 'backcharge', 'medium', 147544302520, '', NULL, 'active', 0, '2016-09-07 06:55:25'),
(64, 52, 70, 'Test', 'Level 3 > Apartment 3.02', 'medium', NULL, '2016-08-25 00:00:00', 'builder', 'open', '0000-00-00', '2016-08-25', 'backcharge', 'medium', 147544302520, '', NULL, 'active', 0, '2016-09-07 06:55:25'),
(65, 53, 71, 'test common', 'Level 4 > Common Areas', 'medium', NULL, '2016-08-26 00:00:00', 'builder', 'open', '0000-00-00', '2016-08-26', 'backcharge', 'low', 147544302520, '', NULL, 'active', 0, '2016-09-07 06:55:25'),
(66, 54, 15, 'h test', 'Level 1', 'medium', NULL, '2016-08-26 00:00:00', 'builder', 'open', '0000-00-00', '2016-08-26', 'backcharge', 'low', 147544302520, '', NULL, 'active', 0, '2016-09-07 06:55:25'),
(67, 55, 22, 'h test 1', 'Level 2 > Apartment 2.01', 'medium', NULL, '2016-08-26 00:00:00', 'architect', 'open', '0000-00-00', '2016-08-11', 'none', 'low', 147544302520, '', NULL, 'active', 0, '2016-09-07 06:55:25'),
(68, 56, 16, 'h desc', 'Level 1 > Apartment 1.1', 'medium', NULL, '2016-08-26 00:00:00', 'builder', 'open', '0000-00-00', '2016-08-10', 'backcharge', 'medium', 147544302520, '', NULL, 'active', 0, '2016-09-07 06:55:25'),
(69, 57, 22, 'desc', 'Level 2 > Apartment 2.01', 'medium', NULL, '2016-08-26 00:00:00', 'architect', 'open', '0000-00-00', '2016-08-26', 'none', '', 147544302520, '', NULL, 'active', 0, '2016-09-07 06:55:25'),
(70, 58, 71, 'haresh test', 'Level 4 > Common Areas', 'medium', NULL, '2016-08-26 00:00:00', 'builder', 'open', '0000-00-00', '2017-04-27', 'none', 'low', 147544302520, '', NULL, 'active', 0, '2016-09-07 06:55:25'),
(71, 58, 71, 'haresh test', 'Level 4 > Common Areas', 'medium', NULL, '2016-08-26 00:00:00', 'builder', 'disputed', '0000-00-00', '2016-11-03', 'variation', 'high', 147544302520, '', NULL, 'active', 0, '2016-09-07 06:55:25'),
(72, 59, 22, 'This is a new test image adding working', 'Level 2 > Apartment 2.01', 'medium', NULL, '2016-08-26 00:00:00', 'builder', 'open', '0000-00-00', '2016-08-03', 'none', '', 147544302520, '', NULL, 'active', 0, '2016-09-07 06:55:25'),
(73, 59, 22, 'This is a new test image adding working', 'Level 2 > Apartment 2.01', 'medium', NULL, '2016-08-26 00:00:00', 'builder', 'open', '0000-00-00', '2016-08-30', 'none', 'select', 147544302520, '', NULL, 'active', 0, '2016-09-07 06:55:25'),
(77, 58, 71, 'haresh test', 'Level 4 &gt; Common Areas', 'medium', NULL, '0000-00-00 00:00:00', '', 'disputed', '0000-00-00', '2016-09-01', 'backcharge', 'medium', 147544302520, '', NULL, 'deleted', 0, '2016-09-07 06:55:25'),
(78, 58, 71, 'haresh test', 'Level 4 &gt; Common Areas', 'medium', NULL, '0000-00-00 00:00:00', '', 'disputed', '0000-00-00', '2016-08-27', 'backcharge', 'low', 147544302520, '', NULL, 'active', 0, '2016-09-07 06:55:25'),
(79, 60, 71, 'my testing', 'Level 4 > Common Areas', 'medium', NULL, '2016-08-28 00:00:00', 'builder', 'open', '0000-00-00', '2016-08-29', 'backcharge', 'low', 147544302520, '', NULL, 'active', 0, '2016-09-07 06:55:25'),
(80, 61, 44, 'testing21', ' > Apartment 1.2', 'medium', NULL, '2016-08-29 00:00:00', 'builder', 'open', '0000-00-00', '2014-11-28', 'backcharge', 'low', 147544302520, '', NULL, 'active', 0, '2016-09-07 06:55:25'),
(81, 62, 69, 'testing 212', 'Level 3 > Apartment 3.01', 'medium', NULL, '2016-08-29 00:00:00', 'builder', 'disputed', '0000-00-00', '2016-08-28', 'backcharge', 'low', 147544302520, '', NULL, 'active', 0, '2016-09-07 06:55:25'),
(82, 63, 2, 'Desc', ' > Apartment G.01', 'medium', NULL, '2016-09-02 00:00:00', 'builder', 'open', '0000-00-00', '2016-09-02', 'backcharge', 'low', 147544302520, '', NULL, 'active', 0, '2016-09-07 06:55:25'),
(83, 64, 2, 'Scratch in paint', 'Ground > Apartment G.01', 'medium', NULL, '2016-09-06 00:00:00', 'architect', 'open', '0000-00-00', '2016-09-06', 'none', '', 147544302520, '', NULL, 'active', 0, '2016-09-07 06:55:25'),
(84, 65, 71, 'Scratch in paint', 'Level 4 > Common Areas', 'medium', NULL, '2016-09-06 00:00:00', 'builder', 'open', '0000-00-00', '2016-09-06', 'none', '', 147544302520, '', NULL, 'active', 0, '2016-09-07 06:55:25'),
(85, 66, 69, 'Scratch in paint', 'Level 3 > Apartment 3.01', 'medium', NULL, '2016-09-07 00:00:00', 'builder', 'open', '0000-00-00', '2016-09-07', 'none', '', 147544302520, '', NULL, 'active', 0, '2016-09-07 06:55:01'),
(86, 67, 2, 'test1', 'Ground > Apartment G.01', 'medium', NULL, '2016-09-07 00:00:00', 'builder', 'disputed', '0000-00-00', '2016-09-07', 'backcharge', 'low', 147544302520, 'carpentry', NULL, 'active', 0, '2016-09-07 06:51:34'),
(87, 68, 69, 'Scratch in paint', 'Level 3 > Apartment 3.01', 'medium', NULL, '2016-09-07 00:00:00', 'builder', 'open', '0000-00-00', '2018-05-02', 'none', '', 14710163270, '', NULL, 'active', 0, '2016-09-07 11:54:39'),
(88, 69, 69, 'Desc2', ' > Apartment 3.01', 'medium', NULL, '2016-09-07 00:00:00', 'builder', 'disputed', '0000-00-00', '2016-09-07', 'backcharge', 'low', 147544302520, 'carpentry', NULL, 'active', 0, '2016-09-07 12:35:43'),
(89, 70, 16, 'Test desc1', 'Level 1 > Apartment 1.1', 'medium', NULL, '2016-09-09 00:00:00', 'superintendant', 'disputed', '0000-00-00', '2016-09-08', 'variation', 'low', 147069846340, '', NULL, 'active', 0, '2016-09-08 15:37:07'),
(90, 71, 71, 'Broken pipe leaking', 'Level 4 > Common Areas', 'medium', NULL, '2016-09-09 00:00:00', 'builder', 'open', '0000-00-00', '2016-09-09', 'none', '', 147448262990, '', NULL, 'active', 0, '2016-09-09 04:12:13'),
(91, 72, 71, 'Broken pipe leaking', 'Level 4 > Common Areas', 'medium', NULL, '2016-09-09 00:00:00', 'builder', 'open', '0000-00-00', '1970-01-01', 'none', '', 147448262990, '', NULL, 'active', 0, '2016-09-09 06:44:53'),
(92, 73, 69, 'test haresh', 'Level 3 > Apartment 3.01', 'medium', NULL, '2016-09-09 00:00:00', 'builder', 'disputed', '0000-00-00', '2016-09-09', 'backcharge', 'low', 147544302520, '', NULL, 'active', 0, '2016-09-09 09:20:47'),
(93, 74, 70, 'test me 11', ' > Apartment 3.02', 'medium', NULL, '2016-09-09 00:00:00', 'builder', 'disputed', '0000-00-00', '2016-09-09', 'variation', 'low', 147544302520, '', NULL, 'active', 0, '2016-09-09 09:46:04'),
(94, 75, 16, 'add new test', 'Level 1 > Apartment 1.1', 'medium', NULL, '2016-09-09 00:00:00', 'builder', 'open', '0000-00-00', '1970-01-01', 'none', '', 147544302520, '', NULL, 'active', 0, '2016-09-09 12:01:37'),
(95, 76, 71, 'last test', 'Level 4 > Common Areas', 'medium', NULL, '2016-09-09 00:00:00', 'builder', 'open', '0000-00-00', '2016-09-09', 'backcharge', '', 147069846340, '', NULL, 'active', 0, '2016-09-12 02:59:27'),
(96, 77, 71, 'Broken pipe leaking', 'Level 4 > Common Areas', 'medium', NULL, '2016-09-09 00:00:00', 'builder', 'open', '0000-00-00', '2016-09-08', 'none', '', 147448262990, '', NULL, 'active', 0, '2016-09-09 13:42:03'),
(97, 78, 27, 'Broken pipe leaking', 'Level 2 > Apartment 2.01 > Ensuite', 'medium', NULL, '2016-09-12 00:00:00', 'builder', 'open', '0000-00-00', '2016-12-12', 'none', '', 147448262990, '', NULL, 'active', 0, '2016-09-12 03:13:07'),
(98, 79, 71, 'Broken pipe leaking', 'Level 4 > Common Areas', 'medium', NULL, '2016-09-12 00:00:00', 'builder', 'open', '0000-00-00', '2016-09-12', 'none', '', 147448262990, '', NULL, 'active', 0, '2016-09-12 12:35:29'),
(99, 79, 71, 'Broken pipe leaking', 'Level 4 > Common Areas', 'medium', NULL, '2016-09-12 00:00:00', 'builder', 'open', '0000-00-00', '1970-01-01', 'none', 'select', 147515362130, '', NULL, 'active', 0, '2016-09-12 12:35:29'),
(100, 80, 2, 'test', 'Ground > Apartment G.01', 'medium', NULL, '2016-09-13 00:00:00', 'builder', 'open', '0000-00-00', '2016-09-13', 'none', '', 147839541640, '', NULL, 'active', 0, '2016-09-13 04:14:19'),
(101, 80, 2, 'test', 'Ground > Apartment G.01', 'medium', NULL, '2016-09-13 00:00:00', 'builder', 'open', '0000-00-00', '1970-01-01', 'none', 'select', 147544302520, '', NULL, 'active', 0, '2016-09-13 04:14:19'),
(102, 81, 16, 'Test Desc', 'Level 1 > Apartment 1.1', 'medium', NULL, '2016-09-13 00:00:00', 'builder', 'open', '0000-00-00', '2016-09-07', 'none', '', 147544302520, '', NULL, 'active', 0, '2016-09-13 05:32:29'),
(103, 82, 27, 'Broken pipe leaking', 'Level 2 > Apartment 2.01 > Ensuite', 'medium', NULL, '2016-09-13 00:00:00', 'builder', 'open', '0000-00-00', '2016-09-06', 'none', '', 147448262990, '', NULL, 'active', 0, '2016-09-13 12:22:30'),
(104, 82, 27, 'Broken pipe leaking', 'Level 2 > Apartment 2.01 > Ensuite', 'medium', NULL, '2016-09-13 00:00:00', 'builder', 'open', '0000-00-00', '2019-08-08', 'none', 'select', 147515362130, '', NULL, 'active', 0, '2016-09-13 12:22:30'),
(105, 83, 22, 'Blah', 'Level 2 > Apartment 2.01', 'medium', NULL, '2016-09-13 00:00:00', 'builder', 'open', '0000-00-00', '2016-09-13', 'none', '', 147448262990, '', NULL, 'active', 0, '2016-09-13 12:30:48'),
(106, 49, 3, 'haresh test 25-08', 'Ground &gt; Apartment G.01 &gt; Bedroom 1', 'medium', NULL, '0000-00-00 00:00:00', '', 'open', '0000-00-00', '2016-09-13', 'none', 'select', 147515362130, '', NULL, 'active', 0, '2016-09-13 12:32:35'),
(107, 91, 44, 'asdsaf', 'assaf', 'medium', NULL, '2016-10-13 00:00:00', 'builder', 'open', NULL, '2016-09-28', 'none', '', 147017872811, NULL, NULL, 'active', 0, '2016-10-13 06:56:18'),
(108, 93, 44, 'haresg1212sssssss', 'Level 1 > Apartment 1.1 > Apartment 1.2', 'medium', NULL, '2016-10-13 00:00:00', 'builder', 'open', '2016-10-13', '2016-09-28', 'none', '', 147017872811, NULL, NULL, 'active', 0, '2016-10-13 06:57:35'),
(109, 94, 44, 'haresg1212sssssss', 'Level 1 > Apartment 1.1 > Apartment 1.2', 'medium', NULL, '2016-10-13 00:00:00', 'builder', 'open', '2016-10-13', '2016-09-28', 'none', '', 147017872811, NULL, NULL, 'active', 0, '2016-10-13 06:58:31'),
(110, 95, 44, 'haresg1212sssssss', 'Level 1 > Apartment 1.1 > Apartment 1.2', 'medium', NULL, '2016-10-13 00:00:00', 'builder', 'open', '2016-10-13', '2016-09-28', 'none', '', 147017872811, NULL, NULL, 'active', 0, '2016-10-13 06:58:52'),
(111, 99, 2, 'test1234', 'Ground > Apartment G.01', 'medium', NULL, '2016-10-13 00:00:00', 'builder', 'open', '2016-10-13', '2016-10-13', 'none', '', 147069846340, NULL, NULL, 'active', 0, '2016-10-13 10:02:22'),
(112, 100, 2, 'test1234', 'Ground > Apartment G.01', 'medium', NULL, '2016-10-13 00:00:00', 'builder', 'open', '2016-10-13', '2016-10-13', 'none', '', 147069846340, NULL, NULL, 'active', 0, '2016-10-13 10:05:35'),
(113, 101, 2, 'test1234', 'Ground > Apartment G.01', 'medium', NULL, '2016-10-13 00:00:00', 'builder', 'open', '2016-10-13', '2016-10-13', 'none', '', 147448262990, '', NULL, 'active', 0, '2016-10-14 06:05:32'),
(114, 101, 2, 'test1234', 'Ground > Apartment G.01', 'medium', NULL, '2016-10-14 00:00:00', '1', 'open', NULL, '2016-10-14', 'none', '', 147017872811, '', NULL, 'active', 0, '2016-10-14 06:12:16'),
(115, 101, 2, 'test1234', 'Ground > Apartment G.01', 'medium', NULL, '2016-10-14 00:00:00', '1', 'disputed', NULL, '2016-10-28', 'backcharge', 'medium', 147544302520, '', NULL, 'active', 0, '2016-10-14 06:12:17');

-- --------------------------------------------------------

--
-- Table structure for table `issued_to`
--

CREATE TABLE `issued_to` (
  `id` bigint(25) NOT NULL,
  `issuedto_id` bigint(25) NOT NULL,
  `master_issuedto_id` bigint(25) NOT NULL,
  `master_contact_id` bigint(25) NOT NULL,
  `project_id` bigint(25) NOT NULL,
  `name` varchar(255) NOT NULL,
  `company` varchar(250) NOT NULL,
  `activity` varchar(100) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `email` varchar(250) NOT NULL,
  `tag` varchar(250) NOT NULL,
  `trade` varchar(255) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` bigint(25) NOT NULL,
  `modified_on` datetime NOT NULL,
  `original_modified_date` datetime NOT NULL,
  `modified_by` bigint(25) NOT NULL,
  `resource_type` varchar(100) NOT NULL DEFAULT 'Webserver',
  `row_status` varchar(20) NOT NULL DEFAULT 'active',
  `is_default` tinyint(2) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `issued_to`
--

INSERT INTO `issued_to` (`id`, `issuedto_id`, `master_issuedto_id`, `master_contact_id`, `project_id`, `name`, `company`, `activity`, `phone`, `email`, `tag`, `trade`, `created_on`, `created_by`, `modified_on`, `original_modified_date`, `modified_by`, `resource_type`, `row_status`, `is_default`) VALUES
(1, 147220519210, 147220512240, 147220529010, 5, 'Andrew Lamrock', 'Wiseworking', '', '400135135', 'andrew@defectid.com', 'location1; location1;', 'Sample Trade 1; Sample Trade 2;', '2016-09-02 09:48:19', 0, '2016-09-01 07:27:31', '0000-00-00 00:00:00', 0, 'Webserver', 'deleted', 1),
(2, 147220526210, 147220512240, 147220527450, 5, 'Steve Pump', 'Wiseworking', '', '400135145', 'test@defectid.com', 'location2; location2;', 'Sample Trade 2;', '2016-09-02 09:48:19', 0, '2016-09-01 07:30:04', '0000-00-00 00:00:00', 0, 'Webserver', 'deleted', 1),
(3, 147220521230, 147220524800, 147220526850, 5, 'John  James', 'Marco Polo', '', '400135145', 'test@defectid.com', 'location3; location3;', 'Sample Trade 3;', '2016-09-02 09:48:19', 0, '2016-09-01 07:27:32', '0000-00-00 00:00:00', 0, 'Webserver', 'deleted', 1),
(4, 147220526740, 147220523100, 147220526140, 5, 'Billy Johns', 'Smith Plumbing', 'plumbing', '390056010', 'info@spbjplumbing.com.au', 'location5; location5;', 'Sample Trade 5;', '2016-09-02 09:55:21', 0, '2016-09-07 09:38:41', '2016-09-07 09:38:41', 0, 'Webserver', 'deleted', 1),
(5, 147220529620, 147220528170, 147220525640, 5, 'Justin Smith', 'ABC Kitchens', 'electrical,fire services', '390066111', 'accounts@abckitjs.com.au', 'location7; location7;', 'Sample Trade 7;', '2016-09-02 09:02:10', 0, '2016-09-07 07:42:24', '2016-09-07 07:42:24', 0, 'Webserver', 'deleted', 1),
(6, 147220528740, 147220528170, 147220523220, 5, 'Andrew Lamrock', 'ABC Kitchens', '', '400531531', 'mail@123concretesp.com.au', 'location8; location8;', 'Sample Trade 8;', '2016-09-02 08:55:28', 0, '2016-09-01 07:27:32', '0000-00-00 00:00:00', 0, 'Webserver', 'deleted', 1),
(7, 147220522430, 147220527820, 14722052580, 5, 'Steve Pump', 'Adams Clearing', 'cleaning', '400531531', 'mail@123concretesp.com.au', 'location9; location9;', 'Sample Trade 9;', '2016-09-02 09:44:23', 0, '2016-09-12 10:41:11', '2016-09-12 10:41:11', 0, 'Webserver', 'deleted', 1),
(8, 147273037750, 14727303590, 147273031640, 5, 'test', 'test', '', '', '', '', '', '2016-09-02 09:48:19', 0, '2016-09-01 08:55:03', '0000-00-00 00:00:00', 0, 'Webserver', 'deleted', 1),
(9, 147307489480, 147307481630, 147307482970, 5, 'Test1', 'Test Comp1', '', '1236547890', 'test@yopmail.com', 'Ground; Apartment G.01;', 'Sample Trade 1;', '2016-09-02 09:48:19', 0, '2016-09-01 09:52:28', '0000-00-00 00:00:00', 0, 'Webserver', 'deleted', 1),
(10, 147307488530, 147307489310, 147307487290, 5, 'Test2', 'Test Comp 2', '', '1111111111', 'test2@yopmail.com', 'Level 1;', 'Sample Trade 2;', '2016-09-02 09:48:19', 0, '2016-09-01 09:52:28', '0000-00-00 00:00:00', 0, 'Webserver', 'deleted', 1),
(11, 147307483210, 147307486230, 147307482250, 5, 'Test3', 'Test Comp 3', '', '40011135145', 'test3@yopmail.com', 'Level 2;', 'Sample Trade 3;', '2016-09-02 09:55:21', 0, '2016-09-01 09:52:28', '0000-00-00 00:00:00', 0, 'Webserver', 'deleted', 1),
(12, 147308845140, 147308845120, 147308845770, 5, 'QA Contact name 01', 'FX Test QA 01', '', '42121231233', 'cc@yopmail.com', 'Basement; Ground; Level1;', 'test trade 123;', '2016-09-02 09:48:19', 0, '2016-09-01 09:54:44', '0000-00-00 00:00:00', 0, 'Webserver', 'deleted', 1),
(13, 147001113130, 147001113240, 147001119620, 5, 'I test', 'I test', 'brickwork', '', '', '', '', '2016-09-02 09:48:19', 0, '2016-09-02 05:08:31', '0000-00-00 00:00:00', 0, 'Webserver', 'deleted', 1),
(14, 147069846340, 147220528170, 147220525640, 5, 'Justin Smith', 'ABC Kitchens', 'electrical,fire services', '390066111', 'accounts@abckitjs.com.au', 'location7; location7;', 'Sample Trade 7;', '2016-09-02 07:03:04', 0, '2016-09-07 07:42:24', '2016-09-07 07:42:24', 0, 'Webserver', 'active', 0),
(15, 147096392300, 147220527820, 14722052580, 5, 'Steve Pump', 'Adams Clearing', 'cleaning', '400531531', 'mail@123concretesp.com.au', 'location9; location9;', 'Sample Trade 9;', '2016-09-02 09:46:44', 0, '2016-09-12 10:41:11', '2016-09-12 10:41:11', 0, 'Webserver', 'deleted', 0),
(16, 14710163270, 147220527820, 14722052580, 5, 'Steve Pump', 'Adams Clearing', 'cleaning', '400531531', 'mail@123concretesp.com.au', 'location9; location9;', 'Sample Trade 9;', '2016-09-02 07:56:03', 0, '2016-09-12 10:41:11', '2016-09-12 10:41:11', 0, 'Webserver', 'active', 0),
(17, 147324745010, 147001113240, 147001119620, 5, 'I test', 'I test', '', '', '', '', '', '2016-09-04 23:40:35', 0, '2016-09-05 09:41:14', '0000-00-00 00:00:00', 0, 'Webserver', 'deleted', 0),
(18, 147572799310, 14727303590, 147273031640, 5, 'test', 'test', '', '', '', '', '', '2016-09-05 06:34:03', 0, '2016-09-05 04:34:39', '0000-00-00 00:00:00', 0, 'Webserver', 'deleted', 0),
(19, 14757279450, 147307481630, 147307482970, 5, 'Test1', 'Test Comp1', '', '1236547890', 'test@yopmail.com', 'Ground; Apartment G.01;', 'Sample Trade 1;', '2016-09-05 06:34:03', 0, '2016-09-05 04:34:39', '0000-00-00 00:00:00', 0, 'Webserver', 'deleted', 0),
(20, 147572802080, 147307486230, 147307482250, 5, 'Test3', 'Test Comp 3', '', '40011135145', 'test3@yopmail.com', 'Level 2;', 'Sample Trade 3;', '2016-09-05 06:34:03', 0, '2016-09-05 04:34:40', '0000-00-00 00:00:00', 0, 'Webserver', 'deleted', 0),
(21, 147492732890, 147220527820, 147492736570, 5, 'Trevor', ' Adams Clearing', 'cleaning', '1234567890', '', '', '', '2016-09-06 06:07:53', 0, '2016-09-12 10:40:58', '2016-09-12 10:40:58', 0, 'Webserver', 'active', 1),
(22, 147544302520, 147544305220, 147544301900, 5, 'H test', 'H test', 'carpentry,cleaning', '1542514521', 'test@dev.com', '', '', '2016-09-06 07:33:50', 0, '2016-09-06 07:34:13', '2016-09-06 07:34:13', 0, 'Webserver', 'active', 1),
(23, 147448262990, 147220523100, 147220526140, 5, 'Billy Johns', 'Smith Plumbing', 'plumbing', '390056010', 'info@spbjplumbing.com.au', 'location5; location5;', 'Sample Trade 5;', '2016-09-07 08:40:26', 0, '2016-09-07 09:38:41', '2016-09-07 09:38:41', 0, 'Webserver', 'active', 0),
(24, 147515136740, 147515132400, 147515134050, 5, 'Tom Hardy', 'Structural Steel', 'Structural steel', '', '', '', 'Structural Steel;', '2016-09-12 03:38:16', 0, '2016-09-12 01:38:33', '0000-00-00 00:00:00', 0, 'Webserver', 'deleted', 1),
(25, 147515362130, 147515367640, 147515362290, 5, 'Tom Hardy', 'Structural Steel', 'Structural steel', '', '', '', 'Structural Steel;', '2016-09-12 01:38:56', 0, '2016-09-12 01:38:56', '2016-09-12 01:38:56', 0, 'Webserver', 'active', 1),
(26, 147839541640, 147839541470, 147839547940, 5, 'Vin Samatino', 'Meridian Concrete', 'concreting', '', '', '', '', '2016-09-12 10:39:14', 0, '2016-09-12 10:39:14', '2016-09-12 10:39:14', 0, 'Webserver', 'active', 1),
(27, 147017872811, 147017879591, 147017871311, 5, 'hareshjj', 'hareshjj', 'carpentry,cleaning', '5252252552', 'kumarssss.haresh@fxbytes.com', '', '', '2016-10-10 05:46:27', 1, '2016-10-10 11:16:27', '2016-10-10 11:16:27', 1, 'Webserver', 'active', 1),
(28, 147440291831, 147017879591, 147440295211, 5, 'my 25252ssssssffff525252testfffass', 'hareshjj', 'brickwork', '425252525252', 'eqweqqrkumar.haresh@fxbytes.com', '', NULL, '2016-10-11 21:17:09', 1, '2016-10-12 03:54:57', '2016-10-12 03:54:57', 1, 'Webserver', 'active', 1);

-- --------------------------------------------------------

--
-- Table structure for table `issued_to--`
--

CREATE TABLE `issued_to--` (
  `id` int(11) NOT NULL,
  `project_id` int(11) DEFAULT NULL,
  `issue_to_company_id` int(11) NOT NULL,
  `contact_name` varchar(100) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `tags` varchar(255) NOT NULL,
  `row_status` varchar(25) NOT NULL DEFAULT 'active',
  `modified_by` int(11) DEFAULT NULL,
  `modified_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `issued_to--`
--

INSERT INTO `issued_to--` (`id`, `project_id`, `issue_to_company_id`, `contact_name`, `phone`, `email`, `tags`, `row_status`, `modified_by`, `modified_on`) VALUES
(1, 5, 1, 'testdemo', '121451251251', 'testdemo@gmail.com', 'level1;level2;', 'active', NULL, '2016-08-16 04:39:41'),
(2, 5, 2, 'testdemo', '121451251251', 'testdemo@gmail.com', 'level1;level2;', 'active', NULL, '2016-08-16 04:39:41'),
(3, 0, 3, 'testdemo', '121451251251', 'testdemo@gmail.com', 'level1;level2;', 'active', NULL, '2016-08-29 10:20:20');

-- --------------------------------------------------------

--
-- Table structure for table `issueTo_company`
--

CREATE TABLE `issueTo_company` (
  `id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `number_of_users` int(11) NOT NULL,
  `row_status` varchar(50) NOT NULL DEFAULT 'active',
  `modified_by` int(11) DEFAULT NULL,
  `modified_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `issueTo_company`
--

INSERT INTO `issueTo_company` (`id`, `project_id`, `name`, `number_of_users`, `row_status`, `modified_by`, `modified_on`) VALUES
(1, 5, 'issue to company', 0, 'active', NULL, '2016-08-16 04:40:13'),
(2, 5, 'company', 0, 'active', NULL, '2016-08-16 04:40:13'),
(3, 5, 'test', 0, 'active', NULL, '2016-08-16 04:40:13');

-- --------------------------------------------------------

--
-- Table structure for table `location`
--

CREATE TABLE `location` (
  `id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL COMMENT 'this is a hierarchy structure so every item needs a parent',
  `label` varchar(64) DEFAULT NULL COMMENT 'this is the label or description of the location. eg level 1 or kitchen',
  `order_id` int(11) NOT NULL,
  `row_status` varchar(32) NOT NULL DEFAULT 'active',
  `created_date` timestamp NULL DEFAULT NULL,
  `modified_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modified_by` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `location`
--

INSERT INTO `location` (`id`, `project_id`, `parent_id`, `label`, `order_id`, `row_status`, `created_date`, `modified_on`, `modified_by`, `created_by`) VALUES
(1, 5, 0, 'Ground', 1, 'active', '2016-08-03 08:25:34', '2016-08-19 04:51:31', 1, 1),
(2, 5, 1, 'Apartment G.01', 0, 'active', '2016-08-03 08:25:34', '2016-08-04 08:32:26', 1, 1),
(3, 5, 2, 'Bedroom 1', 0, 'active', '2016-08-03 08:25:34', '2016-08-03 08:25:34', 1, 1),
(4, 5, 3, 'Walk in robe', 0, 'active', '2016-08-03 08:25:34', '2016-08-03 08:25:34', 1, 1),
(5, 5, 3, 'Bathroom', 0, 'active', '2016-08-03 08:25:34', '2016-08-03 08:25:34', 1, 1),
(6, 5, 5, 'Untitled Location', 0, 'active', '2016-08-03 08:25:34', '2016-08-03 08:25:34', 1, 1),
(7, 5, 3, 'Balcony', 0, 'active', '2016-08-03 08:25:34', '2016-08-03 08:25:34', 1, 1),
(8, 5, 2, 'Bedroom 2', 0, 'active', '2016-08-03 08:25:34', '2016-08-03 08:25:34', 1, 1),
(9, 5, 2, 'Ensuite', 0, 'active', '2016-08-03 08:25:34', '2016-08-03 08:25:34', 1, 1),
(10, 5, 2, 'Bathroom', 0, 'active', '2016-08-03 08:25:34', '2016-08-03 08:25:34', 1, 1),
(11, 5, 2, 'Kitchen', 0, 'deleted', '2016-08-03 08:25:34', '2016-08-04 06:39:49', 1, 1),
(12, 5, 11, 'Washroom', 0, 'deleted', '2016-08-03 08:25:34', '2016-08-04 06:39:49', 1, 1),
(13, 5, 2, 'Dinning', 0, 'deleted', '2016-08-03 08:25:34', '2016-08-05 09:35:51', 1, 1),
(14, 5, 2, 'Balcony 2', 0, 'deleted', '2016-08-03 08:25:34', '2016-08-04 06:39:20', 1, 1),
(15, 5, 0, 'Level 1', 2, 'active', '2016-08-03 08:25:34', '2016-08-19 04:51:31', 1, 1),
(16, 5, 15, 'Apartment 1.1', 1, 'active', '2016-08-03 08:25:34', '2016-08-19 04:52:21', 1, 1),
(17, 5, 16, 'Bedroom 1', 0, 'active', '2016-08-03 08:25:34', '2016-08-03 08:25:34', 1, 1),
(18, 5, 17, 'Walk in robe', 0, 'active', '2016-08-03 08:25:34', '2016-08-03 08:25:34', 1, 1),
(19, 5, 17, 'Bathroom', 0, 'deleted', '2016-08-03 08:25:34', '2016-08-04 08:57:09', 1, 1),
(20, 5, 17, 'Balcony', 0, 'active', '2016-08-03 08:25:35', '2016-08-03 08:25:35', 1, 1),
(21, 5, 0, 'Level 2', 3, 'active', '2016-08-03 08:25:35', '2016-08-11 10:44:24', 1, 1),
(22, 5, 21, 'Apartment 2.01', 0, 'active', '2016-08-03 08:25:35', '2016-08-04 05:18:50', 1, 1),
(23, 5, 22, 'Bedroom 2', 0, 'active', '2016-08-03 08:25:35', '2016-08-03 08:25:35', 1, 1),
(24, 5, 23, 'Walk in robe', 0, 'active', '2016-08-03 08:25:35', '2016-08-03 08:25:35', 1, 1),
(25, 5, 23, 'Bathroom', 0, 'active', '2016-08-03 08:25:35', '2016-08-03 08:25:35', 1, 1),
(26, 5, 23, 'Balcony', 0, 'active', '2016-08-03 08:25:35', '2016-08-03 08:25:35', 1, 1),
(27, 5, 22, 'Ensuite', 0, 'active', '2016-08-03 08:25:35', '2016-08-03 08:25:35', 1, 1),
(28, 5, 22, 'Bathroom', 0, 'active', '2016-08-03 08:25:35', '2016-08-03 08:25:35', 1, 1),
(29, 5, 22, 'Kitchen', 0, 'active', '2016-08-03 08:25:35', '2016-08-03 08:25:35', 1, 1),
(30, 5, 29, 'Washroom', 0, 'active', '2016-08-03 08:25:35', '2016-08-03 08:25:35', 1, 1),
(31, 5, 22, 'Dinning', 0, 'deleted', '2016-08-03 08:25:35', '2016-08-03 08:45:37', 1, 1),
(32, 5, 21, 'Level 1 (Copy 1)', 0, 'deleted', '2016-08-03 08:46:53', '2016-08-04 08:57:41', 1, 1),
(33, 5, 32, 'Apartment 2', 0, 'deleted', '2016-08-03 08:46:53', '2016-08-04 08:57:41', 1, 1),
(34, 5, 33, 'Bedroom 1', 0, 'active', '2016-08-03 08:46:53', '2016-08-03 10:46:37', NULL, 1),
(35, 5, 34, 'Walk in robe', 0, 'active', '2016-08-03 08:46:53', '2016-08-03 10:46:37', NULL, 1),
(36, 5, 34, 'Bathroom', 0, 'active', '2016-08-03 08:46:53', '2016-08-03 10:46:37', NULL, 1),
(37, 5, 34, 'Balcony', 0, 'active', '2016-08-03 08:46:53', '2016-08-03 10:46:37', NULL, 1),
(38, 5, 1, 'test11', 0, 'deleted', '2016-08-03 10:20:20', '2016-08-04 01:58:29', 1, 1),
(39, 5, 17, 'Ensuite', 0, 'active', '2016-08-04 08:57:01', '2016-08-04 08:57:01', 1, 1),
(40, 5, 16, 'Bedroom 1 (Copy 1)', 0, 'active', '2016-08-04 08:58:14', '2016-08-03 22:57:57', NULL, 1),
(41, 5, 40, 'Walk in robe', 0, 'active', '2016-08-04 08:58:14', '2016-08-03 22:57:57', NULL, 1),
(42, 5, 40, 'Balcony', 0, 'active', '2016-08-04 08:58:14', '2016-08-03 22:57:57', NULL, 1),
(43, 5, 40, 'Ensuite', 0, 'active', '2016-08-04 08:58:14', '2016-08-03 22:57:57', NULL, 1),
(44, 5, 15, 'Apartment 1.2', 2, 'active', '2016-08-04 08:58:50', '2016-08-19 04:52:21', NULL, 1),
(45, 5, 44, 'Bedroom 1', 0, 'active', '2016-08-04 08:58:50', '2016-08-03 22:58:34', NULL, 1),
(46, 5, 44, 'Bedroom 1 (Copy 1)', 0, 'active', '2016-08-04 08:58:50', '2016-08-03 22:58:34', NULL, 1),
(47, 5, 45, 'Walk in robe', 0, 'active', '2016-08-04 08:58:50', '2016-08-03 22:58:34', NULL, 1),
(48, 5, 45, 'Balcony', 0, 'active', '2016-08-04 08:58:50', '2016-08-03 22:58:34', NULL, 1),
(49, 5, 45, 'Ensuite', 0, 'active', '2016-08-04 08:58:50', '2016-08-03 22:58:34', NULL, 1),
(50, 5, 46, 'Walk in robe', 0, 'active', '2016-08-04 08:58:50', '2016-08-03 22:58:34', NULL, 1),
(51, 5, 46, 'Balcony', 0, 'active', '2016-08-04 08:58:50', '2016-08-03 22:58:34', NULL, 1),
(52, 5, 46, 'Ensuite', 0, 'active', '2016-08-04 08:58:50', '2016-08-03 22:58:34', NULL, 1),
(53, 5, 15, 'Apartment 3', 0, 'deleted', '2016-08-04 01:34:42', '2016-08-04 01:34:48', 1, 1),
(54, 5, 21, 'test', 0, 'deleted', '2016-08-04 06:48:26', '2016-08-08 02:48:44', 1, 1),
(55, 5, 1, 'Test', 0, 'deleted', '2016-08-05 09:35:11', '2016-08-05 09:35:56', 1, 1),
(56, 5, 0, 'test location', 0, 'deleted', '2016-08-05 02:39:07', '2016-08-05 03:00:48', 1, 1),
(57, 5, 0, 'my test', 0, 'deleted', '2016-08-05 03:00:59', '2016-08-05 03:08:53', 1, 1),
(58, 5, 0, 'test location', 0, 'deleted', '2016-08-05 03:09:20', '2016-08-08 02:48:23', 1, 1),
(59, 5, 0, 'Level 3', 4, 'active', '2016-08-05 04:40:42', '2016-08-11 10:44:24', 1, 1),
(60, 5, 0, 'wwwww', 0, 'deleted', '2016-08-05 04:42:53', '2016-08-05 04:54:51', 1, 1),
(61, 5, 0, 'Level 4', 5, 'active', '2016-08-06 12:07:43', '2016-08-11 10:44:25', 1, 1),
(62, 5, 0, 'htest', 0, 'deleted', '2016-08-08 05:35:56', '2016-08-08 02:48:19', 1, 1),
(63, 9, 0, 'Apartments', 0, 'active', '2016-08-17 04:57:46', '2016-08-17 04:57:46', 1, 1),
(64, 9, 0, 'Podium', 0, 'active', '2016-08-17 04:57:57', '2016-08-17 04:57:57', 1, 1),
(65, 9, 64, 'Ground Floor', 0, 'active', '2016-08-17 04:58:20', '2016-08-17 04:58:20', 1, 1),
(66, 9, 64, 'Level 01', 0, 'active', '2016-08-17 04:58:31', '2016-08-17 04:58:31', 1, 1),
(67, 9, 63, 'Level 03', 0, 'active', '2016-08-17 04:58:42', '2016-08-17 04:58:42', 1, 1),
(68, 9, 63, 'Level 04', 0, 'active', '2016-08-17 04:58:47', '2016-08-17 04:58:47', 1, 1),
(69, 5, 59, 'Apartment 3.01', 0, 'active', '2016-08-24 03:45:40', '2016-08-24 03:45:40', 1, 1),
(70, 5, 59, 'Apartment 3.02', 0, 'active', '2016-08-25 03:40:46', '2016-08-25 03:40:46', 1, 1),
(71, 5, 61, 'Common Areas', 0, 'active', '2016-08-26 08:55:00', '2016-08-26 08:55:00', 1, 1),
(72, 5, 6, 'Balcony', 0, 'active', '2016-09-05 04:37:26', '2016-09-05 04:37:26', 1, 1),
(73, 5, 6, 'Wash area', 0, 'active', '2016-09-05 04:37:42', '2016-09-05 04:37:42', 1, 1),
(74, 5, 0, 'qwert', 0, 'active', '2016-10-10 00:14:06', '2016-10-10 00:14:06', 1, 1),
(75, 5, 0, 'sdsdf', 0, 'active', '2016-10-10 00:23:23', '2016-10-10 00:23:23', 1, 1),
(76, 5, 74, 'aaaaaaaa', 0, 'deleted', '2016-10-10 00:29:52', '2016-10-10 00:31:10', 1, 1),
(77, 5, 75, 'sdaf', 0, 'active', '2016-10-10 00:31:37', '2016-10-10 00:31:37', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `master_issued_to`
--

CREATE TABLE `master_issued_to` (
  `id` int(11) NOT NULL,
  `master_issuedto_id` bigint(25) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `company` varchar(255) NOT NULL,
  `activity` varchar(100) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `email` varchar(255) NOT NULL,
  `tag` text,
  `trade` varchar(255) NOT NULL,
  `modified_on` datetime NOT NULL,
  `original_modified_date` datetime NOT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `resource_type` varchar(255) DEFAULT 'Webserver',
  `row_status` varchar(20) NOT NULL DEFAULT 'active'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_issued_to`
--

INSERT INTO `master_issued_to` (`id`, `master_issuedto_id`, `name`, `company`, `activity`, `phone`, `email`, `tag`, `trade`, `modified_on`, `original_modified_date`, `modified_by`, `created_date`, `created_by`, `resource_type`, `row_status`) VALUES
(1, 147220512240, 'Andrew Lamrock', 'Wiseworking', '', '400135135', 'andrew@defectid.com', 'location1; location1;', 'Sample Trade 1; Sample Trade 2;', '2016-09-01 07:27:31', '2016-09-01 07:27:31', 0, '2016-09-01 07:27:31', 0, 'Webserver', 'active'),
(2, 147220524800, 'John  James', 'Marco Polo', '', '400135145', 'test@defectid.com', 'location3; location3;', 'Sample Trade 3;', '2016-09-01 07:27:32', '2016-09-01 07:27:32', 0, '2016-09-01 07:27:32', 0, 'Webserver', 'active'),
(3, 147220523100, 'Billy Johns', 'Smith Plumbing', '', '390056010', 'info@spbjplumbing.com.au', 'location5; location5;', 'Sample Trade 5;', '2016-09-01 07:27:32', '2016-09-01 07:27:32', 0, '2016-09-01 07:27:32', 0, 'Webserver', 'active'),
(4, 147220528170, 'Justin Smith', 'ABC Kitchens', '', '390066111', 'accounts@abckitjs.com.au', 'location7; location7;', 'Sample Trade 7;', '2016-09-01 07:27:32', '2016-09-01 07:27:32', 0, '2016-09-01 07:27:32', 0, 'Webserver', 'active'),
(5, 147220527820, 'Steve Pump', 'Adams Clearing', '', '400531531', 'mail@123concretesp.com.au', 'location9; location9;', 'Sample Trade 9;', '2016-09-01 07:27:32', '2016-09-01 07:27:32', 0, '2016-09-01 07:27:32', 0, 'Webserver', 'active'),
(6, 14727303590, 'test', 'test', '', '', '', '', '', '2016-09-01 08:55:03', '2016-09-01 08:55:03', 0, '2016-09-01 08:55:03', 0, 'Webserver', 'active'),
(7, 147307481630, 'Test1', 'Test Comp1', '', '1236547890', 'test@yopmail.com', 'Ground; Apartment G.01;', 'Sample Trade 1;', '2016-09-01 09:52:28', '2016-09-01 09:52:28', 0, '2016-09-01 09:52:28', 0, 'Webserver', 'active'),
(8, 147307489310, 'Test2', 'Test Comp 2', '', '1111111111', 'test2@yopmail.com', 'Level 1;', 'Sample Trade 2;', '2016-09-01 09:52:28', '2016-09-01 09:52:28', 0, '2016-09-01 09:52:28', 0, 'Webserver', 'active'),
(9, 147307486230, 'Test3', 'Test Comp 3', '', '40011135145', 'test3@yopmail.com', 'Level 2;', 'Sample Trade 3;', '2016-09-01 09:52:28', '2016-09-01 09:52:28', 0, '2016-09-01 09:52:28', 0, 'Webserver', 'active'),
(10, 147308845120, 'QA Contact name 01', 'FX Test QA 01', '', '42121231233', 'cc@yopmail.com', 'Basement; Ground; Level1;', 'test trade 123;', '2016-09-01 09:54:44', '2016-09-01 09:54:44', 0, '2016-09-01 09:54:44', 0, 'Webserver', 'active'),
(11, 147001113240, 'I test', 'I test', 'brickwork', '', '', '', '', '2016-09-02 05:08:31', '2016-09-02 05:08:31', 0, '2016-09-02 05:08:31', 0, 'Webserver', 'active'),
(12, 147544305220, 'H test', 'H test', 'carpentry,cleaning', '1542514521', 'test@dev.com', '', '', '2016-09-06 07:33:50', '2016-09-06 07:33:50', 0, '2016-09-06 07:33:50', 0, 'Webserver', 'active'),
(13, 147515132400, 'Tom Hardy', 'Structural Steel', 'Structural steel', '', '', '', 'Structural Steel;', '2016-09-12 01:38:33', '2016-09-12 01:38:33', 0, '2016-09-12 01:38:33', 0, 'Webserver', 'active'),
(14, 147515367640, 'Tom Hardy', 'Structural Steel', 'Structural steel', '', '', '', 'Structural Steel;', '2016-09-12 01:38:56', '2016-09-12 01:38:56', 0, '2016-09-12 01:38:56', 0, 'Webserver', 'active'),
(15, 147839541470, 'Vin Samatino', 'Meridian Concrete', 'concreting', '', '', '', '', '2016-09-12 10:39:14', '2016-09-12 10:39:14', 0, '2016-09-12 10:39:14', 0, 'Webserver', 'active'),
(16, 147017879591, 'hareshjj', 'hareshjj', 'carpentry,cleaning', '5252252552', 'kumarssss.haresh@fxbytes.com', '', '', '2016-10-10 11:16:27', '2016-10-10 11:16:27', 1, '2016-10-10 05:46:27', 1, 'Webserver', 'active');

-- --------------------------------------------------------

--
-- Table structure for table `master_issued_to_contact`
--

CREATE TABLE `master_issued_to_contact` (
  `id` int(11) NOT NULL,
  `contact_id` bigint(25) NOT NULL,
  `master_issuedto_id` bigint(25) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `company` varchar(255) NOT NULL,
  `activity` varchar(100) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `email` varchar(255) NOT NULL,
  `tag` text,
  `trade` varchar(255) DEFAULT NULL,
  `modified_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `original_modified_date` datetime NOT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `resource_type` varchar(255) DEFAULT 'Webserver',
  `row_status` varchar(20) NOT NULL DEFAULT 'active' COMMENT '0 for non delete ,1 for deleted',
  `is_default` tinyint(2) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_issued_to_contact`
--

INSERT INTO `master_issued_to_contact` (`id`, `contact_id`, `master_issuedto_id`, `name`, `company`, `activity`, `phone`, `email`, `tag`, `trade`, `modified_on`, `original_modified_date`, `modified_by`, `created_date`, `created_by`, `resource_type`, `row_status`, `is_default`) VALUES
(1, 147220529010, 147220512240, 'Andrew Lamrock', 'Wiseworking', '', '400135135', 'andrew@defectid.com', 'location1; location1;', 'Sample Trade 1; Sample Trade 2;', '2016-09-01 07:27:31', '2016-09-01 07:27:31', 0, '2016-09-01 07:27:31', 0, 'Webserver', 'active', 1),
(2, 147220527450, 147220512240, 'Steve Pump', 'Wiseworking', '', '400135145', 'test@defectid.com', 'location2; location2;', 'Sample Trade 2;', '2016-09-01 07:30:04', '2016-09-01 07:30:04', 0, NULL, 0, 'Webserver', 'active', 1),
(3, 147220526850, 147220524800, 'John  James', 'Marco Polo', '', '400135145', 'test@defectid.com', 'location3; location3;', 'Sample Trade 3;', '2016-09-01 07:27:32', '2016-09-01 07:27:32', 0, '2016-09-01 07:27:32', 0, 'Webserver', 'active', 1),
(4, 147220526140, 147220523100, 'Billy Johns', 'Smith Plumbing', 'plumbing', '390056010', 'info@spbjplumbing.com.au', 'location5; location5;', 'Sample Trade 5;', '2016-09-07 09:38:41', '2016-09-07 09:38:41', 0, '2016-09-01 07:27:32', 0, 'Webserver', 'active', 1),
(5, 147220525640, 147220528170, 'Justin Smith', 'ABC Kitchens', 'electrical,fire services', '390066111', 'accounts@abckitjs.com.au', 'location7; location7;', 'Sample Trade 7;', '2016-09-07 07:42:24', '2016-09-07 07:42:24', 0, '2016-09-01 07:27:32', 0, 'Webserver', 'active', 1),
(6, 147220523220, 147220528170, 'Andrew Lamrock', 'ABC Kitchens', '', '400531531', 'mail@123concretesp.com.au', 'location8; location8;', 'Sample Trade 8;', '2016-09-01 07:27:32', '2016-09-01 07:27:32', 0, NULL, 0, 'Webserver', 'active', 1),
(7, 14722052580, 147220527820, 'Steve Pump', 'Adams Clearing', 'cleaning', '400531531', 'mail@123concretesp.com.au', 'location9; location9;', 'Sample Trade 9;', '2016-09-12 10:41:11', '2016-09-12 10:41:11', 0, '2016-09-01 07:27:32', 0, 'Webserver', 'active', 1),
(8, 147273031640, 14727303590, 'test', 'test', '', '', '', '', '', '2016-09-01 08:55:03', '2016-09-01 08:55:03', 0, '2016-09-01 08:55:03', 0, 'Webserver', 'active', 1),
(9, 147307482970, 147307481630, 'Test1', 'Test Comp1', '', '1236547890', 'test@yopmail.com', 'Ground; Apartment G.01;', 'Sample Trade 1;', '2016-09-01 09:52:28', '2016-09-01 09:52:28', 0, '2016-09-01 09:52:28', 0, 'Webserver', 'active', 1),
(10, 147307487290, 147307489310, 'Test2', 'Test Comp 2', '', '1111111111', 'test2@yopmail.com', 'Level 1;', 'Sample Trade 2;', '2016-09-01 09:52:28', '2016-09-01 09:52:28', 0, '2016-09-01 09:52:28', 0, 'Webserver', 'active', 1),
(11, 147307482250, 147307486230, 'Test3', 'Test Comp 3', '', '40011135145', 'test3@yopmail.com', 'Level 2;', 'Sample Trade 3;', '2016-09-01 09:52:28', '2016-09-01 09:52:28', 0, '2016-09-01 09:52:28', 0, 'Webserver', 'active', 1),
(12, 147308845770, 147308845120, 'QA Contact name 01', 'FX Test QA 01', '', '42121231233', 'cc@yopmail.com', 'Basement; Ground; Level1;', 'test trade 123;', '2016-09-01 09:54:44', '2016-09-01 09:54:44', 0, '2016-09-01 09:54:44', 0, 'Webserver', 'active', 1),
(13, 147001119620, 147001113240, 'I test', 'I test', 'brickwork', '', '', '', '', '2016-09-02 05:08:31', '2016-09-02 05:08:31', 0, '2016-09-02 05:08:31', 0, 'Webserver', 'active', 1),
(14, 147492736570, 147220527820, 'Trevor', ' Adams Clearing', 'cleaning', '1234567890', '', '', '', '2016-09-12 10:40:58', '2016-09-12 10:40:58', 0, '2016-09-06 06:07:53', 0, 'Webserver', 'active', 0),
(15, 147544301900, 147544305220, 'H test', 'H test', 'carpentry,cleaning', '1542514521', 'test@dev.com', '', '', '2016-09-06 07:34:13', '2016-09-06 07:34:13', 0, '2016-09-06 07:33:50', 0, 'Webserver', 'active', 1),
(16, 147515134050, 147515132400, 'Tom Hardy', 'Structural Steel', 'Structural steel', '', '', '', 'Structural Steel;', '2016-09-12 01:38:33', '2016-09-12 01:38:33', 0, '2016-09-12 01:38:33', 0, 'Webserver', 'active', 1),
(17, 147515362290, 147515367640, 'Tom Hardy', 'Structural Steel', 'Structural steel', '', '', '', 'Structural Steel;', '2016-09-12 01:38:56', '2016-09-12 01:38:56', 0, '2016-09-12 01:38:56', 0, 'Webserver', 'active', 1),
(18, 147839547940, 147839541470, 'Vin Samatino', 'Meridian Concrete', 'concreting', '', '', '', '', '2016-09-12 10:39:14', '2016-09-12 10:39:14', 0, '2016-09-12 10:39:14', 0, 'Webserver', 'active', 1),
(19, 147017871311, 147017879591, 'hareshjj', 'hareshjj', 'carpentry,cleaning', '5252252552', 'kumarssss.haresh@fxbytes.com', '', '', '2016-10-10 05:46:27', '2016-10-10 11:16:27', 1, '2016-10-10 11:16:27', 1, 'Webserver', 'active', 1),
(20, 147439184041, 147017879591, 'my 25252525252testfffa', 'hareshjj', 'brickwork', '425252525252', 'eqweqqrkumar.haresh@fxbytes.com', '', NULL, '2016-10-11 21:15:18', '2016-10-12 02:45:18', 1, '2016-10-12 02:45:18', 1, 'Webserver', 'active', 0),
(21, 147439757971, 147017879591, 'my 25252ffff525252testfffa', 'hareshjj', 'brickwork', '425252525252', 'eqweqqrkumar.haresh@fxbytes.com', '', NULL, '2016-10-11 21:16:15', '2016-10-12 02:46:15', 1, '2016-10-12 02:46:15', 1, 'Webserver', 'active', 0),
(22, 147440295211, 147017879591, 'my 25252ssssssffff525252testfffass', 'hareshjj', 'brickwork', '425252525252', 'eqweqqrkumar.haresh@fxbytes.com', '', NULL, '2016-10-11 22:24:57', '2016-10-12 03:54:57', 1, '2016-10-12 02:47:09', 1, 'Webserver', 'active', 0);

-- --------------------------------------------------------

--
-- Table structure for table `project`
--

CREATE TABLE `project` (
  `id` int(11) NOT NULL,
  `code` varchar(16) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `type` varchar(32) NOT NULL DEFAULT 'other',
  `logo` varchar(512) DEFAULT NULL,
  `image` varchar(512) DEFAULT NULL,
  `description` text NOT NULL,
  `address1` text NOT NULL,
  `address2` text NOT NULL,
  `geo_code` varchar(100) NOT NULL,
  `suburb` varchar(100) NOT NULL,
  `postal_code` varchar(100) NOT NULL,
  `state` varchar(100) NOT NULL,
  `country` varchar(100) NOT NULL,
  `is_deleted` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1 for active,o for deleted',
  `created_by` int(11) NOT NULL,
  `created_on` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `project`
--

INSERT INTO `project` (`id`, `code`, `name`, `type`, `logo`, `image`, `description`, `address1`, `address2`, `geo_code`, `suburb`, `postal_code`, `state`, `country`, `is_deleted`, `created_by`, `created_on`, `modified_by`, `modified_on`) VALUES
(1, 'HH-ww', 'WiseWorking Test Project', 'test', 'http://dev.constructionid.com/php/images/probuild_logo.png', NULL, '', '', '', '', '', '', '', '', 1, 0, '0000-00-00 00:00:00', 0, '2016-10-06 04:38:16'),
(2, 'HH-ww-rr', 'WiseWorking Test Project', 'residential', 'http://dev.constructionid.com/php/images/projects.jpg', '1469707941.jpg', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy', '', '', '', '', '', '', '', 1, 0, '0000-00-00 00:00:00', 0, '2016-10-06 04:41:56'),
(3, '21452', 'WiseWorking Test Project', 'test', 'http://wiseworking.com.au/wp-content/uploads/WW-Logo-Header-Desktop.png', NULL, '', '', '', '', '', '', '', '', 1, 0, '0000-00-00 00:00:00', NULL, '2016-10-06 04:40:29'),
(4, 'HH-ww', 'WiseWorking Test Project', 'industrial', 'http://wiseworking.com.au/wp-content/uploads/WW-Logo-Header-Desktop.png', NULL, '', '', '', '', '', '', '', '', 1, 0, '0000-00-00 00:00:00', NULL, '2016-10-06 04:40:29'),
(5, '', 'Wiseworking demo project (test)', 'residential', 'http://dev.constructionid.com/php/images/wise_working.png', '1469782418.jpg', 'This project is a milestone project located in Melbourne CBD and design by Daryl Jackson Architects.  it is 42 levels mostly residential, (700 apartments and two levels of retail) and has high level finishes throughout the project.\r\n\r\nThe first level has an atrium which has a large reception area and conference centre, a hub of activity and changing the face of Melbourne CBD.', '', '', '', '', '', '', '', 1, 0, '0000-00-00 00:00:00', 0, '2016-10-06 04:40:29'),
(6, 'test', 'Test update haresh up', 'industrial', 'http://dev.constructionid.com/pms.php?sect=add_project', '1469723701.png', 'testtest', 'my test', 'daf', '', '4152', 'upadte', 'SA', '82', 1, 0, '0000-00-00 00:00:00', 1, '2016-10-07 05:24:13'),
(7, 'PC-01', 'Fxqa Project', 'residential', 'http://dev.constructionid.com/php/images/projects.jpg', '1469774263.jpeg', 'A topic sentence usually comes at the beginning of a paragraph; that is, it is usually the first sentence in a formal academic paragraph.  (Sometimes this is not true, but as you practice writing with this online lesson site, please keep to this rule unless you are instructed otherwise.)  Not only is a topic sentence the first sentence of a paragraph, but, more importantly, it is the most general sentence in a paragraph.  What does "most general" mean?  It means that there are not many details in the sentence, but that the sentence introduces an overall idea that you want to discuss later in the paragraph.', '', '', '', '', '', '', '', 1, 0, '0000-00-00 00:00:00', 0, '2016-10-06 04:42:39'),
(8, 'HH-ww-rr', 'Another Test', 'residential', 'http://wiseworking.com.au/wp-content/uploads/revslider/Avada_Page_Slider/slide_1.jpg', '1469797589.jpg', 'This is a test for Justin. Wiseworking will change the way that you work.', '', '', '', '', '', '', '', 1, 0, '0000-00-00 00:00:00', 0, '2016-10-06 04:40:29'),
(9, '1234', 'Empire Apartments', 'residential', 'http://wiseworking.com.au/wp-content/uploads/zoom-18133986-3.jpg', '1470728833.jpg', 'Elizabeth St Apartment Project located in Melbourne CBD the architect is Daryl Jackson and project spectifics include:\r\n487 Apartments \r\n61-storeys\r\n198m\r\n\r\nThe project will be completed by August 2017 and handed over in two seperable portions.', '', '', '', '', '', '', '', 1, 0, '0000-00-00 00:00:00', 0, '2016-10-06 04:40:29'),
(10, 'my testdsff', 'Adas', 'educational', 'https://www.nasa.gov/sites/default/files/styles/image_card_4x3_ratio/public/thumbnails/image/nh-scatteringmapcontext_06_29_16-v3-small4review.jpg?itok=fw6PtiG7', 'uploads/project/12049193_908428189227926_6734537963185321592_n6.jpg', 'asfsfaf', 'eretrtyy', 'aaaaaaaaaaaammmmmmm', 'fa', 'aaaaaaaaaaaammm', 'aaaaaaaaaaaam', 'SA', '82', 1, 1, '2016-10-07 02:16:32', 1, '2016-10-07 03:16:32'),
(11, 'my testdsff', 'Adas', 'educational', 'https://www.nasa.gov/sites/default/files/styles/image_card_4x3_ratio/public/thumbnails/image/nh-scatteringmapcontext_06_29_16-v3-small4review.jpg?itok=fw6PtiG7', 'uploads/project/12049193_908428189227926_6734537963185321592_n6.jpg', 'asfsfaf', 'eretrtyy', 'aaaaaaaaaaaammmmmmm', 'fa', 'aaaaaaaaaaaammm', 'aaaaaaaaaaaam', 'SA', '82', 1, 1, '2016-10-07 02:16:52', 1, '2016-10-07 03:16:52');

-- --------------------------------------------------------

--
-- Table structure for table `project_address_mapping`
--

CREATE TABLE `project_address_mapping` (
  `id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `address_id` int(11) NOT NULL,
  `row_status` int(11) NOT NULL DEFAULT '0',
  `modified_by` int(11) DEFAULT NULL,
  `modified_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `project_address_mapping`
--

INSERT INTO `project_address_mapping` (`id`, `project_id`, `address_id`, `row_status`, `modified_by`, `modified_on`) VALUES
(1, 1, 5, 0, 0, '2016-05-31 11:20:08'),
(2, 2, 6, 0, 0, '2016-05-31 11:22:43'),
(3, 3, 7, 0, 0, '2016-06-08 08:44:18'),
(4, 4, 8, 0, 0, '2016-06-08 08:48:21'),
(5, 5, 9, 0, 0, '2016-07-26 22:14:13'),
(6, 6, 10, 0, 0, '2016-07-28 16:34:56'),
(7, 7, 11, 0, 0, '2016-07-29 06:37:36'),
(8, 8, 12, 0, 0, '2016-07-29 13:06:21'),
(9, 9, 13, 0, 0, '2016-08-09 07:47:12');

-- --------------------------------------------------------

--
-- Table structure for table `project_alert`
--

CREATE TABLE `project_alert` (
  `id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `description` text NOT NULL,
  `type` varchar(25) NOT NULL,
  `row_status` varchar(50) NOT NULL DEFAULT 'active',
  `modified_by` int(11) DEFAULT NULL,
  `modified_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `project_alert`
--

INSERT INTO `project_alert` (`id`, `project_id`, `date`, `description`, `type`, `row_status`, `modified_by`, `modified_on`) VALUES
(1, 5, '2016-08-10 00:00:00', 'test', '3', 'deleted', NULL, '2016-08-10 05:04:25'),
(2, 5, '2016-08-11 00:00:00', 'Nothing to descriptt', '4', 'deleted', NULL, '2016-08-10 05:16:41'),
(3, 5, '2016-08-08 00:00:00', 'nothing ', '2', 'deleted', NULL, '2016-10-05 05:17:49'),
(4, 5, '2016-08-09 00:00:00', 'High wind', '1', 'active', NULL, '2016-08-10 06:38:15'),
(5, 9, '2016-08-09 00:00:00', 'High Winds', '1', 'active', NULL, '2016-08-10 22:45:32'),
(6, 5, '2016-10-12 17:01:00', 'qe', '1', 'active', NULL, '2016-10-04 11:56:11'),
(7, 5, '2016-10-05 08:15:00', 'dfgdfg', '1', 'active', NULL, '2016-10-05 02:46:01'),
(8, 5, '2016-10-17 00:00:00', 'dsfsdf', '1', 'deleted', NULL, '2016-10-05 05:22:16'),
(9, 5, '2016-10-12 08:46:00', 'safaf', '3', 'active', NULL, '2016-10-05 03:16:25'),
(10, 5, '2016-10-12 08:46:00', 'safaf', '3', 'active', NULL, '2016-10-05 03:16:27'),
(11, 5, '2016-10-12 08:46:00', 'safaf', '3', 'active', NULL, '2016-10-05 03:16:36'),
(12, 5, '2016-10-12 08:46:00', 'safaf', '3', 'deleted', NULL, '2016-10-05 05:25:48'),
(13, 5, '2016-10-12 08:47:00', 'fas', '1', 'active', NULL, '2016-10-05 03:17:35'),
(14, 5, '2016-10-12 08:47:00', 'fas', '1', 'active', NULL, '2016-10-05 03:18:36'),
(15, 5, '2016-10-19 08:49:00', 'ytry', '1', 'deleted', NULL, '2016-10-05 05:18:48'),
(16, 5, '2016-10-18 08:50:00', 'aasda', '1', 'deleted', NULL, '2016-10-05 05:17:59'),
(17, 5, '2016-10-12 08:54:00', 'asff', '3', 'active', NULL, '2016-10-05 03:24:56'),
(18, 5, '2016-10-26 08:57:00', 'saasf', '1', 'deleted', NULL, '2016-10-05 05:26:29'),
(19, 5, '2016-10-12 08:58:00', 'qwqwr', '1', 'active', NULL, '2016-10-05 03:28:59'),
(20, 5, '2016-10-19 08:59:00', 'asfa', '3', 'deleted', NULL, '2016-10-05 05:19:00'),
(21, 5, '2016-10-19 08:59:00', 'asfa', '3', 'deleted', NULL, '2016-10-05 05:20:24'),
(22, 5, '2016-10-19 08:59:00', 'asfa', '3', 'active', NULL, '2016-10-05 03:42:41'),
(23, 5, '2019-10-16 00:00:00', 'asfatrerrrrr', '3', 'active', NULL, '2016-10-05 06:20:43'),
(24, 5, '2016-10-19 08:59:00', 'asfa', '3', 'deleted', NULL, '2016-10-05 05:23:48'),
(25, 5, '2016-10-12 09:13:00', 'rr', '1', 'active', NULL, '2016-10-05 03:43:37'),
(26, 5, '2016-10-12 09:15:00', 'sdsa', '1', 'active', NULL, '2016-10-05 03:45:17'),
(27, 5, '2016-10-10 09:19:00', 'dsa', '1', 'active', NULL, '2016-10-05 03:49:11'),
(28, 5, '2016-10-10 09:19:00', 'dsa', '1', 'active', NULL, '2016-10-05 03:49:39'),
(29, 5, '2016-10-12 09:20:00', 'sadas', '1', 'deleted', NULL, '2016-10-05 05:25:16'),
(30, 5, '2016-10-12 09:20:00', 'sadas', '1', 'active', NULL, '2016-10-05 03:53:10'),
(31, 5, '2016-10-19 09:28:00', 'assf', '1', 'deleted', NULL, '2016-10-05 05:24:48'),
(32, 5, '2016-10-19 09:28:00', 'assf', '1', 'deleted', NULL, '2016-10-05 05:24:55'),
(33, 5, '2016-10-12 09:34:00', 'dwdeqw', '2', 'deleted', NULL, '2016-10-05 05:22:55'),
(34, 5, '2009-08-16 00:00:00', 'High wind', '1', 'active', NULL, '2016-10-05 05:56:48'),
(35, 5, '2016-08-09 00:00:00', 'High wind', '1', 'active', NULL, '2016-10-05 05:57:08'),
(36, 5, '2012-10-16 00:00:00', 'asff', '3', 'active', NULL, '2016-10-05 06:05:06');

-- --------------------------------------------------------

--
-- Table structure for table `project_leave`
--

CREATE TABLE `project_leave` (
  `id` bigint(25) NOT NULL,
  `project_id` bigint(25) NOT NULL,
  `date` datetime NOT NULL,
  `type` varchar(255) NOT NULL,
  `reason` varchar(500) NOT NULL,
  `is_leave` int(2) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` bigint(25) NOT NULL,
  `original_modified` datetime DEFAULT NULL,
  `modified_on` datetime NOT NULL,
  `modified_by` bigint(25) NOT NULL,
  `resource_type` varchar(100) NOT NULL DEFAULT 'Webserver',
  `row_status` varchar(20) NOT NULL DEFAULT 'active'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `project_leave`
--

INSERT INTO `project_leave` (`id`, `project_id`, `date`, `type`, `reason`, `is_leave`, `created_on`, `created_by`, `original_modified`, `modified_on`, `modified_by`, `resource_type`, `row_status`) VALUES
(33, 5, '2016-09-11 00:00:00', 'Non working day', 'testing', 0, '2016-09-12 03:09:22', 0, '2016-09-11 00:00:00', '2016-09-11 00:00:00', 0, 'Webserver', 'active'),
(34, 5, '2016-09-13 00:00:00', 'Rostered day off', '', 0, '2016-09-12 03:12:02', 0, NULL, '2016-09-12 01:13:00', 0, 'Webserver', 'active'),
(35, 5, '2016-09-14 00:00:00', 'Fixed long weekend', 'tset', 0, '2016-09-12 03:12:02', 0, NULL, '2016-09-12 01:13:00', 0, 'Webserver', 'active'),
(36, 5, '2016-09-14 00:00:00', 'Rostered day off', '', 0, '2016-09-12 03:12:06', 0, NULL, '2016-09-12 01:13:04', 0, 'Webserver', 'active'),
(37, 5, '2016-09-15 00:00:00', 'Rostered day off', '', 0, '2016-09-12 03:12:08', 0, NULL, '2016-09-12 01:13:06', 0, 'Webserver', 'active');

-- --------------------------------------------------------

--
-- Table structure for table `project_noticeboard`
--

CREATE TABLE `project_noticeboard` (
  `id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `description` text NOT NULL,
  `row_status` varchar(50) NOT NULL DEFAULT 'active',
  `modified_by` int(11) DEFAULT NULL,
  `modified_on` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `project_noticeboard`
--

INSERT INTO `project_noticeboard` (`id`, `project_id`, `date`, `description`, `row_status`, `modified_by`, `modified_on`) VALUES
(24, 9, '2016-08-05 00:00:00', 'BBQ with Saturday working hours.\r\n\r\nExtended break checklist', 'active', NULL, NULL),
(14, 5, '2016-08-08 00:00:00', 'Toolbox Talk in lunchroom at 10am', 'active', NULL, '2016-08-08 22:53:06'),
(21, 9, '2016-08-01 00:00:00', 'Site toolbox talk at 7am on level 2 lunchroom', 'active', NULL, NULL),
(22, 9, '2016-08-03 00:00:00', 'Crane Jump at 12:00pm', 'active', NULL, NULL),
(23, 9, '2016-08-04 00:00:00', 'Empire external QHSE Audit', 'active', NULL, NULL),
(10, 5, '2016-08-08 00:00:00', 'Crane Lift', 'deleted', NULL, '2016-08-08 22:52:28'),
(19, 5, '2016-08-08 00:00:00', 'test 1', 'deleted', NULL, '2016-08-08 10:50:46'),
(20, 5, '2016-08-09 00:00:00', 'Crane Lift Today', 'active', NULL, NULL),
(25, 5, '2016-08-19 00:00:00', 'nothing to diplay', 'active', NULL, NULL),
(26, 5, '2016-12-10 12:00:00', 'afaf', 'deleted', NULL, '2016-10-05 11:16:13'),
(27, 5, '2002-01-03 12:00:00', 'hjareshtest', 'active', NULL, '2016-10-05 11:08:13'),
(28, 5, '1970-01-01 10:00:00', 'haresh test update', 'active', NULL, '2016-10-05 10:31:14'),
(29, 5, '2016-10-12 12:00:00', 'daaaaaaa=', 'deleted', NULL, '2016-10-05 11:16:46');

-- --------------------------------------------------------

--
-- Table structure for table `project_user_mapping`
--

CREATE TABLE `project_user_mapping` (
  `id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `is_deleted` tinyint(2) NOT NULL DEFAULT '1' COMMENT '1 for active,o for deleted',
  `created_by` int(11) DEFAULT NULL,
  `created_on` datetime NOT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `project_user_mapping`
--

INSERT INTO `project_user_mapping` (`id`, `project_id`, `user_id`, `company_id`, `is_deleted`, `created_by`, `created_on`, `modified_by`, `modified_on`) VALUES
(1, 2, 0, 0, 1, NULL, '0000-00-00 00:00:00', NULL, '2016-07-14 11:10:38'),
(2, 1, 0, 0, 1, NULL, '0000-00-00 00:00:00', NULL, '2016-07-14 11:10:23'),
(3, 1, 2, 0, 1, NULL, '0000-00-00 00:00:00', NULL, '2016-06-06 02:13:15'),
(4, 1, 9, 0, 1, NULL, '0000-00-00 00:00:00', NULL, '2016-06-06 02:13:33'),
(5, 1, 1, 0, 1, NULL, '0000-00-00 00:00:00', NULL, '2016-06-06 02:13:40'),
(6, 2, 11, 0, 1, NULL, '0000-00-00 00:00:00', NULL, '2016-06-06 11:13:23'),
(7, 2, 12, 0, 1, NULL, '0000-00-00 00:00:00', NULL, '2016-06-07 11:24:30'),
(8, 1, 11, 0, 1, NULL, '0000-00-00 00:00:00', NULL, '2016-06-07 04:09:44'),
(9, 1, 12, 0, 1, NULL, '0000-00-00 00:00:00', NULL, '2016-06-07 04:22:53'),
(10, 1, 14, 0, 1, NULL, '0000-00-00 00:00:00', NULL, '2016-06-06 23:03:10'),
(11, 2, 13, 0, 1, NULL, '0000-00-00 00:00:00', NULL, '2016-06-07 11:21:33'),
(12, 2, 14, 0, 1, NULL, '0000-00-00 00:00:00', NULL, '2016-06-08 07:08:41'),
(13, 4, 14, 0, 1, NULL, '0000-00-00 00:00:00', NULL, '2016-06-14 06:34:07'),
(14, 5, 0, 0, 1, NULL, '0000-00-00 00:00:00', NULL, '2016-07-26 22:14:13'),
(15, 6, 0, 0, 1, NULL, '0000-00-00 00:00:00', NULL, '2016-07-28 16:34:56'),
(16, 7, 0, 0, 1, NULL, '0000-00-00 00:00:00', NULL, '2016-07-29 06:37:36'),
(17, 8, 0, 0, 1, NULL, '0000-00-00 00:00:00', NULL, '2016-07-29 13:06:22'),
(18, 9, 0, 0, 1, NULL, '0000-00-00 00:00:00', NULL, '2016-08-09 07:47:12'),
(19, 11, 1, 1, 1, 1, '2016-10-07 02:16:52', 1, '2016-10-07 03:16:52');

-- --------------------------------------------------------

--
-- Table structure for table `reference_type`
--

CREATE TABLE `reference_type` (
  `id` int(11) NOT NULL,
  `key_type` varchar(32) NOT NULL,
  `the_key` varchar(32) NOT NULL,
  `the_value` varchar(64) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `modified_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `reference_type`
--

INSERT INTO `reference_type` (`id`, `key_type`, `the_key`, `the_value`, `order`, `modified_on`) VALUES
(1, 'row_status', 'active', '0', 0, '2016-05-18 11:18:47'),
(2, 'row_status', 'deleted', '1', 0, '2016-05-18 11:18:47'),
(3, 'row_status', 'archived', '2', 0, '2016-05-18 11:18:47'),
(4, 'row_status', 'superseded', '3', 0, '2016-05-18 11:18:47'),
(5, 'row_status', 'default', '4', 0, '2016-05-18 11:18:47'),
(6, 'address_type', 'street', '11', 0, '2016-05-18 11:18:47'),
(7, 'address_type', 'postal', '12', 0, '2016-05-18 11:18:47'),
(8, 'address_type', 'billing', '13', 0, '2016-05-18 11:18:47'),
(9, 'address_type', 'site', '14', 0, '2016-05-18 11:18:47'),
(10, 'states', 'ACT', 'Australian Capital Teritory', 0, '2016-05-18 11:18:47'),
(11, 'states', 'NSW', 'New South Wales', 0, '2016-05-18 11:18:47'),
(12, 'states', 'VIC', 'Victoria', 0, '2016-05-18 11:18:47'),
(13, 'states', 'QLD', 'Queensland', 0, '2016-05-18 11:18:47'),
(14, 'states', 'SA', 'South Australia', 0, '2016-05-18 11:18:47'),
(15, 'states', 'WA', 'Western Australia', 0, '2016-05-18 11:18:47'),
(16, 'states', 'TAS', 'Tasmania', 0, '2016-05-18 11:18:47'),
(17, 'states', 'NT', 'Northern Teritory', 0, '2016-05-18 11:18:47'),
(18, 'company_type', 'wiseworking', 'Wiseworking', 0, '2016-05-18 11:18:47'),
(19, 'company_type', 'developer', 'Developer', 0, '2016-05-18 11:18:47'),
(20, 'company_type', 'builder', 'Builder', 0, '2016-05-18 11:18:47'),
(21, 'company_type', 'sub_contractor', 'Sub_Contractor', 0, '2016-05-18 11:18:47'),
(22, 'contact_details_type', 'self', 'Wiseworking', 0, '2016-05-18 11:18:47'),
(23, 'contact_details_type', 'email', 'Email', 0, '2016-05-18 11:18:47'),
(24, 'contact_details_type', 'phone', 'Phone', 0, '2016-05-18 11:18:47'),
(25, 'contact_details_type', 'mobile', 'Mobile', 0, '2016-05-18 11:18:47'),
(26, 'contact_details_type', 'pager', 'Pager', 0, '2016-05-18 11:18:47'),
(27, 'contact_details_type', 'fax', 'Fax', 0, '2016-05-18 11:18:47'),
(28, 'contact_details_type', 'person', 'Person', 0, '2016-05-18 11:18:47'),
(29, 'event_type', 'login', 'login', 0, '2016-05-18 11:18:47'),
(30, 'event_type', 'logoff', 'logoff', 0, '2016-05-18 11:18:47'),
(31, 'user_role', 'contact', '1', 0, '2016-05-18 11:18:47'),
(32, 'user_role', 'user', '2', 0, '2016-05-18 11:18:47'),
(33, 'user_role', 'contractor', '3', 0, '2016-05-18 11:18:47'),
(34, 'user_role', 'inspector', '4', 0, '2016-05-18 11:18:47'),
(35, 'user_role', 'dummy', '5', 0, '2016-05-18 11:18:47'),
(36, 'user_role', 'manager', '6', 0, '2016-05-18 11:18:47'),
(37, 'user_role', 'dummy2', '7', 0, '2016-05-18 11:18:47'),
(38, 'user_role', 'company', '8', 0, '2016-05-18 11:18:47'),
(39, 'user_role', 'ww admin', '9', 0, '2016-05-18 11:18:47'),
(40, 'password_status', 'active', 'Active', 0, '2016-05-18 11:18:47'),
(41, 'password_status', 'change', 'Change Password', 0, '2016-05-18 11:18:47'),
(42, 'password_status', 'expired', 'Has Expired', 0, '2016-05-18 11:18:47'),
(43, 'password_status', 'locked', 'Has Been Locked', 0, '2016-05-18 11:18:47'),
(44, 'project_type', 'test', 'Test', 0, '2016-05-18 11:18:47'),
(45, 'project_type', 'other', 'Other', 0, '2016-05-18 11:18:47'),
(46, 'project_type', 'residential', 'Residential', 0, '2016-05-18 11:18:47'),
(47, 'project_type', 'educational', 'Educational', 0, '2016-05-18 11:18:47'),
(48, 'project_type', 'offices', 'Offices', 0, '2016-05-18 11:18:47'),
(49, 'project_type', 'retail', 'Retail', 0, '2016-05-18 11:18:47'),
(50, 'project_type', 'industrial', 'Industrial', 0, '2016-05-18 11:18:47'),
(51, 'inspection_type', 'defect', 'Defect', 1, '2016-08-16 09:27:46'),
(52, 'inspection_type', 'safety', 'Safety Walk', 6, '2016-08-16 09:28:40'),
(53, 'inspection_type', 'hazard', 'Hazard', 7, '2016-08-16 09:28:45'),
(54, 'inspection_type', 'tenant_issue', 'Tenant Issue', 8, '2016-08-22 11:06:40'),
(55, 'issue_status', 'open', 'Open', 0, '2016-05-18 11:18:47'),
(56, 'issue_status', 'closed', 'Closed', 5, '2016-08-16 09:24:49'),
(57, 'issue_status', 're_opened', 'Re-Opened', 4, '2016-08-16 09:24:44'),
(58, 'issue_status', 'disputed', 'Disputed', 2, '2016-08-16 09:24:18'),
(59, 'issue_status', 'Fixed', 'Fixed', 3, '2016-08-16 09:24:22'),
(60, 'issue_resolution', 'open', 'Open', 1, '2016-08-16 09:24:05'),
(61, 'issue_resolution', 'fixed', 'Fixed', 0, '2016-05-18 11:18:47'),
(62, 'issue_resolution', 're-opened', 'Re-Opened', 0, '2016-05-18 11:18:47'),
(63, 'issue_resolution', 'blocked', 'Blocked', 0, '2016-05-18 11:18:47'),
(64, 'issue_resolution', 'pending', 'Pending', 0, '2016-05-18 11:18:47'),
(65, 'issue_resolution', 'disputed', 'Disputed', 0, '2016-05-18 11:18:47'),
(66, 'issue_resolution', 'unknown', 'Unknown', 0, '2016-05-18 11:18:47'),
(67, 'document_status', 'open', 'Open', 0, '2016-05-18 11:18:47'),
(68, 'document_status', 'in_process', 'In process', 0, '2016-05-18 11:18:47'),
(69, 'document_status', 'reviewed', 'Reviewed', 0, '2016-05-18 11:18:47'),
(70, 'document_status', 'final', 'Final', 0, '2016-05-18 11:18:47'),
(71, 'document_status', 'archived', 'Archived', 0, '2016-05-18 11:18:47'),
(72, 'document_status', 'superseded', 'Superseded', 0, '2016-05-18 11:18:47'),
(73, 'document_status', 'closed', 'Closed', 0, '2016-05-18 11:18:47'),
(74, 'document_status', 'deleted', 'Deleted', 0, '2016-05-18 11:18:47'),
(75, 'trade_classification', 'chippy', 'Carpenter', 0, '2016-05-18 11:18:47'),
(76, 'trade_classification', 'sparky', 'Electrician', 0, '2016-05-18 11:18:47'),
(77, 'trade_classification', 'brickie', 'Brick Layer', 0, '2016-05-18 11:18:47'),
(78, 'trade_classification', 'dunny_diver', 'Plumber', 0, '2016-05-18 11:18:47'),
(79, 'trade_classification', 'shiny_bum', 'Manager', 0, '2016-05-18 11:18:47'),
(80, 'trade_classification', 'desk_driver', 'Office Staff', 0, '2016-05-18 11:18:47'),
(81, 'trade_classification', 'trady', 'Trade Person', 0, '2016-05-18 11:18:47'),
(82, 'country', 'country', 'Australia', 0, '2016-05-18 11:18:47'),
(83, 'cost_attribute', 'none', 'None', 1, '2016-08-23 12:01:08'),
(84, 'cost_attribute', 'backcharge', 'Backcharge', 2, '2016-08-23 12:01:14'),
(85, 'cost_attribute', 'variation', 'Variation', 3, '2016-08-23 12:01:18'),
(86, 'inspection_type', 'Incomplete Works', 'Incomplete Works', 2, '2016-08-16 09:27:57'),
(87, 'inspection_type', 'Quality Issue', 'Quality Issue', 3, '2016-08-16 09:28:01'),
(88, 'inspection_type', 'Purchaser Change', 'Purchaser Change', 4, '2016-08-16 09:28:05'),
(89, 'inspection_type', 'Warranty Issue', 'Warranty Issue', 5, '2016-08-16 09:28:10'),
(90, 'raised_by', 'builder', 'Builder', 1, '2016-08-19 02:53:31'),
(91, 'raised_by', 'architect', 'Architect', 2, '2016-08-19 02:54:17'),
(92, 'raised_by', 'structural_engineer', 'Structural Engineer', 3, '2016-08-19 02:45:03'),
(93, 'raised_by', 'superintendant', 'Superintendant', 4, '2016-08-19 02:45:03'),
(94, 'raised_by', 'general_consultant', 'General Consultant', 5, '2016-08-19 02:45:03'),
(95, 'raised_by', 'client', 'Client', 6, '2016-08-19 02:45:03'),
(96, 'raised_by', 'purchaser', 'Purchaser', 7, '2016-08-19 02:45:03'),
(97, 'cost_impact', 'low', 'Low', 1, '2016-08-19 02:45:03'),
(98, 'cost_impact', 'medium', 'Medium', 2, '2016-08-19 02:45:03'),
(99, 'cost_impact', 'high', 'High', 3, '2016-08-19 02:45:03'),
(100, 'activity_company', 'brickwork', 'Brickwork', 1, '2016-08-19 02:45:03'),
(101, 'activity_company', 'carpentry', 'Carpentry', 2, '2016-08-19 02:45:03'),
(102, 'activity_company', 'cleaning', 'Cleaning', 3, '2016-08-19 02:45:03'),
(103, 'activity_company', 'concreting', 'Concreting', 4, '2016-08-19 02:45:03'),
(104, 'activity_company', 'electrical', 'Electrical', 5, '2016-09-02 05:19:43'),
(105, 'activity_company', 'fire services', 'Fire services', 6, '2016-09-02 05:19:43'),
(106, 'activity_company', 'plasterboard', 'Plasterboard', 7, '2016-09-02 05:19:43'),
(107, 'activity_company', 'plumbing', 'Plumbing', 8, '2016-09-02 05:19:43'),
(108, 'activity_company', 'metalwork', 'Metalwork', 9, '2016-09-02 05:19:43'),
(109, 'activity_company', 'Structural steel', 'structural steel', 11, '2016-09-02 05:19:43'),
(110, 'activity_company', 'waterproofing', 'Waterproofing', 12, '2016-09-02 05:19:43'),
(111, 'report_type', 'IR', 'Internal Report', 0, '2016-09-12 10:23:33'),
(112, 'report_type', 'LIR', 'Large Image Report', 0, '2016-09-12 10:23:33'),
(113, 'report_type', 'SRWN', 'Summary Report with Notes', 0, '2016-09-12 10:33:17'),
(114, 'report_type', 'SRWON', 'Summary Report without Notes', 0, '2016-09-12 10:33:17'),
(115, 'report_type', 'SRWCI', 'Summary Report with Cost Impact', 0, '2016-09-12 10:33:17'),
(116, 'report_type', 'SRFSC', 'Summary Report for Sub Contractors', 0, '2016-09-12 10:33:17'),
(117, 'report_type', 'ER', 'Executive Report', 0, '2016-09-12 10:33:17'),
(118, 'report_type', 'EITR', 'Executive (Issued to) Report', 0, '2016-09-12 10:33:17'),
(119, 'report_type', 'CSVR', 'CSV Report', 0, '2016-09-12 10:33:17'),
(120, 'sort_by', 'location', 'Location', 0, '2016-09-12 10:47:46'),
(121, 'sort_by', 'IT', 'Issued To', 0, '2016-09-12 10:47:46'),
(122, 'activity_company', 'Painting', 'Painting', 13, '2016-09-14 02:42:35'),
(123, 'run_report_type', 'SR', 'Summary Report', 0, '2016-09-16 03:51:05'),
(124, 'run_report_type', 'ER', 'Executive Report', 0, '2016-09-16 03:51:05'),
(125, 'run_report_type', 'CSVE', 'CSV Export', 0, '2016-09-16 03:53:39'),
(126, 'CSVE', 'CSVR', 'CSV Report', 0, '2016-09-16 04:55:51'),
(127, 'ER', 'EITR', 'Executive (Issued to) Report', 0, '2016-09-16 04:56:18'),
(128, 'ER', 'ER', 'Executive Report', 0, '2016-09-16 04:56:09'),
(129, 'SR', 'SRFSC', 'Report for Sub Contractors', 0, '2016-09-16 06:58:27'),
(130, 'SR', 'SRWCI', 'Report with Cost Impact', 0, '2016-09-16 06:58:20'),
(131, 'SR', 'SRWN', 'Report with Notes', 0, '2016-09-16 06:58:13'),
(132, 'SR', 'SRWON', 'Report without Notes', 0, '2016-09-16 06:58:06'),
(133, 'run_report_type', 'SCZIPP', 'Sub Contractor ZIP Package', 0, '2016-09-20 02:53:39');

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE `role` (
  `id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `row_status` varchar(32) NOT NULL DEFAULT 'active',
  `modified_by` int(11) DEFAULT NULL,
  `modified_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`id`, `name`, `row_status`, `modified_by`, `modified_on`) VALUES
(0, 'Super Admin', 'active', NULL, '2016-05-18 11:18:47'),
(1, 'Contact', 'active', NULL, '2016-06-07 11:18:35'),
(2, 'user', 'active', NULL, '2016-06-07 11:18:35'),
(3, 'contractor', 'active', NULL, '2016-06-07 11:18:35'),
(4, 'contractor', 'active', NULL, '2016-06-07 11:18:35'),
(5, 'dummy', 'active', NULL, '2016-06-07 11:18:35'),
(6, 'manager', 'active', NULL, '2016-06-07 11:18:35'),
(7, 'dummy2', 'active', NULL, '2016-06-07 11:18:35'),
(8, 'company', 'active', NULL, '2016-06-07 11:18:35'),
(9, 'ww admin', 'active', NULL, '2016-06-07 11:18:35');

-- --------------------------------------------------------

--
-- Table structure for table `role_authorisation_mapping`
--

CREATE TABLE `role_authorisation_mapping` (
  `id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `authorisation_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `role_authorisation_mapping`
--

INSERT INTO `role_authorisation_mapping` (`id`, `role_id`, `authorisation_id`) VALUES
(1, 0, 1),
(2, 0, 2),
(3, 0, 3),
(4, 0, 4),
(5, 0, 5),
(6, 0, 6),
(7, 0, 7),
(8, 0, 8),
(9, 0, 9),
(10, 0, 10),
(11, 0, 11),
(12, 0, 12),
(13, 0, 13),
(14, 0, 14),
(15, 0, 15),
(16, 0, 16),
(17, 0, 17),
(18, 0, 18),
(19, 0, 19),
(20, 0, 20),
(21, 0, 21),
(22, 0, 22),
(23, 0, 23),
(24, 0, 24),
(25, 0, 25),
(26, 0, 26),
(27, 0, 27),
(28, 0, 28),
(29, 0, 29),
(30, 0, 30),
(31, 0, 31),
(32, 0, 32),
(33, 0, 33),
(34, 0, 34),
(35, 0, 35),
(36, 0, 36),
(37, 0, 37),
(38, 0, 38),
(39, 0, 39),
(40, 0, 40),
(41, 0, 41),
(42, 0, 42),
(43, 0, 43),
(44, 0, 44),
(45, 0, 45),
(46, 0, 46),
(47, 0, 47),
(48, 0, 48),
(49, 0, 49),
(50, 0, 50),
(51, 0, 51),
(52, 0, 52),
(53, 0, 53),
(54, 0, 54),
(55, 0, 55),
(56, 0, 56),
(57, 0, 57),
(58, 0, 58),
(59, 0, 59);

-- --------------------------------------------------------

--
-- Table structure for table `standard_defect`
--

CREATE TABLE `standard_defect` (
  `id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `description` text NOT NULL,
  `issue_to_Id` bigint(25) NOT NULL,
  `issue_to` varchar(255) NOT NULL,
  `tags` varchar(255) NOT NULL,
  `activity` varchar(255) NOT NULL,
  `row_status` varchar(100) NOT NULL DEFAULT 'active',
  `modified_by` int(11) DEFAULT NULL,
  `modified_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `original_modified_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` bigint(25) NOT NULL,
  `resource_type` varchar(255) NOT NULL DEFAULT 'webserver'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `standard_defect`
--

INSERT INTO `standard_defect` (`id`, `project_id`, `description`, `issue_to_Id`, `issue_to`, `tags`, `activity`, `row_status`, `modified_by`, `modified_on`, `original_modified_date`, `created_date`, `created_by`, `resource_type`) VALUES
(1, 5, 'Test Desc', 147069846340, 'Justin Smith', '', 'carpentry,cleaning', 'active', 0, '2016-09-09 01:12:02', '2016-09-09 01:12:02', '2016-09-09 01:12:02', 0, 'webserver'),
(2, 5, 'Test desc1', 147544302520, 'H test', '', 'carpentry,cleaning', 'active', 0, '2016-09-09 01:12:40', '2016-09-09 01:12:40', '2016-09-09 01:12:40', 0, 'webserver'),
(3, 5, 'Broken pipe leaking', 147448262990, 'Billy Johns', '', 'plumbing', 'active', 0, '2016-09-09 02:11:59', '2016-09-09 02:11:59', '2016-09-09 02:11:59', 0, 'webserver'),
(4, 5, 'Structural steel needs caulking', 147515362130, 'Tom Hardy', '', 'Structural steel', 'active', 0, '2016-09-12 10:34:06', '2016-09-12 10:34:06', '2016-09-12 10:33:53', 0, 'webserver');

-- --------------------------------------------------------

--
-- Table structure for table `unique_id`
--

CREATE TABLE `unique_id` (
  `id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `user_name` varchar(32) NOT NULL,
  `full_name` varchar(128) NOT NULL,
  `password` varchar(128) NOT NULL,
  `enc_password` varchar(256) NOT NULL,
  `password_status` varchar(32) NOT NULL,
  `row_status` varchar(32) NOT NULL DEFAULT 'active',
  `created_at` datetime NOT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `role_id`, `user_name`, `full_name`, `password`, `enc_password`, `password_status`, `row_status`, `created_at`, `modified_by`, `modified_on`) VALUES
(0, 0, 'admin', 'Justin Williams', '123456', '$2y$10$jEP63Vx/0vnJMAFiTFavOeAU4I8gHJS5JzgtXt8msDT3DASs1ERpy', 'active', 'active', '2016-07-14 00:00:00', 0, '2016-09-27 06:06:22'),
(1, 0, 'company', 'Wiseworking Company', 'Wi5eW0rker', '', 'active', '0', '2016-07-14 00:00:00', 0, '2016-07-14 12:04:12'),
(2, 0, 'cron', 'Cron User', 'Wi5eW0rker', ' ', 'active', '0', '2016-07-14 00:00:00', 0, '2016-07-14 12:04:18'),
(9, 1, 'jwilliams', 'test', '123456', 'e10adc3949ba59abbe56e057f20f883e', 'active', '0', '2016-07-14 00:00:00', 0, '2016-07-14 12:04:21'),
(10, 1, 'foadm', 'Foad Momtazi', '123456', 'e10adc3949ba59abbe56e057f20f883e', 'active', '0', '2016-07-14 00:00:00', 0, '2016-07-14 12:04:25'),
(11, 1, 'test', 'test', '123456', 'e10adc3949ba59abbe56e057f20f883e', 'active', 'active', '2016-07-14 04:00:00', 0, '2016-07-14 12:04:32'),
(12, 1, 'test1', 'test', '11111', 'b0baee9d279d34fa1dfd71aadb908c3f', 'active', 'active', '2016-07-14 03:00:00', 0, '2016-07-14 12:04:38'),
(13, 1, 'qwert', 'qwert', 'qwert', 'a384b6463fc216a5f8ecb6670f86456a', 'active', 'active', '2016-07-14 03:00:00', 0, '2016-07-14 12:04:48'),
(14, 0, 'testuser', 'testuser', '123456', '$2y$10$jEP63Vx/0vnJMAFiTFavOeAU4I8gHJS5JzgtXt8msDT3DASs1ERpy', 'active', 'active', '2016-07-14 01:00:00', 0, '2016-09-27 05:07:47');

-- --------------------------------------------------------

--
-- Table structure for table `user_company_mapping`
--

CREATE TABLE `user_company_mapping` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `default_flag` int(11) NOT NULL DEFAULT '1' COMMENT '1 means this is the default mapping',
  `row_status` varchar(32) NOT NULL DEFAULT 'active',
  `modified_by` int(11) DEFAULT NULL,
  `modified_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_company_mapping`
--

INSERT INTO `user_company_mapping` (`id`, `user_id`, `company_id`, `default_flag`, `row_status`, `modified_by`, `modified_on`) VALUES
(0, 0, 0, 1, 'active', 0, '2016-06-06 11:01:20'),
(1, 11, 0, 1, 'active', NULL, '2016-06-06 11:01:12'),
(2, 12, 0, 1, 'active', NULL, '2016-06-06 11:01:00'),
(3, 13, 0, 1, 'active', NULL, '2016-06-06 11:02:18'),
(4, 14, 0, 1, 'active', NULL, '2016-06-06 11:06:21');

-- --------------------------------------------------------

--
-- Table structure for table `user_contact_mapping`
--

CREATE TABLE `user_contact_mapping` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `contact_id` int(11) NOT NULL,
  `primary_contact` int(11) NOT NULL DEFAULT '0' COMMENT '1=primary or default contact',
  `row_status` varchar(32) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_contact_mapping`
--

INSERT INTO `user_contact_mapping` (`id`, `user_id`, `contact_id`, `primary_contact`, `row_status`, `modified_by`, `modified_on`) VALUES
(0, 0, 0, 1, 'active', 0, '2016-07-28 12:19:49'),
(1, 0, 1, 1, 'active', 0, '2016-07-28 12:19:54'),
(2, 14, 0, 1, 'active', 0, '2016-07-28 12:19:49'),
(3, 14, 1, 1, 'active', 0, '2016-07-28 12:19:54');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `address`
--
ALTER TABLE `address`
  ADD PRIMARY KEY (`id`),
  ADD KEY `index_type_id` (`type_id`),
  ADD KEY `index_post_code_id` (`post_code`);

--
-- Indexes for table `authorisation`
--
ALTER TABLE `authorisation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `index_code` (`code`);

--
-- Indexes for table `client_properties`
--
ALTER TABLE `client_properties`
  ADD PRIMARY KEY (`id`),
  ADD KEY `index_company_id` (`company_id`);

--
-- Indexes for table `company`
--
ALTER TABLE `company`
  ADD PRIMARY KEY (`id`),
  ADD KEY `type_id` (`type_id`);

--
-- Indexes for table `company_address_mapping`
--
ALTER TABLE `company_address_mapping`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `company_address_key` (`company_id`,`address_id`),
  ADD KEY `index_company_id` (`company_id`),
  ADD KEY `index_address_id` (`address_id`);

--
-- Indexes for table `company_company_mapping`
--
ALTER TABLE `company_company_mapping`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `company_company_key` (`company_id`,`company2_id`),
  ADD KEY `index_company_id` (`company_id`),
  ADD KEY `index_company2_id` (`company2_id`);

--
-- Indexes for table `company_contact_details_mapping`
--
ALTER TABLE `company_contact_details_mapping`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `company_contact_details_key` (`company_id`,`contact_details_id`),
  ADD KEY `index_company_id` (`company_id`),
  ADD KEY `index_contact_details_id` (`contact_details_id`);

--
-- Indexes for table `company_project_mapping`
--
ALTER TABLE `company_project_mapping`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `index_project_company` (`company_id`,`project_id`),
  ADD KEY `index_project_id` (`project_id`),
  ADD KEY `index_company_id` (`company_id`);

--
-- Indexes for table `contact_details`
--
ALTER TABLE `contact_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `index_type_id` (`contact_type`);

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`),
  ADD KEY `index_user_id` (`user_id`),
  ADD KEY `index_project_id` (`project_id`),
  ADD KEY `index_company_id` (`company_id`);

--
-- Indexes for table `image`
--
ALTER TABLE `image`
  ADD PRIMARY KEY (`id`),
  ADD KEY `image_gallery_id` (`image_gallery_id`);

--
-- Indexes for table `image_gallery`
--
ALTER TABLE `image_gallery`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `inspection`
--
ALTER TABLE `inspection`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `index_project_id` (`project_id`),
  ADD KEY `index_inspection_type` (`inspection_type`),
  ADD KEY `index_inspected_by` (`inspected_by`),
  ADD KEY `index_status` (`inspection_status`);

--
-- Indexes for table `inspection_activity`
--
ALTER TABLE `inspection_activity`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inspection_item`
--
ALTER TABLE `inspection_item`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `index_location_id` (`location_id`),
  ADD KEY `index_inspection_id` (`inspection_id`),
  ADD KEY `index_raised_by` (`raised_by`),
  ADD KEY `index_status` (`item_status`),
  ADD KEY `priority` (`priority`);

--
-- Indexes for table `issued_to`
--
ALTER TABLE `issued_to`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `issued_to--`
--
ALTER TABLE `issued_to--`
  ADD PRIMARY KEY (`id`),
  ADD KEY `project_id` (`project_id`),
  ADD KEY `issue_to_company_id` (`issue_to_company_id`);

--
-- Indexes for table `issueTo_company`
--
ALTER TABLE `issueTo_company`
  ADD PRIMARY KEY (`id`),
  ADD KEY `project_id` (`project_id`);

--
-- Indexes for table `location`
--
ALTER TABLE `location`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `index_project_id` (`project_id`),
  ADD KEY `index_parent_id` (`parent_id`),
  ADD KEY `index_label` (`label`);

--
-- Indexes for table `master_issued_to`
--
ALTER TABLE `master_issued_to`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `master_issued_to_contact`
--
ALTER TABLE `master_issued_to_contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `project`
--
ALTER TABLE `project`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `company_id` (`id`);

--
-- Indexes for table `project_address_mapping`
--
ALTER TABLE `project_address_mapping`
  ADD PRIMARY KEY (`id`),
  ADD KEY `index_project_id` (`project_id`),
  ADD KEY `address_id` (`address_id`);

--
-- Indexes for table `project_alert`
--
ALTER TABLE `project_alert`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `project_leave`
--
ALTER TABLE `project_leave`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `project_noticeboard`
--
ALTER TABLE `project_noticeboard`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `project_user_mapping`
--
ALTER TABLE `project_user_mapping`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `index_project_user` (`user_id`,`project_id`),
  ADD KEY `index_project_id` (`project_id`),
  ADD KEY `index_user_id` (`user_id`);

--
-- Indexes for table `reference_type`
--
ALTER TABLE `reference_type`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `type_key` (`key_type`,`the_key`),
  ADD KEY `index_the_key` (`the_key`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_authorisation_mapping`
--
ALTER TABLE `role_authorisation_mapping`
  ADD PRIMARY KEY (`id`),
  ADD KEY `index_role_id` (`role_id`),
  ADD KEY `index_authorisation_id` (`authorisation_id`);

--
-- Indexes for table `standard_defect`
--
ALTER TABLE `standard_defect`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `unique_id`
--
ALTER TABLE `unique_id`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_name` (`user_name`),
  ADD KEY `index_user_name` (`user_name`),
  ADD KEY `role_id` (`role_id`);

--
-- Indexes for table `user_company_mapping`
--
ALTER TABLE `user_company_mapping`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_company_key` (`user_id`,`company_id`),
  ADD KEY `index_user_id` (`user_id`),
  ADD KEY `index_company_id` (`company_id`);

--
-- Indexes for table `user_contact_mapping`
--
ALTER TABLE `user_contact_mapping`
  ADD PRIMARY KEY (`id`),
  ADD KEY `index_contact_id` (`contact_id`),
  ADD KEY `user_id` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `address`
--
ALTER TABLE `address`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `authorisation`
--
ALTER TABLE `authorisation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;
--
-- AUTO_INCREMENT for table `client_properties`
--
ALTER TABLE `client_properties`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `company`
--
ALTER TABLE `company`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `company_address_mapping`
--
ALTER TABLE `company_address_mapping`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `company_company_mapping`
--
ALTER TABLE `company_company_mapping`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `company_contact_details_mapping`
--
ALTER TABLE `company_contact_details_mapping`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `company_project_mapping`
--
ALTER TABLE `company_project_mapping`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `contact_details`
--
ALTER TABLE `contact_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `image`
--
ALTER TABLE `image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;
--
-- AUTO_INCREMENT for table `image_gallery`
--
ALTER TABLE `image_gallery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `inspection`
--
ALTER TABLE `inspection`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=102;
--
-- AUTO_INCREMENT for table `inspection_activity`
--
ALTER TABLE `inspection_activity`
  MODIFY `id` bigint(25) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;
--
-- AUTO_INCREMENT for table `inspection_item`
--
ALTER TABLE `inspection_item`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=116;
--
-- AUTO_INCREMENT for table `issued_to`
--
ALTER TABLE `issued_to`
  MODIFY `id` bigint(25) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `issued_to--`
--
ALTER TABLE `issued_to--`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `issueTo_company`
--
ALTER TABLE `issueTo_company`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `location`
--
ALTER TABLE `location`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=78;
--
-- AUTO_INCREMENT for table `master_issued_to`
--
ALTER TABLE `master_issued_to`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `master_issued_to_contact`
--
ALTER TABLE `master_issued_to_contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `project`
--
ALTER TABLE `project`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `project_address_mapping`
--
ALTER TABLE `project_address_mapping`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `project_alert`
--
ALTER TABLE `project_alert`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT for table `project_leave`
--
ALTER TABLE `project_leave`
  MODIFY `id` bigint(25) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT for table `project_noticeboard`
--
ALTER TABLE `project_noticeboard`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `project_user_mapping`
--
ALTER TABLE `project_user_mapping`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `reference_type`
--
ALTER TABLE `reference_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=134;
--
-- AUTO_INCREMENT for table `role`
--
ALTER TABLE `role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `role_authorisation_mapping`
--
ALTER TABLE `role_authorisation_mapping`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;
--
-- AUTO_INCREMENT for table `standard_defect`
--
ALTER TABLE `standard_defect`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `unique_id`
--
ALTER TABLE `unique_id`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `user_company_mapping`
--
ALTER TABLE `user_company_mapping`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `user_contact_mapping`
--
ALTER TABLE `user_contact_mapping`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `client_properties`
--
ALTER TABLE `client_properties`
  ADD CONSTRAINT `client_properties_ibfk_1` FOREIGN KEY (`company_id`) REFERENCES `company` (`id`);

--
-- Constraints for table `company_address_mapping`
--
ALTER TABLE `company_address_mapping`
  ADD CONSTRAINT `company_address_mapping_ibfk_1` FOREIGN KEY (`company_id`) REFERENCES `company` (`id`),
  ADD CONSTRAINT `company_address_mapping_ibfk_2` FOREIGN KEY (`address_id`) REFERENCES `address` (`id`);

--
-- Constraints for table `company_company_mapping`
--
ALTER TABLE `company_company_mapping`
  ADD CONSTRAINT `company_company_mapping_ibfk_1` FOREIGN KEY (`company_id`) REFERENCES `company` (`id`),
  ADD CONSTRAINT `company_company_mapping_ibfk_2` FOREIGN KEY (`company2_id`) REFERENCES `company` (`id`);

--
-- Constraints for table `company_contact_details_mapping`
--
ALTER TABLE `company_contact_details_mapping`
  ADD CONSTRAINT `company_contact_details_mapping_ibfk_1` FOREIGN KEY (`contact_details_id`) REFERENCES `contact_details` (`id`),
  ADD CONSTRAINT `company_contact_details_mapping_ibfk_2` FOREIGN KEY (`company_id`) REFERENCES `company` (`id`);

--
-- Constraints for table `company_project_mapping`
--
ALTER TABLE `company_project_mapping`
  ADD CONSTRAINT `company_project_mapping_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`),
  ADD CONSTRAINT `company_project_mapping_ibfk_2` FOREIGN KEY (`company_id`) REFERENCES `company` (`id`);

--
-- Constraints for table `events`
--
ALTER TABLE `events`
  ADD CONSTRAINT `events_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `events_ibfk_2` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`),
  ADD CONSTRAINT `events_ibfk_3` FOREIGN KEY (`company_id`) REFERENCES `company` (`id`);

--
-- Constraints for table `issued_to--`
--
ALTER TABLE `issued_to--`
  ADD CONSTRAINT `issued_to--_ibfk_2` FOREIGN KEY (`issue_to_company_id`) REFERENCES `issueTo_company` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `issueTo_company`
--
ALTER TABLE `issueTo_company`
  ADD CONSTRAINT `issueTo_company_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `project_address_mapping`
--
ALTER TABLE `project_address_mapping`
  ADD CONSTRAINT `project_address_mapping_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`),
  ADD CONSTRAINT `project_address_mapping_ibfk_2` FOREIGN KEY (`address_id`) REFERENCES `address` (`id`);

--
-- Constraints for table `project_user_mapping`
--
ALTER TABLE `project_user_mapping`
  ADD CONSTRAINT `project_user_mapping_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `project_user_mapping_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `role_authorisation_mapping`
--
ALTER TABLE `role_authorisation_mapping`
  ADD CONSTRAINT `role_authorisation_mapping_ibfk_1` FOREIGN KEY (`authorisation_id`) REFERENCES `authorisation` (`id`),
  ADD CONSTRAINT `role_authorisation_mapping_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`);

--
-- Constraints for table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `user_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`);

--
-- Constraints for table `user_company_mapping`
--
ALTER TABLE `user_company_mapping`
  ADD CONSTRAINT `user_company_mapping_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `user_company_mapping_ibfk_2` FOREIGN KEY (`company_id`) REFERENCES `company` (`id`);

--
-- Constraints for table `user_contact_mapping`
--
ALTER TABLE `user_contact_mapping`
  ADD CONSTRAINT `user_contact_mapping_ibfk_1` FOREIGN KEY (`contact_id`) REFERENCES `contact_details` (`id`),
  ADD CONSTRAINT `user_contact_mapping_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
