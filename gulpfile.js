const elixir = require('laravel-elixir');

require('laravel-elixir-vue-2');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

// elixir(mix => {
//     mix.sass('app.scss')
//        .webpack('app.js');
// });
//Login css and js
elixir(function(mix) {
    mix.styles([
        'font-awesome.min.css',
        'bootstrap.min.css',
        'uniform.default.css',
        'components-rounded.css',
        'login.css',
        'plugins.css',
        'light.css',
        'layout.css'
    ]);
});

elixir(function(mix) {
    mix.scripts([
        'jquery.min.js',
        'jquery-migrate.min.js',
        'bootstrap.min.js',
        'jquery.uniform.min.js',
        'jquery.validate.min.js',
        'metronic.js',
        'layout.js',
        'login.js',
    ]);
});

//Frontend css and js
elixir(function(mix) {
    mix.styles([
        'font-awesome.min.css',
        'bootstrap.min.css',
        'simple-line-icons.min.css',
        'morris.css',
        'components-rounded.css',
        'frontend/layout.css',      
        'uniform.default.css',
        'datepicker3.css',
        'select2.css',
        'todo.css',        
        'default.css',
        // 'frontend/custom.css',
        'plugins1.css',
        'sweetalert.css',
    ], 'public/css/frontend.css');
});

elixir(function(mix) {
    mix.scripts([
        'jquery.min.js',
        'jquery-migrate.min.js',
        'jquery-ui.min.js',
        'bootstrap.min.js',
        'bootstrap-hover-dropdown.min.js',
        'jquery.slimscroll.min.js',                
        'bootstrap-datetimepicker.js',
        'select2.min.js',        
        'jquery.validate.min.js',
        'metronic.js',
        'frontend/layout.js',
        'demo.js',           
        'sweetalert-dev.js',           
    ], 'public/js/frontend.js');
});

//Frontend css and js
elixir(function(mix) {
    mix.styles([
        'font-awesome.min.css',        
        'simple-line-icons.min.css',            
        'backend/bootstrap.min.css',
        'backend/uniform.default.css',
        'bootstrap-switch.min.css',
        'default/style.min.css',
        'dataTables.bootstrap.css',        
        'components-rounded.css',
        'plugins.css',        
        'layout.css',      
        'jquery-ui.css',        
        'backend/light.css',
        'sweetalert.css'       
    ], 'public/css/backend.css');
});

elixir(function(mix) {
    mix.scripts([
        'jquery.min.js',
        'jquery-migrate.min.js',
        'jquery-ui.min.js',
        'bootstrap.min.js',
        'bootstrap-hover-dropdown.min.js',
        'jquery.slimscroll.min.js',                                
        'select2.min.js',        
        'backend/js/jquery.dataTables.min.js',
        'backend/js/jquery.dataTables.js',
        'backend/dataTables.bootstrap.js',
        'metronic.js',
        'layout.js',
        'jquery.validate.js',              
        'sweetalert-dev.js',           
    ], 'public/js/backend.js');
});
// Versioning 
// elixir(function(mix) {
//     mix.version(['css/all.css', 'js/app.js']);
// });
