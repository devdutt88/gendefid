/**
 * Created by Amit Thakkar on 24/03/15.
 */
(function (ng) {
    var app = ng.module('tree', ['tree.service', 'tree.directives']);
    app.controller("TreeController", ["TreeService", "$scope", function (TreeService, $scope) {
        var tc = this;
        buildTree();
        function buildTree() {
            TreeService.getTree().then(function (result) {
                tc.tree = result.data;
            }, function (result) {
                alert("Tree no available, Error: " + result);
            });
        }

        // $scope.storRolePermission = function(){
        //     url = baseUrl + "/superadmin/store-rolePermision";
        //     console.log($scope.node);
        //     $scope.showLoader = false;
        //     $http({
        //         method: 'POST',
        //         url: url,
        //         headers: {'Content-Type': 'application/x-www-form-urlencoded'},            
        //         data: $.param($scope.node)
        //     }).then(function successCallback(response) {
        //         console.log(response.data);
        //         $scope.showLoader = false;
        //         window.location.href= baseUrl+"/superadmin/roles";
        //      }, function errorCallback(response) {
        //         $scope.isDisabled = false;
        //         console.log(response);
        //         //swal('This is embarassing. An error has occured. Please check your internet connections');
        //     });
        // };
    }]);
})(angular);
