/**
 * Created by Amit Thakkar on 24/03/15.
 */
(function (ng) {
    var treeServiceApp = ng.module('tree.service', []);
    treeServiceApp.service("TreeService", ["$http", function ($http) {
        this.getTree = function (roleid) {
        	//console.log(roleid);
        	var url = baseUrl+"/superadmin/role-permissions/"+roleid;
            return $http.get(url);
        };
    }]);
})(angular);
