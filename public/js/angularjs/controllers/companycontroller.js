var company = angular.module('companyModule', ['ngMessages','mp.colorPicker']);
    company.constant('API_URL', baseUrl+'/superadmin/');    
    company.constant("CSRF_TOKEN", $('[name="_token"]').val());
    company.directive('ngFiles', ['$parse', function ($parse) {

        function fn_link(scope, element, attrs) {
            var onChange = $parse(attrs.ngFiles);
            element.on('change', function (event) {
                onChange(scope, { $files: event.target.files });
            });
        };

        return {
            link: fn_link
        }
    } ]);


/***
** Custom directive for email exist check
**/
company.directive('emailExists', function($http , $q ) {

    return {
        restrict: 'AE',
        require: 'ngModel',
        link: function($scope, elem, attrs, ngModel) { 
            ngModel.$asyncValidators.uniqueEmail = function(modelValue, viewValue) {
                var value = modelValue || viewValue;
                console.log(id);
                // Lookup user by password                
                return $http.get(baseUrl+ '/superadmin/comp_check_emailexist/' + id +'/'+ value).
                then(function resolved(response) {
                   //password exists, this means validation fails
                   var valid = response.data;
                    if(valid == "true"){
                        // $scope.show_err= true;
                        ngModel.$setValidity('emailExists', false);
                        return $q.reject();
                    }else{
                        // $scope.show_err= false;
                        ngModel.$setValidity('emailExists', true);
                        return $q.resolve();
                    }                        
                }, function rejected(response) {
                    //email does not exist, therefore this validation passes
                    console.log(response.data);
                    return false;
                });
            };
        }
    } 
});



company.controller('companyController', function ($scope,$log, $http, API_URL,CSRF_TOKEN) {

    setTimeout(function(){
        $(".alert-success").hide();    
    }, 2000);
    $scope.cmp = {};    
    $scope.show_err= false;
    //show modal form
    $scope.showModelFrm = function (id,userform) {                
        switch (userform) {
            case 'company':
            var url = API_URL + "edit-company-details/" + id + '/company';
            $http({method: 'GET', url: url}).
            then(function(response) {
                    //console.log(response);         
                    //alert(response.data.cmpname);
                    $scope.cmp = response.data;
                    //alert($scope.cmpname);
                    $('#companyModal').modal('show');
                }, function(response) {
                    $scope.data = response.data || 'Request failed';
                    $scope.status = response.status;
            });
            break;
            case 'address':
                //$('#addressModal').modal('show');
                    $scope.cmpdetails = {};
                    var url = API_URL + "edit-company-details/" + id + '/address';
                    $http({method: 'GET', url: url}).
                    then(function(response) {                        
                      $scope.cmpdetails = response.data;                      
                      $('#addressModal').modal('show');
                  }, function(response) {
                      $scope.data = response.data || 'Request failed';
                      $scope.status = response.status;
            });
            break;
            default:
            swal("Please select any form");
            break;
        }
    };

    $scope.reset = function(form) {
        form.$setPristine();
        form.$setUntouched();
    };

    $scope.check_company_name = function(id){         
        if(typeof $scope.cmp.name == "undefined")
        {            
            $('#error_msg').hide();
            $('.angfrm').attr('disabled',true);
        }else{            
            var name = $scope.cmp.name;            
            var url=baseUrl+"/superadmin/comp_check_isexist?id="+id+'&name='+name;
            $http({method: 'GET', url: url}).
            then(function(response) {
                console.log(response.data);                    
                if(response.data == "false"){  
                    $('#error_msg').show();                        
                    $('#company_form').attr('disabled',true);

                }else{
                    $('#error_msg').hide();
                    $('#company_form').attr('disabled',false);
                }                                        
                  //$scope.user = response.data;        
              }, function(response) {
                  $scope.data = response.data || 'Request failed';
                  $scope.status = response.status;
              });
        }            
    }

    $scope.check_company_email = function(id){         
        if(typeof $scope.cmp.email == "undefined")
        {            
            $('#error_msg').hide();
            $('.angfrm').attr('disabled',true);
        }else{            
            var email = $scope.cmp.email;                    
            var url=baseUrl+"/superadmin/comp_check_isexist?id="+id+'&email='+email;
            $http({method: 'GET', url: url}).
            then(function(response) {
                console.log(response.data);                    
                if(response.data == "false"){  
                    $('#error_msg_email').show();                        
                    $('#company_form').attr('disabled',true);

                }else{
                    $('#error_msg_email').hide();
                    $('#company_form').attr('disabled',false);
                }                                        
                  //$scope.user = response.data;        
              }, function(response) {
                  $scope.data = response.data || 'Request failed';
                  $scope.status = response.status;
              });
        }            
    }

    $scope.saveComapny = function(id){                
        $scope.cmp.company_form = 'company';
        $scope.cmp.id = id;
        $scope.cmp._token = CSRF_TOKEN;
        $scope.showLoader = true;
        var url = API_URL + "update_company";                        
        $('#custom_error').html('');
        if ($scope.cmpInfo.$valid) {
            $http({
                method: 'POST',
                url: url,            
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},            
                data: $.param($scope.cmp)
            }).then(function successCallback(response) {
                //console.log(response.data);
                if(response.status == 200)
                {
                    $scope.showLoader = false;
                    $scope.cmpInfo.$valid = false;
                    window.location.href= baseUrl+"/superadmin/company-details/"+id;
                }
             }, function errorCallback(response) {
                $scope.showLoader = false;
                $('#custom_error').html('Something went wrong, Please try again!');
                //alert('This is embarassing. An error has occured. Please check your internet connections');
            });
        }
    }

    $scope.saveCompanyDetail = function(){        
        $scope.cmpdetails.company_form = 'address';
        $scope.cmpdetails.id = id;    
        $scope.cmpdetails._token = CSRF_TOKEN;
        var url = API_URL + "update_company"; 
        $scope.showLoader = true;  
        if ($scope.addressInfo.$valid) {                     
            $http({
                method: 'POST',
                url: url,
                 headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param($scope.cmpdetails)
                }).then(function successCallback(response) {
                    //console.log(response.data);
                    if(response.status == 200)
                    {
                        $scope.showLoader = false;
                        $scope.addressInfo.$valid = false;
                        window.location.href= baseUrl+"/superadmin/company-details/"+id;
                    }
                 }, function errorCallback(response) {
                    $scope.showLoader = false;
                    $('#custom_error').html('Something went wrong, Please try again!');
                    //alert('This is embarassing. An error has occured. Please check your internet connections');
            });
        }
    }

    var formdata = new FormData();
    $scope.getTheFiles = function ($files) {
        angular.forEach($files, function (value, key) {
            formdata.append(key, value);
        });
    };

    $scope.uploadLogo = function(id,logo,company_name){
        
        var url = API_URL + "update_company";        
        formdata.append('company_form' , logo);
        formdata.append('cmp_id' , id);
        formdata.append('company_name' , company_name);
        formdata.append('_token' , CSRF_TOKEN);
        
        //formdata.append(key, value);
        $scope.showLogoLoader = true;
        $http({
            method: 'POST',
            url: url,
            headers: {'Content-Type': undefined},
            data: formdata, //forms user object
            }).then(function successCallback(response) {
                $scope.showLogoLoader = false;
                //$('#upload_btn').hide();
                 window.location.href= baseUrl+"/superadmin/company-details/"+id;
                // when the response is available
            }, function errorCallback(response) {
                $scope.showLogoLoader = false;
                // called asynchronously if an error occurs
                // or server returns response with an error status.
        });
    }
    
    $scope.theme = {};
    $scope.themeSettings = function(company_id){
        //alert(company_id);
        $('#themeSettingsModal').modal('show');
        var url = API_URL + "get_theme_colors/"+company_id;   
        formdata.append('_token' , CSRF_TOKEN);
        $http({
            method: 'POST',
            url: url,
            headers: {'Content-Type': undefined},
            data: formdata,
            }).then(function successCallback(response) {
                //$scope.showLogoLoader = false;
                //$('#upload_btn').hide();
                $scope.theme = response.data;                
                $scope.theme.cur_val = $scope.theme.cst_bar;
                //alert(response.data);
                $scope.setColorPicker('bar');
                $('#themeSettingsModal').modal('show');
                // when the response is available
            }, function errorCallback(response) {
                //$scope.showLogoLoader = false;
                alert('Theme CSS not found. Please go ahead to create the theme CSS file');
                // called asynchronously if an error occurs
                // or server returns response with an error status.
        });
    }

    $scope.setColorPicker = function(tab){
        switch (tab) {
            case 'bar':
                $scope.theme.cur_val = $scope.theme.cst_bar;
                $scope.theme.cur_tab = 'cst_bar';
            break;
            case 'btn':
                $scope.theme.cur_val = $scope.theme.cst_btn;
                $scope.theme.cur_tab = 'cst_btn';
            break;
            case 'label':
                $scope.theme.cur_val = $scope.theme.cst_label;
                $scope.theme.cur_tab = 'cst_label';
            break;
            case 'btn_text':
                $scope.theme.cur_val = $scope.theme.cst_label;
                $scope.theme.cur_tab = 'cst_btn_text';
            break;
            default:
            swal("Please select any color");
            break;
        }
    }

    $scope.changeTabValue = function(value){
        switch (value) {
            case 'cst_bar':
                $scope.theme.cst_bar = $scope.theme.cur_val;
            break;
            case 'cst_btn':
                $scope.theme.cst_btn = $scope.theme.cur_val;
            break;
            case 'cst_label':
                $scope.theme.cst_label = $scope.theme.cur_val;
            break;
            case 'cst_btn_text':
                $scope.theme.cst_btn_text = $scope.theme.cur_val;
            break;
            default:
            swal("Please select any color");
            break;
        }
    }

    $scope.saveThemeSettings = function(company_id,label){
        switch(label){
            case 'themeColors':
                $scope.theme.company_form = 'themeColors';
            break;
            case 'setToDefault':
                $scope.theme.company_form = 'setToDefault';       
            break;
            default:
            break;
        }
        
        $scope.theme.id = company_id;
        $scope.theme._token = CSRF_TOKEN;
        var url = API_URL + "update_company"; 
        // $scope.showLoader = true;  
        // if ($scope.addressInfo.$valid) {                     
            $http({
                method: 'POST',
                url: url,
                 headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param($scope.theme)
                }).then(function successCallback(response) {
                    //console.log(response.data);
                    if(response.status == 200)

                    {
                        alert('Saved Successfully');
                        $('#themeSettingsModal').modal('hide');
                        //$scope.showLoader = false;
                        //$scope.addressInfo.$valid = false;
                        //window.location.href= baseUrl+"/superadmin/company-details/"+id;
                    }
                 }, function errorCallback(response) {
                    //$scope.showLoader = false;
                    //$('#custom_error').html('Something went wrong, Please try again!');
                    alert('This is embarassing. An error has occured. Please check your internet connections');
            });
        // }
    }

    function readURL(input) {

        if (input.files && input.files[0]) {
            var reader = new FileReader();        
            reader.onload = function (e) {  

                var ext = $('#imgInp').val().split('.').pop().toLowerCase();
                    if($.inArray(ext, ['png','jpg','jpeg']) == -1) {
                        swal('Invalid file type!','File extension should be ,png,jpg,jpeg!');
                        return false;
                    }
                
                var file = document.getElementById('imgInp').files[0];
                if(file.size >= 2097152){
                    swal("File size is too long.","File size should not be more than 2mb!");
                    //$('#logo_image').attr('src', baseUrl+"/uploads/company_icon.png");
                    $('#upload_btn').hide();
                    return false;
                }
        

                $('#logo_image').attr('src', e.target.result);
                $('#logo_image').attr('width', 200);
                $('#logo_image').attr('hight', 200);
                $('#upload_btn').show();
            }              
            reader.readAsDataURL(input.files[0]);
        }
    }


    $("#imgInp").change(function(){
        readURL(this);    
    });

});