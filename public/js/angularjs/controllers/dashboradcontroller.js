var dashboard = angular.module("dashboardModule", ["chart.js"]);
dashboard.controller("dashboardController", ['$scope', '$timeout', '$http' , function ($scope, $timeout, $http) {

	//chart-options = [];
	$scope.colors = ['#e35b5a', '#44b6ae', '#7c699f'];
	$scope.labels = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
	$scope.series = ['Users', 'Projects','Issued To'];
	
	$scope.getCompnies = function(){
	// Get data from api request
		url = baseUrl+ '/superadmin/companies';
		$http.get(url).
		then(function successCallback(response){
			//console.log(response.data);
			$scope.companies = response.data; 
		},function errorCallback(response){
			console.log(response);
		});
	};

	$scope.cmpGraphData = function(){
	// Get data from api request
		var cmpId = (!$scope.company) ? 0 : $scope.company;
		var url = baseUrl+ '/superadmin/comp-graph/' + cmpId;
		$http.get(url).
		then(function successCallback(response){
			//console.log(response.data);
			var data = response.data; 
			var array = [], finarr = [];
			Object.keys(data).forEach(function (key) {
			    //console.log(data[key]);
			  	array = $.map(data[key], function(value, index) {
			    return [value];
				});
				finarr.push(array);	
			});

			console.log(array);
			$scope.data = finarr;
		},function errorCallback(response){
			console.log(response);
		});
	};

	

	// $scope.data = [
	// [12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12],
	// [12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12],
	// [12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12]
	// ];

	$scope.onClick = function (points, evt) {
	console.log(points, evt);
	};

	// Simulate async data update
	$timeout(function () {
	// Get data from api request
		var cmpId = (!$scope.company) ? 0 : $scope.company;
		var url = baseUrl+ '/superadmin/comp-graph/' + cmpId;
		$http.get(url).
		then(function successCallback(response){
			//console.log(response.data);
			var data = response.data; 
			var array = [], finarr = [];
			Object.keys(data).forEach(function (key) {
			    //console.log(data[key]);
			  	array = $.map(data[key], function(value, index) {
			    return [value];
				});
				finarr.push(array);	
			});

			console.log(finarr);
			$scope.data = finarr;
		},function errorCallback(response){
			console.log(response);
		});
	}, 3000);

}]);