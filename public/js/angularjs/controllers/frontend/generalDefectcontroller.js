var generalDefect = angular.module('generalDefectModule', ['ngFlash', 'ngMessages', 'multipleSelect']);    
    generalDefect.constant("CSRF_TOKEN", $('[name="_token"]').val());


    generalDefect.controller('generalDefectController', function($scope, $http, $timeout,CSRF_TOKEN) {
    //show modal form
    setTimeout(function(){
        $(".alert-success").hide();    
    }, 2000);    

       $timeout(function() {
            var pid = $scope.project_id;
        })   

    $(document).on('click', '.edit-general-defect' , function(){        
        var anc = angular.element("editGeneralDefect");
        var id = $(this).attr('gd-id');                
        anc.bind('click', $scope.showModelFrm('edit', id,$scope.project_id));
    });

    $scope.label = "add";

    $scope.showModelFrm = function(state,defect_general_id,pid){                
        switch(state){
            case 'add':
                $scope.getLocation(pid);
                $scope.formname = "Add";
                $scope.label = "add";
                $('#generalDefectModel').modal('show');
            break;
            case 'edit':
                $scope.formname = "Edit";
                $scope.label = "edit";
                $scope.getLocation(pid);
                $scope.defect_general_id = defect_general_id;
                var url = baseUrl + '/edit_general_defect/'+defect_general_id;
                $http({
                    method: 'GET',
                    url: url,
                }).then(function successCallback(response) {
                    $scope.defect.selectedList = response.data[0].locations;
                    $scope.defect.description = response.data[0].description;
                    $('#generalDefectModel').modal('show');
                });
            break;
        }
    }

    $scope.getLocation = function(pid){
        var url = baseUrl + '/locationTagsval/'+pid;
        $http({
            method: 'GET',
            url: url,                   
        }).then(function successCallback(response) {
            $scope.optionsList = response.data;
        });   
    }

    $scope.save = function(modalState){      
        $scope.loader = true; 
        switch(modalState){
            case 'add':
                $scope.isDisable = true;                
                $scope.generalDefect = {};
                $scope.generalDefect.description = $scope.defect.description;
                $scope.generalDefect.location = $scope.defect.selectedList;
                $scope.generalDefect._token = CSRF_TOKEN;
                $scope.generalDefect.project_id = $scope.project_id;     
                // console.log($scope.multipleSelectForm); 
                // return false;          
                var url = baseUrl+"/save-general-defect";
                    $http({
                        method: 'POST',
                        url: url,
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                        data: $.param($scope.generalDefect)
                    }).then(function successCallback(response) {            
                        //$scope.showLoader = false;
                        if(response.status == 200)
                        {
                            $scope.showLoader = false;                        
                            window.location.href= baseUrl+"/general-defect/"+$scope.project_id;
                        }
                        }, function errorCallback(response) {
                            $scope.showLoader = false;                            
                            swal('This is embarassing. An error has occured. Please check your internet connections');
                    });
            break;
            case 'edit':
                $scope.isDisable = false;
                $scope.generalDefect = {};
                $scope.generalDefect.description = $scope.defect.description;
                $scope.generalDefect.location = $scope.defect.selectedList;
                $scope.generalDefect._token = CSRF_TOKEN;
                $scope.generalDefect.project_id = $scope.project_id;                                      
                var url = baseUrl+"/update-general-defect/"+$scope.defect_general_id;
                    $http({
                        method: 'POST',
                        url: url,
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                        data: $.param($scope.generalDefect)
                    }).then(function successCallback(response) {            
                        //$scope.showLoader = false;
                        if(response.status == 200)
                        {
                            $scope.showLoader = false;                        
                            window.location.href= baseUrl+"/general-defect/"+$scope.project_id;
                        }
                        }, function errorCallback(response) {
                            $scope.showLoader = false;                            
                            swal('This is embarassing. An error has occured. Please check your internet connections');
                    });
            break;
            default:
            break;
        }
    }

    $scope.cancel = function (form) {
        $scope.defect.selectedList=[];
        $scope.defect.description='';
        form.$setPristine();
        form.$setUntouched();
    };

    $scope.deleteGeneralDefect = function(id,project_id){
        swal({
          title: "Are you sure?",
          text: "General defect will get deleted from this list!",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Yes, delete it!",
          cancelButtonText: "No, cancel please!",
          closeOnConfirm: false,
          closeOnCancel: false
        },
        function(isConfirm){
          if (isConfirm) {                    
            $scope.prjId = {};
            $scope.prjId.id = id;
            var url = baseUrl+"/delete-general-defect/"+id;
            $http({
                method: 'Get',
                url: url,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},                
            }).then(function successCallback(response) {            
                //$scope.showLoader = false;
                if(response.status == 200)
                {
                    $scope.showLoader = false;      
                    swal("Deleted!", "A General defect has been deleted.", "success");                  
                    window.location.href= baseUrl+"/general-defect/"+project_id;
                }
                }, function errorCallback(response) {
                    $scope.showLoader = false;
                    $scope.isaddPermissionDisabled = false;
                    swal('This is embarassing. An error has occured. Please check your internet connections');
            });
          } else {
            swal("Cancelled", "Your General defect is safe :)", "error");
          }
        });
    }

    $(document).on('click', '.delete-general-defect' , function(){        
        var anc = angular.element("deleteGdefectId");
        var id = $(this).attr('deleteGdefectId');
        anc.bind('click', $scope.deleteGeneralDefect(id,$scope.project_id));
    });

    $(document).ready(function() {
        $( "input[name='multipleSelect']" ).addClass('form-control');

        var csrf_token = $('[name="_token"]').val();
        var project_id = $scope.project_id;
        $('#sample_1').DataTable( {

            "processing": true,
            "serverSide": true,
            "ajax": baseUrl+"/general_defect_list/?project_id="+project_id,
            columns: [
                {data: 'description', name: 'description'},
                {data: 'location', name: 'location'},               
                {data: 'action', name: 'action', orderable: false, searchable: false},
            ]
        });

    });

});
