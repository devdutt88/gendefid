var profile = angular.module('profileModule', ['ngFlash' ,'ngMessages']);
    profile.constant('API_URL', baseUrl+'/');    
    profile.constant("CSRF_TOKEN", $('[name="_token"]').val());
    profile.directive('passwordVerify', passwordVerify);
    profile.directive('passwordExists', passwordExists);
    profile.directive('ngFiles', ['$parse', function ($parse) {

        function fn_link(scope, element, attrs) {
            var onChange = $parse(attrs.ngFiles);
            element.on('change', function (event) {
                onChange(scope, { $files: event.target.files });
            });
        };

        return {
            link: fn_link
        }
    } ]);


/***
** Custom directive for email exist check
**/
profile.directive('emailExists', function($http , $q ) {

    return {
        restrict: 'AE',
        require: 'ngModel',
        link: function($scope, elem, attrs, ngModel) { 
            ngModel.$asyncValidators.uniqueEmail = function(modelValue, viewValue) {                                
                var value = modelValue || viewValue;
                var id = $scope.profile.id;
                // console.log(id);
                // // Lookup user by password                
                return $http.get(baseUrl+ '/user_check_emailexist/' + id +'/'+ value).
                then(function resolved(response) {
                   //password exists, this means validation fails
                   var valid = response.data;
                    if(valid == "true"){
                        // $scope.show_err= true;
                        ngModel.$setValidity('emailExists', false);
                        return $q.reject();
                    }else{
                        // $scope.show_err= false;
                        ngModel.$setValidity('emailExists', true);
                        return $q.resolve();
                    }                        
                }, function rejected(response) {
                    //email does not exist, therefore this validation passes
                    console.log(response.data);
                    return false;
                });
            };
        }
    } 
});

 /***
    ** Custom directive for password match
    **/
    function passwordVerify() {
        return {
            restrict: 'A', // only activate on element attribute
            require: '?ngModel', // get a hold of NgModelController
            link: function(scope, elem, attrs, ngModel) {
                if (!ngModel) return; // do nothing if no ng-model

                // watch own value and re-validate on change
                scope.$watch(attrs.ngModel, function() {
                  validate();
                });

                // observe the other value and re-validate on change
                attrs.$observe('passwordVerify', function(val) {
                  validate();
                });

                var validate = function() {
                  // values
                  var val1 = ngModel.$viewValue;
                  var val2 = attrs.passwordVerify;

                  // set validity
                  ngModel.$setValidity('passwordVerify', val1 === val2);
                };
            }
        }
    }

    /***
    ** Custom directive for password exist check
    **/
    function passwordExists ($http , $q) {
        return {
            restrict: 'AE',
            require: 'ngModel',
            link: function($scope, elem, attrs, ngModel) { 
                ngModel.$asyncValidators.uniquePassword = function(modelValue, viewValue) {
                    var value = modelValue || viewValue;
                    //console.log(encodeURIComponent(value));

                    // Lookup user by password
                    return $http.get(baseUrl+ '/userCurrentPwd/' + encodeURIComponent(value)).
                    then(function resolved(response) {
                       //password exists, this means validation fails
                       var valid = response.data;
                        if(valid == "true"){
                            ngModel.$setValidity('passwordExists', true);
                            return  $q.resolve();
                        }else
                            ngModel.$setValidity('passwordExists', false);
                            return $q.reject();
                    }, function rejected(response) {
                        //password does not exist, therefore this validation passes
                        console.log(response.data);
                        return false;
                    });
                };
            }
        } 
    }



profile.controller('profileController',['$scope','$http','API_URL','$filter','Flash','CSRF_TOKEN', function ($scope,$http,API_URL,$filter,Flash,CSRF_TOKEN) {  
    var url = API_URL + 'getProfileData';
    $http({method: 'GET', url: url}).
    then(function successCallback(response) {
            //console.log(response.data);                     
            $scope.profile = response.data;               
            if(response.data.logo_image == null || !response.data.logo_image){                
                $scope.user_img = API_URL+"/uploads/user_icon.jpg";
                $scope.user_logo = API_URL+"/img/images/no_image.png";
            }else{                
                //alert(response.data.logo_image);                
                $scope.user_img = $scope.user_logo = API_URL+"/uploads/user/thumbnail_200x200/"+response.data.logo_image;
                //$scope.user_logo = API_URL+"/img/images/no_image.png";
                //$scope.logo_image = response.data.logo_image;
            }            
    }, function errorCallback(response) {
            console.log(response);
            swal('This is embarassing. An error has occured. Please check your internet connections');
    });

    $('.fileinput-exists').click(function(){
        $('#logo_image_profile').attr('src', $scope.user_logo);
        $('#user_logo').attr('src', $scope.user_logo);            
    });
    // Javascript image reader method
    function readLogoImage(input) {
        console.log(input);
        if (input.files && input.files[0]) {
            var reader = new FileReader();        
            reader.onload = function (e) {  
                var ext = $('#user_image').val().split('.').pop().toLowerCase();
                    if($.inArray(ext, ['png','jpg','jpeg']) == -1) {
                        $scope.showLoader = false;
                        swal('Invalid file type!','File extension should be ,png,jpg,jpeg!');
                        //$('.fileinput-preview').attr('src',$scope.user_logo);
                        $(".fileinput-exists").click();
                        var img = $('<img>');
                        img.attr('src', $scope.user_logo);
                        img.appendTo('.fileinput-preview');

                        $('#logo_image_profile').attr('src', $scope.user_logo);
                        $('#user_logo').attr('src', $scope.user_logo);                                                    
                        return false;
                    }
                
                var file = document.getElementById('user_image').files[0];
                if(file.size >= 2097152){
                    $scope.showLoader = false;
                    swal("File size is too long.","File size should not be more than 2mb!");
                    //$('#logo_image').attr('src', baseUrl+"/uploads/user_icon.jpg");
                    //$('#upload_btn').hide();
                    return false;
                }                         
                $('#logo_image_profile').attr('src', e.target.result);
                $('#user_logo').attr('src', e.target.result);                            
                $('#upload_btn').show();
            }              
            reader.readAsDataURL(input.files[0]);
        }
    };

    $("#user_image").change(function(){
        readLogoImage(this);    
    });

    // reset form data
    $scope.reset = function (formid) {
        form.$setPristine();
        form.$setUntouched();
    };


    $scope.save = function(label){              
        var url = API_URL + 'saveProfile';
        $scope.profile._token = CSRF_TOKEN;
        $scope.profile.profile = label;
        switch(label){
            case 'profile': 
                //console.log($scope.profile); 
                console.log($scope.profile.email);               
                    $http({
                        method: 'POST',
                        url: url,            
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},            
                        data: $.param($scope.profile)
                    }).then(function successCallback(response) {                        
                        if(response.status == 200)
                        {
                            $scope.showLoader = false;
                            $scope.profile.$valid = false;
                            Flash.create('success', "Profile Updated Successfully!", 2000, {class: 'cst-alert cst-company-success-alert'});
                        }
                     }, function errorCallback(response) {
                        $scope.showLoader = false;                        
                        Flash.create('danger', "Something went wrong ! Try Again ", 2000, {class: 'cst-alert cst-company-danger-alert'});
                    });
                break;
            case 'image':
                var formdata = new FormData(document.getElementById("profileInfoAvtar"));                                
                formdata.append('userform' , formdata);
                formdata.append('id' , $scope.profile.id);
                formdata.append('_token', CSRF_TOKEN);
                formdata.append('profile', label);                                
                $http({
                    method: 'POST',
                    url: url,
                    headers: {'Content-Type': undefined},
                    data: formdata
                }).then(function successCallback(response) {                    
                    if(response.data == 1){
                        Flash.create('success', "User image Updated Successfully!", 2000, {class: 'cst-alert cst-company-success-alert'});
                    }else if(response.data == 2){
                        Flash.create('danger', "User image Updated Failed!", 2000, {class: 'cst-alert cst-company-danger-alert'});    
                    }else{
                        Flash.create('danger', "Please select Images", 2000, {class: 'cst-alert cst-company-danger-alert'});    
                    }
                 }, function errorCallback(response) {
                    $scope.showLoader = false;
                    Flash.create('danger', "Something went wrong ! Try Again ", 2000, {class: 'cst-alert cst-company-danger-alert'});
                });
            break;
            case 'chngpwd':
                $scope.adminPassword._token = CSRF_TOKEN;
                var url = baseUrl+ "/changeUserPassword";
                // cheack form data is valid
                //console.log($scope.changePasswordFrm.$valid);
                if ($scope.changePasswordFrm.$valid) {
                    $http({
                        method: 'POST',
                        url: url,
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                        data: $.param($scope.adminPassword)
                    }).then(function successCallback(response) {                       

                            Flash.create('success', "Password Updated Successfully!", 2000, {class: 'cst-alert cst-company-success-alert'});
                            $scope.adminPassword = {};
                            // changePasswordFrm.$setPristine();
                            // changePasswordFrm.$setUntouched();
                            $scope.changePasswordFrm = angular.copy($scope.master);
                            $scope.changePasswordFrm.$setPristine();

                        }, function errorCallback(response) {
                            console.log(response.data);
                            Flash.create('danger', "Something went wrong ! Try Again ", 2000, {class: 'cst-alert cst-company-danger-alert'});
                    });
                }else{
                    Flash.create('danger', "Something went wrong ! Try Again ", 2000, {class: 'cst-alert cst-company-danger-alert'});
                }            
            break;
            default:
                break;
        }
    }    

}]);