var project = angular.module('ProjectApp', ['ngFlash', 'ngMessages']);
    project.constant('API_URL', baseUrl);
    project.constant("CSRF_TOKEN", $('[name="_token"]').val());

project.controller('projectController', ['$scope','$http','API_URL','$filter','Flash','CSRF_TOKEN','$location', function ($scope, $http, API_URL,$filter,Flash,CSRF_TOKEN,$location) {

    setTimeout(function(){
        $(".alert-success").hide();    
    }, 2000);
    
    //save new record / update existing record
    $scope.update_details = function (id,projectform,name) {
        console.log(id,projectform,name);
        var url = API_URL + "/update-logo";
        $scope.project = {};
        $scope.project.id = id;
        $scope.project.projectform = projectform;
        $scope.project._token = CSRF_TOKEN;                
        switch (projectform) {            
            case 'logo':
                var formdata = new FormData(document.getElementById("projectLogoFrm"));
                formdata.append('projectform' , projectform);
                formdata.append('id' , id);
                formdata.append('name' , name);
                $scope.showLogoLoader = true;

                $http({
                    method: 'POST',
                    url: url,
                    headers: {'Content-Type': undefined},
                    data: formdata
                }).then(function successCallback(response) {
                    //console.log(response.data);
                    $scope.showLogoLoader = false;                    
                    if(response.status == 200)
                    {
                        location.reload(); 
                    }
                 }, function errorCallback(response) {
                    $scope.showLogoLoader = false;
                    swal('This is embarassing. An error has occured. Please check your internet connections');
                });
                break;
            default: 
                swal("Please select any form");
                break;
        }           
    };
    
    $scope.cancel = function (form) {
        form.$setPristine();
        form.$setUntouched();
    };

    function readLogoImage(input) {
        console.log(input);
        if (input.files && input.files[0]) {
            var reader = new FileReader();        
            reader.onload = function (e) {  

                var ext = $('#projectImg').val().split('.').pop().toLowerCase();
                    if($.inArray(ext, ['png','jpg','jpeg']) == -1) {
                        $scope.showLogoLoader = false;
                        swal('Invalid file type!','File extension should be ,png,jpg,jpeg!');
                        return false;
                    }
                
                var file = document.getElementById('projectImg').files[0];
                if(file.size >= 2097152){
                    $scope.showLogoLoader = false;
                    swal("File size is too long.","File size should not be more than 2mb!");
                    //$('#logo_image').attr('src', baseUrl+"/uploads/project_icon.png");
                    $('#upload_btn').hide();
                    return false;
                }


                $('#logo_image').attr('src', e.target.result);
                $('#upload_btn').show();
            }              
            reader.readAsDataURL(input.files[0]);
        }
    };

    $("#projectImg").change(function(){
        readLogoImage(this);    
    });
}]);
