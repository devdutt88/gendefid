// Javascript image reader method
    function readLogoImage(input) {
        console.log(input);
        if (input.files && input.files[0]) {
            var reader = new FileReader();        
            reader.onload = function (e) {  
                var ext = $('#locationcsv').val().split('.').pop().toLowerCase();
                    if($.inArray(ext, ['csv']) == -1) {        
                        $('#remove').click();
                        swal('Invalid file type!','File extension should be csv!');
                        return false;
                    }
                
                var file = document.getElementById('locationcsv').files[0];
                if(file.size >= 2097152){                    
                    swal("File size is too long.","File size should not be more than 2mb!");
                    //$('#logo_image').attr('src', baseUrl+"/uploads/user_icon.jpg");
                    //$('.fileinput-exists').click();
                    $('#remove').click();
                    return false;
                }                         
                $('#logo_image').attr('src', e.target.result);
                $('#upload_btn').show();
            }              
            reader.readAsDataURL(input.files[0]);
        }
    };

    $("#locationcsv").change(function(){      
        readLogoImage(this);    
    });