var projectAssociateUser = angular.module('projectAssociateUserModule', ['AxelSoft']);    
    projectAssociateUser.constant("CSRF_TOKEN", $('[name="_token"]').val());

projectAssociateUser.controller('projectAssociateUsercontroller', function($scope, $http, $timeout,CSRF_TOKEN,$q) {
    setTimeout(function(){
        $(".alert-success").hide();    
    }, 2000);
    //console.log($scope.project_id);
    // $rootScope.btn_label = "Select Roles";
    var pid = "";
    $timeout(function() {
        pid = $scope.project_id;
        //alert(pid);
        var url = baseUrl + '/user-list/'+pid;
        $http({
            method: 'GET',
            url: url,                   
        }).then(function successCallback(response) {                    
            $scope.people = response.data;
            $scope.getRole();
        });

      })
    $scope.showLoader = false;
    $scope.getRole = function(){
      var url = baseUrl + '/role-list';
      $http({
          method: 'GET',
          url: url,
      }).then(function successCallback(response) {                    
          $scope.role = response.data;
      });
    }
    
    $scope.saveAssociateUser = function(modalState){      
      switch(modalState){
        case 'companyAssociate':
          $scope.loader  = true;
          $scope.isDisable = true;
          $scope.assoc = {};
          $scope.assoc.user = users.id;
          $scope.assoc.role = roles.id;
          $scope.assoc._token = CSRF_TOKEN;
          $scope.assoc.project_id = $scope.project_id;
          var url = baseUrl + "/save-associate";                
          $http({
              method: 'POST',
              url: url,
              data: $.param($scope.assoc),
              headers: {'Content-Type': 'application/x-www-form-urlencoded'}
          }).then(function successCallback(response) {
              console.log(response);
              location.reload();
          }, function errorCallback(response) {
              console.log(response);
              alert('This is embarassing. An error has occured. Please check the log for details');
          });
        break;
        default:
        break;
      }
    }

    $scope.showEditForm = function(modalState,id){
      switch(modalState){
        case 'add':
        break;
        case 'edit':
          console.log($scope.role);
          $scope.getRole();
          $scope.uid = id;
          $scope.allRoles = $scope.role;
          $scope.isDisabled = true;
          $('#project_role').modal('show');
        break;
        default:
        break;
      }
    }

    $scope.changeRole = function(uid){   
      $scope.showLoader = true;
      $scope.userRole = {};
      $scope.userRole._token = CSRF_TOKEN;
      $scope.userRole.rolename = $scope.role.rolename;
      var url = baseUrl + "/update-user-role/"+uid;
          $http({
              method: 'POST',
              url: url,
              data: $.param($scope.userRole),
              headers: {'Content-Type': 'application/x-www-form-urlencoded'}
          }).then(function successCallback(response) {
              console.log(response);
              location.reload();
          }, function errorCallback(response) {
              console.log(response);
              alert('This is embarassing. An error has occured. Please check the log for details');
          });
    }

    $(document).on('click', '.edit-ist' , function(){        
        var anc = angular.element("editPAURto");
        var id = $(this).attr('paur-id');             
        anc.bind('click', $scope.showEditForm('edit', id));
    });

    // datatables
    $(document).ready(function() {
        //alert($scope.project_id);
        var csrf_token = $('[name="_token"]').val();
        var project_id = $scope.project_id;
        $('#sample_1').DataTable( {

            "processing": true,
            "serverSide": true,
            "ajax": baseUrl+"/associate-user-list?project_id="+project_id,
            columns: [
                {data: 'name', name: 'name'},
                {data: 'full_name', name: 'full_name'},
                {data: 'role_name', name: 'role_name'},                
                {data: 'modified_by', name: 'modified_by'},                
                {data: 'modified_at', name: 'modified_at'},                
                {data: 'action', name: 'action', orderable: false, searchable: false},
            ]
        });        
        
    } );
        
});