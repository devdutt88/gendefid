var projectChecklist = angular.module('projectChecklistModule', ['ngTagsInput']);
    projectChecklist.constant('API_URL', baseUrl+'/superadmin/');

projectChecklist.controller('projectChecklistController', function($scope, $http, API_URL,$timeout) {
    //show modal form
    setTimeout(function(){
        $(".alert-success").hide();    
    }, 2000);
    $scope.checklist = {};
    $scope.checklistTask = {};
    $scope.showChecklist = function(modalstate='', id='') {
        $scope.modalstate = modalstate;
        switch (modalstate) {
            case 'add':
            //alert("test");
                $scope.form_title = "Add New Checklist";
                $scope.label = "add";
                $scope.id = 0;
                $scope.issueto = {};
                $('#checklistModal').modal('show');
                break;
            case 'edit':
                $scope.label = "edit";
                $scope.id = id;
                $('#checklistModal').modal('show');
                $scope.form_title = "Checklist Detail";
                $scope.id = id;
                $scope.project_id;
                var url = baseUrl + '/edit-checklist/' + id;
                $http({
                    method: 'GET',
                    url: url,                   
                }).then(function successCallback(response) {
                    $scope.checklist = response.data;
                });  
                break;
            // case 'cmpIsto':
            //     $('#issuetoCmpModal').modal('show');
            //     break;
            default:
                break;
        }
    }

    $timeout(function() {
        var pid = $scope.project_id;
    })

    $scope.loadTags = function(query) {
         return $http.get(baseUrl+'/qc-locations?tag=' + query);
    };

    //save new record / update existing record
    $scope.saveChecklist = function(modalstate, id=0) {  

        $scope.checklist.id = id;
        $scope.checklist.modalstate = modalstate;
        $scope.checklist.project_id = $scope.project_id;
        var url = baseUrl + "/save_checklist";
        $http({
            method: 'POST',
            url: url,
            data: $.param($scope.checklist),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).then(function successCallback(response) {
            console.log(response);
            location.reload();
        }, function errorCallback(response) {
            console.log(response);
            alert('This is embarassing. An error has occured. Please check the log for details');
        });
    }

    $(document).on('click', '.edit-ist' , function(){
        var anc = angular.element("editChecklist");
        var id = $(this).attr('Is-id');
        anc.bind('click', $scope.showChecklist('edit', id));
    });

    $scope.showChecklistTask = function(modalstate, id='', checklist_id='') {
        
        $scope.checklistTask.modalstate = modalstate;
        console.log('test');
        switch (modalstate) {
            case 'add':

                $scope.checklistTask.form_title = "Add New Task";
                $scope.checklistTask = {};
                console.log($scope.checklistTask);
                $('#checklistTaskModal').modal('show');
                break;
            case 'edit':
                $('#checklistTaskModal').modal('show');
                $scope.form_title = "Checklist Task Detail";
                $scope.id = id;
                $scope.project_id;
                var url = API_URL + 'projectConfiguration/checklist/edit-checklist-task/'+ id;
                $http({
                    method: 'POST',
                    url: url,
                    //data: $.param($scope.issueto),
                    //headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).then(function successCallback(response) {
                    console.log(response);
                    $scope.checklistTask = response.data;
                });  
                break;
            default:
                break;
        }
        //$('#editUserModel').modal('show');
    }

    $(document).on('click', '.edit_ius' , function(){
        var anc = angular.element("editT");
        var id = $(this).attr('IsUID');
        anc.bind('click', $scope.showChecklistTask('edit', id));
    });

    $scope.saveIssuetoUser = function(issuedto_id='') {

        var url = API_URL + "projectConfiguration/save_issueto_user/"+issuedto_id;
        $http({
            method: 'POST',
            url: url,
            data: $.param($scope.issuetoUser),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).then(function successCallback(response) {
            $scope.showLoader = false;
            if(response.status == 200)
            {
                window.location.href= baseUrl+"/superadmin/projectConfiguration/issueto-details/"+issuedto_id;
            }
        }, function errorCallback(response) {
            console.log(response);
            alert('This is embarassing. An error has occured. Please check the log for details');
        });
    }

    $scope.cancel = function (formid) {
        $('#'+formid).modal('hide');
    };

    $scope.orderList = "name";
    var url = baseUrl+'/superadmin/projectConfiguration/getAllProjectData';
    $http({method: 'GET', url: url}).
    then(function(response) {
        console.log(response);         
        //alert(response.data.cmpname);
        $scope.plists = response.data;
        console.log(response.data);
        //alert($scope.cmpname);                  
    }, function(response) {
        $scope.data = response.data || 'Request failed';
        $scope.status = response.status;
    });
    
    // datatables
    $(document).ready(function() {
        //alert($scope.project_id);
        var csrf_token = $('[name="_token"]').val();
        var project_id = $scope.project_id;
        $('#sample_1').DataTable( {

            "processing": true,
            "serverSide": true,
            "ajax": baseUrl+"/checklist/?project_id="+project_id,
            columns: [
                {data: 'checklist_name', name: 'checklist_name'},
                {data: 'checklist_tags', name: 'checklist_tags'},
                {data: 'action', name: 'action', orderable: false, searchable: false},
            ]
        });

        // $('#sample_2').DataTable( {
        //     "processing": true,
        //     "serverSide": true,
        //     "ajax": baseUrl+"/superadmin/projectConfiguration/issueto_users_list/"+issueto_id,
        //     columns: [
        //         {data: 'name', name: 'name'},
        //         {data: 'email', name: 'email'},
        //         {data: 'phone', name: 'phone'},
        //         {data: 'activity', name: 'activity'},
        //         {data: 'action', name: 'action', orderable: false, searchable: false},
        //     ]
        // });
        
    } ); 

});