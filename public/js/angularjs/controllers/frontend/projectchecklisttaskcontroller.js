var projectChecklistTask = angular.module('projectChecklistTaskModule', ['ngFlash', 'ngMessages', 'ngTagsInput']);    
    projectChecklistTask.constant("CSRF_TOKEN", $('[name="_token"]').val());

/***
** Custom directive for email exist check
**/
/*
projectIssuetoUser.directive('issuetoEmailExists', function($http , $q ) {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function($scope, elem, attrs, ngModel) { 
            ngModel.$asyncValidators.uniqueEmail = function(modelValue, viewValue) {
                var value = modelValue || viewValue;
                var id = (!$scope.issueto.id) ? 0 : $scope.issueto.id;
                // Lookup user by password
                return $http.get(baseUrl+ '/superadmin/issueto_emailid/' + id +'/'+ value).
                then(function resolved(response) {
                   //password exists, this means validation fails
                   var valid = response.data;
                    if(valid == "true"){
                        ngModel.$setValidity('emailExists', false);
                        return $q.reject();
                    }else
                        ngModel.$setValidity('emailExists', true);
                        return $q.resolve();
                }, function rejected(response) {
                    //email does not exist, therefore this validation passes
                    console.log(response.data);
                    return false;
                });
            };
        }
    } 
});
*/

projectChecklistTask.controller('projectChecklistTaskController', function($scope, $http, $timeout,CSRF_TOKEN) {
    //show modal form
    setTimeout(function(){
        $(".alert-success").hide();    
    }, 2000);
    $scope.checklist = {};
    $scope.checklistTask = {};
    
    // Company issue to list

       $timeout(function() {
            var pid = $scope.project_id;
        })   

    $(document).on('click', '.edit-ist' , function(){        
        var anc = angular.element("editChecklist");
        var id = $(this).attr('is-id');
        anc.bind('click', $scope.showChecklistTask('edit', id));
    });


    // $scope.cancel = function (formid) {
    //     $('#'+formid).modal('hide');
    // };
        
    // Checklist Task
    $scope.showChecklistTask = function(modalstate,id=0){
        switch(modalstate){
            case 'add':
                $scope.checklistTask = {};
                $scope.checklistTask.form_title = "Add New Task";
                $scope.checklistTask.checklist_id = $scope.checklist_id;
                console.log($scope);
                $('#checklistTaskModal').modal('show');
                break;
            break;
            case 'edit':
                // alert($scope.issuedto_id);
                // alert(id);
                var url = baseUrl + '/edit-checklistTask/' + id;
                $scope.checklistTask.form_title = "Checklist Task Details";
                $http({
                    method: 'GET',
                    url: url,
                }).then(function successCallback(response) {
                    $scope.checklistTask = response.data;
                    //alert(response.data.issueto_id);
                    $('#checklistTaskModal').modal('show');
                });            
                break;
            default:
            break;
        }
    }

    $scope.saveChecklistTask = function(checklist_id='') {
    $scope.checklistTask.checklist_id = $scope.checklist_id;
    //alert($scope.checklist_id);
    $scope.checklistTask._token = CSRF_TOKEN;
        var url = baseUrl + "/save_checklist_task/"+$scope.project_id;
        $http({
            method: 'POST',
            url: url,
            data: $.param($scope.checklistTask),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).then(function successCallback(response) {            
            $scope.showLoader = false;
            location.reload();
        }, function errorCallback(response) {
            console.log(response);
            alert('This is embarassing. An error has occured. Please check the log for details');
        });
    }

    $scope.reset = function(form) {
        form.$setPristine();
        form.$setUntouched();
        $scope.issueto.phone = null;
        $scope.issueto.email = null;
        $scope.checklistTask.phone = null;
        $scope.checklistTask.email = null;
        
        //angular.copy({},form);
    };

    $scope.loadTags = function(query) {
        return $http.get(baseUrl+'/locationTags?tag=' + query);
    };

    // datatables
    $(document).ready(function() {
        // /alert($scope.issueTo_id);
        var csrf_token = $('[name="_token"]').val();
        var project_id = $scope.project_id;
        $('#sample_1').DataTable( {

            "processing": true,
            "serverSide": true,
            "ajax": baseUrl+"/checklist-taskList/"+$scope.checklist_id,
            columns: [
                {data: 'name', name: 'name'},                
                {data: 'status', name: 'status'},
                {data: 'comment', name: 'comment'},
                {data: 'action', name: 'action', orderable: false, searchable: false},
            ]
        });

        // $('#sample_2').DataTable( {
        //     "processing": true,
        //     "serverSide": true,
        //     "ajax": baseUrl+"/superadmin/projectConfiguration/issueto_users_list/"+$scope.issueto_id,
        //     columns: [
        //         {data: 'name', name: 'name'},
        //         {data: 'email', name: 'email'},
        //         {data: 'phone', name: 'phone'},
        //         {data: 'activity', name: 'activity'},
        //         {data: 'action', name: 'action', orderable: false, searchable: false},
        //     ]
        // });
        
    } );    

});