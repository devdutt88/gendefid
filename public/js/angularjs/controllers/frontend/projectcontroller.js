var project = angular.module('ProjectApp', ['ProjectApp.controllers','datatables','ngFlash', 'ngMessages']);        
    project.constant("CSRF_TOKEN", $('[name="_token"]').val());

/***
** Custom directive for email exist check
**/
project.directive('projectExists', function($http , $q ) {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function($scope, elem, attrs, ngModel) { 
            ngModel.$asyncValidators.uniqueEmail = function(modelValue, viewValue) {
                var value = modelValue || viewValue;                
                id = document.getElementsByName('project_hid')[0].value;
                //alert(id);
                var id = (!id) ? 0 : id;
                // Lookup user by password
                //return $http.post(baseUrl+ '/superadmin/project_isexist/' + id +'/'+ value),data: {'name':value,'jq':0}.
                var url = baseUrl+ '/project_isexist/' + id;
                return $http({
                        method: 'POST',
                        url: url,
                        headers: {'Content-Type': 'application/json; charset=utf-8'},
                        data: {'name':value,'jq':0}
                    }).
                then(function resolved(response) {
                   //password exists, this means validation fails
                   var valid = response.data;
                    if(valid == "true"){
                        ngModel.$setValidity('projectExists', false);
                        return $q.reject();
                    }else
                        ngModel.$setValidity('projectExists', true);
                        return $q.resolve();
                }, function rejected(response) {
                    //email does not exist, therefore this validation passes
                    console.log(response.data);
                    return false;
                });
            };
        }
    } 
});


angular.module('ProjectApp.controllers', []).controller('projectController', function($scope,$http,DTOptionsBuilder, DTColumnBuilder,CSRF_TOKEN) {
        /***********project list************/        
        var url = baseUrl + '/project_list';
                $http({method: 'GET', url: url}).
                then(function(response) {
                console.log(response);         
                   //alert(response.data.cmpname);
                   console.log(response.data);
                    //$scope.plists = response.data;
                    console.log(response.data);
                    $scope.projectList = response.data;
                  //alert($scope.cmpname);                  
                }, function(response) {
                    $scope.data = response.data || 'Request failed';
                    $scope.status = response.status;
                });    
              
                $scope.vm = {};

                $scope.vm.dtOptions = DTOptionsBuilder.newOptions()
                  .withOption('order', [0, 'asc']);
                  DTOptionsBuilder.newOptions().withOption('initComplete', function() {
                angular.element('.dataTables_filter input').attr('placeholder', 'Search ...');
        })


        // //add and edit project************/    

        setTimeout(function(){
            $(".alert-success").hide();    
        }, 2000);    
        $scope.types = ['Educational','Industrial',"Offices","Residential","Retail","Other"];
        $scope.states = ['NSW','QLD','SA','VIC','WA'];
        $scope.project = {};
        $scope.project.country = "Australia";
        // // alert(angular.element(document.getElementsByName('hid')[0]).val());
        //alert(angular.element(document.getElementsByName('hid')[0]).val());
        var project_hid = document.getElementsByName('project_hid')[0];        
        if(typeof project_hid != 'undefined')
        {
            
            var id = document.getElementsByName('project_hid')[0].value;            
            if(id != '0'){
                $scope.label = "Edit Project";    
                $scope.label_button = "Update";
            }else{
                $scope.label = "Add Project";
                $scope.label_button = "Submit";
            }            
            if(id!=0)
            {
                var url = baseUrl + '/form-data/'+id;
                $http({method: 'GET', url: url}).
                then(function(response) {
                console.log(response);         
                   //alert(response.data.cmpname);
                   console.log(response.data);
                   $scope.project = response.data;
                    //$scope.plists = response.data;            
                    //$scope.projectList = response.data;
                  //alert($scope.cmpname);                  
                }, function(response) {
                    $scope.data = response.data || 'Request failed';
                    $scope.status = response.status;
                });
            }
        }
        else{
            $scope.label = "Add Project";
        }
       
        $scope.submitForm = function(status,id){
            switch(status){
                case 'addProject':
                    // console.log($scope.project);
                    // return false;
                    $scope.isaddPermissionDisabled = true;                    
                    $scope.project._token = CSRF_TOKEN;                            
                    $scope.showLoader = true;                    
                    var url = baseUrl+"/save-project";
                    $http({
                        method: 'POST',
                        url: url,
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                        data: $.param($scope.project)
                    }).then(function successCallback(response) {            
                        //$scope.showLoader = false;
                        if(response.status == 200)
                        {
                            $scope.showLoader = false;                        
                            window.location.href= baseUrl+"/project";
                        }
                        }, function errorCallback(response) {
                            $scope.showLoader = false;
                            $scope.isaddPermissionDisabled = false;
                            swal('This is embarassing. An error has occured. Please check your internet connections');
                    });
                break;
                case 'editProject':
                    $scope.isaddPermissionDisabled = true;                    
                    $scope.project._token = CSRF_TOKEN;  
                    $scope.project.id = id;                          
                    $scope.showLoader = true;
                    var url = baseUrl+"/update-project";
                    $http({
                        method: 'POST',
                        url: url,
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                        data: $.param($scope.project)
                    }).then(function successCallback(response) {            
                        //$scope.showLoader = false;
                        if(response.status == 200)
                        {
                            $scope.showLoader = false;                        
                            window.location.href= baseUrl+"/project";
                        }
                        }, function errorCallback(response) {
                            $scope.showLoader = false;
                            $scope.isaddPermissionDisabled = false;
                            swal('This is embarassing. An error has occured. Please check your internet connections');
                    });
                break;
                default:
                break;
            }
        }

        $scope.delete = function(id){
            swal({
                  title: "Are you sure?",
                  text: "Project will get deleted from this list!",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonColor: "#DD6B55",
                  confirmButtonText: "Yes, delete it!",
                  cancelButtonText: "No, cancel please!",
                  closeOnConfirm: false,
                  closeOnCancel: false
                },
                function(isConfirm){
                  if (isConfirm) {                    
                    $scope.prjId = {};
                    $scope.prjId.id = id;
                    var url = baseUrl+"/delete-project";
                    $http({
                        method: 'POST',
                        url: url,
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                        data: $.param($scope.prjId)
                    }).then(function successCallback(response) {            
                        //$scope.showLoader = false;
                        if(response.status == 200)
                        {
                            $scope.showLoader = false;      
                            swal("Deleted!", "Project has been deleted.", "success");                  
                            window.location.href= baseUrl+"/project";
                        }
                        }, function errorCallback(response) {
                            $scope.showLoader = false;
                            $scope.isaddPermissionDisabled = false;
                            swal('This is embarassing. An error has occured. Please check your internet connections');
                    });
                  } else {
                    swal("Cancelled", "Your project is safe :)", "error");
                  }
                });
        }
});