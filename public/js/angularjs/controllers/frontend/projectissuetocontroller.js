var projectIssueto = angular.module('projectIssuetoModule', ['ngMessages']);
    projectIssueto.constant('API_URL', baseUrl+'/superadmin/');

    /***
    ** Custom directive for email exist check
    **/
    projectIssueto.directive('issuetoEmailExists', function($http , $q ) {
        return {
            restrict: 'A',
            require: 'ngModel',
            link: function($scope, elem, attrs, ngModel) { 
                ngModel.$asyncValidators.uniqueEmail = function(modelValue, viewValue) {
                    var value = modelValue || viewValue;                        
                    //id = document.getElementsByName('project_hid')[0].value;                    
                    var id = (!$scope.issueto.id) ? 0 : $scope.issueto.id;
                    $scope.showLoader = true;
                    // Lookup user by password
                    //return $http.post(baseUrl+ '/superadmin/project_isexist/' + id +'/'+ value),data: {'name':value,'jq':0}.
                    var url = baseUrl+ '/issueto-email-exist/' + id;
                    return $http({
                            method: 'POST',
                            url: url,
                            headers: {'Content-Type': 'application/json; charset=utf-8'},
                            data: {'email':value,'pid':$scope.project_id,'jq':0}
                        }).
                    then(function resolved(response) {
                        $scope.showLoader = false;
                       //password exists, this means validation fails
                       var valid = response.data;
                        if(valid == "true"){
                            ngModel.$setValidity('issuetoEmailExists', false);
                            return $q.reject();
                        }else
                            ngModel.$setValidity('issuetoEmailExists', true);
                            return $q.resolve();
                    }, function rejected(response) {
                        //email does not exist, therefore this validation passes
                        console.log(response.data);
                        return false;
                    });
                };
            }
        } 
    });

projectIssueto.controller('projectIssuetoController', function($scope, $http, API_URL,$timeout) {
    //show modal form
    setTimeout(function(){
        $(".alert-success").hide();    
    }, 2000);
    $scope.issueto = {};
    $scope.issuetoUser = {};        
    $scope.showIssueto = function(modalstate='', id='') {
        $scope.modalstate = modalstate;       
        switch (modalstate) {
            case 'add':
            //alert("test");
                // $scope.form_title = "Add New Issueto";
                $scope.label = "add";
                $scope.id = 0;
                $scope.issueto = {};
                $scope.form_title = "Add New Issueto";
                $('#issuetoModal').modal('show');
                break;
            case 'edit':
                $scope.label = "edit";
                $scope.id = id;
                $('#issuetoModal').modal('show');
                $scope.form_title = "Issueto Detail";
                $scope.id = id;
                $scope.project_id;
                var url = baseUrl + '/edit-issueto/' + id;
                $http({
                    method: 'GET',
                    url: url,                   
                }).then(function successCallback(response) {                    
                    $scope.issueto = response.data;
                });  
                break;
            case 'cmpIsto':
                $('#issuetoCmpModal').modal('show');
                break;
            default:
                break;
        }
    }

    // Company issue to list

       $timeout(function() {
            var pid = $scope.project_id;
        })
    // end company issue to list
    //save new record / update existing record
    $scope.saveIssueto = function(modalstate, id=0) {  
        $scope.loader  = true;
        $scope.isDisable = true;
        $scope.issueto.id = id;
        $scope.issueto.modalstate = modalstate;
        $scope.issueto.project_id = $scope.project_id;
        var url = baseUrl + "/save_issueto";                
        $http({
            method: 'POST',
            url: url,
            data: $.param($scope.issueto),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).then(function successCallback(response) {
            console.log(response);
            location.reload();
        }, function errorCallback(response) {
            console.log(response);
            alert('This is embarassing. An error has occured. Please check the log for details');
        });
    }
    
    
    $scope.isDisabled = true;
    $scope.issuedto_cmp_id = new Array();
    $scope.checkAll = function() {  
    $scope.checkVal = true;      
        angular.forEach($scope.plists, function(list) {          
          list.select = $scope.selectAll;
          if($scope.selectAll == true) {
            $scope.issuedto_cmp_id.push(list.id);
          }else {
            $scope.issuedto_cmp_id = [];
          }          
        });           
        $scope.is_disable($scope.issuedto_cmp_id);        
      };    

    
    $scope.addCheckedvalue = function(issuedto_id,event){        
        //$scope.issuedto_cmp_id.push(issuedto_id);
        if(event.target.checked != false) {
            $scope.issuedto_cmp_id.push(issuedto_id);
            $scope.is_disable($scope.issuedto_cmp_id);
        }else {
            y = jQuery.grep($scope.issuedto_cmp_id, function(value) {
              return value != issuedto_id;
            });
            $scope.issuedto_cmp_id = y;            
            $scope.is_disable($scope.issuedto_cmp_id);
        }        
        //console.log($scope.issuedto_cmp_id,$scope.plists.length);
    }

    $scope.is_disable = function(arr){
        if(arr.length != 0){
            $scope.isDisabled = false;
        }else{
            $scope.isDisabled = true;
            $scope.checkVal = false;
        }

        if($scope.plists.length == arr.length){
            $scope.checkVal = true;
        }else{
            $scope.checkVal = false;
        }
    }
    

    $scope.cancel = function (formid) {
        $('#'+formid).modal('hide');
    };
    
    $scope.companyIssueToList = function(){
        //alert($scope.project_id);
        $scope.orderList = "name";
        var url = baseUrl + "/getAllIssuetoCompanies/"+$scope.project_id;
        $http({method: 'GET', url: url}).
        then(function successCallback(response) {
            //console.log(response.data);         
            $scope.plists = response.data;
        }, function errorCallback(response) {
              $scope.data = response.data || 'Request failed';
              $scope.status = response.status;
        });
    };

    $scope.addIssueto = function(){               
        $scope.isDisabled = true;
        $scope.loader  = true;
        $scope.issueto = {};
        $scope.issueto.id = $scope.issuedto_cmp_id;
        $scope.issueto.project_id = $scope.project_id;        
        var url = baseUrl + "/saveIssueToFromCompany";                
        $http({
            method: 'POST',
            url: url,
            data: $.param($scope.issueto),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).then(function successCallback(response) {
            location.reload();
            $('#issuetoCmpModal').modal('show');
        }, function errorCallback(response) {
            console.log(response);
            alert('This is embarassing. An error has occured. Please check the log for details');
        });
    }

    

    $(document).on('click', '.edit-ist' , function(){
        var anc = angular.element("editIssuto");
        var jid = $(this).attr('Is-id');
        anc.bind('click', $scope.showIssueto('edit', jid));
    });

    $scope.getClass = function(status){
        switch(status){
            case 'linked':                            
                return 'added_issueTo';
            break;
            default:
            break;
        }
    }
    
    // datatables
    $(document).ready(function() {
        //alert($scope.project_id);
        var csrf_token = $('[name="_token"]').val();
        var project_id = $scope.project_id;
        $('#sample_1').DataTable( {

            "processing": true,
            "serverSide": true,
            "ajax": baseUrl+"/issueto_list/?project_id="+project_id,
            columns: [
                {data: 'name', name: 'name'},
                {data: 'auther', name: 'auther'},
                {data: 'email', name: 'email'},
                {data: 'phone', name: 'phone'},
                {data: 'activity', name: 'activity'},
                {data: 'action', name: 'action', orderable: false, searchable: false},
            ]
        });        
        
    } );    

});