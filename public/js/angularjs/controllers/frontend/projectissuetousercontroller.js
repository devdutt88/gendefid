var projectIssuetoUser = angular.module('projectIssuetoUserModule', ['ngFlash', 'ngMessages', 'ngTagsInput']);    
    projectIssuetoUser.constant("CSRF_TOKEN", $('[name="_token"]').val());

/***
** Custom directive for email exist check
**/
projectIssuetoUser.directive('issuetoUserEmailExists', function($http , $q ) {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function($scope, elem, attrs, ngModel) { 
            ngModel.$asyncValidators.uniqueEmail = function(modelValue, viewValue) {
                var value = modelValue || viewValue;                        
                //id = document.getElementsByName('project_hid')[0].value;                 
                var id = (!$scope.issuetoUser.id) ? 0 : $scope.issuetoUser.id;
                $scope.showLoader = true;
                // Lookup user by password
                //return $http.post(baseUrl+ '/superadmin/project_isexist/' + id +'/'+ value),data: {'name':value,'jq':0}.
                var url = baseUrl+ '/issueto-user-email-check/' + id;
                return $http({
                        method: 'POST',
                        url: url,
                        headers: {'Content-Type': 'application/json; charset=utf-8'},
                        data: {'email':value,'issuedto_id':$scope.issueto_id,'jq':0}
                    }).
                then(function resolved(response) {
                    $scope.showLoader = false;
                   //password exists, this means validation fails
                   var valid = response.data;
                    if(valid == "true"){
                        ngModel.$setValidity('issuetoUserEmailExists', false);
                        return $q.reject();
                    }else
                        ngModel.$setValidity('issuetoUserEmailExists', true);
                        return $q.resolve();
                }, function rejected(response) {
                    //email does not exist, therefore this validation passes
                    console.log(response.data);
                    return false;
                });
            };
        }
    } 
});

projectIssuetoUser.controller('projectIssuetoUserController', function($scope, $http, $timeout,CSRF_TOKEN) {
    //show modal form
    setTimeout(function(){
        $(".alert-success").hide();    
    }, 2000);
    $scope.issueto = {};
    $scope.issuetoUser = {};    
    
    // Company issue to list

       $timeout(function() {
            var pid = $scope.project_id;            
        })   

    $(document).on('click', '.edit-ist' , function(){        
        var anc = angular.element("editIssuto");
        var id = $(this).attr('is-id');        
        anc.bind('click', $scope.showIssuetoUser('edit', id));
    });

    $scope.saveIssuetoUser = function(issuedto_id='') { 
    $scope.isDisabled = true;
    $scope.loader  = true;           
    $scope.issuetoUser.issueto_id = $scope.issueto_id;
    //alert($scope.issueto_id);
    $scope.issuetoUser._token = CSRF_TOKEN;
        var url = baseUrl + "/save_issueto_user/"+$scope.project_id;
        $http({
            method: 'POST',
            url: url,
            data: $.param($scope.issuetoUser),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).then(function successCallback(response) {            
            $scope.showLoader = false;
            location.reload();
        }, function errorCallback(response) {
            console.log(response);
            alert('This is embarassing. An error has occured. Please check the log for details');
        });
    }

    // $scope.cancel = function (formid) {
    //     $('#'+formid).modal('hide');
    // };
        
    // Issue to user
    $scope.showIssuetoUser = function(modalstate,id=0){
        switch(modalstate){
            case 'add':
                $scope.issuetoUser = {};
                $scope.issuetoUser.form_title = "Add New Issuedto User";
                $scope.issuetoUser.issueto_id = $scope.issuedto_id;
                console.log($scope);
                $('#issuetoUserModal').modal('show');
                break;
            break;
            case 'edit':            
            // alert($scope.issuedto_id);
            // alert(id);
            var url = baseUrl + '/edit-issuetoUser/' + id;
            $http({
                method: 'GET',
                url: url,                   
            }).then(function successCallback(response) {                                    
                $scope.issuetoUser = response.data;
                //alert(response.data.issueto_id);
                $('#issuetoUserModal').modal('show');
            });            
            break;
            default:
            break;
        }
    }

    $scope.reset = function(form) {
        form.$setPristine();
        form.$setUntouched();
        $scope.issueto.phone = null;
        $scope.issueto.email = null;
        $scope.issuetoUser.phone = null;
        $scope.issuetoUser.email = null;
        
        //angular.copy({},form);
    };

    $scope.loadTags = function(query) {
        return $http.get(baseUrl+'/locationTags?tag=' + query);
    };

    // datatables
    $(document).ready(function() {
        // /alert($scope.issueTo_id);
        var csrf_token = $('[name="_token"]').val();
        var project_id = $scope.project_id;
        $('#sample_1').DataTable( {

            "processing": true,
            "serverSide": true,
            "ajax": baseUrl+"/issueTo-userList/"+$scope.issueTo_id,
            columns: [
                {data: 'name', name: 'name'},                
                {data: 'email', name: 'email'},
                {data: 'phone', name: 'phone'},
                {data: 'tag', name: 'tags'},
                {data: 'action', name: 'action', orderable: false, searchable: false},
            ]
        });        
        
    } );    

});