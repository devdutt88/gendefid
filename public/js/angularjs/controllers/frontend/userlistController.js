var user = angular.module('userModule', ['user.directives','ngMessages','datatables']);
    user.constant('APIURL', baseUrl+'/');
    user.constant("CSRFTOKEN", $('[name="_token"]').val());

user.controller('userListController', function($scope, $http, APIURL, CSRFTOKEN){
   
    $scope.user = {};
    setTimeout(function(){
        $(".alert-success").hide();    
    }, 2000);

    $scope.reset = function(form) {
        form.$setPristine();
        form.$setUntouched();
        $scope.user.email = null;
        $scope.user.phone = null;
        $scope.user.usertype = null;
    };

    $('#userList').DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": baseUrl+"/usr_list",
        "language": {
            "lengthMenu": "_MENU_ Records",            
        },
        columns: [
            {data: 'logo_image', name: 'logo_image', orderable: false, searchable: false, className: 'text-center'},
            {data: 'name', name: 'name'},
            {data: 'full_name', name: 'full_name'},
            {data: 'email', name: 'email'},
            {data: 'roles', name: 'roles', orderable: false, searchable: false},
            {data: 'created_at', name: 'Create Date'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    } );

    //show modal form
    $scope.showModelFrm = function (action , id) {
     //   alert('test');
        console.log(id,action);
        $scope.userRoles();
        switch (action) {
            case 'add':
                $scope.user = {};
                $scope.action = action;
                $scope.formheader = "Add"; 
                $scope.user.form_title = "Add New User";
                $('#userformModel').modal('show');
                break;
            case 'edit':
                $scope.user = {};
                var url = APIURL + 'edit-user/' + id + '/profile';
                $http({method: 'GET', url: url}).
                    then(function(response) {
                    //console.log(response.data);
                        $scope.user = response.data;
                        $scope.action = action;
                        $scope.formheader = "Edit"; 
                        $scope.user.form_title = "Edit User";
                        $('#userformModel').modal('show');
                    }, function(response) {
                    console.log(response.data);
                    $scope.data = response.data || 'Request failed';
                    $scope.status = response.status;
                });
                break;
            default:
                swal("Please select any form");
                break;
        }            
    };

    $(document).on('click', '.edit-user' , function(){
        var anc = angular.element("editUser");
        var id = $(this).attr('userid');
        anc.bind('click', $scope.showModelFrm('edit', id));
    });

    $(document).on('click', '.delete-user' , function(){        
        var anc = angular.element("deleteUser");
        var id = $(this).attr('deleteUserid');        
        anc.bind('click', $scope.deleteUser(id));
    });

    $scope.deleteUser = function(id){
        swal({
          title: "Are you sure?",
          text: "User will get deleted from this list!",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Yes, delete it!",
          cancelButtonText: "No, cancel please!",
          closeOnConfirm: false,
          closeOnCancel: false
        },
        function(isConfirm){
          if (isConfirm) {                    
            $scope.prjId = {};
            $scope.prjId.id = id;
            var url = baseUrl+"/delete-user/"+id;
            $http({
                method: 'Get',
                url: url,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},                
            }).then(function successCallback(response) {            
                //$scope.showLoader = false;
                if(response.status == 200)
                {
                    $scope.showLoader = false;      
                    swal("Deleted!", "User has been deleted.", "success");                  
                    window.location.href= baseUrl+"/user";
                }
                }, function errorCallback(response) {
                    $scope.showLoader = false;
                    $scope.isaddPermissionDisabled = false;
                    swal('This is embarassing. An error has occured. Please check your internet connections');
            });
          } else {
            swal("Cancelled", "Your User is safe :)", "error");
          }
        });
    }

    $scope.userRoles = function(){
        var url = APIURL+"user_types";
        $http({method: 'GET', url: url}).
            then(function(response) {
              //console.log(response.data);
              $scope.roles = response.data;
            }, function(response) {
              $scope.data = response.data || 'Request failed';
              $scope.status = response.status;
        });
    };

    $scope.submitForm = function(action){
        console.log("action ",$scope.action);
        $scope.showLoader = true;
        $scope.user._token = CSRFTOKEN;
        switch($scope.action){
            case 'add' :
                if ($scope.userForm.$valid) {
                    url = APIURL+'save_user';
                    $http({
                        method: 'POST',
                        url: url,
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                        data: $.param($scope.user)
                    }).then(function successCallback(response) {
                            console.log(response.data);                            
                            $scope.showLoader = false;
                            var id = response.data;
                            window.location.href= baseUrl+"/user";
                        }, function errorCallback(response) {
                            console.log(response.data);
                            $scope.showLoader = false;
                            $scope.data = response.data || 'Request failed';
                            $scope.status = response.status;
                            
                    });
                } else {
                    swal('This is embarassing. An error has occured');
                }
                break;
            case 'edit':
                if ($scope.userForm.$valid) {
                    url = APIURL+'updateUser';
                    $http({
                        method: 'POST',
                        url: url,
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                        data: $.param($scope.user)
                    }).then(function successCallback(response) {
                            console.log(response.data);                            
                            $scope.showLoader = false;
                            window.location.href= baseUrl+"/user";
                        }, function errorCallback(response){
                            $scope.showLoader = false;
                            $scope.data = response.data || 'Request failed';
                            $scope.status = response.status;
                    });
                } else {
                    swal('This is embarassing. An error has occured');
                }
                break;
            default:
                swal("Please select any form");
                break;
        }
    };
    // End of controller metthod
});

