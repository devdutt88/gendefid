var cmpissueto = angular.module('issuetoModule', ['ngFlash', 'ngMessages', 'ngTagsInput']);
    cmpissueto.constant('API_URL', baseUrl+'/superadmin/');
    cmpissueto.constant("CSRF_TOKEN", $('[name="_token"]').val());

/***
** Custom directive for email exist check
**/
cmpissueto.directive('issuetoEmailExists', function($http , $q ) {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function($scope, elem, attrs, ngModel) { 
            ngModel.$asyncValidators.uniqueEmail = function(modelValue, viewValue) {
                var value = modelValue || viewValue;
                var id = (!$scope.issueto.id) ? 0 : $scope.issueto.id;
                // Lookup user by password
                return $http.get(baseUrl+ '/superadmin/issueto_emailid/' + id +'/'+ value).
                then(function resolved(response) {
                   //password exists, this means validation fails
                   var valid = response.data;
                    if(valid == "true"){
                        ngModel.$setValidity('emailExists', false);
                        return $q.reject();
                    }else
                        ngModel.$setValidity('emailExists', true);
                        return $q.resolve();
                }, function rejected(response) {
                    //email does not exist, therefore this validation passes
                    console.log(response.data);
                    return false;
                });
            };
        }
    } 
});
cmpissueto.directive('issuetoUserEmailExists', function($http , $q ) {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function($scope, elem, attrs, ngModel) { 
            ngModel.$asyncValidators.uniqueEmail = function(modelValue, viewValue) {
                var value = modelValue || viewValue;
                var id = (!$scope.issuetoUser.id) ? 0 : $scope.issuetoUser.id;
                var issueto_id = $scope.issuetoUser.issueto_id;
                // Lookup user by password
                return $http.get(baseUrl+ '/superadmin/issuetoUser_emailid/' + id +'/' + issueto_id + '/' + value).
                then(function resolved(response) {
                   //password exists, this means validation fails
                   var valid = response.data;
                    if(valid == "true"){
                        ngModel.$setValidity('emailExists', false);
                        return $q.reject();
                    }else
                        ngModel.$setValidity('emailExists', true);
                        return $q.resolve();
                }, function rejected(response) {
                    //email does not exist, therefore this validation passes
                    console.log(response.data);
                    return false;
                });
            };
        }
    } 
});

cmpissueto.controller('issuetoController', ['$scope','$http','API_URL','$filter','Flash','CSRF_TOKEN', function ($scope, $http, API_URL,$filter,Flash,CSRF_TOKEN) {
    
    setTimeout(function(){
        $(".alert-success").hide();
    }, 2000);
    $scope.master = {};
    $scope.issueto= {};
    $scope.issuetoUser = {};
    $scope.showIssueto = function(modalstate, id) {
        // open model popup 
        switch (modalstate) {
            case 'add':
                $scope.issueto = {};
                $scope.issueto.form_title = "Add New Issuedto";
                $('#issuetoModal').modal('show');
                break;
            case 'edit':
                console.log(id);
                var url = API_URL + 'edit-issueto/' + id;
                $http({method: 'GET', url: url}).
                then(function successCallback(response) {
                    $scope.issueto = response.data;
                    $scope.issueto.form_title = "Issuedto Detail";
                    $('#issuetoModal').modal('show');
                    
                }, function errorCallback(response) {
                    //console.log(response);
                    swal('This is embarassing. An error has occured. Please check the log for details');
                });  
                break;
            default:
                swal("please select any row !");
                break;
        }
    };

    //save new record / update existing record
    $scope.saveIssueto = function() {
        var url = API_URL + "save-issueto";
        $scope.issueto._token = CSRF_TOKEN;
        $scope.showLoader = true;
        $scope.isDisabled = true;
        //append users id to the URL if the form is in edit mode
        if ($scope.issuetoInfo.$valid) {
            $http({
                method: 'POST',
                url: url,
                data: $.param($scope.issueto),
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).then(function successCallback(response) {
                    window.location.href= baseUrl+"/superadmin/issueto";
            }, function errorCallback(response) {
                $scope.showLoader = false;
                $scope.isDisabled = false;
                swal('Issuedto could not save. An error has occured !');
            });
        }else{
            swal('This is embarassing. An error has occured. Please check your internet connections');
        }
    };

    $(document).on('click', '.edit-ist' , function(){
        var anc = angular.element("editIssuto");
        var jid = $(this).attr('Is-id');
        anc.bind('click', $scope.showIssueto('edit', jid));
    });

    $scope.showIssuetoUser = function(modalstate, issuedto_id, id) {
        
        $scope.issuetoUser.modalstate = modalstate;
        console.log('test');
        switch (modalstate) {
            case 'add':
                $scope.issuetoUser = {};
                $scope.issuetoUser.form_title = "Add New Issuedto User";
                $scope.issuetoUser.issueto_id = issuedto_id;
                $('#issuetoUserModal').modal('show');
                break;
            case 'edit':
                var url = API_URL + 'edit-issueto-user/'+ id;
                $http({
                    method: 'POST',
                    url: url,
                }).then(function successCallback(response) {
                    $scope.issuetoUser = response.data;
                    $scope.issuetoUser.form_title = "Issuedto User Detail";
                    $scope.issuetoUser.issueto_id = issuedto_id;
                    $('#issuetoUserModal').modal('show');
                });  
                break;
            default:
                break;
        }
        //$('#editUserModel').modal('show');
    };

    $(document).on('click', '.edit_ius' , function(){
        var anc = angular.element("editT");
        var id = $(this).attr('IsUID');
        var issueto_id = $(this).attr('Is-Id');
        anc.bind('click', $scope.showIssuetoUser('edit', issueto_id, id));
    });

    $scope.saveIssuetoUser = function(issuedto_id) {

        var url = API_URL + "save-issueto-user";
        $scope.issuetoUser.issueto_id = issuedto_id;
        $scope.issuetoUser._token = CSRF_TOKEN;
        $scope.showLoader = true;
        $scope.isDisabled = true;

        //post data
        if ($scope.issuetoUserFrm.$valid) {
            $http({
                method: 'POST',
                url: url,
                data: $.param($scope.issuetoUser),
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).then(function successCallback(response) {
                $scope.issuetoUserFrm.$valid = false;
                // redirect page to list page !
                window.location.href= baseUrl+"/superadmin/issueto-details/"+issuedto_id;
            }, function errorCallback(response) {
                $scope.showLoader = false;
                $scope.isDisabled = false;
                alert('This is embarassing. An error has occured. Please check your internet connections');
            });
        }else{
            swl('This is embarassing. An error has occured. Please check your internet connections');
        }
    };

    $scope.reset = function(form) {
        form.$setPristine();
        form.$setUntouched();
        $scope.issueto.phone = null;
        $scope.issueto.email = null;
        $scope.issuetoUser.phone = null;
        $scope.issuetoUser.email = null;
        
        //angular.copy({},form);
    };

    $scope.loadTags = function(query) {
         return $http.get(baseUrl+'/superadmin/qc-locations?tag=' + query);
    };

    $scope.companyList = function(issuetoId){
        $scope.orderList = "name";
        var url = API_URL + "getAllIssuetoCompanies/" + issuetoId;
        $http({method: 'GET', url: url}).
        then(function successCallback(response) {
            //console.log(response.data);
            $scope.clists = response.data;
        }, function errorCallback(response) {
              $scope.data = response.data || 'Request failed';
              $scope.status = response.status;
        });
    };

    // get the user's company list
    $scope.issuetoCompanyList = function(issuetoId){
        $scope.orderList = "name";
        var url = API_URL + "getIssuetoCompanies/" + issuetoId;
        $http({method: 'GET', url: url}).
        then(function successCallback(response) {
            console.log(response.data);  
            //$scope.clists = {}; 
            $scope.iclists = response.data;
        }, function errorCallback(response) {
              $scope.data = response.data || 'Request failed';
              $scope.status = response.status;
        });
    };

    $scope.company = {}
    $scope.associateCompany = function(companyId,issuetoId){
        //console.log(companyId,issuetoId);
        $scope.showCompanyLoader = true;
        $scope.company.companyId = companyId;
        $scope.company.issuetoId = issuetoId;
        $http({
            method: 'POST',
            url: API_URL + "associateIssuetoCompany",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            data: $.param($scope.company)
        }).then(function successCallback(response) {
            console.log(response.data);
            $scope.showCompanyLoader = false;
            Flash.create('success', "Company associated to this issuedto", 2000, {class: 'cst-alert cst-company-alert'});
            $scope.companyList(issuetoId);
            $scope.issuetoCompanyList(issuetoId);
         }, function errorCallback(response) {
            $scope.showCompanyLoader = false;
            //console.log(response);
            Flash.create('danger', "Something went wrong ! Try Again ", 2000, {class: 'cst-alert cst-company-alert'});
        });
    };

    $scope.companymap = {}
    $scope.unlinkCompany = function(companyId,issuetoId){
        $scope.showCompanyLoader = true;
        $scope.companymap.companyId = companyId;
        $scope.companymap.issuetoId = issuetoId;
        $http({
            method: 'POST',
            url: API_URL + "unlinkIssuetoCompany",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            data: $.param($scope.companymap)
        }).then(function successCallback(response) {
            console.log(response.data);
            $scope.showCompanyLoader = false;
            Flash.create('success', "Company reomved from this issuedto", 2000, {class: 'cst-alert cst-company-alert'});
            $scope.companyList(issuetoId);
            $scope.issuetoCompanyList(issuetoId);
         }, function errorCallback(response) {
            console.log(response);
            $scope.showCompanyLoader = false;
            Flash.create('danger', "Something went wrong ! Try Again ", 2000, {class: 'cst-alert cst-company-alert'});
        });
    };
    
    $(document).ready(function() {

        var new_issueto_id = $('#new_issueto_id').val()?$('#new_issueto_id').val():'';
        $('#IssuedtoList').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": baseUrl+"/superadmin/issueto_list",
            "language": {
                "lengthMenu": "_MENU_ Records",
            },
            columns: [
                {data: 'name', name: 'name'},
                {data: 'auther', name: 'auther'},
                {data: 'phone', name: 'phone'},
                {data: 'email', name: 'email'},
                {data: 'activity', name: 'activity'},
                {data: 'modified_at', name: 'Create Date'},
                {data: 'action', name: 'action', orderable: false, searchable: false},
            ]
        } );

        $('#IssuedUserstoList').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": baseUrl+"/superadmin/issueto-user-list/"+new_issueto_id,
            "language": {
                "lengthMenu": "_MENU_ Records",
            },
            columns: [
                {data: 'name', name: 'name'},
                {data: 'email', name: 'email'},
                {data: 'phone', name: 'phone'},
                {data: 'tag', name: 'tag', orderable: false},
                {data: 'action', name: 'action', orderable: false, searchable: false},
            ]
        });
    });

    //show modal form
    $scope.showModelFrm = function (id,issuetoform) {
        console.log(id,issuetoform);
        switch (issuetoform) {
            case 'companies':
                    var url = API_URL + "getAllIssuetoCompanies/" + id;
                    $scope.orderList = "name";
                    $http({method: 'GET', url: url}).
                    then(function successCallback(response) {
                        //console.log(response.data);
                        $scope.clists = response.data;
                        $('#companyModal').modal('show');
                    }, function errorCallback(response) {
                        $scope.data = response.data || 'Request failed';
                        $scope.status = response.status;
                    });
                break;
            default:
                swal("Please select any form");
                break;
        }   
    };
}]);