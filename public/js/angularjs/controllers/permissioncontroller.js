var permission = angular.module('permissionModule', ['ngMessages']);
    permission.constant('API_URL', baseUrl+'/superadmin/');
    permission.constant("CSRF_TOKEN", $('[name="_token"]').val()); 

/***
** Custom directive for email exist check
**/
permission.directive('permissionExists', function($http , $q ) {

    return {
        restrict: 'AE',
        require: '?ngModel',
        link: function($scope, elem, attrs, ngModel) { 
            ngModel.$asyncValidators.uniquePermission = function(modelValue, viewValue) {
                var value = modelValue || viewValue;
                console.log($scope.permission.id);
                var id = (!$scope.permission.id) ? 0 : $scope.permission.id;                    
                // Lookup user by password                
                return $http.get(baseUrl+ '/superadmin/check-permission/' + id +'/'+ value).
                then(function resolved(response) {
                   //password exists, this means validation fails
                   var valid = response.data;
                    if(valid == "true"){
                        // $scope.show_err= true;
                        ngModel.$setValidity('permissionExists', false);
                        return $q.reject();
                    }else{
                        // $scope.show_err= false;
                        ngModel.$setValidity('permissionExists', true);
                        return $q.resolve();
                    }                        
                }, function rejected(response) {
                    //email does not exist, therefore this validation passes
                    console.log(response.data);
                    return false;
                });
            };
        }
    } 
});

permission.controller('permissionController', function($scope, $http,API_URL,CSRF_TOKEN) {
    setTimeout(function(){
        $(".alert-success").hide();    
    }, 2000);
    
    $scope.types = ['Menu','Submenu',"Internal permission"];
    var url = API_URL + 'getModule';
    $http({method: 'GET', url: url}).
    then(function(response) {
       console.log(response);                    
            $scope.module_names = response.data;
            console.log($scope.module_names);                        
      }, function(response) {
          $scope.data = response.data || 'Request failed';
          $scope.status = response.status;
      });

    $scope.showModelFrm = function(){        
        $('.add_permission').attr('disabled',true);
        $scope.show_module = false;
        $scope.permission = {};
        $scope.label = 'add';
        $scope.isDisabled = false;
        $scope.button = "Add";
        $scope.isaddPermissionDisabled = false;
        $scope.heading = "Add";                                
        $('#permissionEditModel').modal('show');
    }    

    $scope.savePermission = function(label){             
        switch (label) {
            case 'add':
                $scope.isaddPermissionDisabled = true;
                var type = $scope.permission.type;        
                var module_name = $scope.permission.module_id;  
                $scope.permission._token = CSRF_TOKEN;        
                //console.log($scope.permission);
                $scope.showLoader = true;
                if(type != "Menu"){
                    if(module_name == '')
                    {
                        $scope.msg = 'Please Select Module';
                        return false;
                    }
                }
                if(typeof $scope.permission.type == "undefined"){
                    $scope.msg1 = 'Please Select Type';
                    return false;
                }                
                var url = API_URL+"save-permission";
                $http({
                    method: 'POST',
                    url: url,
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                    data: $.param($scope.permission)
                }).then(function successCallback(response) {            
                    //$scope.showLoader = false;
                    if(response.status == 200)
                    {
                        $scope.showLoader = false;                        
                        window.location.href= baseUrl+"/superadmin/permission";
                    }
                    }, function errorCallback(response) {
                        $scope.showLoader = false;
                        $scope.isaddPermissionDisabled = false;
                        swal('This is embarassing. An error has occured. Please check your internet connections');

                });
            break;
            case 'edit':
                $scope.isaddPermissionDisabled = true;
                var type = $scope.permission.type;
                var module_name = $scope.permission.module_id;   
                $scope.permission._token = CSRF_TOKEN;    
                if(type != "Menu"){
                    if(module_name == '')
                    {
                        $scope.msg = 'Please Select Module';
                        return false;
                    }
                }        
                $scope.showLoader = true;
                var url = API_URL+"update-permission";
                $http({
                    method: 'POST',
                    url: url,
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                    data: $.param($scope.permission)
                }).then(function successCallback(response) {            
                    //$scope.showLoader = false;
                    if(response.status == 200)
                    {                        
                        window.location.href= baseUrl+"/superadmin/permission";
                    }
                    }, function errorCallback(response) {
                        $scope.isaddPermissionDisabled = false;
                        $scope.showLoader = false;
                        swal('This is embarassing. An error has occured. Please check your internet connections');
                });
            break;
            default:
                break;
        }
    } 

    
    $(document).on('click', '.edit' , function(){
        var anc = angular.element("editT");
        var id = $(this).attr('perid');
        anc.bind('click', $scope.showPermission('edit', id));
    });

    $scope.cancel = function(form) {
        $scope.show_module = false;
        form.$setPristine();
        form.$setUntouched();
    };

    $scope.showPermission = function(modalstate, id) {    
         switch (modalstate) {
                case 'edit':
                $scope.button = "Update";
                $scope.heading = "Edit";
                    var url = API_URL + 'editPermisson/'+ id;
                    $http({
                        method: 'POST',
                        url: url,
                        //data: $.param($scope.issueto),
                        //headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                    }).then(function successCallback(response) {
                        console.log(response.data);                                 
                        $scope.permission = response.data;                       
                        if(response.data.type != "Menu"){                        
                            $scope.isDisabled = false;
                            $scope.show_module = true;
                        }else{
                            $scope.isDisabled = true;
                            $scope.show_module = false;
                        }                    
                    });  
                    $('.error_msg_name').hide();
                    $('#permissionid').val(id);
                    $scope.label = 'edit';
                    $('#permissionEditModel').modal('show');
                break;
                default:
                    break;
            }
    }

    $scope.checkRule = function(){        
        if($scope.permission.type != "Menu" && (typeof $scope.permission.type != "undefined")) {
            $scope.label_validation = true;
            $scope.show_module = true;
        }        
        else {
            $scope.label_validation = false;
            $scope.show_module = false;
        }
    }

});