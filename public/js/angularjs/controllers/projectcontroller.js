var project = angular.module('projectModule', ['ngFlash', 'ngMessages']);
    project.constant('API_URL', baseUrl+'/superadmin/');
    project.constant("CSRF_TOKEN", $('[name="_token"]').val());


/***
** Custom directive for email exist check
**/
project.directive('projectExists', function($http , $q ) {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function($scope, elem, attrs, ngModel) { 
            ngModel.$asyncValidators.uniqueEmail = function(modelValue, viewValue) {
                var value = modelValue || viewValue;
                //var id = (!$scope.issueto.id) ? 0 : $scope.issueto.id;
                // Lookup user by password
                //return $http.post(baseUrl+ '/superadmin/project_isexist/' + id +'/'+ value),data: {'name':value,'jq':0}.
                var url = baseUrl+ '/superadmin/project_isexist/' + id;
                return $http({
                        method: 'POST',
                        url: url,
                        headers: {'Content-Type': 'application/json; charset=utf-8'},
                        data: {'name':value,'jq':0}
                    }).
                then(function resolved(response) {
                   //password exists, this means validation fails
                   var valid = response.data;
                    if(valid == "true"){
                        ngModel.$setValidity('projectExists', false);
                        return $q.reject();
                    }else
                        ngModel.$setValidity('projectExists', true);
                        return $q.resolve();
                }, function rejected(response) {
                    //email does not exist, therefore this validation passes
                    console.log(response.data);
                    return false;
                });
            };
        }
    } 
});

project.controller('projectController', ['$scope','$http','API_URL','$filter','Flash','CSRF_TOKEN', function ($scope, $http, API_URL,$filter,Flash,CSRF_TOKEN) {

    setTimeout(function(){
        $(".alert-success").hide();
    }, 2000);
    
    //show modal forms for edit data
    $scope.project = {};
    $scope.show_details = function (id,projectform) {
        switch (projectform) {
            case 'details':
                var url = API_URL + "edit-project/" + id + '/details';
                //console.log(url);
                $http({method: 'GET', url: url}).
                    then(function(response) {
                      // console.log(response);
                      $scope.project = response.data;
                      $scope.types = ['Educational', 'Industrial', 'Offices', 'Residential', 'Retail', 'Other'];
                      $('#detailsModal').modal('show');
                    }, function(response) {
                      $scope.data = response.data || 'Request failed';
                      $scope.status = response.status;
                });
                break;
            case 'companies':
                var url = API_URL + "edit-project/" + id + '/companies';
                $http({method: 'GET', url: url}).
                    then(function(response) {
                      $scope.project = response.data;
                      $('#companiesModal').modal('show');
                    }, function(response) {
                      $scope.data = response.data || 'Request failed';
                      $scope.status = response.status;
                });
                //alert('test');
                break;
            default:
                swal("Please select any form");
                break;
        }            
    };

    //save new record / update existing record
    $scope.update_details = function (id,projectform,name) {
        var url = API_URL + "update_project";
        $scope.project.id = id;
        $scope.project.projectform = projectform;
        $scope.project._token = CSRF_TOKEN;
        switch (projectform) {
            case 'details':
                if ($scope.projectInfo.$valid) {
                    //console.log('project form is in scope');
                    $scope.showLoader = true;
                    $http({
                        method: 'POST',
                        url: url,
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                        data: $.param($scope.project)
                    }).then(function(response) {
                        if(response.status == 200)
                        {
                            $scope.showLoader = false;
                            window.location.href= baseUrl+"/superadmin/project-details/"+id;
                        }
                    }).error(function(response) {
                        //console.log(response);
                        $scope.showLoader = false;
                        swal('This is embarassing. An error has occured. Please check your internet connections');
                    });
                } else {
                    console.log('projectform is not in scope');
                }
                break;
            case 'logo':
                var formdata = new FormData(document.getElementById("projectLogoFrm"));
                formdata.append('projectform' , projectform);
                formdata.append('id' , id);
                formdata.append('name' , name);
                $scope.showLogoLoader = true;

                $http({
                    method: 'POST',
                    url: url,
                    headers: {'Content-Type': undefined},
                    data: formdata
                }).then(function successCallback(response) {
                    //console.log(response.data);
                    $scope.showLogoLoader = false;
                    if(response.status == 200)
                    {
                        window.location.href= baseUrl+"/superadmin/project-details/"+id;
                    }
                 }, function errorCallback(response) {
                    $scope.showLogoLoader = false;
                    swal('This is embarassing. An error has occured. Please check your internet connections');
                });
                break;
            default: 
                swal("Please select any form");
                break;
        }           
    };
    
    $scope.cancel = function (form) {
        form.$setPristine();
        form.$setUntouched();
    };

    function readLogoImage(input) {
        console.log(input);
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {  

                var ext = $('#projectImg').val().split('.').pop().toLowerCase();
                    if($.inArray(ext, ['png','jpg','jpeg']) == -1) {
                        $scope.showLogoLoader = false;
                        swal('Invalid file type!','File extension should be ,png,jpg,jpeg!');
                        return false;
                    }
                
                var file = document.getElementById('projectImg').files[0];
                if(file.size >= 2097152){
                    $scope.showLogoLoader = false;
                    swal("File size is too long.","File size should not be more than 2mb!");
                    //$('#logo_image').attr('src', baseUrl+"/uploads/project_icon.png");
                    $('#upload_btn').hide();
                    return false;
                }


                $('#logo_image').attr('src', e.target.result);
                $('#upload_btn').show();
            }
            reader.readAsDataURL(input.files[0]);
        }
    };

    $("#projectImg").change(function(){
        readLogoImage(this);
    });

    // get the company list
    $scope.companyList = function(projectId){
        $scope.orderList = "name";
        var url = API_URL + "getAllCompanies/" + projectId;
        $http({method: 'GET', url: url}).
        then(function successCallback(response) {
            //console.log(response.data);
            $scope.clists = response.data;
        }, function errorCallback(response) {
              $scope.data = response.data || 'Request failed';
              $scope.status = response.status;
        });
    };

    // get the user's company list
    $scope.projectCompanyList = function(projectId){
        $scope.orderList = "name";
        var url = API_URL + "getProjectCompanies/" + projectId;
        $http({method: 'GET', url: url}).
        then(function successCallback(response) {
            console.log(response.data);  
            //$scope.clists = {};
            $scope.pclists = response.data;
        }, function errorCallback(response) {
              $scope.data = response.data || 'Request failed';
              $scope.status = response.status;
        });
    };

    $scope.company = {}
    $scope.associateCompany = function(companyId,projectId){
        //console.log(companyId,projectId);
        $scope.showCompanyLoader = true;
        $scope.company.companyId = companyId;
        $scope.company.projectId = projectId;
        $http({
            method: 'POST',
            url: API_URL + "associateCompany",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            data: $.param($scope.company)
        }).then(function successCallback(response) {
            console.log(response.data);
            $scope.showCompanyLoader = false;
            Flash.create('success', "Company associated to this project", 2000, {class: 'cst-alert cst-company-success-alert'});
            $scope.companyList(projectId);
            $scope.projectCompanyList(projectId);
         }, function errorCallback(response) {
            $scope.showCompanyLoader = false;
            console.log(response);
            Flash.create('danger', "Something went wrong ! Try Again ", 2000, {class: 'cst-alert cst-company-danger-alert'});
        });
    };

    $scope.companymap = {}
    $scope.unlinkCompany = function(companyId,projectId){
        $scope.companymap.companyId = companyId;
        $scope.companymap.projectId = projectId;
        $scope.showCompanyLoader = true;
        $http({
            method: 'POST',
            url: API_URL + "unlinkCompany",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            data: $.param($scope.companymap)
        }).then(function successCallback(response) {
            console.log(response.data);
            $scope.showCompanyLoader = false;
            Flash.create('success', "Company reomved from this project", 2000, {class: 'cst-alert cst-company-success-alert'});
            $scope.companyList(projectId);
            $scope.projectCompanyList(projectId);
        }, function errorCallback(response) {
            $scope.showCompanyLoader = false;
            console.log(response);
            Flash.create('danger', "Something went wrong ! Try Again ", 2000, {class: 'cst-alert cst-company-danger-alert'});
        });
    };

    $scope.checkIsexist = function(){
        // alert($scope.project.name);
        var id = $('#project_id').val();
        var name = $scope.project.name;
        if(typeof name == "undefined"){
            $scope.msg = "Project name is required";
            $('#profile_form').attr('disabled',true);
            return false;
        }
        var url=baseUrl+"/superadmin/project_isexist?id="+id+'&name='+name;
        $http({method: 'GET', url: url}).
        then(function(response) {
            console.log(response.data);
            if(response.data == "false"){  
                $scope.msg = "Project name already exists.";
                $('#profile_form').attr('disabled',true);

            }else{
                $scope.msg = "";
                $('#profile_form').attr('disabled',false);
            }
              //$scope.user = response.data;
        }, function(response) {
              $scope.data = response.data || 'Request failed';
              $scope.status = response.status;
        });
    };
    
    //show modal form
    $scope.showModelFrm = function (id,projectform) {
        console.log(id,projectform);
        switch (projectform) {
            case 'companies':
                    var url = API_URL + "getAllCompanies/" + id;
                    $scope.orderList = "name";
                    $http({method: 'GET', url: url}).
                    then(function successCallback(response) {
                        //console.log(response.data);
                        $scope.clists = response.data;
                        $('#companyModal').modal('show');
                    }, function errorCallback(response) {
                        $scope.data = response.data || 'Request failed';
                        $scope.status = response.status;
                    });
                break;
            default:
                swal("Please select any form");
                break;
        }            
    };

}]);
