var projectIssueto = angular.module('projectIssuetoModule', []);
    projectIssueto.constant('API_URL', baseUrl+'/superadmin/');

projectIssueto.controller('projectIssuetoController', function($scope, $http, API_URL) {
    //show modal form
    setTimeout(function(){
        $(".alert-success").hide();    
    }, 2000);
    $scope.issueto = {};
    $scope.issuetoUser = {};
    $scope.showIssueto = function(modalstate='', id='') {
        $scope.modalstate = modalstate;

        switch (modalstate) {
            case 'add':
                $scope.form_title = "Add New Issueto";

                $('#issuetoModal').modal('show');
                break;
            /*case 'edit':
                $('#issuetoModal').modal('show');
                $scope.form_title = "Issueto Detail";
                $scope.id = id;
                $scope.project_id;
                var url = API_URL + 'project/issueto/edit-issueto/' + project_id + '/' + id;
                $http({
                    method: 'POST',
                    url: url,
                    //data: $.param($scope.issueto),
                    //headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).then(function successCallback(response) {
                    console.log(response);
                    $scope.issueto = response.data;
                });  
                break;*/
            default:
                break;
        }
    }

    //save new record / update existing record
    $scope.saveIssueto = function(modalstate, id=0) {

        $scope.issueto.id = id;
        $scope.issueto.modalstate = modalstate;
        var url = API_URL + "projectConfiguration/save_issueto";
        
        $http({
            method: 'POST',
            url: url,
            data: $.param($scope.issueto),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).then(function successCallback(response) {
            console.log(response);
            location.reload();
        }, function errorCallback(response) {
            console.log(response);
            alert('This is embarassing. An error has occured. Please check the log for details');
        });
    }

    $scope.showIssuetoUser = function(modalstate, id='', issuedto_id='') {
        
        $scope.issuetoUser.modalstate = modalstate;
        console.log('test');
        switch (modalstate) {
            case 'add':

                $scope.issuetoUser.form_title = "Add New Issueto";
                $scope.issuetoUser = {};
                console.log($scope.issuetoUser);
                $('#issuetoUserModal').modal('show');
                break;
            case 'edit':
                $('#issuetoUserModal').modal('show');
                $scope.form_title = "Issueto Detail";
                $scope.id = id;
                $scope.project_id;
                var url = API_URL + 'projectConfiguration/issueto/edit-issueto-user/'+ id;
                $http({
                    method: 'POST',
                    url: url,
                    //data: $.param($scope.issueto),
                    //headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).then(function successCallback(response) {
                    console.log(response);
                    $scope.issuetoUser = response.data;
                });  
                break;
            default:
                break;
        }
        //$('#editUserModel').modal('show');
    }

    $(document).on('click', '.edit_ius' , function(){
        var anc = angular.element("editT");
        var id = $(this).attr('IsUID');
        anc.bind('click', $scope.showIssuetoUser('edit', id));
    });

    $scope.saveIssuetoUser = function(issuedto_id='') {

        var url = API_URL + "projectConfiguration/save_issueto_user/"+issuedto_id;
        $http({
            method: 'POST',
            url: url,
            data: $.param($scope.issuetoUser),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).then(function successCallback(response) {
            $scope.showLoader = false;
            if(response.status == 200)
            {
                window.location.href= baseUrl+"/superadmin/projectConfiguration/issueto-details/"+issuedto_id;
            }
        }, function errorCallback(response) {
            console.log(response);
            alert('This is embarassing. An error has occured. Please check the log for details');
        });
    }

    $scope.cancel = function (formid) {
        $('#'+formid).modal('hide');
    };

    $scope.orderList = "name";
    var url = baseUrl+'/superadmin/projectConfiguration/getAllProjectData';
    $http({method: 'GET', url: url}).
    then(function(response) {
        console.log(response);         
        //alert(response.data.cmpname);
        $scope.plists = response.data;
        console.log(response.data);
        //alert($scope.cmpname);                  
    }, function(response) {
        $scope.data = response.data || 'Request failed';
        $scope.status = response.status;
    });
    
    $(document).ready(function() {

        var csrf_token = $('[name="_token"]').val();
        var issueto_id = $('#issueto_id').val()?$('#issueto_id').val():'';
        // var company_id = $('#company_id').val()?$('#company_id').val():'';
        // var project_id = $('#project_id').val()?$('#project_id').val():'';
        
        $('#sample_1').DataTable( {

            "processing": true,
            "serverSide": true,
            "ajax": baseUrl+"/superadmin/projectConfiguration/issueto_list/",
            columns: [
                {data: 'name', name: 'name'},
                {data: 'email', name: 'email'},
                {data: 'phone', name: 'phone'},
                {data: 'activity', name: 'activity'},
                {data: 'action', name: 'action', orderable: false, searchable: false},
            ]
        });

        $('#sample_2').DataTable( {

            "processing": true,
            "serverSide": true,
            "ajax": baseUrl+"/superadmin/projectConfiguration/issueto_users_list/"+issueto_id,
            columns: [
                {data: 'name', name: 'name'},
                {data: 'email', name: 'email'},
                {data: 'phone', name: 'phone'},
                {data: 'activity', name: 'activity'},
                {data: 'action', name: 'action', orderable: false, searchable: false},
            ]
        });
        
    } );

});