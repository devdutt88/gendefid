var cmpchecklist = angular.module('checklistModule', ['ngFlash','ngTagsInput']);
    cmpchecklist.constant('API_URL', baseUrl+'/superadmin/');

//issueto.controller('issuetoController', function($scope, $http, API_URL) {
cmpchecklist.controller('checklistController', ['$scope','$http','API_URL','$filter','Flash', function ($scope, $http, API_URL,$filter,Flash) {
    
    setTimeout(function(){
        $(".alert-success").hide();
    }, 2000);
    $scope.master = {};
    $scope.checklist = {};
    $scope.checklistTask = {};
    $scope.showChecklist = function(modalstate, id) {
        // open model popup 
        switch (modalstate) {
            case 'add':
                $scope.checklist = {};
                $scope.checklist.form_title = "Add New Checklist";
                $('#checklistModal').modal('show');
                break;
            case 'edit':
                $scope.checklist.id = id;
                var url = API_URL + 'edit-quality-checklist/' + id;
                $http({
                    method: 'POST',
                    url: url,
                }).then(function successCallback(response) {
                    $scope.checklist = response.data;
                    $scope.checklist.form_title = "Edit Quality Checklist Details";
                    console.log($scope.checklist);
                    $('#checklistModal').modal('show');
                }, function errorCallback(response) {
                    console.log(response);
                    swal('This is embarassing. An error has occured. Please check the log for details');
                });  
                break;
            default:
                swal("please select any row !");
                break;
        }
    };

    $scope.loadTags = function(query) {
         return $http.get(baseUrl+'/superadmin/qc-locations?tag=' + query);
    };

    //save new record / update existing record
    $scope.saveChecklist = function() {
        //$scope.issueto.modalstate = modalstate;
        var url = API_URL + "save-checklist";
         $scope.showLoader = true;
        //append users id to the URL if the form is in edit mode
        $http({
            method: 'POST',
            url: url,
            data: $.param($scope.checklist),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).then(function successCallback(response) {
            $scope.showLoader = false;
            if(response.status == 200)
            {
                window.location.href= baseUrl+"/superadmin/quality-checklist";
            }
        }, function errorCallback(response) {
            $scope.showLoader = false;
           swal('This is embarassing. An error has occured. Please check your internet connections');
        });
    };

    $(document).on('click', '.edit-ist' , function(){
        var anc = angular.element("editChecklist");
        var id = $(this).attr('Is-id');
        anc.bind('click', $scope.showChecklist('edit', id));
    });

    $scope.showChecklistTask = function(modalstate, id) {
        $scope.checklistTask.holdpoint = "Yes";
        $scope.checklistTask.modalstate = modalstate;
        $scope.holdpoints = ['Yes','No'];
        //console.log('test');
        switch (modalstate) {
            case 'add':
                $scope.checklistTask = {};
                $scope.checklistTask.form_title = "Add New Checklist Task";
                $('#checklistTaskModal').modal('show');
                break;
            case 'edit':
                var url = API_URL + 'edit-checklist-task/'+ id;
                $('#checklistTaskModal').modal('show');
                
                $http({
                    method: 'POST',
                    url: url,
                }).then(function successCallback(response) {
                    $scope.checklistTask.holdpoint = "Yes";
                    $scope.checklistTask = response.data;
                    $scope.checklistTask.form_title = "Edit Checklist Task Details";
                });  
                break;
            default:
                break;
        }
        //$('#editUserModel').modal('show');
    };

    $(document).on('click', '.edit_ius' , function(){
        var anc = angular.element("editT");
        var id = $(this).attr('IsUID');
        anc.bind('click', $scope.showChecklistTask('edit', id));
    });

    $scope.saveChecklistTask = function(checklist_id) {

        var url = API_URL + "save-checklist-task";
        $scope.checklistTask.checklist_id = checklist_id;
        $scope.showLoader = true;
        //console.log(url); return true;
        $http({
            method: 'POST',
            url: url,
            data: $.param($scope.checklistTask),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).then(function successCallback(response) {
            $scope.showLoader = false;
            if(response.status == 200)
            {
                window.location.href= baseUrl+"/superadmin/quality-checklist-details/"+checklist_id;
            }
        }, function errorCallback(response) {
            $scope.showLoader = false;
            swal('This is embarassing. An error has occured. Please check your internet connections');
        });
    };

    $scope.reset = function(form) {
        form.$setPristine();
        form.$setUntouched();
        //form.$setUninvalid();
        //$scope.checklist.phone = null;
        //$scope.checklistTask.phone = null;
        //angular.copy({},form);
    };

    $scope.companyList = function(checklistId){
        $scope.orderList = "name";
        var url = API_URL + "getAllChecklistCompanies/" + checklistId;
        $http({method: 'GET', url: url}).
        then(function successCallback(response) {
            //console.log(response.data);
            $scope.clists = response.data;
        }, function errorCallback(response) {
              $scope.data = response.data || 'Request failed';
              $scope.status = response.status;
        });
    };

    // get the user's company list
    $scope.checklistCompanyList = function(checklistId){
        $scope.orderList = "name";
        var url = API_URL + "getChecklistCompanies/" + checklistId;
        $http({method: 'GET', url: url}).
        then(function successCallback(response) {
            console.log(response.data);  
            //$scope.clists = {};
            $scope.qclists = response.data;
        }, function errorCallback(response) {
              $scope.data = response.data || 'Request failed';
              $scope.status = response.status;
        });
    };

    $scope.company = {}
    $scope.associateCompany = function(companyId,checklistId){
        //console.log(companyId,checklistId);
        $scope.company.companyId = companyId;
        $scope.company.checklistId = checklistId;
        $scope.showCompanyLoader = true;
        $http({
            method: 'POST',
            url: API_URL + "associateChecklistCompany",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            data: $.param($scope.company)
        }).then(function successCallback(response) {
            //console.log(response.data);
            $scope.showCompanyLoader = false;
            Flash.create('success', "Company associated to this quality checklist", 2000, {class: 'cst-alert cst-company-success-alert'});
            $scope.companyList(checklistId);
            $scope.checklistCompanyList(checklistId);
         }, function errorCallback(response) {
            $scope.showCompanyLoader = false;
            //console.log(response);
            Flash.create('danger', "Something went wrong ! Try Again ", 2000, {class: 'cst-alert cst-company-danger-alert'});
        });
    };

    $scope.companymap = {}
    $scope.unlinkCompany = function(companyId,checklistId){
        
        $scope.companymap.companyId = companyId;
        $scope.companymap.checklistId = checklistId;
         $scope.showCompanyLoader = true;
        $http({
            method: 'POST',
            url: API_URL + "unlinkChecklistCompany",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            data: $.param($scope.companymap)
        }).then(function successCallback(response) {
            //console.log(response.data);
            $scope.showCompanyLoader = false;
            Flash.create('success', "Company reomved from this quality checklist", 2000, {class: 'cst-alert cst-company-success-alert'});
            $scope.companyList(checklistId);
            $scope.checklistCompanyList(checklistId);
         }, function errorCallback(response) {
            $scope.showCompanyLoader
            //console.log(response);
            Flash.create('danger', "Something went wrong ! Try Again ", 2000, {class: 'cst-alert cst-company-danger-alert'});
        });
    };
    
    $(document).ready(function() {
        var new_checklist_id = $('#new_checklist_id').val()?$('#new_checklist_id').val():'';
        $('#Checklist').DataTable( {

            "processing": true,
            "serverSide": true,
            "ajax": baseUrl+"/superadmin/get-quality-checklist",
            "language": {
                "lengthMenu": "_MENU_ Records",
            },
            columns: [
                {data: 'name', name: 'name'},
                {data: 'tags', name: 'tags'},
                {data: 'modified_at', name: 'Create Date'},
                {data: 'action', name: 'action', orderable: false, searchable: false},
            ]
        } );

        $('#ChecklistTaskList').DataTable( {

            "processing": true,
            "serverSide": true,
            "ajax": baseUrl+"/superadmin/get-quality-checklist-tasks/"+new_checklist_id,
            "language": {
                "lengthMenu": "_MENU_ Records",
            },
            columns: [
                {data: 'name', name: 'name'},
                {data: 'holdpoint', name: 'holdpoint'},
                {data: 'comment', name: 'comment'},
                {data: 'created_at', name: 'Create Date'},
                {data: 'action', name: 'action', orderable: false, searchable: false},
            ]
        } );


    } );

    //show modal form
    $scope.showModelFrm = function (id,checklistform) {
        console.log(id,checklistform);
        switch (checklistform) {
            case 'companies':
                    var url = API_URL + "getAllChecklistCompanies/" + id;
                    $scope.orderList = "name";
                    $http({method: 'GET', url: url}).
                    then(function successCallback(response) {
                        //console.log(response.data);
                        $scope.clists = response.data;
                        $('#companyModal').modal('show');
                    }, function errorCallback(response) {
                        $scope.data = response.data || 'Request failed';
                        $scope.status = response.status;
                    });
                break;
            default:
                swal("Please select any form");
                break;
        }
    };

}]);