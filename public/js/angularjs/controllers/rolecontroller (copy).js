var role = angular.module('roleModule', ['ngMessages']);
    role.constant('API_URL', baseUrl+'/superadmin/');
    role.constant("CSRF_TOKEN", $('[name="_token"]').val());

/***
** Custom directive for email exist check
**/
role.directive('roleExists', function($http , $q ) {

    return {
        restrict: 'AE',
        require: 'ngModel',
        link: function($scope, elem, attrs, ngModel) { 
            ngModel.$asyncValidators.uniqueEmail = function(modelValue, viewValue) {
                var value = modelValue || viewValue;                
                var id = (!$scope.role.id) ? 0 : $scope.role.id;                
                // Lookup user by password                
                return $http.get(baseUrl+ '/superadmin/check-role/' + id +'/'+ value).
                then(function resolved(response) {
                   //password exists, this means validation fails
                   var valid = response.data;
                    if(valid == "true"){
                        // $scope.show_err= true;
                        $scope.isaddRoleDisabled = true;
                        ngModel.$setValidity('roleExists', false);
                        return $q.reject();
                    }else{
                        // $scope.show_err= false;
                        $scope.isaddRoleDisabled = false;
                        ngModel.$setValidity('roleExists', true);
                        return $q.resolve();
                    }                        
                }, function rejected(response) {
                    //email does not exist, therefore this validation passes
                    console.log(response.data);
                    return false;
                });
            };
        }
    } 
});


role.controller('roleController', function($scope, $http, API_URL,CSRF_TOKEN) {
    setTimeout(function(){
        $(".alert-success").hide();    
    }, 2000);
    $scope.orderList = "name";
    var url = API_URL + 'getRoleList';
    $http({method: 'GET', url: url}).
    then(function(response) {
       console.log(response);         
           //alert(response.data.cmpname);
          $scope.plists = response.data;
           console.log(response.data);
          //alert($scope.cmpname);                  
      }, function(response) {
          $scope.data = response.data || 'Request failed';
          $scope.status = response.status;
      });




    //show modal form
    $scope.id='';
    $scope.showModelFrm = function(condition,id){        
        switch (condition) {
            case 'add-role':  
                $scope.role  = {};
                $scope.button = "Add";
                $scope.label = 'add';
                $scope.heading = "Add";                                
                $('#roleModel').modal('show');
            break;
            case 'edit':
                $scope.role  = {};
                $scope.button = "Update";
                $scope.label = 'edit'; 
                $scope.heading = "Edit";
                var url = baseUrl+'/superadmin/edit-role/'+id;
                $http({method: 'GET', url: url}).
                    then(function successCallback(response) {                        
                        $scope.role = response.data 
                        console.log($scope.role);
                        $('#roleModel').modal('show');                            
                    }, function errorCallback(response) {
                      $scope.data = response.data || 'Request failed';
                      $scope.status = response.status;
                });
            break;
            default:
                break;
        }
        
    }
    
    //save new record / update existing record
    $scope.saveRole = function(condition) {        
        switch (condition) {
            case 'add':            
            $scope.isaddRoleDisabled = true;
            $scope.role._token = CSRF_TOKEN;
            var url = API_URL + "save-role";             
            //if ($scope.rolenInfo.$valid) {   
            $scope.showLoader = true;    
                $http({
                    method: 'POST',
                    url: url,
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},            
                    data: $.param($scope.role)
                }).then(function successCallback(response) {
                    //console.log(response.data);
                    // if(response.status == 200)
                    // {
                        //$scope.rolenInfo.$valid = false;
                        $scope.showLoader = false;
                        window.location.href= baseUrl+"/superadmin/role";
                    //}
                 }, function errorCallback(response) {
                    $scope.isaddRoleDisabled = false;
                    $('#custom_error').html('Something went wrong, Please try again!');
                    //swal('This is embarassing. An error has occured. Please check your internet connections');
                });
            // }else{
            //     $scope.isaddRoleDisabled = false;
            // }
            break;
            case 'edit':                       
            $scope.iseditRoleDisabled = true; 
            var url = API_URL + "update-role"; 
            $scope.role._token = CSRF_TOKEN;
            if ($scope.rolenInfo.$valid) { 
            $scope.showLoader = true;     
                $http({
                    method: 'POST',
                    url: url,
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},            
                    data: $.param($scope.role)
                }).then(function successCallback(response) {
                    //console.log(response.data);
                    if(response.status == 200)
                    {               
                        $scope.showLoader = false;     
                        $scope.rolenInfo.$valid = false;
                        window.location.href= baseUrl+"/superadmin/role";
                    }
                 }, function errorCallback(response) {
                    $scope.iseditRoleDisabled = false; 
                    swal('Issuedto could not save. An error has occured !');
                });
            }else{
                $scope.showLoader = false; 
                swal('This is embarassing. An error has occured. Please check your internet connections');    
            }
            break;
            default:
            break;
        }
    }

    var permission = '';
    if($('#perm_hidevalue').text()){        
        permission = $('#perm_hidevalue').text();
        permission = JSON.parse(permission); 
        if(permission !='')
        {            
            $('#treeview-checkbox-demo').treeview({
                debug : true,
                data : permission,
            });       
        }else{
            $('#treeview-checkbox-demo').treeview({
                debug : true,
                data : ['selected'],
            });   
        }        
    }   

    $scope.getRole = function(id){        
        var url = API_URL + "get-role/"+id;  
        $('.role_list').each(function(){            
            if($(this).attr('id') == "role_list "+id){
                $(this).css('color',"#5b9bd1");
                $(this).css('font-weight',"600");
            }else{
                $(this).css('color',"");
                $(this).css('font-weight',"");
            }
        });        
        $http({method: 'GET', url: url}).
        then(function(response) {
            $('#hid').val(id);
           var arr = response.data;                   
            $(".role").each(function() {                             
                if($.inArray( this.value, arr ) != -1){
                     $('#perm'+this.value).parent().addClass('checked');
                }else{
                    $('#perm'+this.value).parent().removeClass('checked');
                }
            });
          }, function(response) {
              $scope.data = response.data || 'Request failed';
              $scope.status = response.status;
        });
    }

    $scope.form = function(id){
        $scope.id = $('#hid').val(); 
        if($scope.id == ''){
            swal('Please select Role from Role List.');
            return false;
        }
        $scope.permission = [];
        $("#form"+id).find('.checked').each(function() {
            var chk1 = $(this).children();
            $scope.permission.push($(chk1).attr('value'));
        });
        $scope.permissionUnchecked = [];
        $("#form"+id).find('.role').each(function() {            
            $scope.permissionUnchecked.push($(this).val());
        });         
        var url = API_URL + "store-rolePermision";                
            $http({
                method: 'POST',
                url: url,
                headers: {'Content-Type': 'application/json'},
                data: { 'permissionData':$scope.permission,'id':$scope.id,"uncheckData":$scope.permissionUnchecked,'_token':CSRF_TOKEN}, //forms user object
            }).then(function(response) {                                    
                console.log(response.data);
               if(response.status == 200)
                    {
                        $('.'+id).show();
                        $('.'+id).fadeIn('slow').delay(2000).hide(0);
                    }
            }).error(function(response) {
                console.log(response);
                swal('This is embarassing. An error has occured. Please check your internet connections');
            });
    }

    $scope.cancel = function (form) {
        $scope.role.name = "";        
        form.$setPristine();
        form.$setUntouched();
    };
});

