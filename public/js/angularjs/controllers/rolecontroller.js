var role = angular.module('roleModule', ['ngMessages','datatables','tree.service', 'tree.directives','ngFlash']);
    role.constant('API_URL', baseUrl+'/superadmin/');
    role.constant("CSRF_TOKEN", $('[name="_token"]').val());



/***
** Custom directive for email exist check
**/
role.directive('roleExists', function($http , $q ) {

    return {
        restrict: 'AE',
        require: 'ngModel',
        link: function($scope, elem, attrs, ngModel) { 
            ngModel.$asyncValidators.uniqueEmail = function(modelValue, viewValue) {
                var value = modelValue || viewValue;                
                var id = (!$scope.role.id) ? 0 : $scope.role.id;                
                // Lookup user by password                
                return $http.get(baseUrl+ '/superadmin/check-role/' + id +'/'+ value).
                then(function resolved(response) {
                   //password exists, this means validation fails
                   var valid = response.data;
                    if(valid == "true"){
                        // $scope.show_err= true;
                        $scope.isaddRoleDisabled = true;
                        ngModel.$setValidity('roleExists', false);
                        return $q.reject();
                    }else{
                        // $scope.show_err= false;
                        $scope.isaddRoleDisabled = false;
                        ngModel.$setValidity('roleExists', true);
                        return $q.resolve();
                    }                        
                }, function rejected(response) {
                    //email does not exist, therefore this validation passes
                    //console.log(response.data);
                    return false;
                });
            };
        }
    } 
});

role.directive('ngConfirmClick', [
    function(){
        return {
            link: function (scope, element, attr) {
                var msg = attr.ngConfirmClick || "Are you sure?";
                var clickAction = attr.confirmedClick;
                element.bind('click',function (event) {                   
                    swal({
                      title: "Do you want to delete this role?",
                      //text: "You will not be able to recover this imaginary file!",
                      type: "warning",
                      showCancelButton: true,
                      confirmButtonColor: "#DD6B55",
                      confirmButtonText: "Yes, delete it!",
                      cancelButtonText: "No, cancel please!",
                      closeOnConfirm: false,
                      closeOnCancel: false
                    },
                    function(isConfirm){
                      if (isConfirm) {
                        scope.$eval(clickAction)
                      } else {
                        swal("Cancelled", "", "error");
                      }
                    });


                });
            }
        };
}])

role.controller("TreeController", ["TreeService", "$scope","$rootScope", "$http","Flash", function (TreeService, $scope,$rootScope, $http,Flash) {
    var tc = this;
    var roleid = 0;
    //$scope.buildTree(roleid);
    $rootScope.buildTree = function(roleid) {
        TreeService.getTree(roleid).then(function (result) {
            tc.tree = result.data;
            $('#permissionModel').modal('show');
        }, function (result) {
            alert("Tree no available, Error: " + result);
        });
    };

    $scope.storRolePermission = function(roleid){
        //console.log(roleid);
        $scope.showLoader = true; 
        url = baseUrl + "/superadmin/store-rolePermision/"+roleid;
        
        $http({
            method: 'POST',
            url: url,
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},            
            data: $.param($scope.tc)
        }).then(function successCallback(response) {
            //console.log(response.data);
            $scope.showLoader = false;
            $('#permissionModel').modal('hide');
            Flash.create('success', "Permission updated successfully", 2000);
         }, function errorCallback(response) {
            //console.log(response);
            $('#permissionModel').modal('hide');
            Flash.create('danger', "Something went wrong ! Try Again ", 2000);
        });
    };
}]);



//angular.module('roleModule.controllers', []).controller('roleController', function($scope, $http, API_URL,CSRF_TOKEN, DTOptionsBuilder, DTColumnBuilder) {
role.controller('roleController', ['$rootScope','$scope', '$http', 'API_URL','CSRF_TOKEN','Flash','DTOptionsBuilder','DTColumnBuilder', function($rootScope,$scope, $http, API_URL,CSRF_TOKEN,Flash,DTOptionsBuilder, DTColumnBuilder) {
    setTimeout(function(){
        $(".alert-success").hide();    
    }, 2000);
    
    //$scope.orderList = "name";
    // Get the role list data
    $scope.roleList = function(){
        var url = API_URL + 'get_roles';
        $http({method: 'GET', url: url}).
        then(function successCallback(response) {
                //console.log(response.data);         
                //alert(response.data.cmpname);
                $scope.roleList = response.data;
        }, function errorCallback(response) {
                //console.log(response);
                //swal('This is embarassing. An error has occured. Please check your internet connections');
        });

        $scope.vm = {};

        $scope.vm.dtOptions = DTOptionsBuilder.newOptions().
            withOption('order', [0, 'asc']);
            DTOptionsBuilder.newOptions().withOption('initComplete', function() {
            angular.element('.dataTables_filter input').attr('placeholder', 'Search ...');
        });
    
    }

    
    
    //show modal form
    $scope.id='';
    $scope.showModelFrm = function(condition,id,role_name){        
        switch (condition) {
            case 'add-role':  
                $scope.role  = {};
                $scope.button = "Add";
                $scope.label = 'add';
                $scope.heading = "Add";                                
                $('#roleModel').modal('show');
            break;
            case 'edit':
                $scope.role  = {};
                $scope.button = "Update";
                $scope.label = 'edit'; 
                $scope.heading = "Edit";
                var url = baseUrl+'/superadmin/edit-role/'+id;
                $http({method: 'GET', url: url}).
                    then(function successCallback(response) {                        
                        $scope.role = response.data 
                        //console.log($scope.role);
                        $scope.isaddRoleDisabled = false; 
                        $('#roleModel').modal('show');                            
                    }, function errorCallback(response) {
                      $scope.data = response.data || 'Request failed';
                      $scope.status = response.status;
                });
            break;
            case 'permission':                
                $rootScope.roleid = id;
                $rootScope.buildTree(id);
                $rootScope.roleName = role_name;                                
            break;
            default:
                break;
        }
        
    }
    
    //save new record / update existing record
    $scope.saveRole = function(condition) {         
        $scope.showLoader = true;        
        switch (condition) {
            case 'add':            
                $scope.isaddRoleDisabled = true;
                $scope.role._token = CSRF_TOKEN;
                var url = API_URL + "save-role";             
                if ($scope.rolenInfo.$valid) {   
                    $http({
                        method: 'POST',
                        url: url,
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},            
                        data: $.param($scope.role)
                    }).then(function successCallback(response) {
                            //console.log(response.data);
                            //$scope.rolenInfo.$valid = false;
                            $scope.showLoader = false;
                            $('#roleModel').modal('hide');
                            //$scope.roleList();
                            //window.location.href= baseUrl+"/superadmin/roles";
                       
                     }, function errorCallback(response) {
                        $scope.isaddRoleDisabled = false;
                        $('#custom_error').html('Something went wrong, Please try again!');
                        //swal('This is embarassing. An error has occured. Please check your internet connections');
                    });
                }else{
                    $scope.isaddRoleDisabled = false;
                }
                break;
            case 'edit':                       
                $scope.isaddRoleDisabled = true; 
                var url = API_URL + "update-role"; 
                $scope.role._token = CSRF_TOKEN;
                if ($scope.rolenInfo.$valid) { 
                    
                    $http({
                        method: 'POST',
                        url: url,
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},            
                        data: $.param($scope.role)
                    }).then(function successCallback(response) {
                        //console.log(response.data);
                        if(response.status == 200)
                        {               
                            $scope.showLoader = false;     
                            $scope.rolenInfo.$valid = false;
                            //window.location.href= baseUrl+"/superadmin/roles";
                        }
                     }, function errorCallback(response) {
                        $scope.isaddRoleDisabled = false; 
                        swal('Server could not be responsed !');
                    });
                }else{
                    $scope.showLoader = false; 
                    swal('This is embarassing. An error has occured. Please check your internet connections');    
                }
                break;
            default:
                break;
        }
    }

    $scope.showPermissions = function(roleid){
        $rootScope.roleid = roleid;
        $rootScope.buildTree(roleid);
    }

    $scope.cancel = function (form) {
        $scope.role.name = "";        
        form.$setPristine();
        form.$setUntouched();
    };

    $scope.delete = function(roleid){        
        window.location.href = API_URL+"deleteRole/"+roleid;
    }

}]);


