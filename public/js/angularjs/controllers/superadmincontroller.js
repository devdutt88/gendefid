(function() {
  "use strict";
  angular
    .module('adminModule', ['ngMessages'])
    .controller('superadminController', mainCtrl)
    .directive('passwordVerify', passwordVerify)
    .directive('passwordExists', passwordExists)
    .constant("CSRFTOKEN", $('[name="_token"]').val());

    // Angular main controller
    function mainCtrl($scope, $http , CSRFTOKEN) {
        
        setTimeout(function(){
            $(".alert-success").hide();    
        }, 2000);
        $scope.admin = {};

        // Reset form data & null scope model
        $scope.reset = function (form) {
            form.$setPristine();
            form.$setUntouched();
        };
        //show modal form
        $scope.showModal = function () {
            var url = baseUrl +'/superadmin/edit-admin-profile';
            $http({method: 'GET', url: url}).
                then(function(response) {
                  //console.log(response.data);                
                  $scope.admin = response.data;
                  $('#adminModelFrm').modal('show');
                }, function(response) {
                  $scope.data = response.data || 'Request failed';
                  $scope.status = response.status;
            });
        };

        //save new record / update existing record
        $scope.submitForm = function (adminform) {
            $scope.showLoader = true;
            var url = baseUrl+ "/superadmin/update_admin";
            $scope.admin.adminform = adminform;

            switch (adminform) {
                case 'profile':
                    if ($scope.adminInfo.$valid) {
                        $scope.admin._token = CSRFTOKEN;
                        $http({
                            method: 'POST',
                            url: url,
                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                            data: $.param($scope.admin)
                        }).then(function successCallback(response) {
                            $scope.adminLoader = false;
                            if(response.status == 200)
                            {
                                window.location.href= baseUrl+"/superadmin/profile"; //+response.data;
                            }
                            }, function errorCallback(response) {
                                console.log(response);
                                swal('This is embarassing. An error has occured. Please check your internet connections');
                        });
                    } else {
                         swal('This is embarassing. An error has occured. Prevented to csrf attack!');
                    }
                break;
                case 'logo':
                    var formdata = new FormData(document.getElementById("adminLogoFrm"));
                    formdata.append('adminform' , adminform);
                    formdata.append('_token' , CSRFTOKEN);                    
                    $http({
                        method: 'POST',
                        url: url,
                        headers: {'Content-Type': undefined},
                        data: formdata
                    }).then(function successCallback(response) {
                        //console.log(response.data);
                        $scope.adminLoader = false;
                        if(response.status == 200)
                        {
                            window.location.href= baseUrl+"/superadmin/profile";
                        }
                     }, function errorCallback(response) {
                        $scope.adminLoader = false;
                        swal('This is embarassing. An error has occured. Please check your internet connections');
                    });
                break;
                default:
                    swal("Nothing to update");
                break;
            }
        };

        // Read image javascript handler
        function readLogoImage(input) {
            console.log(input);
            if (input.files && input.files[0]) {
                var reader = new FileReader();        
                reader.onload = function (e) {
                    var ext = $('#userImg').val().split('.').pop().toLowerCase();
                    if($.inArray(ext, ['png','jpg','jpeg']) == -1) {
                        $scope.showLoader = false;
                        swal('Invalid file type!','File extension should be ,png,jpg,jpeg!');
                        return false;
                    }
                
                    var file = document.getElementById('userImg').files[0];
                    if(file.size >= 2097152){
                        $scope.showLoader = false;
                        swal("File size is too long.","File size should not be more than 2mb!");                        
                        $('#upload_btn').hide();
                        return false;
                    }

                    $('#logo_image').attr('src', e.target.result);
                    $('#upload_btn').show();
                }              
                reader.readAsDataURL(input.files[0]);
            }
        };

        $("#userImg").change(function(){
            readLogoImage(this);    
        });

       

        // Update password of superadmin
        $scope.changePassword = function(){
            $scope.adminLoader = true;
            $scope.adminPassword._token = CSRFTOKEN;
            var url = baseUrl+ "/superadmin/changeAdminPassword";
            // cheack form data is valid
            console.log($scope.changePasswordFrm.$valid);
            if ($scope.changePasswordFrm.$valid) {
                $http({
                    method: 'POST',
                    url: url,
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                    data: $.param($scope.adminPassword)
                }).then(function successCallback(response) {
                        console.log(response.data);
                        $scope.adminLoader = false;
                        window.location.href= baseUrl+"/superadmin/profile";
                    }, function errorCallback(response) {
                        console.log(response.data);
                        swal('An error has occured. Please check your internet connections');
                });
            }else{
                swal('This is embarassing. An error has occured. Prevented to csrf attack!');
            }
        };

    }

    /***
    ** Custom directive for password match
    **/
    function passwordVerify() {
        return {
            restrict: 'A', // only activate on element attribute
            require: '?ngModel', // get a hold of NgModelController
            link: function(scope, elem, attrs, ngModel) {
                if (!ngModel) return; // do nothing if no ng-model

                // watch own value and re-validate on change
                scope.$watch(attrs.ngModel, function() {
                  validate();
                });

                // observe the other value and re-validate on change
                attrs.$observe('passwordVerify', function(val) {
                  validate();
                });

                var validate = function() {
                  // values
                  var val1 = ngModel.$viewValue;
                  var val2 = attrs.passwordVerify;

                  // set validity
                  ngModel.$setValidity('passwordVerify', val1 === val2);
                };
            }
        }
    }

    /***
    ** Custom directive for password exist check
    **/
    function passwordExists ($http , $q) {
        return {
            restrict: 'AE',
            require: 'ngModel',
            link: function($scope, elem, attrs, ngModel) { 
                ngModel.$asyncValidators.uniquePassword = function(modelValue, viewValue) {
                    var value = modelValue || viewValue;
                    //console.log(encodeURIComponent(value));

                    // Lookup user by password
                    return $http.get(baseUrl+ '/superadmin/check_admin_current_password/' + encodeURIComponent(value)).
                    then(function resolved(response) {
                       //password exists, this means validation fails
                       var valid = response.data;
                        if(valid == "true"){
                            ngModel.$setValidity('passwordExists', true);
                            return  $q.resolve();
                        }else
                            ngModel.$setValidity('passwordExists', false);
                            return $q.reject();
                    }, function rejected(response) {
                        //password does not exist, therefore this validation passes
                        console.log(response.data);
                        return false;
                    });
                };
            }
        } 
    }

    // end functions
})();



