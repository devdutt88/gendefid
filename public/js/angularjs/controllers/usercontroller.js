var user = angular.module('userModule', ['user.directives', 'ngFlash' , 'ngMessages']);
    user.constant('API_URL', baseUrl+'/superadmin/');
    user.constant("CSRF_TOKEN", $('[name="_token"]').val());

//user.controller('userController', function ($scope, $http, API_URL) {
user.controller('showuserController', ['$scope','$http','API_URL','$filter','Flash','CSRF_TOKEN', function ($scope, $http, API_URL,$filter,Flash,CSRF_TOKEN) {

    setTimeout(function(){
        $(".alert-success").hide();
    }, 2000);
    $scope.user = {};
    $scope.address = {};
    $scope.tabstate='profile';
    $scope.tab_active1 = 'active';

    $scope.tabstate='profile';
    var lastTab = localStorage.getItem('lastTab');
    if(lastTab == "address"){
        $scope.tab_active1 = '';
        $scope.tab_active2 = 'active';
        $scope.tabstate = "address"
        $('#tab_1_2').show();
        $('#tab_1_2').addClass('active');
        $('#tab_1_1').hide();
    }else{
        $scope.tab_active1 = 'active';
        $scope.tab_active2 = '';
        $scope.tabstate = "profile"
        $('#tab_1_1').show();
        $('#tab_1_1').addClass('active');
        $('#tab_1_2').hide();
    }

    $scope.userRoles = function(){
        var url = API_URL+"user_types";
        $http({method: 'GET', url: url}).
            then(function(response) {
              //console.log(response.data);
              $scope.roles = response.data;
            }, function(response) {
              $scope.data = response.data || 'Request failed';
              $scope.status = response.status;
        });
    };

    //show modal form
    $scope.showModelFrm = function (id,userform) {
        console.log(id,userform);
        switch (userform) {
            case 'profile':
                var url = API_URL + "edit-user/" + id + '/profile';
                $http({method: 'GET', url: url}).
                    then(function(response) {
                      $scope.user = response.data;
                      $('#profileModal').modal('show');
                    }, function(response) {
                      $scope.data = response.data || 'Request failed';
                      $scope.status = response.status;
                });
                break;
            case 'address':
                var url = API_URL + "edit-user/" + id + '/address';
                $http({method: 'GET', url: url}).
                    then(function(response) {
                        var resultData = response.data;
                        console.log(resultData);
                        if(resultData != 'false'){
                            $scope.address = response.data;
                        }else{
                             $scope.address = {};
                        }
                        $scope.states = ['NSW','QLD','SA','VIC','WA','Other'];
                        $scope.address.bill_country = 'Austrailia';  
                        $scope.address.busi_country  = 'Austrailia';
                        $('#addressModal').modal('show');
                    }, function(response) {
                      $scope.data = response.data || 'Request failed';
                      $scope.status = response.status;
                });
                break;
            case 'companies':
                    var url = API_URL + "getAllUserCompanies/" + id;
                    $scope.orderList = "name";
                    $http({method: 'GET', url: url}).
                    then(function successCallback(response) {
                        //console.log(response.data);
                        $scope.clists = response.data;
                        $('#companyModal').modal('show');
                    }, function errorCallback(response) {
                        $scope.data = response.data || 'Request failed';
                        $scope.status = response.status;
                    });
                break;
            default:
                swal("Please select any form");
                break;
        }   
    };

    //save new record / update existing record
    $scope.submitForm = function (id,userform) {
        console.log(id,userform);
        $scope.showLoader = true;
        var url = API_URL + "update_user";
        $scope.user.id = id;
        console.log($scope);
        switch (userform) {
            case 'profile':
                if ($scope.userInfo.$valid) {
                    $scope.user.id = id;
                    $scope.user.userform = userform;
                    $scope.user._token = CSRF_TOKEN;
                    $http({
                        method: 'POST',
                        url: url,
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                        data: $.param($scope.user)
                    }).then(function successCallback(response) {
                            //console.log(response);
                            $scope.showLoader = false;
                            localStorage.setItem('lastTab', 'profile');
                            window.location.href= baseUrl+"/superadmin/user-details/"+id;
                        }, function errorCallback(response) {
                            $scope.showLoader = false;
                            swal('This is embarassing. An error has occured. Please check your internet connections');
                    });
                } else {
                    swal('This is embarassing. An error has occured. Please check your internet connections');
                }
                break;
            case 'address':
                if ($scope.addressInfo.$valid) {
                    $scope.address.id = id;
                    $scope.address.userform = userform;
                    $scope.address._token = CSRF_TOKEN;
                    $http({
                        method: 'POST',
                        url: url,
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                        data: $.param($scope.address)
                    }).then(function successCallback(response) {
                        //console.log(response);
                            $scope.showLoader = false;
                            localStorage.setItem('lastTab', 'address');
                            window.location.href= baseUrl+"/superadmin/user-details/"+id;
                        }, function errorCallback(response) {
                            $scope.showLoader = false;
                            swal('This is embarassing. An error has occured. Please check your internet connections');
                    });
                } else {
                    swal('This is embarassing. An error has occured. Please check your internet connections');
                }
                break;
            case 'logo':
                var formdata = new FormData(document.getElementById("userLogoFrm"));
                formdata.append('userform' , userform);
                formdata.append('id' , id);
                formdata.append('_token', CSRF_TOKEN);
                //console.log(formdata);
                $http({
                    method: 'POST',
                    url: url,
                    headers: {'Content-Type': undefined},
                    data: formdata
                }).then(function successCallback(response) {
                    //console.log(response.data);
                        $scope.showLoader = false;
                        window.location.href= baseUrl+"/superadmin/user-details/"+id;
                 }, function errorCallback(response) {
                    $scope.showLoader = false;
                    swal('This is embarassing. An error has occured. Please check your internet connections');
                });
                break;
            default:
                swal("Nothing to update");
            break;
        }
    };

    // Javascript image reader method
    function readLogoImage(input) {
        console.log(input);
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {  
                var ext = $('#userImg').val().split('.').pop().toLowerCase();
                    if($.inArray(ext, ['png','jpg','jpeg']) == -1) {
                        $scope.showLoader = false;
                        swal('Invalid file type!','File extension should be ,png,jpg,jpeg!');
                        return false;
                    }
                
                var file = document.getElementById('userImg').files[0];
                if(file.size >= 2097152){
                    $scope.showLoader = false;
                    swal("File size is too long.","File size should not be more than 2mb!");
                    //$('#logo_image').attr('src', baseUrl+"/uploads/user_icon.jpg");
                    $('#upload_btn').hide();
                    return false;
                }       
                $('#logo_image').attr('src', e.target.result);
                $('#upload_btn').show();
            }     
            reader.readAsDataURL(input.files[0]);
        }
    };

    $("#userImg").change(function(){
        readLogoImage(this);
    });

    // reset form data
    $scope.reset = function (formid) {
        form.$setPristine();
        form.$setUntouched();
    };

    // get the company list
    $scope.companyList = function(userId){
        $scope.orderList = "name";
        var url = API_URL + "getAllUserCompanies/" + userId;
        $http({method: 'GET', url: url}).
        then(function successCallback(response) {
            //console.log(response.data);
            $scope.clists = response.data;
        }, function errorCallback(response) {
              $scope.data = response.data || 'Request failed';
              $scope.status = response.status;
        });
    };

    // get the user's company list
    $scope.userCompanyList = function(userId){
        $scope.orderList = "name";
        var url = API_URL + "getUserCompanies/" + userId;
        $http({method: 'GET', url: url}).
        then(function successCallback(response) {
            //console.log(response.data);
            $scope.uclists = response.data;
        }, function errorCallback(response) {
              $scope.data = response.data || 'Request failed';
              $scope.status = response.status;
        });
    };



    // map company to user
    $scope.company = {}
    $scope.associateCompany = function(companyId,userId){
        console.log(companyId,userId);
        $scope.showLoader = true;
        $scope.company.companyId = companyId;
        $scope.company.userId = userId;
        $http({
            method: 'POST',
            url: API_URL + "associateUserCompany",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            data: $.param($scope.company)
        }).then(function successCallback(response) {
            console.log(response.data);
            $scope.showLoader = false;
            Flash.create('success', "Company associated to this user", 2000, {class: 'cst-alert cst-company-success-alert'});
            $scope.companyList(userId);
            $scope.userCompanyList(userId);
         }, function errorCallback(response) {
            console.log(response);
            Flash.create('danger', "Something went wrong ! Try Again ", 2000, {class: 'cst-alert cst-company-danger-alert'});
        });
    };

    // unlink company from user
    $scope.companymap = {}
    $scope.unlinkCompany = function(companyId,userId){
        $scope.showLoader = true;
        $scope.companymap.companyId = companyId;
        $scope.companymap.userId = userId;
        $http({
            method: 'POST',
            url: API_URL + "unlinkUserCompany",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            data: $.param($scope.companymap)
        }).then(function successCallback(response) {
            console.log(response.data);
            $scope.showLoader = false;
             Flash.create('success', "Company reomved from this user", 2000, {class: 'cst-alert cst-company-success-alert'});
            $scope.companyList(userId);
            $scope.userCompanyList(userId);
         }, function errorCallback(response) {
            console.log(response);
            Flash.create('danger', "Something went wrong ! Try Again ", 2000, {class: 'cst-alert cst-company-danger-alert'});
        });
    };

    $scope.setTabstate = function(tab_state){
        $scope.tabstate = tab_state;
        console.log($scope.tabstate);
        // alert($scope.tabstate);
        if($scope.tabstate == "profile")
        {
            $scope.tab_active1 = 'active';
            $scope.tab_active2 = '' 
            $('#tab_1_1').show();
            $('#tab_1_1').addClass('active');
            $('#tab_1_2').hide();
        }else{
            $scope.tab_active1 = '';
            $scope.tab_active2 = 'active' 
            $('#tab_1_2').show();
            $('#tab_1_2').addClass('active');
            $('#tab_1_1').hide(); 
        }
    }


    //$(document).ready(function() {
    $scope.userProjects = function(userid) {
        $('#user_projects').DataTable( {
            "processing": true,
            "serverSide": true,
            "bFilter": true,
            "paging": true,
            "ajax": baseUrl+"/superadmin/getUserProjectData/"+ userid,
            "language": {
                "lengthMenu": "_MENU_ Records",
            },
            columns: [
                {data: 'name', name: 'name'},
                {data: 'type', name: 'type'},
                {data: 'created_at', name: 'Create Date'},
                {data: 'action', name: 'action', orderable: false, searchable: false},
            ]
        });
    };

}]);









