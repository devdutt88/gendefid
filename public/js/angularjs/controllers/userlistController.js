var user = angular.module('userModule', ['user.directives','ngMessages']);
    user.constant('APIURL', baseUrl+'/superadmin/');
    user.constant("CSRFTOKEN", $('[name="_token"]').val());

user.controller('userListController', function($scope, $http, APIURL, CSRFTOKEN){
   
    setTimeout(function(){
        $(".alert-success").hide();    
    }, 2000);

    $scope.reset = function(form) {
        form.$setPristine();
        form.$setUntouched();
        $scope.user.email = null;
        $scope.user.phone = null;
        $scope.user.usertype = null;
    };

    $('#userList').DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": baseUrl+"/superadmin/usr_list",
        "language": {
            "lengthMenu": "_MENU_ Records",            
        },
        columns: [
            {data: 'logo_image', name: 'logo_image', orderable: false, searchable: false, className: 'text-center'},
            {data: 'name', name: 'name'},
            {data: 'full_name', name: 'full_name'},
            {data: 'email', name: 'email'},
            {data: 'roles', name: 'roles', orderable: false, searchable: false},
            {data: 'created_at', name: 'Create Date'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    } );

    //show modal form
    $scope.showModelFrm = function (action , id) {
        console.log(id,action);
        $scope.userRoles();
        switch (action) {
            case 'add':
                $scope.user = {};
                $scope.action = action;
                $scope.formheader = "Add"; 
                $scope.user.form_title = "Add New User";
                $('#userformModel').modal('show');
                break;
            case 'edit':
                var url = APIURL + 'edit-user/' + id + '/profile';
                $http({method: 'GET', url: url}).
                    then(function(response) {
                    //console.log(response.data);
                        $scope.user = response.data;
                        $scope.action = action;
                        $scope.formheader = "Edit"; 
                        $scope.user.form_title = "Edit User";
                        $('#userformModel').modal('show');
                    }, function(response) {
                    console.log(response.data);
                    $scope.data = response.data || 'Request failed';
                    $scope.status = response.status;
                });
                break;
            default:
                swal("Please select any form");
                break;
        }            
    };

    $(document).on('click', '.edit-user' , function(){
        var anc = angular.element("editUser");
        var id = $(this).attr('userid');
        anc.bind('click', $scope.showModelFrm('edit', id));
    });


    $scope.userRoles = function(){
        var url = APIURL+"user_types";
        $http({method: 'GET', url: url}).
            then(function(response) {
              //console.log(response.data);
              $scope.roles = response.data;
            }, function(response) {
              $scope.data = response.data || 'Request failed';
              $scope.status = response.status;
        });
    };

    $scope.submitForm = function(action){
        console.log("action ",$scope.action);
        $scope.showLoader = true;
        $scope.user._token = CSRFTOKEN;
        switch($scope.action){
            case 'add' :
                if ($scope.userForm.$valid) {
                    url = APIURL+'save_user';
                    $http({
                        method: 'POST',
                        url: url,
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                        data: $.param($scope.user)
                    }).then(function successCallback(response) {
                            console.log(response.data);                            
                            $scope.showLoader = false;
                            var id = response.data;
                            window.location.href= baseUrl+"/superadmin/user-details/"+id;
                        }, function errorCallback(response) {
                            console.log(response.data);
                            $scope.showLoader = false;
                            $scope.data = response.data || 'Request failed';
                            $scope.status = response.status;
                            
                    });
                } else {
                    swal('This is embarassing. An error has occured');
                }
                break;
            case 'edit':
                if ($scope.userForm.$valid) {
                    url = APIURL+'updateUser';
                    $http({
                        method: 'POST',
                        url: url,
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                        data: $.param($scope.user)
                    }).then(function successCallback(response) {
                            console.log(response.data);                            
                            $scope.showLoader = false;
                            window.location.href= baseUrl+"/superadmin/user";
                        }, function errorCallback(response){
                            $scope.showLoader = false;
                            $scope.data = response.data || 'Request failed';
                            $scope.status = response.status;
                    });
                } else {
                    swal('This is embarassing. An error has occured');
                }
                break;
            default:
                swal("Please select any form");
                break;
        }
    };
    // End of controller metthod
});

