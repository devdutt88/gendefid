var permiss = angular.module('permissApp', ['ngMessages']);
    permiss.constant("CSRFTOKEN", $('[name="_token"]').val());

permiss.controller('permissController', function($scope, $http, CSRFTOKEN){
   
    $('#permissionDataTable').DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": baseUrl+"/superadmin/all-permissions",
        columns: [            
            {data: 'name', name: 'name'},
            {data: 'description', name: 'description'},
            {data: 'created_at', name: 'Create Date'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    });

    $(document).on('click', '.edit' , function(){
        var anc = angular.element("editT");
        var id = $(this).attr('perid');
        anc.bind('click', $scope.showModelFrm('edit', id));
    });

    $scope.cancel = function(form) {
        form.$setPristine();
        form.$setUntouched();
    };

    $scope.showModelFrm = function(formname,id){        
        switch (formname) {
            case 'add':  
                $scope.permission  = {};
                $scope.button = "Add";
                $scope.label = 'add';
                $scope.heading = "Add";                                
                $('#permissionModel').modal('show');
                break;
            case 'edit':
                $scope.permission  = {};
                $scope.button = "Update";
                $scope.label = 'edit'; 
                $scope.heading = "Edit";
                var url = baseUrl+'/superadmin/edit-permission/'+id;
                $http({method: 'GET', url: url}).
                    then(function successCallback(response) {                        
                        $scope.permission = response.data 
                        $('#permissionModel').modal('show');                            
                    }, function errorCallback(response) {
                      $scope.data = response.data || 'Request failed';
                      $scope.status = response.status;
                });
                break;
            default:
                break;
        }
    };

    //save new record / update existing record
    $scope.savePermission = function(formname) { 
        console.log(formname);
        $scope.showLoader = true;        
        switch (formname) {
            case 'add':            
                $scope.isDisabled = true;
                $scope.permission._token = CSRFTOKEN;
                var url = baseUrl + "/superadmin/save-permission";             
                if ($scope.permissionForm.$valid) {   
                    $http({
                        method: 'POST',
                        url: url,
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},            
                        data: $.param($scope.permission)
                    }).then(function successCallback(response) {
                        console.log(response.data);
                        $scope.showLoader = false;
                        window.location.href= baseUrl+"/superadmin/permissions";
                     }, function errorCallback(response) {
                        $scope.isDisabled = false;
                        console.log(response);
                        //swal('This is embarassing. An error has occured. Please check your internet connections');
                    });
                }else{
                    $scope.isaddRoleDisabled = false;
                }
                break;
            case 'edit':                       
                $scope.isDisabled = true; 
                var url = baseUrl + "/superadmin/update-permission"; 
                $scope.permission._token = CSRFTOKEN;
                if ($scope.permissionForm.$valid) { 
                    $http({
                        method: 'POST',
                        url: url,
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},            
                        data: $.param($scope.permission)
                    }).then(function successCallback(response) {
                        //console.log(response.data);
                        $scope.showLoader = false;     
                        window.location.href= baseUrl+"/superadmin/permissions";
                     }, function errorCallback(response) {
                        $scope.isDisabled = false; 
                        swal('Server could not be responsed !');
                    });
                }else{
                    $scope.showLoader = false; 
                    swal('This is embarassing. An error has occured. Please check your internet connections');    
                }
                break;
            default:
                break;
        }
    }
	
});