var user = angular.module('user.directives', []);
/***
** Custom directive for email exist check
**/
user.directive('emailExists', function($http , $q ) {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function($scope, elem, attrs, ngModel) { 
            ngModel.$asyncValidators.uniqueEmail = function(modelValue, viewValue) {
                var value = modelValue || viewValue;
                // Lookup user by password
                var userid = (!$scope.user.id) ? 0 : $scope.user.id;
                console.log(userid);
                return $http.get(baseUrl+ '/superadmin/check_isexist?id='+ userid +'&email='+ value).
                then(function resolved(response) {
                   //password exists, this means validation fails
                   var valid = response.data;
                    if(valid == "true"){
                        ngModel.$setValidity('emailExists', false);
                        return $q.reject();
                    }else
                        ngModel.$setValidity('emailExists', true);
                        return $q.resolve();
                }, function rejected(response) {
                    //email does not exist, therefore this validation passes
                    console.log(response.data);
                    return false;
                });
            };
        }
    } 
});

/***
** Custom directive for username exist check
**/
user.directive('usernameExists', function($http , $q ) {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function($scope, elem, attrs, ngModel) { 
            ngModel.$asyncValidators.uniqueUsername = function(modelValue, viewValue) {
                var value = modelValue || viewValue;
                // Lookup user by password

                return $http.get(baseUrl+ '/superadmin/check_isexist?username='+ value).
                then(function resolved(response) {
                   //password exists, this means validation fails
                   var valid = response.data;
                    if(valid == "true"){
                        ngModel.$setValidity('usernameExists', false);
                        return $q.reject();
                    }else
                        ngModel.$setValidity('usernameExists', true);
                        return $q.resolve();
                }, function rejected(response) {
                    //email does not exist, therefore this validation passes
                    console.log(response.data);
                    return false;
                });
            };
        }
    } 
});