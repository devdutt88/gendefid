/***********company section***********/
setTimeout(function(){
    $(".alert-success").hide();    
}, 2000);
var csrf_token = $('[name="_token"]').val();
var id = $('#hid').val()?$('#hid').val():'';
console.log(csrf_token);
$("#cmp_section").validate({
    errorClass:'error',
    errorElement:'span',
    highlight: function(element, errorClass) {
        // Override the default behavior here
    },

  rules: {    
    name: {
        required: true, 
        minlength:3,
        remote: baseUrl+'/superadmin/comp_check_isexist?id='+id,
        }, 
    name: {
        required: true, 
        },      
    email: {
        required: true,
        email: true,
        remote: baseUrl+'/superadmin/comp_check_isexist?id='+id,
        },
    phone:{
        required:true,
        minlength:8,
        maxlength:15,
        number: true
    },
  },
  messages: {
    name: {
            required: "Company Name field is required.",                
            remote: "Company Name already exists.",
        },        
    email: {
            required: "Email field is required.",
            email: "Please enter a valid email address.",
            remote: "Email already exists.",
        }
  }
});

$("#chck1").click(function(){
      if($(this). prop("checked") == true){
        $("#buld_add1").val($("#bus_add1").val());
        $("#buld_add2").val($("#bus_add2").val());
        $("#buld_suburb").val($("#bus_suburb").val());
        $("#buld_state").val($("#bus_state").val());
        $("#buld_post").val($("#bus_post").val());
        $("#buld_country").val($("#bus_country").val());
      }
      else{
        $("#buld_add1").val("");
        $("#buld_add2").val("");
        $("#buld_suburb").val("");
        $("#buld_state").val("");
        $("#buld_post").val("");
        $("#buld_country").val("");
      }
  })

$(document).ready(function() {
    $('#sample_1').DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": baseUrl+"/superadmin/cmp_list",
        "language": {
            "lengthMenu": "_MENU_ Records",            
        },
        columns: [            
            {data: 'name', name: 'name'},            
            {data: 'email', name: 'email'},
            {data: 'phone', name: 'phone'},
            {data: 'created_at', name: 'Create Date'},
            {data: 'action', name: 'action', orderable: false, searchable: false}           
        ]
    } );

    $('#assoc_user_list').DataTable( {
        "processing": true,
        "serverSide": true,
        'bFilter': false,
        'paging': true,
        "bLengthChange" : false,
        "ajax": baseUrl+"/superadmin/getCompanyUserData/"+id,
        columns: [            
            {data: 'full_name', name: 'full_name'},
            {data: 'email', name: 'email'},
            {data: 'phone', name: 'phone'},
            {data: 'roles', name: 'roles'},
            {data: 'created', name: 'created'},
        ]
    } );

    $('#assoc_project_list').DataTable( {
        "processing": true,
        "serverSide": true,
        'bFilter': false,
        'paging': true,
        "bLengthChange" : false,
        "ajax": baseUrl+"/superadmin/getCompanyProjectData/"+id,
        columns: [            
            {data: 'name', name: 'name'},
            {data: 'type', name: 'type'},
            {data: 'author', name: 'author'},
            {data: 'created', name: 'created'}
        ]
    } );

} );

/**
* TinyLimiter - this is for textarea limitation
*/

(function($) {
    $.fn.extend( {
        limiter: function(limit, elem) {
            $(this).on("keyup focus", function() {
                setCount(this, elem);
            });
            function setCount(src, elem) {
                if(src)
                {
                    var chars = src.value.length;
                    if (chars > limit) {
                        src.value = src.value.substr(0, limit);
                        chars = limit;
                    }                
                    elem.html( limit - chars + " Character Left" );
                }                
            }
            setCount($(this)[0], elem);
        }
    });
})(jQuery);
$(document).ready( function() {
    var elem = $("#chars");
    $("#description").limiter(120, elem);
});