var csrf_token = $('[name="_token"]').val();
var id = $('#hid').val()?parseInt($('#hid').val()):'';
setTimeout(function(){
    $(".alert-success").hide();    
}, 2000);
$(document).ready( function(){    
    $.contextMenu({
    selector: '.location', 
    build: function($trigger, e) {
        return {
            callback: function(key, options) {
                var operationID = $($trigger)[0];
                console.log(operationID);
                console.log(key , $(operationID).attr('id'));
                switch(key){
                    case "add":
                        addLocation($(operationID).attr('name'),$(operationID).attr('id'));
                    break;

                    case "edit":                    
                        editLocation($(operationID).attr('name'),$(operationID).attr('id'));
                    break;

                    case "delete":
                        deleteLocation($(operationID).attr('id'),$(operationID).attr('project_id'));
                    break;

                } 
            },
            items: {
                "add": {name: "Add", icon: "add"},
                "edit":{name: "Edit", icon: "edit"},
                "delete": {name: "Delete", icon: "delete"},
                }
            };
        }
    });

    $.contextMenu({
    selector: '.add_location', 
    build: function($trigger, e) {
        return {
            callback: function(key, options) {
                var operationID = $($trigger)[0];
                console.log(key , $(operationID).attr('id'));
                switch(key){
                    case "add":
                        addLocation($(operationID).attr('name'),$(operationID).attr('id'));
                    break;
                    default:
                    break;
                } 
            },
            items: {
                "add": {name: "Add", icon: "add"},                
                }
            };
        }
    });

    function addLocation(name,id){
        if(id == undefined)
        {
            $('#addModal').modal('show');
        }else{
            $('#parent').val(name);
            $('#parent_id').val(id);            
            $('#childModal').modal('show');
        }
    } 

    function editLocation(name,id){        
            $('#editModal #location_name').val(name);
            $('#location_id').val(id);            
            $('#editModal').modal('show');        
    } 

    function deleteLocation(id,pid){
        var r = confirm("Do you really want to delete it?");
        if (r == true) {
            window.location.href= baseUrl+"/delete-location/"+id+"/"+pid;
        } else {            
        }
    }        

$("#location").validate({
    errorClass:'error',
    errorElement:'span',
    highlight: function(element, errorClass) {
        // Override the default behavior here
    },

    // Specify validation rules
    rules: {
      level1: {
            required: true,
            minlength: 3,
        }     
    },
    // Specify validation error messages
    messages: {
      level1: {
        required: "The location name field is required.",        
      }      
    },
    // Make sure the form is submitted to the destination defined
    // in the "action" attribute of the form when valid
    submitHandler: function(form) {            
        var level1 = $('#level1').val();
        var project_id1 = $('#project_id1').val();
        var company_id = $('#company_id').val();        
        var data = {'level1':level1,'_token':csrf_token,'case':'parent','project_id':project_id1,'company_id':company_id}     
        $.ajax({
        url: baseUrl+'/save-location',
        type: "post",
        data: data,
        success: function(response) {            
            var res = JSON.parse(response);
            console.log(res);
            $('#browser').append(res.html);            
            $('#level1').val('');
            $('#loc_msg2').show();
            $('#loc_msg2').text(res.msg);
            setTimeout(function(){
                $(".alert-success").hide();    
            }, 2000);
            $('#addModal').modal('hide');                                
        }            
        });
        return false;
        //console.log($('#treeview-checkbox-demo').treeview('selectedValues'));        
    }

    
  });

$("#sublocation").validate({
    errorClass:'error',
    errorElement:'span',
    highlight: function(element, errorClass) {
        // Override the default behavior here
    },
    // Specify validation rules
    rules: {
      sublocation_name: {
            required: true,
            minlength: 3,
        }     
    },
    // Specify validation error messages
    messages: {
      sublocation_name: {
        required: "The sub location name field is required.",        
      }      
    },
    // Make sure the form is submitted to the destination defined
    // in the "action" attribute of the form when valid
    submitHandler: function(form) {       
        var project_id1 = $('#project_id1').val();
        var company_id = $('#company_id').val();   
        var level1 = $('#sublocation_name').val();
        var parent_id = $('#parent_id').val();        
        var data = {'level1':level1,'parent_id':parent_id,'_token':csrf_token,'case':'child','project_id':project_id1,'company_id':company_id}     
        $.ajax({
        url: baseUrl+'/save-location',
        type: "post",
        data: data,
        success: function(response) {
            var res = JSON.parse(response);
            console.log(res);
            if ($('#'+parent_id+' > ul').length) {               
               $('#'+parent_id).removeClass('last expandable lastExpandable').addClass('closed collapsable lastCollapsable');
               if (!($('#'+parent_id+' > div').length)) {
                    $('#'+parent_id).append('<div></div>');
               }
                $('#'+parent_id+' > div').removeClass('hitarea location-hitarea tree-view-hitarea closed-hitarea expandable-hitarea lastExpandable-hitarea').addClass('hitarea location-hitarea tree-view-hitarea closed-hitarea collapsable-hitarea lastCollapsable-hitarea');
                $('#'+parent_id+' > ul').append(res.html);
                $('#'+parent_id).find('ul').show();
            }else{                
                $('#'+parent_id).removeClass('last expandable lastExpandable').addClass('closed collapsable lastCollapsable');
                if (!($('#'+parent_id+' > div').length)) {
                    $('#'+parent_id).append('<div></div>');
                   }
                $('#'+parent_id+' > div').removeClass('hitarea location-hitarea tree-view-hitarea closed-hitarea expandable-hitarea lastExpandable-hitarea').addClass('hitarea location-hitarea tree-view-hitarea closed-hitarea collapsable-hitarea lastCollapsable-hitarea');
                $('#'+parent_id).append('<ul style="display:block">'+res.html+'</ul>');
                $('#'+parent_id).find('ul').show();                
            }
            $('#sublocation_name').val('');
            $('#loc_msg2').show();
            $('#loc_msg2').text(res.msg);
            setTimeout(function(){
                $(".alert-success").hide();    
            }, 2000);                       
            $('#childModal').modal('hide');
            //window.location.href= baseUrl+"/superadmin/projectConfiguration/location";
        }            
        });
        return false;        
    }
});
    
$("#editLocation").validate({
    errorClass:'error',
    errorElement:'span',
    highlight: function(element, errorClass) {
        // Override the default behavior here
    },

    // Specify validation rules
    rules: {
      location_name: {
            required: true,
            minlength: 3,
        }     
    },
    // Specify validation error messages
    messages: {
      location_name: {
        required: "The location name field is required.",        
      }      
    },
    // Make sure the form is submitted to the destination defined
    // in the "action" attribute of the form when valid
    submitHandler: function(form) {                   
        var location_name = $('#location_name').val();
        var location_id = $('#location_id').val();
        var project_id = $('#project_id').val();
        var cmp_id = $('#cmp_id').val();
        var data = {'location_name':location_name,'location_id':location_id,'_token':csrf_token,'cmp_id':cmp_id,'project_id':project_id}     
        $.ajax({
        url: baseUrl+'/update-location',
        type: "post",
        data: data,
        success: function(response) {            
            var res = JSON.parse(response);
            console.log(res);
            $("#"+location_id+' > a').text(location_name);            
            $("#"+location_id).attr('name',location_name);
            $('#loc_msg2').show();
            $('#loc_msg2').text(res.msg);
            setTimeout(function(){
                $(".alert-success").hide();    
            }, 2000);
            $('#editModal').modal('hide');                      
        }            
        });
        return false;        
    }    
});

});