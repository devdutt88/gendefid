var csrf_token = $('[name="_token"]').val();
var id = $('#hid').val()?parseInt($('#hid').val()):'';
setTimeout(function(){
    $(".alert-success").hide();    
}, 2000);
$("#add_permission").validate({
    errorClass:'error',
    errorElement:'span',
    highlight: function(element, errorClass) {
        // Override the default behavior here
    },

    // Specify validation rules
rules: {
    name: {
        required : true,
        minlength : 3,
        remote: baseUrl+'/superadmin/check-permission?id='+id,
    },  
    type: {
        required: true,        
    }, 
    module_name: {
        required: true,
    },           
},
    // Specify validation error messages
    messages: {
      name: {
        required: "The name field is required.",
        remote: "The Permission name already exists.",
      },      
      type:{
        required: "Please select Type.",
      }
    },
    // Make sure the form is submitted to the destination defined
    // in the "action" attribute of the form when valid
    
  });

$("#chck1").click(function(){
      if($(this). prop("checked") == true){
       
        $("#buld_add1").val($("#bus_add1").val());
        $("#buld_add2").val($("#bus_add2").val());
        $("#buld_suburb").val($("#bus_suburb").val());
        $("#buld_state").val($("#bus_state").val());
        $("#buld_post").val($("#bus_post").val());
        $("#buld_country").val($("#bus_country").val());
      }
      else{
        $("#buld_add1").val("");
        $("#buld_add2").val("");
        $("#buld_suburb").val("");
        $("#buld_state").val("");
        $("#buld_post").val("");
        $("#buld_country").val("");
      }
  })

$(document).ready(function() {
    $('#sample_1').DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": baseUrl+"/superadmin/perm_list",
        "language": {
            "lengthMenu": "_MENU_ Records",            
        },
        columns: [            
            {data: 'name', name: 'name'},
            {data: 'type', name: 'type'},
            {data: 'description', name: 'description'},
            {data: 'module_name', name: 'module_name', orderable: false, searchable: false},
            {data: 'created_at', name: 'Create Date'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    } );
} );

// function checkRule(rule){
//     var type = $('#type').val();
//     if(rule.value != "Menu")
//     {                
//         $('.module').show();
//     }else{
//         $('.module').hide();        
//     }
// }