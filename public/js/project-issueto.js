var csrf_token = $('[name="_token"]').val();
var issueto_id = $('#issueto_id').val()?$('#issueto_id').val():'';
var company_id = $('#company_id').val()?$('#company_id').val():'';

//var user_issueto_id = $('#user_issueto_id').val()?$('#user_issueto_id').val():'';
$("#issueto_form").validate({
    errorClass:'error',
    errorElement:'span',
    highlight: function(element, errorClass) {
        // Override the default behavior here
    },

    // Specify validation rules
    rules: {
      name: {
        required : true,
        minlength : 3,
        remote: baseUrl+'/superadmin/issueto_check_isexist?id='+issueto_id,
        },  
      "company[]": "required",
    },
    // Specify validation error messages
    messages: {
      name: {
        remote: "Issueto already exists",
      },
      "company[]":{
        required: "Please select at least one Company",
      }
    },
    // Make sure the form is submitted to the destination defined
    // in the "action" attribute of the form when valid
    
  });

$("#issueto_user_form").validate({
    errorClass:'error',
    errorElement:'span',
    highlight: function(element, errorClass) {
        // Override the default behavior here
    },
    // Specify validation rules
    rules: {
      name: {
        required : true,
        minlength : 6,
        /*remote: function(el) {
                var user_id = $('#issueto_user_id').val();                
                return baseUrl+'/superadmin/issueto_user_check_isexist?id='+user_id+'&issueto_id='+issueto_id;
          },*/
        },
      email: {
        required : true,
        email : true,
        remote: function(el) {
                var user_id = $('#issueto_user_id').val();                
                return baseUrl+'/superadmin/issueto_user_check_isexist?id='+user_id+'&issueto_id='+issueto_id;
          },
        }, 
      phone: {
        required : true,
        number : true,
        minlength : 8,
        maxlength : 15,
        },  
      
    },
    // Specify validation error messages
    messages: {
      name: {
        //remote: "Issueto User already exists",
      },
      email: {
        remote: "Issueto email already exists",
      },
      
    },
    // Make sure the form is submitted to the destination defined
    // in the "action" attribute of the form when valid
    
  });

function show_issueto_form(){
  $('#issueto_form').slideDown();
}

function cancel_issueto(){
  $('#issueto_form').slideUp();
  $('#issueto_form .error').hide();
}

function show_issueto_user_form(){
  $('#issueto_user_form').attr('action', baseUrl+'/superadmin/save_issueto_user');
  $('#issueto_user_form').find("input[type=text], input[type=email], textarea, #issueto_user_id").val('')
  $('#issueto_user_form .error').hide();
  $('#issueto_user_form .caption-subject').text('Add Issueto User');
  $('#issueto_user_form').slideDown();
}

function cancel_issueto_user(){
  $('#issueto_user_form .error').hide();
  $('#issueto_user_form').slideUp();
}

function edit_issueto_user(id){
  jQuery.ajax({
    type: "POST",
    url: baseUrl+"/superadmin/issueto_user_details",
    dataType: 'json',
    data: {_token: csrf_token, id: id},
  }).done(function(res) {
    jQuery('#issueto_user_form .caption-subject').text('Edit Issueto User');
    jQuery('#issueto_user_form').attr('action', baseUrl+'/superadmin/update_issueto_user');
    jQuery("#iu_name").val(res[0].name);
    jQuery("#email").val(res[0].email);
    jQuery("#activity").val(res[0].activity);
    jQuery("#phone").val(res[0].phone);
    jQuery("#user_issueto_id").val(res[0].issueto_id);
    jQuery("#issueto_user_id").val(id);
    jQuery('#issueto_user_form').slideDown();
    jQuery('#iu_name').focus();

  });

}

$(document).ready(function() {

    function getDataTable(){
    	$('#sample_1').DataTable( {

	    	"processing": true,
	    	"serverSide": true,
	    	"ajax": baseUrl+"/superadmin/projectConfiguration/issueto_list/"+$('#project_id').val()+'/'+company_id,
	    	columns: [
	        	{data: 'name', name: 'name'},
	        	{data: 'email', name: 'email'},
	        	{data: 'phone', name: 'phone'},
	        	{data: 'activity', name: 'activity'},
	        	{data: 'action', name: 'action', orderable: false, searchable: false},
      		]
    	});
    }

    getDataTable();

    $('#sample_2').DataTable( {

      	"processing": true,
      	"serverSide": true,
      	"ajax": baseUrl+"/superadmin/projectConfiguration/issueto_users_list/"+issueto_id,
      	columns: [
	    	{data: 'name', name: 'name'},
        	{data: 'email', name: 'email'},
        	{data: 'phone', name: 'phone'},
        	{data: 'activity', name: 'activity'},
        	{data: 'action', name: 'action', orderable: false, searchable: false},
      	]
    });
    $('#project').trigger('change');

} );
