var csrf_token = $('[name="_token"]').val();
var id = $('#project_id').val()?$('#project_id').val():'0';
setTimeout(function(){
    $(".alert-success").hide();    
}, 2000);
$("#add_project_form").validate({
    errorClass:'error',
    errorElement:'span',
    highlight: function(element, errorClass) {
        // Override the default behavior here
    },

    // Specify validation rules
    rules: {
      name: {
        required : true,
        minlength : 3,
        //remote: baseUrl+'/superadmin/project_isexist/'+id+'/',

         remote: {              
              url: baseUrl+'/superadmin/project_isexist/'+id,
              type: "post",
              data:{'_token':csrf_token,'jq':1,'name':function() {
                  return $( "#name" ).val();
                  }
               },
          } 


        },  
      // code: {
      //   required: true,
      //   },
      type: {
        required: true,        
      },
      "company": "required", 
      address1:{
        required:true,
      },
      suburb:{
        required:true,
      },
      postal_code:{
        required:true,
      },
      state:{
        required:true,
      },
      country:{
        required:true,
      }      
    },
    // Specify validation error messages
    messages: {
      name: {
        remote: "Project name already exists",
      },
      // code: {
      //   required: "Please provide code",        
      // },
      type: {
        required: "Please select project type",        
      },
      "company":{
        required: "Please select any one Company",
      },
      address1:{
        required:"Please fill address",
      },
      suburb:{
        required:"Please fill suburb",
      }
      ,
      postal_code:{
        required:"Please fill postal code",
      },
      state:{
        required:"Please fill state",
      },
      country:{
        required:"Please fill country",
      } 
    },
    // Make sure the form is submitted to the destination defined
    // in the "action" attribute of the form when valid
    
  });
  
$(document).ready(function() {
    $('#sample_1').DataTable( {
        "processing": true,
        "serverSide": true,
        "bFilter": true,
        "paging": true,
        "ajax": baseUrl+"/superadmin/project_list",
        "language": {
            "lengthMenu": "_MENU_ Records",            
        },
        columns: [            
            {data: 'name', name: 'name'},            
            {data: 'type', name: 'type'},
            {data: 'auther', name: 'auther'},
            {data: 'company', name: 'company'},
            {data: 'created_at', name: 'Create Date'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    } );

} );

/**
* TinyLimiter - this is for textarea limitation
*/

(function($) {
    $.fn.extend( {
        limiter: function(limit, elem) {
            $(this).on("keyup focus", function() {
                setCount(this, elem);
            });
            function setCount(src, elem) {
              if(src) {
                var chars = src.value.length;
                if (chars > limit) {
                    src.value = src.value.substr(0, limit);
                    chars = limit;
                }                
                elem.html( limit - chars + " Character Left" );
              }
            }
            setCount($(this)[0], elem);
        }
    });
})(jQuery);
$(document).ready( function() {
    var elem = $("#chars");
    $("#description").limiter(120, elem);
});