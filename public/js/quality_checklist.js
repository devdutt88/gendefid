var csrf_token = $('[name="_token"]').val();
var checklist_id = $('#id').val()?$('#id').val():'';

//var user_issueto_id = $('#user_issueto_id').val()?$('#user_issueto_id').val():'';
$("#checklist_form").validate({
    errorClass:'error',
    errorElement:'span',
    highlight: function(element, errorClass) {
        // Override the default behavior here
    },

    // Specify validation rules
    rules: {
      name: {
        required : true,
        minlength : 3,
        remote: baseUrl+'/superadmin/quality_checklist_isexist?id='+checklist_id,
        },  
      //"company[]": "required",
    },
    // Specify validation error messages
    messages: {
      name: {
        remote: "Checklist already exists",
      },
      //"company[]":{
      //  required: "Please select at least one Company",
      //}
    },
    // Make sure the form is submitted to the destination defined
    // in the "action" attribute of the form when valid
    
  });

$("#checklist_task_form").validate({
    errorClass:'error',
    errorElement:'span',
    highlight: function(element, errorClass) {
        // Override the default behavior here
    },
    // Specify validation rules
    rules: {
      chk_task_name: {
        required : true,
        minlength : 3,
        remote: function(el) {
                var task_id = $('#task_id').val();                
                return baseUrl+'/superadmin/quality_checklist_task_isexist?id='+task_id+'&checklist_id='+checklist_id;
          },
        }, 
      status: {
        required : true,
        }, 
      Comments: {
        
        },  
      
    },
    // Specify validation error messages
    messages: {
      chk_task_name: {
        remote: "Checklist task already exists",
      },
      
    },
    // Make sure the form is submitted to the destination defined
    // in the "action" attribute of the form when valid
    
  });


function show_checklist_form(){
  $('#checklist_form').slideDown();
}

function cancel_checklist(){
  $('#checklist_form').slideUp();
  $('#checklist_form .error').hide();
}

function show_checklist_task_form(){
  $('#checklist_task_form').attr('action', baseUrl+'/superadmin/save_checklist_task');
  $('#checklist_task_form .caption-subject').text('Add Task');
  $('#checklist_task_form').find("input[type=text], textarea, #checklist_task_id").val('')
  $('#checklist_task_form .error').hide();
  $('#checklist_task_form').slideDown();
}

function cancel_checklist_task(){
  $('#checklist_task_form .error').hide();
  $('#checklist_task_form').slideUp();
}

function edit_checklist_task(id){
  jQuery.ajax({
    type: "POST",
    url: baseUrl+"/superadmin/checklist_task_details",
    dataType: 'json',
    data: {_token: csrf_token, id: id},
  }).done(function(res) {
    jQuery('#checklist_task_form .caption-subject').text('Edit Task Details');
    jQuery('#checklist_task_form').attr('action', baseUrl+'/superadmin/update_checklist_task');
    jQuery("#chk_task_name").val(res[0].name);
    //jQuery("#status").val(res[0].status);
    jQuery("#"+res[0].status).attr('checked', 'checked');
    jQuery("#uniform-"+res[0].status+" span").addClass('checked');
    jQuery("#comment").val(res[0].comment);
    jQuery("#task_checklist_id").val(res[0].checklist_id);
    jQuery("#task_id").val(id);
    jQuery('#checklist_task_form').slideDown();
    jQuery('#chk_task_name').focus();
  });

}


