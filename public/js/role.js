var csrf_token = $('[name="_token"]').val();
var id = $('#hid').val()?parseInt($('#hid').val()):'';
setTimeout(function(){
    $(".alert-success").hide();    
}, 2000);
$(document).ready( function() {
$("#role").validate({
    errorClass:'error',
    errorElement:'span',
    highlight: function(element, errorClass) {
        // Override the default behavior here
    },

    // Specify validation rules
    rules: {
      name: {
        required : true,
        minlength : 3,
        remote: baseUrl+'/superadmin/check-role?id='+id,
        }          
    },
    // Specify validation error messages
    messages: {
      name: {
        required: "The Role name field is required.",
        remote: "The Role name already exists.",
      }      
    },
    // Make sure the form is submitted to the destination defined
    // in the "action" attribute of the form when valid
    submitHandler: function(form) {
        var name = $('#name').val();
        var permission = $('#treeview-checkbox-demo').treeview('selectedValues');
        permission = permission.slice(1);
        var data = {'name':name,'permission':permission,'_token':csrf_token}
        $.ajax({
            url: baseUrl+'/superadmin/save-role',
            type: "post",
            data: data,
            success: function(response) {
                console.log(response);
                var res = JSON.parse(response);
                if(res.status == 0)
                {
                    window.location.href= baseUrl+"/superadmin/role";
                }
            }
        });
        //console.log($('#treeview-checkbox-demo').treeview('selectedValues'));
        return false;        
    }

    
  });
});


var permission = '';
if($('#perm_hidevalue').text()){
    permission = $('#perm_hidevalue').text();
    permission = JSON.parse(permission);    
}

$('#treeview-checkbox-demo').treeview({
    debug : true,
    data : permission?permission:['selected'],
});
// $('#show-values').on('click', function(){ 
//     console.log($('#treeview-checkbox-demo').treeview('selectedValues'));
//     $('#values').text(
//         $('#treeview-checkbox-demo').treeview('selectedValues')
//     );
// });