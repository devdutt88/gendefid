var csrf_token = $('[name="_token"]').val();
var id = $('#user_id').val()?$('#user_id').val():'';
$(".alert-success").fadeOut(3000);
$("#add_user_form").validate({
    errorClass:'error',
    errorElement:'span',
    highlight: function(element, errorClass) {
        // Override the default behavior here
    },

    // Specify validation rules
    rules: {
      username: {
        required : true,
        minlength : 6,
        remote: baseUrl+'/superadmin/user_check_isexist?id='+id,
        },  
      fullname: {
        required: true,
        minlength: 6,
        },
      password: {
        required: true,
        minlength: 6,
      },
      CPassword: {
        equalTo: "#password",
      },
      "company[]": "required",
      phone: {
        required:true,
        minlength:8,
        maxlength:15,
        number: true,
        },
      usertype: "required",
      email: {
        required: true,
        email: true,
        remote: baseUrl+'/superadmin/user_check_isexist?id='+id,
      },
      
    },
    // Specify validation error messages
    messages: {
      username: {
        remote: "Username already exists.",
      },
      password: {
        required: "Please provide a password",
        minlength: "Your password must be at least 6 characters long.",
      },
      CPassword: {
        required: "Please re-enter the password.",
        equalTo: "Passwords did not match.",
      },
      email: {
        required: "This field is required.",
        email: "Please enter a valid email address.",
        remote: "Email already exists.",
      },
      "company[]":{
        required: "Please select at least one Company.",
      }
    },
    // Make sure the form is submitted to the destination defined
    // in the "action" attribute of the form when valid
    
  });

$("#chck1").click(function(){
      if($(this). prop("checked") == true){
       
        $("#buld_add1").val($("#bus_add1").val());
        $("#buld_add2").val($("#bus_add2").val());
        $("#buld_suburb").val($("#bus_suburb").val());
        $("#buld_state").val($("#bus_state").val());
        $("#buld_post").val($("#bus_post").val());
        $("#buld_country").val($("#bus_country").val());
      }
      else{
        $("#buld_add1").val("");
        $("#buld_add2").val("");
        $("#buld_suburb").val("");
        $("#buld_state").val("");
        $("#buld_post").val("");
        $("#buld_country").val("");
      }
  });

$(document).ready(function() {
    $('#sample_1').DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": baseUrl+"/superadmin/usr_list",
        columns: [            
            {data: 'name', name: 'name'},
            {data: 'full_name', name: 'full_name'},
            {data: 'email', name: 'email'},
            {data: 'roles', name: 'roles', orderable: false, searchable: false},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    } );

      $('#assoc_comp_list').DataTable( {
        "processing": true,
        "serverSide": true,
        "bFilter": false,
        "paging": false,
        "ajax": baseUrl+"/superadmin/getUserCompanyData/"+id,
        columns: [            
            {data: 'name', name: 'name'},
            {data: 'type', name: 'type'},
        ]
    } );
} );