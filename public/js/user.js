var csrf_token = $('[name="_token"]').val();
var id = $('#user_id').val()?$('#user_id').val():'';
setTimeout(function(){
      $(".alert-success").hide();    
  }, 2000);
$("#add_user_form").validate({
    errorClass:'error',
    errorElement:'span',
    highlight: function(element, errorClass) {
        // Override the default behavior here
    },

    // Specify validation rules
    rules: {
      username: {
        required : true,
        minlength : 6,
        remote: baseUrl+'/superadmin/user_check_isexist?id='+id,
        },  
      fullname: {
        required: true,
        minlength: 6,
        },
      password: {
        required: true,
        regex: /^(?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!%*#?&])[A-Za-z\d$@$!%*#?&]{6,20}$/
      },
      CPassword: {
        equalTo: "#password",
      },
      "company[]": "required",
      phone: {
        required:true,
        minlength:8,
        maxlength:15,
        number: true,
        },
      usertype: "required",
      email: {
        required: true,
        email: true,
        remote: baseUrl+'/superadmin/user_check_isexist?id='+id,
      },
      
    },
    // Specify validation error messages
    messages: {
      username: {
        remote: "Username already exists.",
      },
      password: {
        required: "Please provide a password",
      },
      CPassword: {
        required: "Please re-enter the password.",
        equalTo: "Passwords did not match.",
      },
      email: {
        required: "This field is required.",
        email: "Please enter a valid email address.",
        remote: "Email already exists.",
      },
      "company[]":{
        required: "Please select at least one Company.",
      }
    },
    // Make sure the form is submitted to the destination defined
    // in the "action" attribute of the form when valid
    
});

$.validator.addMethod(
    "regex",
    function(value, element, regexp) {
        var re = new RegExp(regexp);
        return this.optional(element) || re.test(value);
    },
    "Password should be atleast 6 characters long and should contain one number,one character and one special character."
);

// $.validator.addMethod("laxEmail", function(value, element) {
//   // allow any non-whitespace characters as the host part
//   return this.optional( element ) || /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@(?:\S{1,63})$/.test( value );
// }, 'Please enter a valid email address.');

$("#chck1").click(function(){
      if($(this). prop("checked") == true){
       
        $("#buld_add1").val($("#bus_add1").val());
        $("#buld_add2").val($("#bus_add2").val());
        $("#buld_suburb").val($("#bus_suburb").val());
        $("#buld_state").val($("#bus_state").val());
        $("#buld_post").val($("#bus_post").val());
        $("#buld_country").val($("#bus_country").val());
      }
      else{
        $("#buld_add1").val("");
        $("#buld_add2").val("");
        $("#buld_suburb").val("");
        $("#buld_state").val("");
        $("#buld_post").val("");
        $("#buld_country").val("");
      }
  });

$(document).ready(function() {
    $('#sample_1').DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": baseUrl+"/superadmin/usr_list",
        "language": {
            "lengthMenu": "_MENU_ Records",            
        },
        columns: [
            {data: 'logo_image', name: 'logo_image', orderable: false, searchable: false, className: 'text-center'},
            {data: 'name', name: 'name'},
            {data: 'full_name', name: 'full_name'},
            {data: 'email', name: 'email'},
            {data: 'roles', name: 'roles', orderable: false, searchable: false},
            {data: 'created_at', name: 'Create Date'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    } );

      $('#assoc_comp_list').DataTable( {
        "processing": true,
        "serverSide": true,
        "bFilter": false,
        "paging": false,
        "ajax": baseUrl+"/superadmin/getUserCompanyData/"+id,
        "language": {
            "lengthMenu": "_MENU_ Records",            
        },
        columns: [            
            {data: 'name', name: 'name'},
            {data: 'type', name: 'type'},
        ]
    } );
});