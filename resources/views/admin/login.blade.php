@extends('layouts.app')

@section('content')
<!-- BEGIN SIDEBAR TOGGLER BUTTON -->


<div class="menu-toggler sidebar-toggler">

</div>

<div class="logo">

</div>
<!-- END SIDEBAR TOGGLER BUTTON -->
<!-- BEGIN LOGIN -->
@if(Session::has('flash_alert_notice'))
        <div class="alert alert-success_login alert-dismissable text-center">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <i class="icon fa fa-check"></i>  
            {{ Session::get('flash_alert_notice') }} 
        </div>
    @endif
<div class="content cst-login">

    <!-- BEGIN LOGIN FORM -->    
    <form class="login-form" role="form" method="POST" action="{{ url('/admin/login') }}" novalidate="novalidate">
        {{ csrf_field() }}
        <h3 class="form-title">Superadmin Login Board</h3>
        <div class="alert alert-danger display-hide">
            <button class="close" data-close="alert"></button>
            <span>
            Enter username and password. </span>
        </div>
        @if($errors->first('error'))
            <div class="alert alert-danger display-show" style="font-size: 11px;">
                <button class="close" data-close="alert"></button>
                <span>
                <img src="{{url('/')}}/assets/img/warn2.png"> {{ $errors->first('error') }} </span>
            </div>
        @endif
        @if ($errors->has('name'))
            <span class="help-block error">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
        @endif
        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
            <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
            <label class="control-label visible-ie8 visible-ie9">Username</label>
            <div class="input-icon">
                <i class="fa fa-user"></i>
                <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Username" name="name"/>
               
            </div>
        </div>
        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
            <label class="control-label visible-ie8 visible-ie9">Password</label>
            <div class="input-icon">
                <i class="fa fa-lock"></i>
                <input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="Password" name="password"/>
            </div>

        </div>
        <div class="form-actions">
            <label class="checkbox">
            <input type="checkbox" name="remember" value="1"/> Remember me </label>
            <button type="submit" class="btn pull-right cst-yellow">
            Login <i class="fa fa-arrow-circle-o-right"></i>
            </button>
        </div>        
        <div class="forget-password">
            <h4>Forgot your password ?</h4>
            <p>
                 no worries, click <a href="javascript:;" id="forget-password">
                here </a>
                to reset your password.
            </p>
        </div>
             
    </form>
    <!-- END LOGIN FORM -->
    <!-- BEGIN FORGOT PASSWORD FORM -->
    <form class="forget-form" action="{{ url('/sendResetLinkAdmin') }}" method="post">
    {{ csrf_field() }}
        <h3>Forget Password ?</h3>
        <p>
             Enter your e-mail address below to reset your password.
        </p>
         <p id="res_msg"></p>
        <div class="form-group">
            <div class="input-icon email">
                <i class="fa fa-envelope"></i>
                <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Email" id="email" name="email"/>                
            </div>
            <p id="res_err_msg"></p>
        </div>
        <div class="form-actions">
            <button type="button" id="back-btn" class="btn cst-back-btn">
            <i class="fa fa-arrow-circle-o-left"></i> Back </button>
            <button type="submit" id="reset_pwd" class="btn pull-right cst-yellow">
            Submit <i class="fa fa-arrow-circle-o-right"></i>
            </button>
        </div>
    </form>
    <!-- END FORGOT PASSWORD FORM -->   
</div>
@endsection
