<!-- BEGIN COPYRIGHT -->
<!-- <div class="copyright">
     Copyright © 2017 All rights reserved - WiseWorking
</div> -->
<script type="text/javascript" src="{{ elixir('js/all.js') }}"></script>

<script type="text/javascript">
	var baseUrl='{{url('/')}}'

	jQuery(document).ready(function() {     
	  Metronic.init(); // init metronic core components
	  Layout.init(); // init current layout
	  Login.init();
	});
</script>

</body>
<!-- END BODY -->
</html>