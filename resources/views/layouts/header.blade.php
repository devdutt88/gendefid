<!-- BEGIN MANDATORY STYLES -->
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" href="{{ elixir('css/all.css') }}">
<link rel="shortcut icon" href="favicon.ico"/>