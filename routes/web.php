<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// Auth::routes();
// Route::get('/', 'HomeController@index');




Auth::routes();

Route::get('/home', 'HomeController@index');
Route::post('/sendResetLink', 'HomeController@sendResetLink');
Route::post('/sendResetLinkAdmin', 'HomeController@sendResetLinkAdmin');
Route::get('/resetLink/{email}', 'HomeController@resetLink');
Route::get('/resetAdminLink/{email}', 'HomeController@resetAdminLink');
Route::post('/reset', 'HomeController@resetPassword');
Route::post('/reset-password', 'HomeController@resetAdminPassword');

Route::any('admin/login', 'Auth\LoginController@adminLogin');